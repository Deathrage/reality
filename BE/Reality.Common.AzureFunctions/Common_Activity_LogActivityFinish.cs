﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Reality.Common.Logging;

namespace Reality.Common.AzureFunctions
{
    public class Common_Activity_LogActivityFinish
    {
        public class Input
        {
            public string Source { get; set; }
            public DateTime BeganAt { get; set; }
            public bool Success { get; set; }
            public string SuccessData { get; set; }
            public Exception Exception { get; set; }
        }

        private readonly IActivityLogger _activityLogger;

        public Common_Activity_LogActivityFinish(IActivityLogger activityLogger)
        {
            _activityLogger = activityLogger ?? throw new ArgumentNullException(nameof(activityLogger));
        }

        [FunctionName(nameof(Common_Activity_LogActivityFinish))]
        public async Task RunAsync([ActivityTrigger] Input input)
        {
            if (input == default)
                throw new ArgumentNullException(nameof(input));

            await _activityLogger.LogAsync(input.Source, input.BeganAt,
                DateTime.Now, input.Success, input.SuccessData, input.Exception);
        }
    }
}