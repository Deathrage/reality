﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;

namespace Reality.Common.AzureFunctions.Extensions
{
    public static class DurableOrchestrationContextExtensions
    {
        public static Task CallLoggingActivity<TSource>(this IDurableOrchestrationContext context, DateTime beganAt,
            string successMessage)
            => CallLoggingActivity(context, typeof(TSource).FullName, beganAt, successMessage);

        public static Task CallLoggingActivity(this IDurableOrchestrationContext context, string source,
            DateTime beganAt, string successMessage)
        {
            return context.CallActivityAsync(
                nameof(Common_Activity_LogActivityFinish),
                new Common_Activity_LogActivityFinish.Input()
                    {Source = source, BeganAt = beganAt, Success = true, SuccessData = successMessage});
        }

        public static Task CallLoggingActivity<TSource>(this IDurableOrchestrationContext context, DateTime beganAt,
            Exception exception)
            => CallLoggingActivity(context, typeof(TSource).FullName, beganAt, exception);

        public static Task CallLoggingActivity(this IDurableOrchestrationContext context, string source,
            DateTime beganAt, Exception exception)
        {
            return context.CallActivityAsync(
                nameof(Common_Activity_LogActivityFinish),
                new Common_Activity_LogActivityFinish.Input()
                    {Source = source, BeganAt = beganAt, Success = false, Exception = exception});
        }
    }
}