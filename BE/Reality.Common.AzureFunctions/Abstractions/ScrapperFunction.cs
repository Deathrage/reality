﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Reality.Common.Logging;

namespace Reality.Common.AzureFunctions.Abstractions
{
    public abstract class ScrapperFunction
    {
        private readonly IActivityLogger _loggingProvider;

        protected ScrapperFunction(IActivityLogger loggingProvider)
        {
            _loggingProvider = loggingProvider;
        }

        /// <summary>
        /// Returns ids of scrapped entities
        /// </summary>
        /// <typeparam name="TScrapper"></typeparam>
        /// <typeparam name="TAzureFunction"></typeparam>
        /// <param name="scrapper"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        protected async Task<IEnumerable<int>> RunPerformerFunction<TScrapper, TAzureFunction>(TScrapper scrapper, ILogger log)
            where TScrapper : IScrapper
        {
            log.LogInformation($"Gathering run began on {typeof(TScrapper).Name}.");

            FinalizeScrapperRunLogAsync finalizeScrapperRunLogAsync = _loggingProvider.LogScrapperRunAsync<TAzureFunction, TScrapper>();

            try
            {
                ImmutableArray<int> addedRecords = (await scrapper.RunAsync()).ToImmutableArray();
                await finalizeScrapperRunLogAsync(true, addedRecords.Length);
                return addedRecords;
            }
            catch (Exception ex)
            {
                await finalizeScrapperRunLogAsync(false, 0, ex);
                log.LogError(ex, "Error");
                throw;
            }
            finally
            {
                log.LogInformation($"Gathering run ended on {typeof(TScrapper).Name}.");
            }
        }
    }
}