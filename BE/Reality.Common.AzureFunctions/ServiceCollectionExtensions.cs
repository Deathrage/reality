﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Reality.Common.Extensions;
using Reality.Common.Serialization;

namespace Reality.Common.AzureFunctions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCommonAzureFunctions(this IServiceCollection source, string azureMapsKey,
            Func<IServiceProvider, ISerializer> serializerFactory)
            => source.AddCommon(azureMapsKey, serializerFactory);
    }
}