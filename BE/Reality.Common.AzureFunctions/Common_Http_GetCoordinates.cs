using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Reality.Common.Geocoder;
using Reality.Common.Geocoder.Results;

namespace Reality.Common.AzureFunctions
{
    public class Common_Http_GetCoordinates
    {
	    private readonly IGeocoder _geocoder;

	    public Common_Http_GetCoordinates(IGeocoder geocoder)
	    {
		    _geocoder = geocoder ?? throw new ArgumentNullException(nameof(geocoder));
	    }

	    [FunctionName(nameof(Common_Http_GetCoordinates))]
        public async Task<IActionResult> RunAsync([HttpTrigger(AuthorizationLevel.Function, "get", Route = "geocoding/coordinates")] HttpRequest req,
	        ILogger log)
        {
	        StringValues queryCollection = req.Headers["query"];
            if (queryCollection.Count == 0 || queryCollection.Count > 1)
                throw new ArgumentException($"Specify single query item!");
            string query = queryCollection.Single();

            log.LogInformation($"Began coordinates lookup for query {query}");

            
            StringValues latCollection = req.Headers["lat"];
            if (latCollection.Count > 1)
	            throw new ArgumentException($"Specify single query item!");
            string lat = latCollection.SingleOrDefault();
            decimal? decimalLat = decimal.TryParse(lat, out decimal resultLat) ? resultLat : default; 

            StringValues lonCollection = req.Headers["lon"];
            if (lonCollection.Count > 1)
	            throw new ArgumentException($"Specify single query item!");
            string lon = lonCollection.SingleOrDefault();
            decimal? decimalLon = decimal.TryParse(lon, out decimal resultLon) ? resultLat : default;

            GeoCoordinatesResult result = await _geocoder.GetGeoCoordinatesAsync(query, decimalLon, decimalLat);
            return new JsonResult(result);
        }
    }
}
