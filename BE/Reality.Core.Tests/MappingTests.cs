using AutoMapper;
using NUnit.Framework;

namespace Reality.Core.Tests
{
    public class MappingTests
    {
        [Test]
        public void Mapping_ValidateConfiguration()
        {
            var configuration = new MapperConfiguration(opts => opts.AddProfile(new CoreMapping()));

            configuration.AssertConfigurationIsValid();
        }
    }
}