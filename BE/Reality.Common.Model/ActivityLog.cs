﻿using System;

namespace Reality.Common.Model
{
	public class ActivityLog: IEntity
	{
		public int Id { get; set; }
		public DateTime BeganAt { get; set; }
		public DateTime EndedAt { get; set; }
		public string Source { get; set; }
		public bool Success { get; set; }
		public string Data { get; set; }
	}
}