﻿using Reality.Common.Model.Mixins;

namespace Reality.Common.Model
{
	public interface INamedEntity: IEntity, IWithName
	{
		
	}
}