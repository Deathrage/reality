﻿namespace Reality.Common.Model
{
	public interface IEntity
	{
		int Id { get; }
	}
}