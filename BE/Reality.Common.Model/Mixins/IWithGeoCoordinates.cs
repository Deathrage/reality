﻿namespace Reality.Common.Model.Mixins
{
	public interface IWithGeoCoordinates
	{
		decimal? Latitude { get; }
		decimal? Longitude { get; }
	}
}