﻿using System;

namespace Reality.Common.Model.Mixins
{
	public interface IWithLink
	{
		Uri Link { get; }
	}
}