﻿using System;

namespace Reality.Common.Model.Mixins
{
    public interface IWithModified
    {
        DateTime Modified { get; set; }
    }
}