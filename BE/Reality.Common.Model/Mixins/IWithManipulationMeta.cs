﻿namespace Reality.Common.Model.Mixins
{
	public interface IWithManipulationMeta: IWithCreated, IWithModified
	{
	}
}