﻿using System;

namespace Reality.Common.Model.Mixins
{
    public interface IWithCreated
    {
        DateTime Created { get; set; }
    }
}