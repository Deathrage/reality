﻿namespace Reality.Common.Model.Mixins
{
	public interface IWithName
	{
		string Name { get; }
	}
}