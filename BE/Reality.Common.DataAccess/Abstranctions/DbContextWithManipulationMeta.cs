﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Common.DataAccess.Abstranctions
{
	public abstract class DbContextWithManipulationMeta: DbContext
	{
		public sealed override ChangeTracker ChangeTracker => base.ChangeTracker;

		private void OnEntityTracked(object sender, EntityTrackedEventArgs e)
			=> e.InitializeEntityManipulationMeta();

		private void OnEntityStateChanged(object sender, EntityStateChangedEventArgs e)
			=> e.MarkModifiedOnEntityManipulationMeta();

		protected DbContextWithManipulationMeta(DbContextOptions options) : base(options)
		{
			ChangeTracker.Tracked += OnEntityTracked;
			ChangeTracker.StateChanged += OnEntityStateChanged;
		}
	}
}