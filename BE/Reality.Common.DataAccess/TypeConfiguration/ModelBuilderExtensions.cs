﻿using Microsoft.EntityFrameworkCore;

namespace Reality.Common.DataAccess.TypeConfiguration
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder ConfigureCommonModel(this ModelBuilder builder)
            => builder.ApplyConfiguration(new ActivityLogConfiguration());
    }
}