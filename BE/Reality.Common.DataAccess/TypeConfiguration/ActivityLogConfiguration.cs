﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Common.DataAccess.Extensions;
using Reality.Common.Model;

namespace Reality.Common.DataAccess.TypeConfiguration
{
    internal class ActivityLogConfiguration: IEntityTypeConfiguration<ActivityLog>
    {
        public void Configure(EntityTypeBuilder<ActivityLog> builder)
        {
            builder.ToTable(nameof(ActivityLog) + "s", "dbo");

            builder.ConfigureEntityKey();
        }
    }
}