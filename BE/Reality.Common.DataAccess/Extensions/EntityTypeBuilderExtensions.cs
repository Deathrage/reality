﻿using System;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Common.Model;

namespace Reality.Common.DataAccess.Extensions
{
	public static class EntityTypeBuilderExtensions
	{
		public static void ConfigureEntityKey<TEntity>(this EntityTypeBuilder<TEntity> source) where TEntity : class, IEntity
		{
			source.HasKey(p => p.Id);
			source.Property<int>(p => p.Id).ValueGeneratedOnAdd();
		}

        public static IndexBuilder<TEntity> HasUniqueIndex<TEntity>(this EntityTypeBuilder<TEntity> source,
            Expression<Func<TEntity, object>> expression) where TEntity : class, IEntity
            => source.HasIndex(expression).IsUnique();

        public static PropertyBuilder<TPropertyType> HasEnumPropertyAsString<TEntity, TPropertyType>(
            this EntityTypeBuilder<TEntity> source,
            Expression<Func<TEntity, TPropertyType>> selector) 
                where TEntity : class
                where TPropertyType : Enum 
            => source.Property(selector)
                .HasConversion(value => value.ToString(), value => (TPropertyType) Enum.Parse(typeof(TPropertyType), value));

        public static PropertyBuilder<TPropertyInnerType?> HasEnumPropertyAsString<TEntity, TPropertyInnerType>(
            this EntityTypeBuilder<TEntity> source,
            Expression<Func<TEntity, TPropertyInnerType?>> selector)
            where TEntity : class
            where TPropertyInnerType : struct, Enum
            => source.Property(selector)
                .HasConversion(value => value.HasValue ? value.ToString() : default,
                    value => string.IsNullOrWhiteSpace(value)
                        ? default
                        : (TPropertyInnerType?) Enum.Parse(typeof(TPropertyInnerType), value));
    }
}