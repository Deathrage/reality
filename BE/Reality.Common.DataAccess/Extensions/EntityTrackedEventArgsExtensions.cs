﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Reality.Common.Model.Mixins;

namespace Reality.Common.DataAccess.Extensions
{
    public static class EntityTrackedEventArgsExtensions
    {
        public static void InitializeEntityManipulationMeta(this EntityTrackedEventArgs source)
        {
            if (!source.FromQuery && source.Entry.State == EntityState.Added &&
                source.Entry.Entity is IWithCreated createdEntity && createdEntity.Created == default)
            {
                createdEntity.Created = DateTime.Now;
            }

            if (!source.FromQuery && source.Entry.State == EntityState.Added &&
                source.Entry.Entity is IWithModified modifiedEntity)
            {
                modifiedEntity.Modified = DateTime.Now;
            }
        }

        public static void MarkModifiedOnEntityManipulationMeta(this EntityStateChangedEventArgs source)
        {
            if (source.NewState == EntityState.Modified && source.Entry.Entity is IWithModified entity)
                entity.Modified = DateTime.Now;
        }
    }
}