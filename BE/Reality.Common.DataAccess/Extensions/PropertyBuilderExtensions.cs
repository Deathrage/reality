﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Reality.Common.DataAccess.Extensions
{
	public static class PropertyBuilderExtensions
	{
		public static PropertyBuilder<decimal> HasDecimalPriceColumnType(this PropertyBuilder<decimal> source)
			=> source.HasColumnType("decimal(14,2)");

		public static PropertyBuilder<decimal?> HasDecimalPriceColumnType(this PropertyBuilder<decimal?> source)
			=> source.HasColumnType("decimal(14,2)");

		public static PropertyBuilder<decimal?> HasDecimalGeoCoordinateColumnType(this PropertyBuilder<decimal?> source)
			=> source.HasColumnType("decimal(9,6)");

		public static PropertyBuilder<Uri> HasUriColumn(this PropertyBuilder<Uri> source)
			=> source.HasConversion(u => u.ToString(), s => new Uri(s));
	}
}