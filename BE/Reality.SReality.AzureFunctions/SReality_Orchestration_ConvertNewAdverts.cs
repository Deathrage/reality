﻿using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.EntityFrameworkCore;
using Reality.Access.Functions.Core;
using Reality.Common;
using Reality.Common.AzureFunctions.Extensions;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Extensions;
using Reality.Core.AdvertConverter_;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using SRealityAdvert = Reality.SReality.Model.Advert;

namespace Reality.SReality.AzureFunctions
{
    public class SReality_Orchestration_ConvertNewAdverts
    {
        public class Input
        {
            public IReadOnlyList<int> NewIds { get; set; }
        }

        private readonly IRepository<SRealityAdvert> _srealityAdvertRepository;
        private readonly IAdvertConverter<SRealityAdvert> _converterSource;

        public SReality_Orchestration_ConvertNewAdverts(IRepository<SRealityAdvert> srealityAdvertRepository,
            IAdvertConverter<SRealityAdvert> converterSource)
        {
            _srealityAdvertRepository = srealityAdvertRepository ??
                                       throw new ArgumentNullException(nameof(srealityAdvertRepository));
            _converterSource = converterSource ?? throw new ArgumentNullException(nameof(converterSource));
        }

        [FunctionName(nameof(SReality_Orchestration_ConvertNewAdverts))]
        public async Task RunAsync([OrchestrationTrigger] IDurableOrchestrationContext context)
        {
            DateTime beganAt = context.CurrentUtcDateTime;

            try
            {
                Input input = context.GetInput<Input>();
                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                IReadOnlyList<ConversionItem> items =
                    await context.CallActivityAsync<IReadOnlyList<ConversionItem>>(
                        nameof(SReality_Orchestration_ConvertNewAdverts) + "_" +
                        nameof(CreateConversionItemsFromIds),
                        input.NewIds ?? ReadOnlyList.Empty<int>());

                await context.CallSubOrchestratorAsync(nameof(Core_Orchestration_ProcessConversionItems),
                    new Core_Orchestration_ProcessConversionItems.Input
                    {
                        ConversionItems = items
                    });
            }
            catch (Exception ex)
            {
                await context.CallLoggingActivity<SReality_Orchestration_ConvertNewAdverts>(beganAt,
                    ex);
                throw;
            }
        }

        [FunctionName(nameof(SReality_Orchestration_ConvertNewAdverts) + "_" + nameof(CreateConversionItemsFromIds))]
        public async Task<IReadOnlyList<ConversionItem>> CreateConversionItemsFromIds([ActivityTrigger] IDurableActivityContext context)
        {
            IReadOnlyList<int> input = context.GetInput<IReadOnlyList<int>>();
            if (input == default)
                throw new ArgumentNullException(nameof(input));

            ImmutableArray<SRealityAdvert> adverts = await _srealityAdvertRepository.AsQueryable()
                .Where(item =>
                    input.Contains(item.Id))
                .Include(item => item.MajorAdvertLabels)
                .ToImmutableArrayAsync();
            
            IReadOnlyList<ConversionItem> items = _converterSource.Convert(adverts);

            return items;
        }
    }
}