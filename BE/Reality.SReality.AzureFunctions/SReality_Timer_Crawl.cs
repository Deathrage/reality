using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using Reality.Common.AzureFunctions.Abstractions;
using Reality.Common.AzureFunctions.Extensions;
using Reality.Common.Logging;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;

namespace Reality.SReality.AzureFunctions
{
    public class SReality_Timer_Crawl: ScrapperFunction
    {
	    private readonly ISRealityScrapper _isRealityScrapper;

	    public SReality_Timer_Crawl(IActivityLogger loggingProvider, ISRealityScrapper isRealityScrapper) : base(loggingProvider)
	    {
		    _isRealityScrapper = isRealityScrapper;
	    }

        [FunctionName(nameof(SReality_Timer_Crawl))]
        public async Task RunAsync([TimerTrigger("0 20 1 * * *", RunOnStartup = false)]
            TimerInfo myTimer, ILogger log, [DurableClient] IDurableOrchestrationClient client)
        {
            IEnumerable<int> newIds = await RunPerformerFunction<ISRealityScrapper, SReality_Timer_Crawl>(_isRealityScrapper, log);

            await client.StartNewAsync(nameof(SReality_Orchestration_ConvertNewAdverts),
                new SReality_Orchestration_ConvertNewAdverts.Input
                {
                    NewIds = newIds.ToImmutableArray()
                });
        }
    }
}
