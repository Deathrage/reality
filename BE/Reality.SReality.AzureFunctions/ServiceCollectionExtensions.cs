﻿using Microsoft.Extensions.DependencyInjection;
using Reality.SReality;

namespace Reality.SReality.AzureFunctions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSRealityAzureFunctions(this IServiceCollection source)
            => source.AddSReality();
    }
}