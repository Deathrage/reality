using AutoMapper;
using NUnit.Framework;
using Reality.SReality.Mapping;

namespace Reality.SReality.Tests
{
    public class MappingTests
    {
        [Test]
        public void Mapping_ValidateConfiguration()
        {
            var configuration = new MapperConfiguration(opts => opts.AddProfile(new SRealityMapping()));

            configuration.AssertConfigurationIsValid();
        }
    }
}