﻿using System;
using NUnit.Framework;
using Reality.SReality.Mapping;
using Reality.SReality.Model.Enums;

namespace Reality.SReality.Tests
{
    public class SRealitySurfaceExtractorTests
    {
        public static object[] Titles = new object[]
        {
            new object[] { AdvertCategory.Flat, "Prodej bytu 4+kk 117 m²", 117, (int?)null },
            new object[] { AdvertCategory.Flat, "Pronájem bytu 3+1 113 m²", 113, (int?)null },
            new object[] { AdvertCategory.Commerce, "Pronájem kanceláře 717 m²", 717, (int?)null },
            new object[] { AdvertCategory.Misc, "Pronájem skladového prostoru 2 603 m²", 2603, (int?)null },
            new object[] { AdvertCategory.House, "Pronájem  rodinného domu 168 m², pozemek 1 367 m²", 168, 1367 },
            new object[] { AdvertCategory.Flat,"Pronájem bytu 1+kk 24 m² (Podkrovní)²", 24, (int?)null },
            new object[] { AdvertCategory.Land,"Pronájem  komerčního pozemku 2 035 m²", (int?)null, 2035 },
            new object[] { AdvertCategory.Land,"Pronájem  komerčního pozemku 667 m²", (int?)null, 667 },
        };

        [Test, TestCaseSource(nameof(Titles))]
        public void SRealitySurfaceExtractor_Extract(AdvertCategory category, string title, int? usableSurface, int? landSurface)
        {
            SRealitySurfaceExtractor sut = new SRealitySurfaceExtractor();

            (int? resultUsableSurface, int? resultLandSurface) = sut.Extract(category, title);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(usableSurface, resultUsableSurface);
                Assert.AreEqual(landSurface, resultLandSurface);
            });
        }

        [Test]
        public void SRealitySurfaceExtractor_Extract_NoM2()
        {
            SRealitySurfaceExtractor sut = new SRealitySurfaceExtractor();

            Assert.Throws<InvalidOperationException>(() =>
            {
                sut.Extract(AdvertCategory.Commerce, "asd aps dajofbnajsbd fga");
            });
        }

        [Test]
        public void SRealitySurfaceExtractor_Extract_TooManyM2()
        {
            SRealitySurfaceExtractor sut = new SRealitySurfaceExtractor();

            Assert.Throws<InvalidOperationException>(() =>
            {
                sut.Extract(AdvertCategory.Commerce, "asd aps 115 m² dajofbnajsbd 0341 m² 13 m² fga");
            });
        }

        [Test]
        public void SRealitySurfaceExtractor_Extract_TwoM2ButNotHouse()
        {
            SRealitySurfaceExtractor sut = new SRealitySurfaceExtractor();

            Assert.Throws<InvalidOperationException>(() =>
            {
                sut.Extract(AdvertCategory.Commerce, "asd aps 115 m² dajofbnajsbd 0341 m² fga");
            });
        }

        [Test]
        public void SRealitySurfaceExtractor_Extract_HouseWithOneM2()
        {
            SRealitySurfaceExtractor sut = new SRealitySurfaceExtractor();

            Assert.Throws<InvalidOperationException>(() =>
            {
                sut.Extract(AdvertCategory.House, "asd aps 115 m²");
            });
        }
    }
}