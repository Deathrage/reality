﻿using Reality.Common.DataAccess.Abstractions;
using Reality.UI.Model;
using Reality.UI.Model.EstateInBoundary_;

namespace Reality.UI.DataAccess.Abstractions
{
    public interface IEstatesInBoundaryProvider : IDataProvider<EstateInBoundary, EstatesInBoundaryParameter>
    {
    }
}