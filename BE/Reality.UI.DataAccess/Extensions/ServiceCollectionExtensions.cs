﻿using Microsoft.Extensions.DependencyInjection;
using Reality.Common.DataAccess.Abstractions;
using Reality.UI.DataAccess.Providers;
using Reality.UI.Model.EstateInBoundary_;

namespace Reality.UI.DataAccess.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddUIDataProvider(this IServiceCollection services)
            => services.AddDataProvider<EstatesInBoundaryProvider>(ServiceLifetime.Scoped)
                .As<EstateInBoundary, EstatesInBoundaryParameter>();
    }
}