﻿using Microsoft.EntityFrameworkCore;
using Reality.UI.DataAccess.TypeConfigurations;

namespace Reality.UI.DataAccess.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder ConfigureUIModel(this ModelBuilder builder)
            => builder.ApplyConfiguration(new ListableAdvertConfiguration())
                .ApplyConfiguration(new EstateInBoundaryConfiguration());
    }
}