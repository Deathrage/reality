﻿using System;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.UI.Model.EstateInBoundary_;

namespace Reality.UI.DataAccess.Providers
{
    internal class EstatesInBoundaryProvider: IDataProvider<EstateInBoundary, EstatesInBoundaryParameter>
    {
        private readonly IRawCommandProvider _rawCommandProvider;

        public EstatesInBoundaryProvider(IRawCommandProvider rawCommandProvider)
        {
            _rawCommandProvider = rawCommandProvider ?? throw new ArgumentNullException(nameof(rawCommandProvider));
        }

        public IQueryable<EstateInBoundary> AsQueryable(EstatesInBoundaryParameter parameter)
        {
            if (parameter == default)
                throw new ArgumentNullException(nameof(parameter));

            ImmutableArray<EstatesInBoundaryCoordinates> coordinates = parameter.Boundary.ToImmutableArray();

            if (coordinates.Length - 1 < 3)
                throw new InvalidOperationException(
                    $"Boundary has to have at least 3 unique (4 total) points, supplied had {coordinates.Length - 1} ({coordinates.Length} total)");

            if (coordinates.Length - 1 > 12)
                throw new InvalidOperationException(
                    $"Boundary can have up to 12 unique (13 total) points only, supplied had {coordinates.Length - 1} ({coordinates.Length } total)");

            string boundaries = string.Join(",", parameter.Boundary.Select(c => $"{c.Lon.ToString("N6", CultureInfo.InvariantCulture)} {c.Lat.ToString("N6", CultureInfo.InvariantCulture)}"));

            string command = $"SELECT EstateId FROM ui.GetEstatesInBoundaryFunc('{boundaries}')";

            return _rawCommandProvider.FromString<EstateInBoundary>(command);
        }
    }
}