﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.UI.Model.EstateInBoundary_;

namespace Reality.UI.DataAccess.TypeConfigurations
{
    internal class EstateInBoundaryConfiguration : IEntityTypeConfiguration<EstateInBoundary>
    {
        public void Configure(EntityTypeBuilder<EstateInBoundary> builder)
        {
            builder.Metadata.SetTableName(null);
            builder.Metadata.SetAnnotation(RelationalAnnotationNames.DbFunction, null);
            builder.HasNoKey();
        }
    }
}