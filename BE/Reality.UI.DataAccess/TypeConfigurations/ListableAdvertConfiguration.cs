﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Common.DataAccess.Extensions;
using Reality.UI.Model;

namespace Reality.UI.DataAccess.TypeConfigurations
{
    internal class ListableAdvertConfiguration: IEntityTypeConfiguration<ListableAdvert>
    {
        public void Configure(EntityTypeBuilder<ListableAdvert> b)
        {
            b.HasKey(p => p.Id);

            b.ToView("ListableAdvertsView", "ui");
            
            b.HasEnumPropertyAsString(p => p.AdvertType);
            b.Property(p => p.PriceCzk).HasDecimalPriceColumnType();

            b.HasOne(p => p.Estate).WithMany()
                .HasForeignKey(_ => _.EstateId).OnDelete(DeleteBehavior.NoAction);
        }
    }
}