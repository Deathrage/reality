﻿using AutoMapper;
using Reality.Core.Models.Entities.Address_;
using Reality.Common.Geocoder.Results;

namespace Reality.Core
{
    public class CoreMapping: Profile
    {
        public CoreMapping()
        {
            CreateMap<AddressResult, Address>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.StreetId, opts => opts.Ignore())
                .ForMember(dst => dst.Created, opts => opts.Ignore())
                .ForMember(dst => dst.Modified, opts => opts.Ignore())
                .ForMember(dst => dst.Estates, opts => opts.Ignore())
                .ForMember(dst => dst.Street, opts => opts.MapFrom(src => src));

            CreateMap<AddressResult, Street>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.Created, opts => opts.Ignore())
                .ForMember(dst => dst.Modified, opts => opts.Ignore())
                .ForMember(dst => dst.Addresses, opts => opts.Ignore())
                .ForMember(dst => dst.MunicipalityId, opts => opts.Ignore())
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Street ?? src.Municipality))
                .ForMember(dst => dst.Municipality, opts => opts.MapFrom(src => src));

            CreateMap<AddressResult, Municipality>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.Created, opts => opts.Ignore())
                .ForMember(dst => dst.Modified, opts => opts.Ignore())
                .ForMember(dst => dst.Streets, opts => opts.Ignore())
                .ForMember(dst => dst.CountryId, opts => opts.Ignore())
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Municipality))
                .ForMember(dst => dst.CountrySubdivision, opts => opts.MapFrom(src => src.CountrySubdivision))
                .ForMember(dst => dst.CountrySecondarySubdivision, opts => opts.MapFrom(src => src.CountrySecondarySubdivision))
                .ForMember(dst => dst.Country, opts => opts.MapFrom(src => src));

            CreateMap<AddressResult, Country>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.Created, opts => opts.Ignore())
                .ForMember(dst => dst.Modified, opts => opts.Ignore())
                .ForMember(dst => dst.Municipalities, opts => opts.Ignore())
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Country))
                .ForMember(dst => dst.Code, opts => opts.MapFrom(src => src.CountryCode));
        }
    }
}