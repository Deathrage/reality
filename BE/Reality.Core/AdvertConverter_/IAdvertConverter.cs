﻿using System.Collections.Generic;

namespace Reality.Core.AdvertConverter_
{
    public interface IAdvertConverter<in TSourceEntity>
    {
        string ModuleName { get; }

        IReadOnlyList<ConversionItem> Convert(IReadOnlyList<TSourceEntity> sources);
    }
}