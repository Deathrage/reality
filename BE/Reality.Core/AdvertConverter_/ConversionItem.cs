﻿using System;
using Reality.Core.Models.Data;
using Reality.Core.Models.Entities.Address_;
using Reality.Core.Models.Entities.Advert_;
using Reality.Core.Models.Entities.Estate_;

namespace Reality.Core.AdvertConverter_
{
    public readonly struct ConversionItem
    {
        public int SourceId => ConversionMeta.SourceAdvertId;

        public Conversion ConversionMeta { get; }
        public Advert Advert { get; }

        public Estate Estate { get; }
        public Address Address { get; }
        public Street Street { get; }
        public Municipality Municipality { get; }
        public Country Country { get; }

        public ConversionItem(Conversion conversionMeta, Advert advert, Estate estate, Address address, Street street,
            Municipality municipality, Country country)
        {
            ConversionMeta = conversionMeta ?? throw new ArgumentNullException(nameof(conversionMeta));
            Advert = advert;
            Estate = estate;
            Address = address;
            Street = street;
            Municipality = municipality;
            Country = country;
        }

        public ConversionItem Fill(Advert advert = null, Estate estate = null, Address address = null,
            Street street = null, Municipality municipality = null, Country country = null)
        {
            return new ConversionItem(ConversionMeta, advert ?? Advert, estate ?? Estate, address ?? Address,
                street ?? Street, municipality ?? Municipality, country ?? Country);
        }
    }
}