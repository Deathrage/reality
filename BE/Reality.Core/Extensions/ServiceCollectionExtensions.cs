﻿using Microsoft.Extensions.DependencyInjection;

namespace Reality.Core.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCore(this IServiceCollection source)
            => source;
    }
}