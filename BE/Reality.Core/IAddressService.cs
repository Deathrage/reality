﻿using System.Threading.Tasks;
using Reality.Core.Models.Entities.Address_;

namespace Reality.Core
{
    public interface IAddressService
    {
        Task<Street> GetProbableStreetAsync(Street constraint);

        Task<Municipality> GetProbableMunicipalityAsync(Municipality constraint);

        Task<Country> GetProbableCountryAsync(Country constraint);

        Task<Address> GetProbableAddressAsync(Address constraint);
    }
}