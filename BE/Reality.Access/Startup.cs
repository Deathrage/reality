﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection.AzureFunctions;
using AutoMapper;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.DependencyInjection;
using Reality.BezRealitky.Mapping;
using Reality.Core;
using Reality.SReality.Mapping;
using Serilog;
using Serilog.Core;
using Reality.BezRealitky.AzureFunctions;
using Reality.Common.AzureFunctions;
using Reality.Core.AzureFunctions;
using Reality.DataAccess;
using Reality.Idnes.Mappings;
using Reality.Idnes.AzureFunctions;
using Reality.SReality.AzureFunctions;
using Reality.UI.AzureFunctions;

[assembly: FunctionsStartup(typeof(Reality.Access.Startup))]

namespace Reality.Access
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            ConfigureServices(builder.Services);
            builder
                .UseAutofacServiceProviderFactory(ConfigureContainer);
        }

        private IEnumerable<Type> GetAzureFunctionTypes(Assembly assembly)
        {
            IEnumerable<Type> azureFunctionsOfAssembly = assembly.GetTypes().Where(type =>
                type.GetMethods().Any(method => method.IsDefined(typeof(FunctionNameAttribute), true)));
            return azureFunctionsOfAssembly;
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            List<Type> azureFunctions = new List<Type>();
            // This assembly reference all AzureFunctions assemblies
            Assembly[] domainAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly domainAssembly in domainAssemblies)
            {
                IEnumerable<Type> functionTypes = GetAzureFunctionTypes(domainAssembly);
                azureFunctions.AddRange(functionTypes);
            }

            foreach (Type azureFunction in azureFunctions)
            {
                builder.RegisterType(azureFunction).AsSelf().InstancePerTriggerRequest();
            }
        }

        public void ConfigureServices(IServiceCollection serviceCollection)
        {
            Logger serilogger = new LoggerConfiguration().WriteTo
                .Console(
                    outputTemplate:
                    "[{Timestamp:HH:mm:ss} {Level:u3}] <s:{SourceContext}> {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
            serviceCollection.AddLogging(b => b.AddSerilog(serilogger));

            serviceCollection.AddAutoMapper(config =>
            {
                config.AddProfile<SRealityMapping>();
                config.AddProfile<BezRealitkyMapping>();
                config.AddProfile<CoreMapping>();
                config.AddProfile<IdnesMapping>();
            });

            serviceCollection.AddHttpClient();
            serviceCollection.AddMemoryCache();
            serviceCollection.AddSingleton<IMessageSerializerSettingsFactory, MessageSerializerSettingsFactory>();

            string sqlConnectionString = Environment.GetEnvironmentVariable("ConnectionStrings:RealityDB");
            string azureMapsKey = Environment.GetEnvironmentVariable("AzureMapsKey");

            serviceCollection
                .AddDataProviders(sqlConnectionString, "Reality.Access")
                .AddCommonAzureFunctions(azureMapsKey,
                    provider => new DefaultSerializer(provider.GetService<IMessageSerializerSettingsFactory>()))
                .AddSRealityAzureFunctions()
                .AddBezRealitkyAzureFunctions()
                .AddIdnesAzureFunctions()
                .AddCoreAzureFunctions()
                .AddUIAzureFunctions();
        }
    }
}