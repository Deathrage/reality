param(
   [Parameter(Mandatory=$True)]
   [string] $Destination
)
if ([string]::IsNullOrEmpty($Destination.Trim())) {
   throw "Parameter -Destination cannot be null or empty"
}

$here = Resolve-Path '.'
Write-Host "Here is $here"

# if (!([System.IO.Path]::IsPathRooted($Destination))) {   
#    $Destination = Resolve-Path $Destination
# }
Write-Host "Destination $Destination"

$functions = Get-ChildItem '..' -Include 'function.json' -Recurse -File | Where-Object { !($_.FullName.Contains($here))  }
$count = $functions.Count;
Write-Host "Found $count function to import";

Foreach ($function in $functions) {
   Copy-Item $function.DirectoryName -Destination $Destination -Recurse -Force
   Write-Host (Get-Item $function.DirectoryName)
} 

# powershell "$(ProjectDir)assembleFunctions.ps1 -Destination '$(OutDir)'"