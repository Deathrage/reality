﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Reality.DataAccess;

namespace Reality.Common.AzureFunctions
{
	public class Service_Http_ApplyMigrations
    {
        private readonly RealityDbContext _context;

        public Service_Http_ApplyMigrations(RealityDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [FunctionName(nameof(Service_Http_ApplyMigrations))]
        public async Task<IActionResult> RunAsync(
			[HttpTrigger(AuthorizationLevel.Function, "get", Route = "db/apply-new-migrations")] HttpRequest req,
			ILogger log)
		{
			try
            {
                await _context.Database.MigrateAsync();
				return new OkObjectResult("OK");
			}
			catch (Exception ex)
			{
				log.LogError(ex, "Error");
				return new ExceptionResult(ex, true);
			}

		}
	}
}