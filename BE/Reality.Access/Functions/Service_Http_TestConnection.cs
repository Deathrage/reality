﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Reality.DataAccess;

namespace Reality.Common.AzureFunctions
{
	public class Service_Http_TestConnection
	{
        private readonly RealityDbContext _context;

        public Service_Http_TestConnection(RealityDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [FunctionName(nameof(Service_Http_TestConnection))]
		public async Task<IActionResult> RunAsync(
			[HttpTrigger(AuthorizationLevel.Function, "get", Route = "db/test-connection")] HttpRequest req,
			ILogger log)
		{
			try
            {
                bool canConnect = await _context.Database.CanConnectAsync();
				return new OkObjectResult(canConnect ? "OK" : "NOT OK");
			}
			catch (Exception ex)
			{
				log.LogError(ex, "Error");
				return new ExceptionResult(ex, true);
			}

		}
	}
}