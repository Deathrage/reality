using System.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;

namespace Reality.Common.AzureFunctions
{
    public static class Service_Http_Version
    {
        [FunctionName(nameof(Service_Http_Version))]
        public static IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "version")] HttpRequest req,
            ILogger log)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            string version = assembly.GetCustomAttribute<AssemblyFileVersionAttribute>()?.Version;

            return new ObjectResult(version);
        }
    }
}
