﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Reality.DataAccess;

namespace Reality.Access
{
    public class RealityDbContextFactory: IDesignTimeDbContextFactory<RealityDbContext>
    {
        public RealityDbContext CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<RealityDbContext> optionsBuilder = new DbContextOptionsBuilder<RealityDbContext>();
            optionsBuilder.UseSqlServer(Environment.GetEnvironmentVariable("ConnectionStrings:RealityDB"),
                b => b.MigrationsAssembly("Reality.Access").UseNetTopologySuite());

            return new RealityDbContext(optionsBuilder.Options);
        }
    }
} 