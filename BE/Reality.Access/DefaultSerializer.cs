﻿using System;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Newtonsoft.Json;
using Reality.Common.Serialization;

namespace Reality.Access
{
    public class DefaultSerializer: ISerializer
    {
        private readonly IMessageSerializerSettingsFactory _settingsFactory;

        private JsonSerializerSettings Settings => _settingsFactory.CreateJsonSerializerSettings();

        public DefaultSerializer(IMessageSerializerSettingsFactory settingsFactory)
        {
            _settingsFactory = settingsFactory ?? throw new ArgumentNullException(nameof(settingsFactory));
        }

        public T Deserialize<T>(string input)
            => JsonConvert.DeserializeObject<T>(input, Settings);

        public string Serialize(object input)
            => JsonConvert.SerializeObject(input, Formatting.None, Settings);

        public string SerializeException(Exception ex)
        {
            JsonSerializerSettings exSettings = Settings;
            exSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            return JsonConvert.SerializeObject(ex, exSettings);
        }
    }
}