﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class IdnesModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "idnes");

            migrationBuilder.CreateTable(
                name: "Adverts",
                schema: "idnes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    ExternalId = table.Column<string>(nullable: true),
                    AdvertType = table.Column<int>(nullable: false),
                    EstateType = table.Column<int>(nullable: false),
                    Locality = table.Column<string>(nullable: true),
                    PriceCzk = table.Column<decimal>(type: "decimal(14,2)", nullable: true),
                    Construction = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Ownership = table.Column<string>(nullable: true),
                    UsableSurface = table.Column<int>(nullable: true),
                    Floor = table.Column<int>(nullable: true),
                    Balcony = table.Column<bool>(nullable: false),
                    Loggia = table.Column<bool>(nullable: false),
                    Terrace = table.Column<bool>(nullable: false),
                    Cellar = table.Column<bool>(nullable: false),
                    Garage = table.Column<bool>(nullable: false),
                    Parking = table.Column<bool>(nullable: false),
                    Lift = table.Column<bool>(nullable: false),
                    EasyAccess = table.Column<bool>(nullable: false),
                    Landline = table.Column<bool>(nullable: false),
                    CableTelevision = table.Column<bool>(nullable: false),
                    Internet = table.Column<bool>(nullable: false),
                    Furnishment = table.Column<string>(nullable: true),
                    Penb = table.Column<string>(nullable: true),
                    Disposition = table.Column<string>(nullable: true),
                    Longitude = table.Column<decimal>(type: "decimal(9,6)", nullable: true),
                    Latitude = table.Column<decimal>(type: "decimal(9,6)", nullable: true),
                    Link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adverts", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Adverts",
                schema: "idnes");
        }
    }
}
