﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class MigrateGeoPointAndAlterEstatesInBoundary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER FUNCTION ui.GetEstatesInBoundaryFunc(
	                    @Boundaries VARCHAR(MAX) -- all boundaries defining the shape in format: lon1 lat1, lon2 lat2 ..	
                ) RETURNS @Result TABLE (
                    EstateId INT
                ) AS
                BEGIN
	                 DECLARE @BoundingPoly geography = geography::STPolyFromText('POLYGON((' + @Boundaries + '))', 4326);
	                 IF (@BoundingPoly.EnvelopeAngle() > 90)
		                SET @BoundingPoly = @BoundingPoly.ReorientObject()

                    INSERT INTO @Result
                        SELECT	estates.Id as EstateId
                    FROM core.Estates estates
                    WHERE estates.Latitude IS NOT NULL and estates.Longitude IS NOT NULL
	                AND @BoundingPoly.STContains(estates.GeoPoint) = 1

                    RETURN;
                END
            ");

            migrationBuilder.Sql(@"
                UPDATE core.Estates SET GeoPoint = geography::Point(Latitude, Longitude, 4326) WHERE GeoPoint IS NULL
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER FUNCTION ui.GetEstatesInBoundaryFunc(
	                    @Boundaries VARCHAR(MAX) -- all boundaries defining the shape in format: lon1 lat1, lon2 lat2 ..	
                ) RETURNS @Result TABLE (
                    EstateId INT
                ) AS
                BEGIN
	                 DECLARE @BoundingPoly geography = geography::STPolyFromText('POLYGON((' + @Boundaries + '))', 4326);
	                 IF (@BoundingPoly.EnvelopeAngle() > 90)
		                SET @BoundingPoly = @BoundingPoly.ReorientObject()

                    INSERT INTO @Result
                        SELECT	estates.Id as EstateId
                    FROM core.Estates estates
                    WHERE estates.Latitude IS NOT NULL and estates.Longitude IS NOT NULL
	                AND @BoundingPoly.STContains(geography::Point(estates.Latitude, estates.Longitude, 4326)) = 1

                    RETURN;
                END
            ");
        }
    }
}
