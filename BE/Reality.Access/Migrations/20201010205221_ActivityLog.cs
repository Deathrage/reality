﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class ActivityLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActivityLogs",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BeganAt = table.Column<DateTime>(nullable: false),
                    EndedAt = table.Column<DateTime>(nullable: false),
                    Source = table.Column<string>(nullable: true),
                    Success = table.Column<bool>(nullable: false),
                    Data = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityLogs", x => x.Id);
                });

            migrationBuilder.Sql(
                "INSERT INTO dbo.ActivityLogs SELECT BeganAt as BeganAt, BeganAt as EndedAt, Source as Source, Success as Success, 'Added ' + CAST(AddedRecords AS NVARCHAR(MAX)) + ' records' as Data FROM dbo.PerformerRunLogs");

            migrationBuilder.DropTable(
                name: "PerformerRunLogs",
                schema: "dbo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityLogs",
                schema: "dbo");

            migrationBuilder.CreateTable(
                name: "PerformerRunLogs",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddedRecords = table.Column<int>(type: "int", nullable: false),
                    BeganAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Source = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Success = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PerformerRunLogs", x => x.Id);
                });
        }
    }
}
