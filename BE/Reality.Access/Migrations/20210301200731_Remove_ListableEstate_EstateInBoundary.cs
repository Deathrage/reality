﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class Remove_ListableEstate_EstateInBoundary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW ui.ListableEstatesView");
            migrationBuilder.Sql("DROP FUNCTION ui.GetEstatesInBoundaryFunc");

            migrationBuilder.CreateIndex(
                name: "IX_Conversions_TargetAdvertId",
                schema: "core",
                table: "Conversions",
                column: "TargetAdvertId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Conversions_TargetAdvertId",
                schema: "core",
                table: "Conversions");

            migrationBuilder.Sql(@"
                CREATE FUNCTION ui.GetEstatesInBoundaryFunc(
	                    @Boundaries VARCHAR(MAX) -- all boundaries defining the shape in format: lat1 long1, lat2 long2 ..	
                ) RETURNS @Result TABLE (
                    EstateId INT
                ) AS
                BEGIN
                        
                    INSERT INTO @Result
                        SELECT
                    estates.Id as EstateId
                    FROM core.Estates estates
                    WHERE estates.Latitude IS NOT NULL and estates.Longitude IS NOT NULL
                    AND geography::STPointFromText('POINT(' + CAST(estates.Latitude AS VARCHAR(MAX)) + ' ' + CAST(estates.Longitude AS VARCHAR(MAX)) + ')', 4326).STIntersects(@Boundaries) = 1

                    RETURN;
                END
            ");

            migrationBuilder.Sql(@"
                  CREATE VIEW ui.ListableEstatesView AS 
                SELECT [Id]
                  ,[Name]
                  ,[AddressId]
                  ,[EstateType]
                  ,[Ownership]
                  ,[Longitude]
                  ,[Latitude]
                  ,[UsableSurface]
                  ,[Furnishment]
                  ,[Construction]
                  ,[Created]
                  ,[Disposition]
                  ,[Floor]
                  ,[Balcony]
                  ,[Terrace]
                  ,[Garage]
                  ,[Lift]
                  ,[NewBuilding]
                  ,[LandSurface]
                  ,[LowEnergy]
                  ,[LandType]
                  ,[Parking]
                  ,[Locality]
                  ,[HasOwnAddress]
                  ,[BusinessType]
              FROM [core].[Estates]
            ");
        }
    }
}
