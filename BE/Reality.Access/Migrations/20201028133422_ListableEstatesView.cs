﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class ListableEstatesView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                  CREATE VIEW ui.ListableEstatesView AS 
                SELECT [Id]
                  ,[Name]
                  ,[AddressId]
                  ,[EstateType]
                  ,[Ownership]
                  ,[Longitude]
                  ,[Latitude]
                  ,[UsableSurface]
                  ,[Furnishment]
                  ,[Construction]
                  ,[Created]
                  ,[Disposition]
                  ,[Floor]
                  ,[Balcony]
                  ,[Terrace]
                  ,[Garage]
                  ,[Lift]
                  ,[NewBuilding]
                  ,[LandSurface]
                  ,[LowEnergy]
                  ,[LandType]
                  ,[Parking]
                  ,[Locality]
                  ,[HasOwnAddress]
                  ,[BusinessType]
              FROM [core].[Estates]
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW ui.ListableEstatesView");
        }
    }
}
