﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class FixColNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "House_Balcony",
                schema: "core",
                table: "Estates");

            migrationBuilder.DropColumn(
                name: "House_Furnishment",
                schema: "core",
                table: "Estates");

            migrationBuilder.DropColumn(
                name: "House_LowEnergy",
                schema: "core",
                table: "Estates");

            migrationBuilder.DropColumn(
                name: "House_NewBuilding",
                schema: "core",
                table: "Estates");

            migrationBuilder.DropColumn(
                name: "House_Terrace",
                schema: "core",
                table: "Estates");

            migrationBuilder.DropColumn(
                name: "LandSurface1",
                schema: "core",
                table: "Estates");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "House_Balcony",
                schema: "core",
                table: "Estates",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "House_Furnishment",
                schema: "core",
                table: "Estates",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "House_LowEnergy",
                schema: "core",
                table: "Estates",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "House_NewBuilding",
                schema: "core",
                table: "Estates",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "House_Terrace",
                schema: "core",
                table: "Estates",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LandSurface1",
                schema: "core",
                table: "Estates",
                type: "int",
                nullable: true);
        }
    }
}
