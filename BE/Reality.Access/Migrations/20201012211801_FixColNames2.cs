﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class FixColNames2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "House_Garage",
                schema: "core",
                table: "Estates");

            migrationBuilder.DropColumn(
                name: "House_UsableSurface",
                schema: "core",
                table: "Estates");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "House_Garage",
                schema: "core",
                table: "Estates",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "House_UsableSurface",
                schema: "core",
                table: "Estates",
                type: "int",
                nullable: true);
        }
    }
}
