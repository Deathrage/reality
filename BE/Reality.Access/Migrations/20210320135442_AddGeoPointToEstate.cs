﻿using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace Reality.Access.Migrations
{
    public partial class AddGeoPointToEstate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Point>(
                name: "GeoPoint",
                schema: "core",
                table: "Estates",
                nullable: true);

            migrationBuilder.Sql(@"
                CREATE SPATIAL INDEX SI_Estates_GeoPoint ON core.Estates (GeoPoint)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP INDEX SI_Estates_GeoPoint ON core.Estates
            ");

            migrationBuilder.DropColumn(
                name: "GeoPoint",
                schema: "core",
                table: "Estates");
        }
    }
}
