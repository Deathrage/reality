﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class AddOwnAddressAndMigrateStreets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasOwnAddress",
                schema: "core",
                table: "Estates",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "EstateInBoundary",
                columns: table => new
                {
                    EstateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.ForeignKey(
                        name: "FK_EstateInBoundary_Estates_EstateId",
                        column: x => x.EstateId,
                        principalSchema: "core",
                        principalTable: "Estates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EstateInBoundary_EstateId",
                table: "EstateInBoundary",
                column: "EstateId");

            migrationBuilder.Sql(@"
                ALTER VIEW core.AdvertEstatesView AS
                SELECT 
	                adverts.Id,
	                adverts.Name,
	                adverts.EstateId,
	                estates.Name as EstateName,
	                adverts.AdvertType,
	                adverts.PriceCzk,
	                estates.EstateType,
	                estates.Ownership,
	                estates.Longitude,
	                estates.Latitude,
	                estates.LandSurface,
	                estates.UsableSurface,
	                estates.Furnishment,
	                estates.Construction,
	                estates.Disposition,
	                estates.Floor as FlatFloor,
	                estates.Balcony,
	                estates.Terrace,
	                estates.Garage,
	                estates.Parking,
	                estates.Lift,
	                estates.LowEnergy,
	                estates.NewBuilding,
	                estates.LandType,
                    estates.HasOwnAddress,
	                adverts.Created,
	                adverts.Modified,
	                estates.Created as EstateCreated,
	                estates.Modified as EstateModified,	
	                municipalities.Id as MunicipalityId,
	                streets.Id as StreetId,
	                countries.Id as CountryId,
	                municipalities.Name as MunicipalityName,
	                streets.Name as StreetName,
	                addresses.StreetNumber,
	                countries.Name as CountryName,
	                countries.Code as CountryCode,
	                adverts.Link as Link
                FROM core.Adverts adverts
                JOIN core.Estates estates ON estates.Id = adverts.EstateId
                LEFT JOIN core.Addresses addresses ON addresses.Id = estates.AddressId
                LEFT JOIN core.Streets streets ON streets.Id = addresses.StreetId
                LEFT JOIN core.Municipalities municipalities ON municipalities.Id = streets.MunicipalityId
                LEFT JOIN core.Countries countries ON countries.Id = municipalities.CountryId
            ");

            // No NULL in streets use town name instead
            migrationBuilder.Sql(@"
               UPDATE core.Streets 
                SET Name = sc.Municipality
                FROM (
	                SELECT streets.Id, muni.Name as Municipality FROM core.Streets streets
	                JOIN core.Municipalities muni ON muni.Id = streets.MunicipalityId
	                WHERE streets.Name IS NULL
                ) sc
                WHERE core.Streets.Id = sc.Id
            ");

            // At the beginning make everyone have own address (some incosistency is ok)
            migrationBuilder.Sql(@"
                UPDATE core.Estates
                SET HasOwnAddress = 1
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EstateInBoundary");

            migrationBuilder.DropColumn(
                name: "HasOwnAddress",
                schema: "core",
                table: "Estates");
        }
    }
}
