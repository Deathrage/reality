﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class AddAdvertEstateView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                CREATE VIEW core.AdvertEstatesView AS
                SELECT 
	                adverts.Id,
	                adverts.Name as EstateName,
	                adverts.EstateId,
	                estates.Name,
	                adverts.AdvertType,
	                adverts.PriceCzk,
	                estates.EstateType,
	                estates.Ownership,
	                estates.Longitude,
	                estates.Latitude,
	                estates.LandSurface,
	                estates.UsableSurface,
	                estates.Furnishment,
	                estates.Construction,
	                estates.Disposition,
	                estates.Floor as FlatFloor,
	                estates.Balcony,
	                estates.Terrace,
	                estates.Garage,
	                estates.Parking,
	                estates.Lift,
	                estates.LowEnergy,
	                estates.NewBuilding,
	                estates.LandType,
	                adverts.Created,
	                adverts.Modified,
	                estates.Created as EstateCreated,
	                estates.Modified as EstateModified,	
	                municipalities.Id as MunicipalityId,
	                streets.Id as StreetId,
	                countries.Id as CountryId,
	                municipalities.Name as MunicipalityName,
	                streets.Name as StreetName,
	                addresses.StreetNumber,
	                countries.Name as CountryName,
	                countries.Code as CountryCode,
	                adverts.Link as Link
                FROM core.Adverts adverts
                JOIN core.Estates estates ON estates.Id = adverts.EstateId
                LEFT JOIN core.Addresses addresses ON addresses.Id = estates.AddressId
                LEFT JOIN core.Streets streets ON streets.Id = addresses.StreetId
                LEFT JOIN core.Municipalities municipalities ON municipalities.Id = streets.MunicipalityId
                LEFT JOIN core.Countries countries ON countries.Id = municipalities.CountryId
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW core.AdvertEstatesView");
        }
    }
}
