﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class AddEstateInBoundaries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema("ui");

            migrationBuilder.Sql(@"
                CREATE FUNCTION ui.GetEstatesInBoundaryFunc(
	                    @Boundaries VARCHAR(MAX) -- all boundaries defining the shape in format: lat1 long1, lat2 long2 ..	
                ) RETURNS @Result TABLE (
                    EstateId INT
                ) AS
                BEGIN
                        
                    INSERT INTO @Result
                        SELECT
                    estates.Id as EstateId
                    FROM core.Estates estates
                    WHERE estates.Latitude IS NOT NULL and estates.Longitude IS NOT NULL
                    AND geography::STPointFromText('POINT(' + CAST(estates.Latitude AS VARCHAR(MAX)) + ' ' + CAST(estates.Longitude AS VARCHAR(MAX)) + ')', 4326).STIntersects(@Boundaries) = 1

                    RETURN;
                END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION ui.GetEstatesInBoundaryFunc");
        }
    }
}
