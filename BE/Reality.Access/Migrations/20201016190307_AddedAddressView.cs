﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class AddedAddressView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                CREATE VIEW core.FullAddressesView AS
                SELECT 
	                address.Id,
	                address.StreetId,
	                streets.MunicipalityId,
	                municipalities.CountryId,
	                streets.Name as Street,
	                address.StreetNumber,
	                municipalities.CountrySubdivision,
	                municipalities.CountrySecondarySubdivision,
	                municipalities.Name as Municipality,
	                countries.Name as Country,
	                countries.Code as CountryCode
                FROM core.Addresses address
                LEFT JOIN core.Streets streets ON streets.id = Address.StreetId
                LEFT JOIN core.Municipalities municipalities ON streets.MunicipalityId = municipalities.Id
                LEFT JOIN core.Countries countries ON countries.Id = municipalities.CountryId
            ");

            migrationBuilder.Sql(@"
                CREATE VIEW core.EstatesPerCountriesView AS
                SELECT COUNT(estate.Id) as EstateCount, countries.Id as CountryId, countries.Name as CountryName, countries.Code as CountryCode
                FROM core.Countries countries
                LEFT JOIN core.Municipalities municipality ON municipality.CountryId = countries.Id
                LEFT JOIN core.Streets street ON street.MunicipalityId = municipality.Id
                LEFT JOIN core.Addresses address ON address.StreetId = street.Id
                LEFT JOIN core.Estates estate ON estate.AddressId = address.Id
                GROUP BY countries.Id, countries.Name, countries.Code
            ");

            migrationBuilder.Sql(@"
                CREATE VIEW core.EstatesPerMunicipalitiesView AS
                SELECT COUNT(estate.Id) as EstateCount, municipality.Id as MunicipalityId, municipality.Name as MunicipalityName, municipality.CountrySubdivision, municipality.CountrySecondarySubdivision, country.Id as CountryId, country.Name as CountryName, country.Code as CountryCode
                FROM core.Municipalities municipality
                LEFT JOIN core.Streets street ON street.MunicipalityId = municipality.Id
                LEFT JOIN core.Addresses address ON address.StreetId = street.Id
                LEFT JOIN core.Estates estate ON estate.AddressId = address.Id
                LEFT JOIN core.Countries country ON municipality.CountryId = country.Id
                GROUP BY municipality.Id, municipality.Name, municipality.CountrySubdivision, municipality.CountrySecondarySubdivision, country.Name, country.Code, country.Id
            ");

            migrationBuilder.Sql(@"
                CREATE VIEW core.EstatesPerStreetsView AS
                SELECT COUNT(estate.Id) as EstateCount,
	                street.Id as StreetId,
	                street.Name as StreetName,
	                municipality.Id as MunicipalityId, 
	                municipality.Name as MunicipalityName,
	                municipality.CountrySubdivision,
	                municipality.CountrySecondarySubdivision, 
	                country.Id as CountryId,
	                country.Name as CountryName,
	                country.Code as CountryCode
                FROM core.Streets street
                LEFT JOIN core.Addresses address ON address.StreetId = street.Id
                LEFT JOIN core.Estates estate ON estate.AddressId = address.Id
                LEFT JOIN core.Municipalities municipality ON municipality.Id = street.MunicipalityId
                LEFT JOIN core.Countries country ON municipality.CountryId = country.Id
                GROUP BY street.Id, street.Name, municipality.Id, municipality.Name, municipality.CountrySubdivision, municipality.CountrySecondarySubdivision, country.Name, country.Code, country.Id

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW core.FullAddressesView");
            migrationBuilder.Sql("DROP VIEW core.EstatesPerCountriesView ");
            migrationBuilder.Sql("DROP VIEW core.EstatesPerMunicipalitiesView");
            migrationBuilder.Sql("DROP VIEW core.EstatesPerStreetsView");
        }
    }
}   
