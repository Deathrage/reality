﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class AdjustedListableAdvert : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                 ALTER VIEW ui.ListableAdvertsView AS
                SELECT 
	                adverts.Id,
	                adverts.Name,
	                adverts.PriceCzk,
	                adverts.AdvertType,
                    adverts.EstateId,
	                conversions.SourceModule as Source,
	                adverts.Created,
	                adverts.Modified
                FROM core.Adverts adverts
                JOIN core.Conversions conversions ON conversions.TargetAdvertId = adverts.Id
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                 ALTER VIEW ui.ListableAdvertsView AS
                SELECT 
	                adverts.Id,
	                adverts.Name,
	                adverts.PriceCzk,
	                adverts.AdvertType,
                    adverts.EstateId,
	                estates.EstateType,
	                estates.Longitude,
	                estates.Latitude,
	                conversions.SourceModule as Source,
	                adverts.Created,
	                adverts.Modified
                FROM core.Adverts adverts
                JOIN core.Estates estates ON adverts.EstateId = estates.Id
                JOIN core.Conversions conversions ON conversions.TargetAdvertId = adverts.Id
            ");
        }
    }
}
