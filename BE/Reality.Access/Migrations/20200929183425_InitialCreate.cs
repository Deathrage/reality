﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "bezreal");

            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.EnsureSchema(
                name: "core");

            migrationBuilder.EnsureSchema(
                name: "sreality");

            migrationBuilder.CreateTable(
                name: "Adverts",
                schema: "bezreal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExternalId = table.Column<long>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Archived = table.Column<bool>(nullable: false),
                    EstateType = table.Column<int>(nullable: false),
                    OfferType = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(type: "decimal(14,2)", nullable: false),
                    Currency = table.Column<int>(nullable: false),
                    Charges = table.Column<decimal>(type: "decimal(14,2)", nullable: true),
                    Deposit = table.Column<decimal>(type: "decimal(14,2)", nullable: true),
                    Surface = table.Column<int>(nullable: true),
                    SurfaceLand = table.Column<int>(nullable: true),
                    SurfaceUsable = table.Column<int>(nullable: true),
                    Disposition = table.Column<int>(nullable: true),
                    Construction = table.Column<int>(nullable: true),
                    Condition = table.Column<int>(nullable: true),
                    LandType = table.Column<int>(nullable: true),
                    Equipped = table.Column<int>(nullable: true),
                    Ownership = table.Column<int>(nullable: true),
                    Floor = table.Column<int>(nullable: true),
                    Etage = table.Column<int>(nullable: true),
                    Balcony = table.Column<bool>(nullable: false),
                    Penb = table.Column<int>(nullable: true),
                    Mirador = table.Column<bool>(nullable: false),
                    Terrace = table.Column<bool>(nullable: false),
                    Garage = table.Column<bool>(nullable: false),
                    NewBuilding = table.Column<bool>(nullable: false),
                    Lift = table.Column<bool>(nullable: false),
                    Heating = table.Column<int>(nullable: true),
                    FrontGarden = table.Column<int>(nullable: true),
                    Reconstruction = table.Column<int>(nullable: true),
                    Water = table.Column<int>(nullable: true),
                    Sewage = table.Column<int>(nullable: true),
                    Locality = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true),
                    Latitude = table.Column<decimal>(type: "decimal(9,6)", nullable: true),
                    Longitude = table.Column<decimal>(type: "decimal(9,6)", nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adverts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                schema: "core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PerformerRunLogs",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BeganAt = table.Column<DateTime>(nullable: false),
                    Source = table.Column<string>(nullable: true),
                    Success = table.Column<bool>(nullable: false),
                    AddedRecords = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PerformerRunLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Adverts",
                schema: "sreality",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExternalId = table.Column<long>(nullable: false),
                    AdvertType = table.Column<int>(nullable: false),
                    MainCategory = table.Column<int>(nullable: false),
                    SubCategory = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    HasPanorama = table.Column<bool>(nullable: false),
                    IsNew = table.Column<bool>(nullable: false),
                    PriceCzk = table.Column<decimal>(type: "decimal(14,2)", nullable: false),
                    PriceUnit = table.Column<string>(nullable: true),
                    Latitude = table.Column<decimal>(type: "decimal(9,6)", nullable: true),
                    Longitude = table.Column<decimal>(type: "decimal(9,6)", nullable: true),
                    Locality = table.Column<string>(nullable: true),
                    SeoLocality = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adverts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Municipalities",
                schema: "core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    CountryId = table.Column<int>(nullable: false),
                    CountrySubdivision = table.Column<string>(nullable: true),
                    CountrySecondarySubdivision = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Municipalities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Municipalities_Countries_CountryId",
                        column: x => x.CountryId,
                        principalSchema: "core",
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MajorAdvertLabels",
                schema: "sreality",
                columns: table => new
                {
                    AdvertId = table.Column<int>(nullable: false),
                    Label = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MajorAdvertLabels", x => new { x.AdvertId, x.Label });
                    table.ForeignKey(
                        name: "FK_MajorAdvertLabels_Adverts_AdvertId",
                        column: x => x.AdvertId,
                        principalSchema: "sreality",
                        principalTable: "Adverts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MinorAdvertLabels",
                schema: "sreality",
                columns: table => new
                {
                    AdvertId = table.Column<int>(nullable: false),
                    Label = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MinorAdvertLabels", x => new { x.AdvertId, x.Label });
                    table.ForeignKey(
                        name: "FK_MinorAdvertLabels_Adverts_AdvertId",
                        column: x => x.AdvertId,
                        principalSchema: "sreality",
                        principalTable: "Adverts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Streets",
                schema: "core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    MunicipalityId = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Streets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Streets_Municipalities_MunicipalityId",
                        column: x => x.MunicipalityId,
                        principalSchema: "core",
                        principalTable: "Municipalities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                schema: "core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StreetId = table.Column<int>(nullable: false),
                    StreetNumber = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Streets_StreetId",
                        column: x => x.StreetId,
                        principalSchema: "core",
                        principalTable: "Streets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Estates",
                schema: "core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    AddressId = table.Column<int>(nullable: false),
                    EstateType = table.Column<string>(nullable: false),
                    Ownership = table.Column<string>(nullable: true),
                    Longitude = table.Column<decimal>(type: "decimal(9,6)", nullable: true),
                    Latitude = table.Column<decimal>(type: "decimal(9,6)", nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    UsableSurface = table.Column<int>(nullable: true),
                    Furnishment = table.Column<string>(nullable: true),
                    Construction = table.Column<string>(nullable: true),
                    Disposition = table.Column<string>(nullable: true),
                    Floor = table.Column<int>(nullable: true),
                    Balcony = table.Column<bool>(nullable: true),
                    Terrace = table.Column<bool>(nullable: true),
                    Garage = table.Column<bool>(nullable: true),
                    Parking = table.Column<bool>(nullable: true),
                    Lift = table.Column<bool>(nullable: true),
                    LowEnergy = table.Column<bool>(nullable: true),
                    NewBuilding = table.Column<bool>(nullable: true),
                    LandSurface = table.Column<int>(nullable: true),
                    House_UsableSurface = table.Column<int>(nullable: true),
                    House_Furnishment = table.Column<string>(nullable: true),
                    House_Balcony = table.Column<bool>(nullable: true),
                    House_Terrace = table.Column<bool>(nullable: true),
                    House_Garage = table.Column<bool>(nullable: true),
                    House_LowEnergy = table.Column<bool>(nullable: true),
                    House_NewBuilding = table.Column<bool>(nullable: true),
                    LandType = table.Column<string>(nullable: true),
                    LandSurface1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Estates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Estates_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalSchema: "core",
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Adverts",
                schema: "core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    EstateId = table.Column<int>(nullable: false),
                    AdvertType = table.Column<string>(nullable: false),
                    PriceCzk = table.Column<decimal>(type: "decimal(14,2)", nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adverts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Adverts_Estates_EstateId",
                        column: x => x.EstateId,
                        principalSchema: "core",
                        principalTable: "Estates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Conversions",
                schema: "core",
                columns: table => new
                {
                    TargetAdvertId = table.Column<int>(nullable: false),
                    SourceAdvertId = table.Column<int>(nullable: false),
                    SourceModule = table.Column<string>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conversions", x => new { x.TargetAdvertId, x.SourceAdvertId, x.SourceModule });
                    table.ForeignKey(
                        name: "FK_Conversions_Adverts_TargetAdvertId",
                        column: x => x.TargetAdvertId,
                        principalSchema: "core",
                        principalTable: "Adverts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_StreetId_StreetNumber",
                schema: "core",
                table: "Addresses",
                columns: new[] { "StreetId", "StreetNumber" },
                unique: true,
                filter: "[StreetNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Adverts_EstateId",
                schema: "core",
                table: "Adverts",
                column: "EstateId");

            migrationBuilder.CreateIndex(
                name: "IX_Countries_Code",
                schema: "core",
                table: "Countries",
                column: "Code",
                unique: true,
                filter: "[Code] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Estates_AddressId",
                schema: "core",
                table: "Estates",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Municipalities_CountryId",
                schema: "core",
                table: "Municipalities",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Municipalities_Name_CountrySubdivision_CountrySecondarySubdivision_CountryId",
                schema: "core",
                table: "Municipalities",
                columns: new[] { "Name", "CountrySubdivision", "CountrySecondarySubdivision", "CountryId" },
                unique: true,
                filter: "[Name] IS NOT NULL AND [CountrySubdivision] IS NOT NULL AND [CountrySecondarySubdivision] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Streets_MunicipalityId",
                schema: "core",
                table: "Streets",
                column: "MunicipalityId");

            migrationBuilder.CreateIndex(
                name: "IX_Streets_Name_MunicipalityId",
                schema: "core",
                table: "Streets",
                columns: new[] { "Name", "MunicipalityId" },
                unique: true,
                filter: "[Name] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Adverts",
                schema: "bezreal");

            migrationBuilder.DropTable(
                name: "Conversions",
                schema: "core");

            migrationBuilder.DropTable(
                name: "PerformerRunLogs",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "MajorAdvertLabels",
                schema: "sreality");

            migrationBuilder.DropTable(
                name: "MinorAdvertLabels",
                schema: "sreality");

            migrationBuilder.DropTable(
                name: "Adverts",
                schema: "core");

            migrationBuilder.DropTable(
                name: "Adverts",
                schema: "sreality");

            migrationBuilder.DropTable(
                name: "Estates",
                schema: "core");

            migrationBuilder.DropTable(
                name: "Addresses",
                schema: "core");

            migrationBuilder.DropTable(
                name: "Streets",
                schema: "core");

            migrationBuilder.DropTable(
                name: "Municipalities",
                schema: "core");

            migrationBuilder.DropTable(
                name: "Countries",
                schema: "core");
        }
    }
}
