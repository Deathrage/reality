﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reality.Access.Migrations
{
    public partial class AddEstateInBoundaries2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema("ui");

            migrationBuilder.Sql(@"
                CREATE FUNCTION ui.GetEstatesInBoundaryFunc(
	                    @Boundaries VARCHAR(MAX) -- all boundaries defining the shape in format: lon1 lat1, lon2 lat2 ..	
                ) RETURNS @Result TABLE (
                    EstateId INT
                ) AS
                BEGIN
	                 DECLARE @BoundingPoly geography = geography::STPolyFromText('POLYGON((' + @Boundaries + '))', 4326);
	                 IF (@BoundingPoly.EnvelopeAngle() > 90)
		                SET @BoundingPoly = @BoundingPoly.ReorientObject()

                    INSERT INTO @Result
                        SELECT	estates.Id as EstateId
                    FROM core.Estates estates
                    WHERE estates.Latitude IS NOT NULL and estates.Longitude IS NOT NULL
	                AND @BoundingPoly.STContains(geography::Point(estates.Latitude, estates.Longitude, 4326)) = 1

                    RETURN;
                END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION ui.GetEstatesInBoundaryFunc");
        }
    }
}
