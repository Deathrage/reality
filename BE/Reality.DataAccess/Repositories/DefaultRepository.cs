﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Reality.Common.DataAccess.Abstractions.Interfaces;

namespace Reality.DataAccess.Repositories
{
    internal class DefaultRepository<TEntity>: IRepository<TEntity> 
        where TEntity: class
    {
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);
        private readonly RealityDbContext _realityDbContext;

        private async Task DoActionWithSemaphore(Func<Task> action)
        {
            await _semaphore.WaitAsync();
            await action();
            _semaphore.Release();
        }

        protected DbSet<TEntity> GetDbSet()
        {
            if (_realityDbContext.Model.GetEntityTypes().Any(item => item.ClrType == typeof(TEntity)))
                return _realityDbContext.Set<TEntity>();

            throw new Exception(
                $"DbContext {nameof(RealityDbContext)} is not configured for entity type {typeof(TEntity).FullName}");
        }

        public DefaultRepository(RealityDbContext realityDbContext)
        {
            _realityDbContext = realityDbContext ?? throw new ArgumentNullException(nameof(realityDbContext));
        }

        public int TrackedSpecifiedEntities => _realityDbContext.ChangeTracker.Entries<TEntity>().Count();
        public int TrackedEntities => _realityDbContext.ChangeTracker.Entries().Count();

        public IQueryable<TEntity> AsQueryable() => GetDbSet().AsQueryable();

        public Task AddAsync(IEnumerable<TEntity> entries)
            => DoActionWithSemaphore(() => GetDbSet().AddRangeAsync(entries));

        public void Update(IEnumerable<TEntity> entries)
            => GetDbSet().UpdateRange(entries);

        public void Remove(IEnumerable<TEntity> entries)
            => GetDbSet().RemoveRange(entries);

        public void Attach(IEnumerable<TEntity> entries)
            => GetDbSet().AttachRange(entries);

        public Task SaveAsync() => DoActionWithSemaphore(() => _realityDbContext.SaveChangesAsync());

        public IRepository<TEntity> Clone()
            => new DefaultRepository<TEntity>(_realityDbContext.Clone());

        object ICloneable.Clone()
            => Clone();
    }
}