﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Extensions;
using Reality.Common.Model;

namespace Reality.DataAccess.Repositories
{
    internal class DefaultEntityRepository<TEntity> : IEntityRepository<TEntity>
        where TEntity : class, IEntity
    {
        private readonly IRepository<TEntity> _repository;

        public int TrackedSpecifiedEntities => _repository.TrackedSpecifiedEntities;
        public int TrackedEntities => _repository.TrackedEntities;

        public DefaultEntityRepository(IRepository<TEntity> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<IReadOnlyList<TEntity>> GetByIdsAsync(IReadOnlyList<int> ids,
            CancellationToken token = default, bool track = true)
        {
            IQueryable<TEntity> query = AsQueryable();
            if (!track)
                query = query.AsNoTracking();

            ImmutableArray<TEntity> results =
                await query.Where(item => ids.Contains(item.Id)).ToImmutableArrayAsync(token);

            IDictionary<int, TEntity> dict = results.ToImmutableDictionary(x => x.Id);

            // reorder results to match order of ids
            return ids.Select(id => dict.TryGetValue(id, out TEntity result) ? result : default).ToImmutableArray();
        }

        public IQueryable<TEntity> AsQueryable()
            => _repository.AsQueryable();

        public Task AddAsync(IEnumerable<TEntity> entries)
            => _repository.AddAsync(entries);

        public void Update(IEnumerable<TEntity> entries)
            => _repository.Update(entries);

        public void Remove(IEnumerable<TEntity> entries)
            => _repository.Remove(entries);

        public void Attach(IEnumerable<TEntity> entries)
            => _repository.Attach(entries);

        public Task SaveAsync()
            => _repository.SaveAsync();

        public IEntityRepository<TEntity> Clone()
            => new DefaultEntityRepository<TEntity>(_repository.Clone());

        IRepository<TEntity> IRepository<TEntity>.Clone()
            => Clone();

        object ICloneable.Clone()
            => Clone();
    }
}