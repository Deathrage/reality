﻿using System;
using Microsoft.EntityFrameworkCore;
using Reality.BezRealitky.DataAccess;
using Reality.Common.DataAccess.Abstranctions;
using Reality.Common.DataAccess.TypeConfiguration;
using Reality.Core.DataAccess;
using Reality.Idnes.DataAccess;
using Reality.SReality.DataAccess;
using Reality.UI.DataAccess.Extensions;

namespace Reality.DataAccess
{
    public class RealityDbContext: DbContextWithManipulationMeta, ICloneable
    {
        private readonly DbContextOptions _options;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ConfigureCommonModel()
                .ConfigureBezRealitkyModel()
                .ConfigureCoreModel()
                .ConfigureSRealityModel()
                .ConfigureIdnesModel()
                .ConfigureUIModel();
        }

        public RealityDbContext(DbContextOptions options) :
            base(options)
        {
            _options = options;
        }

        public RealityDbContext Clone()
            => new RealityDbContext(_options);

        object ICloneable.Clone()
            => Clone();
    }
}
