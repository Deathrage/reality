﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.DataAccess.Providers;
using Reality.DataAccess.Repositories;
using Reality.UI.DataAccess.Extensions;

namespace Reality.DataAccess
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSqlServerDbContext<TContext>(this IServiceCollection source,
            string connectionString, string migrationAssembly) where TContext : DbContext
            => source.AddDbContext<TContext>(
                opts => opts.UseSqlServer(connectionString, b => b.MigrationsAssembly(migrationAssembly).UseNetTopologySuite()));

        public static IServiceCollection AddDataProviders(this IServiceCollection source, string connectionString, string migrationAssembly)
            => source.AddSqlServerDbContext<RealityDbContext>(connectionString, migrationAssembly)
                .AddTransient(typeof(IRepository<>), typeof(DefaultRepository<>))
                .AddTransient(typeof(IEntityRepository<>), typeof(DefaultEntityRepository<>))
                .AddTransient<IRawCommandProvider, DefaultRawCommandProvider>()
                .AddUIDataProvider();
    }
}