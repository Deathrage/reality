﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Reality.Common.DataAccess.Abstractions;
using Reality.Common.DataAccess.Abstractions.Interfaces;

namespace Reality.DataAccess.Providers
{
    internal class DefaultRawCommandProvider: IRawCommandProvider
    {
        private readonly RealityDbContext _realityDbContext;

        public DefaultRawCommandProvider(RealityDbContext realityDbContext)
        {
            _realityDbContext = realityDbContext ?? throw new ArgumentNullException(nameof(realityDbContext));
        }

        public IQueryable<TOutput> FromString<TOutput>(string command)
            where TOutput : class
            => _realityDbContext.Set<TOutput>().FromSqlRaw(command);
    }
}