using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using Reality.Common.AzureFunctions.Abstractions;
using Reality.Common.Logging;

namespace Reality.Idnes.AzureFunctions
{
    public class Idnes_Timer_Crawl : ScrapperFunction
    {
        private readonly IIdnesScrapper _idnesScrapper;

        public Idnes_Timer_Crawl(IActivityLogger loggingProvider, IIdnesScrapper idnesScrapper) : base(loggingProvider)
        {
            _idnesScrapper = idnesScrapper ?? throw new ArgumentNullException(nameof(idnesScrapper));
        }

        [Disable("TODO: Opravit")]
        [FunctionName(nameof(Idnes_Timer_Crawl))]
        public async Task RunTask([TimerTrigger("0 0 1 * * *", RunOnStartup = false)]
            TimerInfo myTimer, ILogger log, [DurableClient] IDurableOrchestrationClient client)
        {
            IEnumerable<int> newIds =
                await RunPerformerFunction<IIdnesScrapper, Idnes_Timer_Crawl>(_idnesScrapper, log);

            await client.StartNewAsync(nameof(Idnes_Orchestration_ConvertNewAdverts),
                new Idnes_Orchestration_ConvertNewAdverts.Input
                {
                    NewIds = newIds.ToImmutableArray()
                });
        }
    }
}