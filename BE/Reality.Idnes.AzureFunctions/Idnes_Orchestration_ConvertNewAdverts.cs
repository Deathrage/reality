﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Reality.Access.Functions.Core;
using Reality.Common;
using Reality.Common.AzureFunctions.Extensions;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Extensions;
using Reality.Core.AdvertConverter_;
using IdnesAdvert = Reality.Idnes.Model.Advert;

namespace Reality.Idnes.AzureFunctions
{
    public class Idnes_Orchestration_ConvertNewAdverts
    {
        public class Input
        {
            public IReadOnlyList<int> NewIds { get; set; }
        }

        private readonly IRepository<IdnesAdvert> _bezRealAdvertRepository;
        private readonly IAdvertConverter<IdnesAdvert> _converterSource;

        public Idnes_Orchestration_ConvertNewAdverts(IRepository<IdnesAdvert> bezRealAdvertRepository,
            IAdvertConverter<IdnesAdvert> converterSource)
        {
            _bezRealAdvertRepository = bezRealAdvertRepository ??
                                       throw new ArgumentNullException(nameof(bezRealAdvertRepository));
            _converterSource = converterSource ?? throw new ArgumentNullException(nameof(converterSource));
        }

        [FunctionName(nameof(Idnes_Orchestration_ConvertNewAdverts))]
        public async Task RunAsync([OrchestrationTrigger] IDurableOrchestrationContext context)
        {
            DateTime beganAt = context.CurrentUtcDateTime;

            try
            {
                Input input = context.GetInput<Input>();
                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                IReadOnlyList<ConversionItem> items =
                    await context.CallActivityAsync<IReadOnlyList<ConversionItem>>(
                        nameof(Idnes_Orchestration_ConvertNewAdverts) + "_" +
                        nameof(CreateConversionItemsFromIds),
                        input.NewIds ?? ReadOnlyList.Empty<int>());

                await context.CallSubOrchestratorAsync(nameof(Core_Orchestration_ProcessConversionItems),
                    new Core_Orchestration_ProcessConversionItems.Input
                    {
                        ConversionItems = items
                    });
            }
            catch (Exception ex)
            {
                await context.CallLoggingActivity<Idnes_Orchestration_ConvertNewAdverts>(beganAt,
                    ex);
                throw;
            }
        }

        [FunctionName(nameof(Idnes_Orchestration_ConvertNewAdverts) + "_" + nameof(CreateConversionItemsFromIds))]
        public async Task<IReadOnlyList<ConversionItem>> CreateConversionItemsFromIds([ActivityTrigger] IDurableActivityContext context)
        {
            IReadOnlyList<int> input = context.GetInput<IReadOnlyList<int>>();
            if (input == default)
                throw new ArgumentNullException(nameof(input));

            ImmutableArray<IdnesAdvert> adverts = await _bezRealAdvertRepository.AsQueryable()
                .Where(item =>
                    input.Contains(item.Id))
                .ToImmutableArrayAsync();
            
            IReadOnlyList<ConversionItem> items = _converterSource.Convert(adverts);

            return items;
        }
    }
}