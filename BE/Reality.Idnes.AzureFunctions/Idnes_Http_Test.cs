using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;

namespace Reality.Idnes.AzureFunctions
{
    public class Idnes_Http_Test
    {
        private readonly IIdnesScrapper _idnesScrapper;

        public Idnes_Http_Test(IIdnesScrapper idnesScrapper)
        {
            _idnesScrapper = idnesScrapper ?? throw new ArgumentNullException(nameof(idnesScrapper));
        }

        [FunctionName(nameof(Idnes_Http_Test))]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "idnes/test-run")] HttpRequest req,
            ILogger log)
        {
            IEnumerable<int> newIds = await _idnesScrapper.RunAsync();
            return new ObjectResult(string.Join(", ", newIds));
        }
    }
}
