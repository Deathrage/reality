﻿using Microsoft.Extensions.DependencyInjection;
using Reality.Idnes.Extensions;

namespace Reality.Idnes.AzureFunctions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddIdnesAzureFunctions(this IServiceCollection source)
            => source.AddIdnes();
    }
}