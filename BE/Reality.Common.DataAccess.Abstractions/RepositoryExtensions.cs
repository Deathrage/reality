﻿using System.Threading.Tasks;
using Reality.Common.DataAccess.Abstractions.Interfaces;

namespace Reality.Common.DataAccess.Abstractions
{
    public static class RepositoryExtensions
    {
        public static Task AddAsync<TEntity>(this IRepository<TEntity> source, TEntity entity)
            => source.AddAsync(new [] {entity});

        public static void Remove<TEntity>(this IRepository<TEntity> source, TEntity entity)
            => source.Remove(new [] {entity});

        public static void Update<TEntity>(this IRepository<TEntity> source, TEntity entity)
            => source.Update(new [] {entity});

        public static void Attach<TEntity>(this IRepository<TEntity> source, TEntity entity)
            => source.Attach(new [] { entity });
    }
}