﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Reality.Common.DataAccess.Abstractions.Interfaces;

namespace Reality.Common.Extensions.Registration
{
    internal readonly struct DataProviderRegistrationBuilder : IDataProviderRegistrationBuilder
    {
        private readonly Type _implementationType;
        private readonly IServiceCollection _serviceCollection;
        private readonly ServiceLifetime _lifetime;

        private Func<Type, Type, IServiceCollection> Register => _lifetime switch
        {
            ServiceLifetime.Scoped => _serviceCollection.AddScoped,
            ServiceLifetime.Transient => _serviceCollection.AddTransient,
            ServiceLifetime.Singleton => _serviceCollection.AddSingleton,
            _ => throw new ArgumentOutOfRangeException()
        };

        public DataProviderRegistrationBuilder(Type implementationType, IServiceCollection serviceCollection,
            ServiceLifetime lifetime)
        {
            _implementationType = implementationType ?? throw new ArgumentNullException(nameof(implementationType));
            _serviceCollection = serviceCollection ?? throw new ArgumentNullException(nameof(serviceCollection));
            _lifetime = lifetime;
        }

        public IServiceCollection As<TReturnValue, TParameter>() where TReturnValue : class
            => Register(typeof(IDataProvider<TReturnValue, TParameter>), _implementationType);

        public IServiceCollection As<TReturnValue>() where TReturnValue : class
            => Register(typeof(IDataProvider<TReturnValue>), _implementationType);
    }
}