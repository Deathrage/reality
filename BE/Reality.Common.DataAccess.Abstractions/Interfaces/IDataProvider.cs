﻿using System.Linq;

namespace Reality.Common.DataAccess.Abstractions.Interfaces
{
    public interface IDataProvider<out TReturnValue>
        where TReturnValue: class
    {
        IQueryable<TReturnValue> AsQueryable();
    }

    public interface IDataProvider<out TReturnValue, in TParameter>
        where TReturnValue: class
    {
        IQueryable<TReturnValue> AsQueryable(TParameter parameter);
    }
}