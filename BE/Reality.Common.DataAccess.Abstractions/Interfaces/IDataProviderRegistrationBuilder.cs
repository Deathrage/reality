﻿using Microsoft.Extensions.DependencyInjection;

namespace Reality.Common.Extensions.Registration
{
    public interface IDataProviderRegistrationBuilder
    {
        IServiceCollection As<TReturnValue, TParameter>() where TReturnValue : class;

        IServiceCollection As<TReturnValue>() where TReturnValue : class;
    }
}