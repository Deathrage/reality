﻿using System.Linq;

namespace Reality.Common.DataAccess.Abstractions.Interfaces
{
    public interface IRawCommandProvider
    {
        IQueryable<TOutput> FromString<TOutput>(string command) where TOutput : class;
    }
}