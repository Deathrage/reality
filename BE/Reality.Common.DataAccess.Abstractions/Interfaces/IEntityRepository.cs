﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Reality.Common.Model;

namespace Reality.Common.DataAccess.Abstractions.Interfaces
{
    public interface IEntityRepository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        Task<IReadOnlyList<TEntity>> GetByIdsAsync(IReadOnlyList<int> ids, CancellationToken token = default,
            bool track = true);

        new IEntityRepository<TEntity> Clone();
    }
}