﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reality.Common.DataAccess.Abstractions.Interfaces
{
    public interface IRepository<TEntity>: ICloneable
    {
        IQueryable<TEntity> AsQueryable();

        Task AddAsync(IEnumerable<TEntity> entries);

        void Update(IEnumerable<TEntity> entries);

        void Remove(IEnumerable<TEntity> entries);

        void Attach(IEnumerable<TEntity> entries);

        /// <summary>
        /// Counts entities tracked by the repository
        /// </summary>
        int TrackedSpecifiedEntities { get; }

        /// <summary>
        /// Counts entities tracked by the repository including their dependencies and other entities shared on the same context
        /// </summary>
        int TrackedEntities { get; }

        Task SaveAsync();

        new IRepository<TEntity> Clone();
    }
}