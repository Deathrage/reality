﻿using Microsoft.Extensions.DependencyInjection;
using Reality.Common.Extensions.Registration;

namespace Reality.Common.DataAccess.Abstractions
{
    public static class ServiceCollectionExtensions
    {
        public static IDataProviderRegistrationBuilder AddDataProvider<TImplementation>(this IServiceCollection source,
            ServiceLifetime lifetime)
            => new DataProviderRegistrationBuilder(typeof(TImplementation), source, lifetime);
    }
}