﻿using System.Linq;
using System.Threading.Tasks;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Model;

namespace Reality.Common.DataAccess.Abstractions
{
    public static class EntityRepositoryExtensions
    {
        public static async Task<TEntity> GetByIdAsync<TEntity>(this IEntityRepository<TEntity> repository, int id)
            where TEntity : class, IEntity
            => (await repository.GetByIdsAsync(new[] {id})).SingleOrDefault();
    }
}