﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Common.DataAccess.Extensions;
using Reality.Idnes.Model;

namespace Reality.Idnes.DataAccess
{
    public class AdvertConfiguration : IEntityTypeConfiguration<Advert>
    {
        public void Configure(EntityTypeBuilder<Advert> b)
        {
            b.ToTable("Adverts", "idnes");
            b.ConfigureEntityKey();

            b.Property(p => p.PriceCzk).HasDecimalPriceColumnType();
            b.Property(p => p.Latitude).HasDecimalGeoCoordinateColumnType();
            b.Property(p => p.Longitude).HasDecimalGeoCoordinateColumnType();
            b.Property(p => p.Link).HasUriColumn();
        }
    }
}