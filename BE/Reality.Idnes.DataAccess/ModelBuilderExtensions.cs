﻿using Microsoft.EntityFrameworkCore;

namespace Reality.Idnes.DataAccess
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder ConfigureIdnesModel(this ModelBuilder builder) =>
            builder.ApplyConfiguration(new AdvertConfiguration());
    }
}