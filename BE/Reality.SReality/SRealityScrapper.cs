﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Reality.Common;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Extensions;
using Reality.SReality.Model;
using Reality.SReality.Model.Enums;
using Reality.SReality.Model.Generated;

namespace Reality.SReality
{
    public interface ISRealityScrapper: IScrapper
    {

    }

	internal class SRealityScrapper: ISRealityScrapper
	{
		private readonly SRealityCrawler _crawler;
		private readonly IMapper _mapper;
		private readonly IRepository<Advert> _advertRepository;
		private readonly ILogger<SRealityScrapper> _logger;

        public SRealityScrapper(SRealityCrawler crawler, IMapper mapper, IRepository<Advert> advertRepository,
            ILogger<SRealityScrapper> logger)
        {
            _crawler = crawler ?? throw new ArgumentNullException(nameof(crawler));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _advertRepository = advertRepository ?? throw new ArgumentNullException(nameof(advertRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<int>> RunAsync()
		{
			IEnumerable<Advert>[] partialsAddedRecords = await Task.WhenAll(
				GetAndSaveAdverts(AdvertType.Rent, AdvertCategory.Flat),
				GetAndSaveAdverts(AdvertType.Sell, AdvertCategory.Flat),
				GetAndSaveAdverts(AdvertType.Rent, AdvertCategory.House),
				GetAndSaveAdverts(AdvertType.Sell, AdvertCategory.House),
				GetAndSaveAdverts(AdvertType.Rent, AdvertCategory.Land),
				GetAndSaveAdverts(AdvertType.Sell, AdvertCategory.Land),
				GetAndSaveAdverts(AdvertType.Rent, AdvertCategory.Commerce),
				GetAndSaveAdverts(AdvertType.Sell, AdvertCategory.Commerce),
				GetAndSaveAdverts(AdvertType.Rent, AdvertCategory.Misc),
				GetAndSaveAdverts(AdvertType.Sell, AdvertCategory.Misc)
			);
			
			_logger.LogInformation($"Writing {_advertRepository.TrackedEntities} records to the DB.");
            ImmutableArray<Advert> incomingAdverts = partialsAddedRecords.SelectMany(x => x).ToImmutableArray();
            ImmutableArray<long> externalIds = incomingAdverts.Select(item => item.ExternalId).ToImmutableArray();


            ImmutableArray<long> externalIdsInDb = await _advertRepository.AsQueryable()
                .Where(row => externalIds.Contains(row.ExternalId))
                .Select(row => row.ExternalId).ToImmutableArrayAsync();

            ImmutableArray<Advert> finalAdverts = incomingAdverts
                .Where(advert => !externalIdsInDb.Contains(advert.ExternalId)).ToImmutableArray();

            await _advertRepository.AddAsync(finalAdverts);
			await _advertRepository.SaveAsync();

            return finalAdverts.Select(item => item.Id);
        }

		private async Task<IEnumerable<Advert>> GetAndSaveAdverts(AdvertType advertType, AdvertCategory advertCategory)
		{
			List<Advert> foundAdverts = new List<Advert>();

			await foreach (IEnumerable<Advert> adverts in GetAdvertsAsync(advertType, advertCategory))
			{
                foundAdverts.AddRange(adverts);
			}

			return foundAdverts;
		}


		private async IAsyncEnumerable<IEnumerable<Advert>> GetAdvertsAsync(AdvertType advertType,
			AdvertCategory advertCategory)
		{
			_logger.LogInformation($"Began work on {advertType} {advertCategory}.");

			await foreach (IEnumerable<Estate> estates in _crawler.CrawlAsync(advertType, advertCategory,
				AdvertAge.Day, 100, 1000))
			{
				Estate[] foundEstatesEstates = estates.ToArray();

				_logger.LogInformation(
					$"While on {advertType} {advertCategory} recovered {foundEstatesEstates.Length} records.");

				IEnumerable<Advert> mappedAdverts = _mapper.Map<IEnumerable<Advert>>(estates);

				yield return mappedAdverts;
			}

			_logger.LogInformation($"Finished work on {advertType} {advertCategory}.");
		}
	}
}