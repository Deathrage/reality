﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Reality.SReality.Model.Generated;

namespace Reality.SReality.Mapping
{
	internal class SRealityAdvertEstateLinkConverter: IValueConverter<Estate, Uri>
	{
		private static readonly IDictionary<int, string> _advertSubTypeMap = new Dictionary<int, string>()
		{
			// byt
			{2, "1+kk"},
			{3, "1+1"},
			{4, "2+kk"},
			{5, "2+1"},
			{6, "3+kk"},
			{7, "3+1"},
			{8, "4+kk"},
			{9, "4+1"},
			{10, "5+kk"},
			{11, "5+1"},
			{12, "6-a-vice"},
			{16, "atypicky"},
			{47, "pokoj"},
			// pozemek
			{18, "komercni"},
			{19, "bydleni"},
			{20, "pole"},
			{21, "les"},
			{22, "louka"},
			{23, "zahrada"},
			{24, "ostatni-pozemky"},
			{46, "rybnik"},
			{48, "sady-vinice"},
			// dum
			{33 ,"chata"},
			{35 ,"pamatka"},
			{37 ,"rodinny"},
			{39 ,"vila"},
			{40 ,"na-klic"},
			{43 ,"chalupa"},
			{44, "zemedelska-usedlost"},
			// komercni
			{25, "kancelare"},
			{26, "sklad"},
			{27, "vyrobni-prostor"},
			{28, "obchodni-prostor"},
			{29, "ubytovani"},
			{30, "restaurace"},
			{31, "zemedelsky"},
			{32, "ostatni-komercni-prostory"},
			{38, "cinzovni-dum"},
			// ostatni
			{34, "garaz"},
			{36, "jine-nemovitosti"},
			{50, "vinny-sklep"},
			{51, "pudni-prostor"},
			{52, "garazove-stani"},
			{53, "mobilni-domek"},
		};


		private static readonly IDictionary<int, string> _advertTypeMan = new Dictionary<int, string>()
		{
			{1, "prodej"},
			{2, "pronajem"}
		};

	
		private static readonly IDictionary<int, string> _advertCategory = new Dictionary<int, string>()
		{
			{1, "byt"},
			{2, "dum"},
			{3, "pozemek"},
			{4, "komercni"},
			{5, "ostatni"}
		};

		private static string Template(string type, string category, string subcategory, string location, string hashId)
			=> $"http://www.sreality.cz/detail/{type}/{category}/{subcategory}/{location}/{hashId}";
		
		public Uri Convert(Estate source, ResolutionContext context)
		{
			if (!_advertTypeMan.TryGetValue(source.seo.category_type_cb, out string type) ||
			    !_advertCategory.TryGetValue(source.seo.category_main_cb, out string category) ||
			    !_advertSubTypeMap.TryGetValue(source.seo.category_sub_cb, out string subcategory)) return null;

			string link = Template(type, category, subcategory, source.seo.locality, source.hash_id.ToString());
			return new Uri(link);
		}
	}
}