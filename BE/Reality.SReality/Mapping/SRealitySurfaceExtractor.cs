﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using Reality.SReality.Model.Enums;

namespace Reality.SReality.Mapping
{
    public class SRealitySurfaceExtractor
    {
        public (int? UsableSurface, int? LandSurface) Extract(AdvertCategory advertType, string name)
        {
            //Regex regex = new Regex(@"\d\s\d+\sm²", );

            ImmutableArray<string> bits = new Regex(@"(\s|[\u00A0])").Split(name).Select(x =>
            {
                if (new Regex(@"[\u00A0]").IsMatch(x))
                    return " ";
                return x;
            }).ToImmutableArray();

            List<int> foundIndexes = new List<int>();
            for (int i = 0; i < bits.Length; i++)
                if (bits[i].Contains("m²")) foundIndexes.Add(i);

            if (foundIndexes.Count == 0)
                throw new InvalidOperationException($"No extraction method for 0");

            if (foundIndexes.Count > 2)
                throw new InvalidOperationException($"No extraction method for more than 2");

            if (advertType != AdvertCategory.House && foundIndexes.Count > 1)
                throw new InvalidOperationException($"More than one start index is supported only by {AdvertCategory.House} (2).");
            
            if (advertType == AdvertCategory.House && foundIndexes.Count != 2)
                throw new InvalidOperationException($"{AdvertCategory.House} requires 2.");

            if (advertType == AdvertCategory.House)
                return (BuildFromIndex(bits, foundIndexes.First()), BuildFromIndex(bits, foundIndexes.Last()));

            if (advertType == AdvertCategory.Land)
                return (null, BuildFromIndex(bits, foundIndexes.Single()));

            return (BuildFromIndex(bits, foundIndexes.Single()), null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bits"></param>
        /// <param name="startIndex">Index of m² string</param>
        private int? BuildFromIndex(ImmutableArray<string> bits, int startIndex)
        {
            List<string> finalBits = new List<string>();
            for (int i = startIndex - 1; i > -1; i--)
            {
                string inspectedBit = bits[i];
                if (inspectedBit == " " || int.TryParse(inspectedBit, out int _))
                {
                    finalBits.Insert(0, inspectedBit);
                    continue;
                }

                if (int.TryParse(finalBits.LastOrDefault(), out int _))
                    finalBits.RemoveAt(0);

                break;
            }

            string finalString = string.Join("", finalBits.Where(x => x != " "));
            return int.TryParse(finalString, out int result) ? result : default;
        }
    }
}