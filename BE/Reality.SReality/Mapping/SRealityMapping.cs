﻿using System;
using System.Collections.Immutable;
using System.Linq;
using AutoMapper;
using Reality.Core.Models.Entities.Estate_;
using Reality.Core.Models.Entities.Estate_.Enums;
using Reality.SReality.Model;
using Reality.SReality.Model.Enums;
using Estate = Reality.SReality.Model.Generated.Estate;

namespace Reality.SReality.Mapping
{
	public class SRealityMapping : Profile
	{
		public SRealityMapping()
		{
			CreateMap<Estate, Advert>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.Created, opts => opts.Ignore())
                .ForMember(dst => dst.Modified, opts => opts.Ignore())
				.ForMember(dst => dst.ExternalId, opts => opts.MapFrom(src => src.hash_id))
				.ForMember(dst => dst.AdvertType, opts => opts.MapFrom(src => src.seo.category_type_cb))
				.ForMember(dst => dst.MainCategory, opts => opts.MapFrom(src => src.seo.category_main_cb))
				.ForMember(dst => dst.SubCategory, opts => opts.MapFrom(src => src.seo.category_sub_cb))
				.ForMember(dst => dst.SeoLocality, opts => opts.MapFrom(src => src.seo.locality))
				.ForMember(dst => dst.Title, opts => opts.MapFrom(src => src.name))
				.ForMember(dst => dst.MajorAdvertLabels,
					opts => opts.MapFrom(
						src => src.labelsAll.First()
							.Select(label => new MajorAdvertLabel() {Label = label})))
				.ForMember(dst => dst.MinorAdvertLabels,
					opts => opts.MapFrom(src =>
						src.labelsAll.Last().Select(label => new MinorAdvertLabel() {Label = label})))
				.ForMember(dst => dst.HasPanorama, opts => opts.MapFrom(src => src.has_panorama))
				.ForMember(dst => dst.IsNew, opts => opts.MapFrom(src => src.@new))
				.ForMember(dst => dst.PriceCzk, opts => opts.MapFrom(src => src.price_czk.value_raw))
				.ForMember(dst => dst.PriceUnit, opts => opts.MapFrom(src => src.price_czk.unit))
				.ForMember(dst => dst.Latitude, opts => opts.MapFrom(src => src.gps.lat))
				.ForMember(dst => dst.Longitude, opts => opts.MapFrom(src => src.gps.lon))
				.ForMember(dst => dst.Locality, opts => opts.MapFrom(src => src.locality))
				.ForMember(dst => dst.Link,
					opts => opts.ConvertUsing(new SRealityAdvertEstateLinkConverter(), src => src));

            #region Estates

            CreateMap<SReality.Model.Advert, Core.Models.Entities.Estate_.Estate>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.GeoPoint, opts => opts.Ignore())
                .ForMember(dst => dst.AddressId, opts => opts.Ignore())
                .ForMember(dst => dst.Address, opts => opts.Ignore())
                .ForMember(dst => dst.Adverts, opts => opts.Ignore())
                .ForMember(dst => dst.HasOwnAddress, opts => opts.Ignore())
                .ForMember(dst => dst.Name, opts => opts.MapFrom((src, dst) =>
                {
                    string objectTypeName = src.MainCategory switch
                    {
                        AdvertCategory.Flat => "Byt",
                        AdvertCategory.House => "Dům",
                        AdvertCategory.Land => "Pozemek",
                        AdvertCategory.Misc => src.SubCategory switch
                        {
                            34 => "Garáž",
                            52 => "Garážové stání",
                            _ => "Objekt"
                        },
                        AdvertCategory.Commerce => src.SubCategory switch
                        {
                            25 => "Kancelář",
                            29 => "Rekreační objekt",
                            30 => "Rekreační objekt",
                            27 => "Výrobní prostor",
                            31 => "Zemědělský prostor",
                            38 => "Činžovní dům",
                            _ => "Nebytový prostor"
                        },
                        _ => "Objekt"
                    };

                    (int? usableSurface, int? landSurface) = new SRealitySurfaceExtractor().Extract(src.MainCategory, src.Title);

                    return $"{objectTypeName} {usableSurface ?? landSurface} m2, {src.Locality}";
                }))
                .ForMember(dst => dst.EstateType, opts => opts.MapFrom((src, dst) =>
                {
                    return src.MainCategory switch
                    {
                        AdvertCategory.Flat => EstateType.Flat,
                        AdvertCategory.House => EstateType.House,
                        AdvertCategory.Commerce => EstateType.Business,
                        AdvertCategory.Land => EstateType.Land,
                        _ => EstateType.Miscellaneous
                    };
                }))
                .ForMember(dst => dst.Ownership, opts => opts.MapFrom((src, dst) =>
                {
                    ImmutableArray<string> majorLabels =
                        src.MajorAdvertLabels.Select(item => item.Label).Distinct().ToImmutableArray();

                    if (majorLabels.Contains("personal"))
                        return Ownership.Personal;
                    if (majorLabels.Contains("collective"))
                        return Ownership.Collective;
                    if (majorLabels.Contains("state"))
                        return Ownership.State;

                    return (Ownership?)null;
                }));

            CreateMap<SReality.Model.Advert, Land>(MemberList.Destination)
                .IncludeBase<SReality.Model.Advert, Core.Models.Entities.Estate_.Estate>()
                .ForMember(dst => dst.LandSurface, opts => opts.MapFrom((src, dst) =>
                {
                    (int? _, int? landSurface) = new SRealitySurfaceExtractor().Extract(src.MainCategory, src.Title);
                    return landSurface ?? 0;
                }))
                .ForMember(dst => dst.LandType, opts => opts.MapFrom((src, dst) =>
                {
                    return src.SubCategory switch
                    {
                        19 => LandType.Housing,
                        21 => LandType.Forest,
                        22 => LandType.Field,
                        20 => LandType.Field,
                        48 => LandType.Field,
                        23 => LandType.Garden,
                        46 => LandType.Pond,
                        18 => LandType.Commerce,
                        _ => default
                    };
                }));

            CreateMap<SReality.Model.Advert, House>(MemberList.Destination)
                .IncludeBase<SReality.Model.Advert, Core.Models.Entities.Estate_.Estate>()
                .ForMember(dst => dst.LandSurface, opts => opts.MapFrom((src, dst) =>
                {
                    (int? _, int? landSurface) = new SRealitySurfaceExtractor().Extract(src.MainCategory, src.Title);
                    return landSurface ?? 0;
                }))
                .ForMember(dst => dst.UsableSurface, opts => opts.MapFrom((src, dst) =>
                {
                    (int? usableSurface, int? _) = new SRealitySurfaceExtractor().Extract(src.MainCategory, src.Title);
                    return usableSurface ?? 0;
                }))
                .ForMember(dst => dst.Furnishment, opts => opts.MapFrom((src, dst) =>
                {
                    ImmutableArray<string> majorLabels =
                        src.MajorAdvertLabels.Select(item => item.Label).Distinct().ToImmutableArray();

                    if (majorLabels.Contains("not_furnished"))
                        return Furnishment.NotFurnished;
                    if (majorLabels.Contains("partly_furnished"))
                        return Furnishment.PartiallyFurnished;
                    if (majorLabels.Contains("furnished"))
                        return Furnishment.PartiallyFurnished;

                    return (Furnishment?) default;
                }))
                .ForMember(dst => dst.LowEnergy,
                    opts => opts.MapFrom(src =>
                        src.MajorAdvertLabels.Select(item => item.Label).Contains("low_energy")))
                .ForMember(dst => dst.Balcony,
                    opts => opts.MapFrom(src => src.MajorAdvertLabels.Select(item => item.Label).Contains("balcony")))
                .ForMember(dst => dst.Terrace,
                    opts => opts.MapFrom(src => src.MajorAdvertLabels.Select(item => item.Label).Contains("terrace")))
                .ForMember(dst => dst.Garage,
                    opts => opts.MapFrom(src => src.MajorAdvertLabels.Select(item => item.Label).Contains("garage")))
                .ForMember(dst => dst.NewBuilding,
                    opts => opts.MapFrom(src =>
                        src.MajorAdvertLabels.Select(item => item.Label).Contains("new_building")));

            CreateMap<SReality.Model.Advert, Flat>(MemberList.Destination)
                .IncludeBase<SReality.Model.Advert, Core.Models.Entities.Estate_.Estate>()
                .ForMember(dst => dst.Floor, opts => opts.MapFrom(_ => (int?)null))
                .ForMember(dst => dst.UsableSurface, opts => opts.MapFrom((src, dst) =>
                {
                    (int? usableSurface, int? _) = new SRealitySurfaceExtractor().Extract(src.MainCategory, src.Title);
                    return usableSurface ?? 0;
                }))
                .ForMember(dst => dst.Furnishment, opts => opts.MapFrom((src, dst) =>
                {
                    ImmutableArray<string> majorLabels =
                        src.MajorAdvertLabels.Select(item => item.Label).Distinct().ToImmutableArray();

                    if (majorLabels.Contains("not_furnished"))
                        return Furnishment.NotFurnished;
                    if (majorLabels.Contains("partly_furnished"))
                        return Furnishment.PartiallyFurnished;
                    if (majorLabels.Contains("furnished"))
                        return Furnishment.PartiallyFurnished;

                    return (Furnishment?) default;
                }))
                .ForMember(dst => dst.Construction, opts => opts.MapFrom((src, dst) =>
                {
                    ImmutableArray<string> majorLabels =
                        src.MajorAdvertLabels.Select(item => item.Label).Distinct().ToImmutableArray();

                    if (majorLabels.Contains("panel"))
                        return ConstructionMaterial.Panel;
                    if (majorLabels.Contains("wooden"))
                        return ConstructionMaterial.Wood;
                    if (majorLabels.Contains("brick"))
                        return ConstructionMaterial.Brick;

                    return (ConstructionMaterial?) null;
                }))
                .ForMember(dst => dst.Disposition, opts => opts.MapFrom((src, dst) =>
                {
                    return src.SubCategory switch
                    {
                        2 => FlatDisposition.Dispition1_KK,
                        47 => FlatDisposition.Dispition1_KK,
                        3 => FlatDisposition.Dispition1_1,

                        4 => FlatDisposition.Dispition2_KK,
                        5 => FlatDisposition.Dispition2_1,

                        6 => FlatDisposition.Dispition3_KK,
                        7 => FlatDisposition.Dispition3_1,

                        8 => FlatDisposition.Dispition4_KK,
                        9 => FlatDisposition.Dispition4_1,

                        10 => FlatDisposition.Dispition5_KK,
                        11 => FlatDisposition.Dispition5_1,

                        12 => FlatDisposition.Dispition6_OrMore,
                        16 => FlatDisposition.Other,
                        _ => default
                    };
                }))
                .ForMember(dst => dst.Parking,
                    opts => opts.MapFrom(src => src.MajorAdvertLabels.Select(x => x.Label).Contains("garage") || src.MajorAdvertLabels.Select(x => x.Label).Contains("parking_lots")))
                .ForMember(dst => dst.LowEnergy,
                    opts => opts.MapFrom(src => src.MajorAdvertLabels.Select(x => x.Label).Contains("low_energy")))
                .ForMember(dst => dst.Balcony,
                    opts => opts.MapFrom(src => src.MajorAdvertLabels.Select(item => item.Label).Contains("balcony")))
                .ForMember(dst => dst.Terrace,
                    opts => opts.MapFrom(src => src.MajorAdvertLabels.Select(item => item.Label).Contains("terrace")))
                .ForMember(dst => dst.Garage,
                    opts => opts.MapFrom(src => src.MajorAdvertLabels.Select(item => item.Label).Contains("garage")))
                .ForMember(dst => dst.Lift,
                    opts => opts.MapFrom(src => src.MajorAdvertLabels.Select(item => item.Label).Contains("elevator")))
                .ForMember(dst => dst.NewBuilding,
                    opts => opts.MapFrom(src =>
                        src.MajorAdvertLabels.Select(item => item.Label).Contains("new_building")));

            CreateMap<SReality.Model.Advert, Business>(MemberList.Destination)
                .IncludeBase<SReality.Model.Advert, Core.Models.Entities.Estate_.Estate>()
                .ForMember(dst => dst.BusinessType, opts => opts.MapFrom((src, dst) =>
                {
                    return src.SubCategory switch
                    {
                        25 => BusinessType.Office,
                        29 => BusinessType.Recreational,
                        30 => BusinessType.Recreational,
                        26 => BusinessType.Manufacturing,
                        27 => BusinessType.Manufacturing,
                        31 => BusinessType.Manufacturing,
                        _ => BusinessType.Other
                    };
                }))
                .ForMember(dst => dst.UsableSurface, opts => opts.MapFrom((src, dst) =>
                {
                    (int? usableSurface, int? _) = new SRealitySurfaceExtractor().Extract(src.MainCategory, src.Title);
                    return usableSurface ?? 0;
                }));

            #endregion

            #region Advert

            CreateMap<SReality.Model.Advert, Core.Models.Entities.Advert_.Advert>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.Conversion, opts => opts.Ignore())
                .ForMember(dst => dst.EstateId, opts => opts.Ignore())
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Title))
                .ForMember(dst => dst.PriceCzk, opts => opts.MapFrom(src => src.PriceCzk))
                .ForMember(dst => dst.AdvertType, opts => opts.MapFrom((src, dst) =>
                {
                    switch (src.AdvertType)
                    {
                        case AdvertType.Sell: return Core.Models.Entities.Advert_.AdvertType.Sell;
                        case AdvertType.Rent: return Core.Models.Entities.Advert_.AdvertType.Rent;
                        default:
                            throw new Exception(
                                $"Cannot map {src.AdvertType} to {nameof(Core.Models.Entities.Advert_.AdvertType)}");
                    }
                }))
                .ForMember(dst => dst.Estate, opts => opts.MapFrom((src, dst, prop, ctx) =>
                {
                    switch (src.MainCategory)
                    {
                        case AdvertCategory.House: return ctx.Mapper.Map<House>(src);
                        case AdvertCategory.Flat: return ctx.Mapper.Map<Flat>(src);
                        case AdvertCategory.Land: return ctx.Mapper.Map<Land>(src);
                        case AdvertCategory.Commerce: return ctx.Mapper.Map<Business>(src);
                        default:
                            return ctx.Mapper.Map<Core.Models.Entities.Estate_.Estate>(src);
                    }
                }))
                .ForMember(dst => dst.Conversion, opts => opts.Ignore());

            #endregion
        }
	}
}