﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Reality.Core.AdvertConverter_;
using Reality.Core.Models.Data;
using Reality.Core.Models.Entities.Advert_;

namespace Reality.SReality.Mapping
{
    public class SRealityConverter: IAdvertConverter<SReality.Model.Advert>
    {
        private readonly IMapper _mapper;

        public SRealityConverter(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public string ModuleName => "sreality";

        public IReadOnlyList<ConversionItem> Convert(
            IReadOnlyList<SReality.Model.Advert> adverts)
        {
            return adverts
                .Select(advert =>
                {
                    Conversion conversion = new Conversion() {SourceModule = ModuleName, SourceAdvertId = advert.Id};
                    Advert coreAdvert = _mapper.Map<Advert>(advert);

                    return new ConversionItem(conversion, coreAdvert, coreAdvert.Estate, default, default, default,
                        default);
                })
                .ToArray();
        }
    }
}