﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Reality.SReality.Model.Enums;
using Reality.SReality.Model.Generated;

namespace Reality.SReality
{
	public class SRealityCrawler
	{
		private static Uri _uri = new Uri("https://www.sreality.cz/api/cs/v2/estates");
		
		private readonly IHttpClientFactory _httpClient;

		private static string MakeUrl(AdvertType advertType, AdvertCategory advertCategory, AdvertAge estateAge,
			int pageSize, int page)
		{
			UriBuilder uriBuilder = new UriBuilder(_uri);

			NameValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
			query.Add("category_type_cb", ((int) advertType).ToString());
			query.Add("category_main_cb", ((int) advertCategory).ToString());
			query.Add("estate_age", ((int) estateAge).ToString());
			query.Add("per_page", pageSize.ToString());
			query.Add("page", page.ToString());
			query.Add("tms", DateTimeOffset.Now.ToUnixTimeSeconds().ToString());
			// Hard coded prague for now
			query.Add("locality_region_id", 10.ToString());
			// no auctions
			query.Add("no_auction", 1.ToString());

			uriBuilder.Query = query.ToString();

			return uriBuilder.ToString();
		}

        public SRealityCrawler(IHttpClientFactory httpClient)
        {
            _httpClient = httpClient;
        }

        public async IAsyncEnumerable<IEnumerable<Estate>> CrawlAsync(AdvertType advertType, AdvertCategory advertCategory, AdvertAge estateAge, int batchSize, int sleep)
		{
			if (batchSize < 1 || batchSize > 999)
				throw new ArgumentException($"Batch size cannot be {batchSize}. Choose number between 1 and 999.");

			int currentPage = 1;
			while (true)
			{
				string url = MakeUrl(advertType, advertCategory, estateAge, batchSize, currentPage);
				HttpResponseMessage response = await _httpClient.CreateClient().GetAsync(url);
				currentPage++;

				response.EnsureSuccessStatusCode();
				string body = await response.Content.ReadAsStringAsync();
				Root result = JsonConvert.DeserializeObject<Root>(body);

				List<Estate> estates = result._embedded.estates;
				if (estates.Count == 0) break;

				yield return estates;

				await Task.Delay(sleep);
			}
		}
	}
}