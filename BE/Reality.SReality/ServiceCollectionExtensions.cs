﻿using Microsoft.Extensions.DependencyInjection;
using Reality.Core.AdvertConverter_;
using Reality.SReality.Mapping;
using Reality.SReality.Model;

namespace Reality.SReality
{
	public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSReality(this IServiceCollection source) =>
            source.AddTransient<SRealityCrawler>()
                .AddTransient<ISRealityScrapper, SRealityScrapper>()
                .AddSingleton<SRealityAdvertEstateLinkConverter>()
                .AddTransient<IAdvertConverter<Advert>, SRealityConverter>();
    }
}