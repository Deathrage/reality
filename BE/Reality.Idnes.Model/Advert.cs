﻿using Reality.Common.Model;
using System;
using Reality.Common.Model.Mixins;

namespace Reality.Idnes.Model
{
    public class Advert: INamedEntity, IWithManipulationMeta
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ExternalId { get; set; }

        public AdvertType AdvertType { get; set; }
        public EstateType EstateType { get; set; }

        public string Locality { get; set; }

        public decimal? PriceCzk { get; set; }
        public string Construction { get; set; }
        public string State { get; set; }
        public string Ownership { get; set; }
        public int? UsableSurface { get; set; }
        public int? Floor { get; set; }

        public bool Balcony { get; set; }
        public bool Loggia { get; set; }
        public bool Terrace { get; set; }
        public bool Cellar { get; set; }
        public bool Garage { get; set; }
        public bool Parking { get; set; }
        public bool Lift { get; set; }
        public bool EasyAccess { get; set; }
        public bool Landline { get; set; }
        public bool CableTelevision { get; set; }
        public bool Internet { get; set; }

        public string Furnishment { get; set; }
        public string Penb { get; set; }
        public string Disposition { get; set; }

        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }

        public Uri Link { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
