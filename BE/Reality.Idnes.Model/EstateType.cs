﻿namespace Reality.Idnes.Model
{
    public enum EstateType
    {
        House,
        Flat,
        Land,
        Commerce,
        Misc
    }
}