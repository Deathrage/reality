﻿using System;
using AzureMapsToolkit.Search;

namespace Reality.Common.Geocoder.Results
{
	public class GeoCoordinatesResult
	{
		public decimal Latitude { get; }
		public decimal Longitude { get; }
		public AddressResult Address { get; }


		public GeoCoordinatesResult(CoordinateAbbreviated coordinates, AddressResult address)
		{
			if (coordinates == default)
				throw new ArgumentNullException(nameof(coordinates));

            Latitude = (decimal)coordinates.Lat;
			Longitude = (decimal)coordinates.Lon;

            Address = address ?? throw new ArgumentNullException(nameof(address));
        }

		public GeoCoordinatesResult(decimal latitude, decimal longitude, AddressResult address)
		{
			Latitude = latitude;
			Longitude = longitude;

            Address = address ?? throw new ArgumentNullException(nameof(address));
		}
	}
}