﻿using System;
using AzureMapsToolkit.Search;

namespace Reality.Common.Geocoder.Results
{
	public class AddressResult
	{
        public string Country { get; }
        public string CountryCode { get; }
        public string CountrySubdivision { get; }
        public string CountrySecondarySubdivision { get; }
        public string Municipality { get; }
        public string Street { get; }
        public string StreetNumber { get; }

        public AddressResult(SearchResultAddress addressResult)
        {
            if (addressResult == default)
                throw new ArgumentNullException(nameof(addressResult));

            Country = addressResult.Country;
            CountryCode = addressResult.CountryCode;
            CountrySubdivision = addressResult.CountrySubdivision;
            CountrySecondarySubdivision = addressResult.CountrySecondarySubdivision;
            Municipality = addressResult.Municipality;
            Street = addressResult.Street;
            StreetNumber = addressResult.StreetNumber;
        }	
	}
}