﻿using System.Threading.Tasks;
using Reality.Common.Geocoder.Results;

namespace Reality.Common.Geocoder
{
    public interface IGeocoder
    {
        Task<GeoCoordinatesResult> GetGeoCoordinatesAsync(string address, decimal? biasLongitude = null,
            decimal? biasLatitude = null);

        Task<AddressResult> GetAddressAsync(decimal longitude, decimal latitude);
    }
}