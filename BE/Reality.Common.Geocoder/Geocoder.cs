﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AzureMapsToolkit;
using AzureMapsToolkit.Common;
using AzureMapsToolkit.Search;
using Reality.Common.Geocoder.Results;

namespace Reality.Common.Geocoder
{
    internal class Geocoder: IGeocoder
	{
		private readonly IAzureMapsServices _azureMapsServices;

		public Geocoder(IAzureMapsServices azureMapsServices)
		{
			_azureMapsServices = azureMapsServices ?? throw new ArgumentNullException(nameof(azureMapsServices));
		}

        public async Task<AddressResult> GetAddressAsync(decimal longitude, decimal latitude)
        {
            SearchAddressReverseRequest request = new SearchAddressReverseRequest()
            {
                Query =
                    $"{latitude.ToString(CultureInfo.InvariantCulture)}, {longitude.ToString(CultureInfo.InvariantCulture)}",
                Limit = 1,
                Language = "cs-CZ"
            };

            Response<SearchAddressReverseResponse> response = await _azureMapsServices.GetSearchAddressReverse(request);
            if (response.Error != default)
                throw new AzureMapsException(response.Error);

            SearchAddressReverseResponse result = response.Result;
            if (result.Addresses.Length == 0)
                return default;
			
            SearchAddressReverseResult addressResult = result.Addresses.First();

			return new AddressResult(addressResult.Address);
        }

        public async Task<GeoCoordinatesResult> GetGeoCoordinatesAsync(string address, decimal? biasLongitude = null, decimal? biasLatitude = null)
		{
			SearchAddressRequest request = new SearchAddressRequest
			{
				Query = address,
				Limit = 1,
				Lon = (double?)biasLongitude,
				Lat = (double?)biasLatitude,
				Language = "cs-CZ"
			};

			Response<SearchAddressResponse> response = await _azureMapsServices.GetSearchAddress(request);
			if (response.Error != default)
				throw new AzureMapsException(response.Error);

			SearchAddressResponse result = response.Result;
			if (result.Results.Length == 0)
				return default;

			SearchAddressResult addressResult = result.Results.First();

			return new GeoCoordinatesResult(addressResult.Position, new AddressResult(addressResult.Address));
		}
	}
}