﻿using System;
using AzureMapsToolkit;
using Microsoft.Extensions.DependencyInjection;

namespace Reality.Common.Geocoder
{
	public static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddGeocoder(this IServiceCollection services, string azureMapsKey)
			=> services.AddTransient<IGeocoder>(_ =>
				new Geocoder(
					new AzureMapsServices(azureMapsKey ?? throw new ArgumentNullException(nameof(azureMapsKey)))));
	}
}