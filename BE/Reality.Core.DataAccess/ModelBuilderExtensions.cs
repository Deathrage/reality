﻿using Microsoft.EntityFrameworkCore;
using Reality.Core.DataAccess.Address_;
using Reality.Core.DataAccess.Estate_;

namespace Reality.Core.DataAccess
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder ConfigureCoreModel(this ModelBuilder builder)
            => builder.ApplyConfiguration(new AdvertConfiguration()).ApplyConfiguration(new EstateConfiguration())
                .ApplyConfiguration(new FlatConfiguration()).ApplyConfiguration(new HouseConfiguration())
                .ApplyConfiguration(new LandConfiguration()).ApplyConfiguration(new AddressConfiguration())
                .ApplyConfiguration(new CountryConfiguration()).ApplyConfiguration(new MunicipalityConfiguration())
                .ApplyConfiguration(new StreetConfiguration()).ApplyConfiguration(new ConversionConfiguration())
                .ApplyConfiguration(new AdvertEstateViewConfiguration())
                .ApplyConfiguration(new EstatesPerCountryViewConfiguration())
                .ApplyConfiguration(new EstatesPerMunicipalityViewConfiguration())
                .ApplyConfiguration(new EstatesPerStreetViewConfiguration())
                .ApplyConfiguration(new BusinessConfiguration())
                .ApplyConfiguration(new FullAddressConfiguration());
    }
}