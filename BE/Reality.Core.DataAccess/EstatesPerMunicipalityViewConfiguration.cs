﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.View;

namespace Reality.Core.DataAccess
{
    public class EstatesPerMunicipalityViewConfiguration : IEntityTypeConfiguration<EstatesPerMunicipalityView>
    {
        public void Configure(EntityTypeBuilder<EstatesPerMunicipalityView> builder)
        {
            builder.HasNoKey();
            builder.ToView("core", "EstatesPerMunicipalitiesView");
        }
    }
}