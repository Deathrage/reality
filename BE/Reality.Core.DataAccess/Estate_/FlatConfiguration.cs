﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Entities.Estate_;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess.Estate_
{
    internal class FlatConfiguration: IEntityTypeConfiguration<Flat>
    {
        public void Configure(EntityTypeBuilder<Flat> b)
        {
            b.HasEnumPropertyAsString(p => p.Furnishment);
            b.HasEnumPropertyAsString(p => p.Construction);
            b.HasEnumPropertyAsString(p => p.Disposition);

            // With House
            b.Property(p => p.Furnishment).HasColumnName(nameof(Flat.Furnishment));
            b.Property(p => p.Balcony).HasColumnName(nameof(Flat.Balcony));
            b.Property(p => p.Terrace).HasColumnName(nameof(Flat.Terrace));
            b.Property(p => p.LowEnergy).HasColumnName(nameof(Flat.LowEnergy));
            b.Property(p => p.NewBuilding).HasColumnName(nameof(Flat.NewBuilding));
            b.Property(p => p.Garage).HasColumnName(nameof(Flat.Garage));
            b.Property(p => p.UsableSurface).HasColumnName(nameof(Flat.UsableSurface));
        }
    }
}