﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Entities.Estate_;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess.Estate_
{
    internal class LandConfiguration: IEntityTypeConfiguration<Land>
    {
        public void Configure(EntityTypeBuilder<Land> b)
        {
            b.HasEnumPropertyAsString(p => p.LandType);

            // With House
            b.Property(p => p.LandSurface).HasColumnName(nameof(Land.LandSurface));
        }
    }
}