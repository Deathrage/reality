﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NetTopologySuite.Geometries;
using Reality.Core.Models.Entities.Estate_;
using Reality.Core.Models.Entities.Estate_.Enums;
using Reality.Common.DataAccess.Extensions;
using Reality.Core.Models;

namespace Reality.Core.DataAccess.Estate_
{
    internal class EstateConfiguration: IEntityTypeConfiguration<Estate>
    {
        public void Configure(EntityTypeBuilder<Estate> b)
        {
            b.ToTable("Estates", "core");
            
            b.ConfigureEntityKey();

            b.Property(p => p.Latitude).HasDecimalGeoCoordinateColumnType();
            b.Property(p => p.Longitude).HasDecimalGeoCoordinateColumnType();

            b.Property(p => p.GeoPoint)
                .HasConversion(point => new Point((double) point.Longitude, (double) point.Longitude) {SRID = 4326},
                    point => new GeoPoint((decimal) point.Y, (decimal) point.X));

            b.HasEnumPropertyAsString(p => p.EstateType);
            b.HasEnumPropertyAsString(p => p.Ownership);

            b.HasDiscriminator(p => p.EstateType)
                .HasValue<Estate>(EstateType.Miscellaneous)
                .HasValue<Flat>(EstateType.Flat)
                .HasValue<House>(EstateType.House)
                .HasValue<Land>(EstateType.Land)
                .HasValue<Business>(EstateType.Business);
            
            b.HasOne(p => p.Address).WithMany(p => p.Estates).HasForeignKey(p => p.AddressId);
            b.HasMany(p => p.Adverts).WithOne(p => p.Estate).HasForeignKey(p => p.EstateId);
        }
    }
}