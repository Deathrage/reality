﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Entities.Estate_;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess.Estate_
{
    internal class 
        HouseConfiguration: IEntityTypeConfiguration<House>
    {
        public void Configure(EntityTypeBuilder<House> b)
        {
            b.HasEnumPropertyAsString(p => p.Furnishment);
            
            // With Land
            b.Property(p => p.LandSurface).HasColumnName(nameof(House.LandSurface));
            // With Flat
            b.Property(p => p.Furnishment).HasColumnName(nameof(House.Furnishment));
            b.Property(p => p.Balcony).HasColumnName(nameof(House.Balcony));
            b.Property(p => p.Terrace).HasColumnName(nameof(House.Terrace));
            b.Property(p => p.LowEnergy).HasColumnName(nameof(House.LowEnergy));
            b.Property(p => p.NewBuilding).HasColumnName(nameof(House.NewBuilding));
            b.Property(p => p.Garage).HasColumnName(nameof(House.Garage));
            b.Property(p => p.UsableSurface).HasColumnName(nameof(House.UsableSurface));
        }
    }
}