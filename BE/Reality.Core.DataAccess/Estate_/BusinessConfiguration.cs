﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Entities.Estate_;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess.Estate_
{
    public class BusinessConfiguration: IEntityTypeConfiguration<Business>
    {
        public void Configure(EntityTypeBuilder<Business> b)
        {
            b.HasEnumPropertyAsString(p => p.BusinessType);
            b.Property(p => p.UsableSurface).HasColumnName(nameof(Business.UsableSurface));
        }
    }
}