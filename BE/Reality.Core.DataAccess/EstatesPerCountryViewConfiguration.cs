﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.View;

namespace Reality.Core.DataAccess
{
    public class EstatesPerCountryViewConfiguration : IEntityTypeConfiguration<EstatesPerCountryView>
    {
        public void Configure(EntityTypeBuilder<EstatesPerCountryView> builder)
        {
            builder.HasNoKey();
            builder.ToView("core", "EstatesPerCountriesView");
        }
    }
}