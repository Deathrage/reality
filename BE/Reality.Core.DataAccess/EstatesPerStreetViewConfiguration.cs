﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.View;

namespace Reality.Core.DataAccess
{
    public class EstatesPerStreetViewConfiguration : IEntityTypeConfiguration<EstatesPerStreetView>
    {
        public void Configure(EntityTypeBuilder<EstatesPerStreetView> builder)
        {
            builder.HasNoKey();
            builder.ToView("core", "EstatesPerStreetsView");
        }
    }
}