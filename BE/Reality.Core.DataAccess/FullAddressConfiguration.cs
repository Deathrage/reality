﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.View;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess
{
    public class FullAddressConfiguration : IEntityTypeConfiguration<FullAddress>
    {
        public void Configure(EntityTypeBuilder<FullAddress> builder)
        {
            builder.ToView("FullAddressesView", "core");
            builder.ConfigureEntityKey();
        }
    }
}