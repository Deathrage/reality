﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Entities.Address_;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess.Address_
{
    internal class CountryConfiguration: IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> b)
        {
            b.ToTable("Countries", "core");

            b.ConfigureEntityKey();

            b.HasUniqueIndex(i => i.Code);
        }
    }
}