﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Entities.Address_;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess.Address_
{
    internal class StreetConfiguration: IEntityTypeConfiguration<Street>
    {
        public void Configure(EntityTypeBuilder<Street> b)
        {
            b.ToTable("Streets", "core");

            b.ConfigureEntityKey();

            b.HasUniqueIndex(i => new {i.Name, i.MunicipalityId});

            b.HasOne(p => p.Municipality).WithMany(p => p.Streets);
        }
    }
}