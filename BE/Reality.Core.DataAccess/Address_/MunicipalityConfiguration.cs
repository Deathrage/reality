﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Entities.Address_;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess.Address_
{
    internal class MunicipalityConfiguration: IEntityTypeConfiguration<Municipality>
    {
        public void Configure(EntityTypeBuilder<Municipality> b)
        {
            b.ToTable("Municipalities", "core");

            b.ConfigureEntityKey();

            b.HasUniqueIndex(i => new {i.Name, i.CountrySubdivision, i.CountrySecondarySubdivision, i.CountryId});

            b.HasOne(p => p.Country).WithMany(p => p.Municipalities);
        }
    }
}