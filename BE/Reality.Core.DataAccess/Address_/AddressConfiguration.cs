﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Entities.Address_;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess.Address_
{
    internal class AddressConfiguration: IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> b)
        {
            b.ToTable("Addresses", "core");

            b.ConfigureEntityKey();
            b.HasUniqueIndex(i => new {i.StreetId, i.StreetNumber});

            b.HasOne(p => p.Street).WithMany(p => p.Addresses);
        }
    }
}