﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.View;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess
{
    public class AdvertEstateViewConfiguration: IEntityTypeConfiguration<AdvertEstateView>
    {
        public void Configure(EntityTypeBuilder<AdvertEstateView> builder)
        {
            builder.ConfigureEntityKey();
            builder.ToView("AdvertEstatesView", "core");
            
            builder.Property(p => p.Latitude).HasDecimalGeoCoordinateColumnType();
            builder.Property(p => p.Longitude).HasDecimalGeoCoordinateColumnType();
            
            builder.Property(p => p.PriceCzk).HasDecimalPriceColumnType();

            builder.HasEnumPropertyAsString(p => p.AdvertType);
            builder.HasEnumPropertyAsString(p => p.EstateType);
            builder.HasEnumPropertyAsString(p => p.Ownership);
            builder.HasEnumPropertyAsString(p => p.Furnishment);
            builder.HasEnumPropertyAsString(p => p.Construction);
            builder.HasEnumPropertyAsString(p => p.Disposition);
            builder.HasEnumPropertyAsString(p => p.LandType);
            builder.Property(p => p.Link).HasUriColumn();
        }
    }
}