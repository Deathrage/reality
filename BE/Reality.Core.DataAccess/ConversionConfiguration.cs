﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Data;

namespace Reality.Core.DataAccess
{
    internal class ConversionConfiguration: IEntityTypeConfiguration<Conversion>
    {
        public void Configure(EntityTypeBuilder<Conversion> b)
        {
            b.ToTable("Conversions", "core");

            b.HasKey(i => new {i.TargetAdvertId, i.SourceAdvertId, i.SourceModule});
        }
    }
}