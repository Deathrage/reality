﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Core.Models.Entities.Advert_;
using Reality.Common.DataAccess.Extensions;

namespace Reality.Core.DataAccess
{
    internal class AdvertConfiguration: IEntityTypeConfiguration<Advert>
    {
        public void Configure(EntityTypeBuilder<Advert> b)
        {
            b.ToTable("Adverts", "core");

            b.ConfigureEntityKey();

            b.HasEnumPropertyAsString(p => p.AdvertType);
            b.Property(p => p.PriceCzk).HasDecimalPriceColumnType();
            b.Property(p => p.Link).HasUriColumn();

            b.HasOne(p => p.Estate).WithMany(p => p.Adverts);
            b.HasOne(p => p.Conversion).WithOne(p => p.TargetAdvert);
        }
    }
}