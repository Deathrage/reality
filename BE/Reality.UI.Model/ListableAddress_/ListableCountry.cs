﻿using Reality.Common.Model;

namespace Reality.UI.Model.ListableAddress_
{
    public class ListableCountry: INamedEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}