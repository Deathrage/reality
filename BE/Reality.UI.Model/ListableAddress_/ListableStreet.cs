﻿using Reality.Common.Model;

namespace Reality.UI.Model.ListableAddress_
{
    public class ListableStreet: INamedEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}