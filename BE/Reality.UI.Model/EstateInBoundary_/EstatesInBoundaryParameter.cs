﻿using System.Collections.Generic;

namespace Reality.UI.Model.EstateInBoundary_
{
    public class EstatesInBoundaryCoordinates
    {
        public decimal Lon { get; set; }
        public decimal Lat { get; set; }

        public EstatesInBoundaryCoordinates(decimal lat, decimal lon)
        {
            Lon = lon;
            Lat = lat;
        }

        public EstatesInBoundaryCoordinates()
        {

        }

        public static implicit operator EstatesInBoundaryCoordinates((decimal Lat, decimal Lon) source)
            => new EstatesInBoundaryCoordinates(source.Lat, source.Lon);

    }

    public class EstatesInBoundaryParameter
    {
        public IEnumerable<EstatesInBoundaryCoordinates> Boundary { get; set; }
    }
}