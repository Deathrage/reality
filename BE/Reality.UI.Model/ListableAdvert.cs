﻿using System;
using Reality.Common.Model;
using Reality.Common.Model.Mixins;
using Reality.Core.Models.Entities.Advert_;
using Reality.Core.Models.Entities.Estate_;

namespace Reality.UI.Model
{
    public class ListableAdvert: INamedEntity, IWithManipulationMeta
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int EstateId { get; set; }

        public AdvertType AdvertType { get; set; }
        public decimal PriceCzk { get; set; }
        
        public string Source { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public Estate Estate {get;set;}
    }
}