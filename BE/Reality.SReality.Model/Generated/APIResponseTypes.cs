﻿using System.Collections.Generic;

namespace Reality.SReality.Model.Generated
{
	// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Seo    {
        public int category_main_cb { get; set; } 
        public int category_sub_cb { get; set; } 
        public int category_type_cb { get; set; } 
        public string locality { get; set; } 
    }

    public class Self    {
        public string profile { get; set; } 
        public string href { get; set; } 
        public string title { get; set; } 
    }

    public class Links    {
        public Self self { get; set; } 
    }

    public class Favourite    {
        public bool is_favourite { get; set; } 
        public Links _links { get; set; } 
    }

    public class Self2    {
        public string profile { get; set; } 
        public string href { get; set; } 
        public string title { get; set; } 
    }

    public class Links2    {
        public Self2 self { get; set; } 
    }

    public class Note    {
        public string note { get; set; } 
        public Links2 _links { get; set; } 
        public bool has_note { get; set; } 
    }

    public class Company    {
        public string url { get; set; } 
        public int id { get; set; } 
        public string name { get; set; } 
        public string logo_small { get; set; } 
    }

    public class Embedded2    {
        public Favourite favourite { get; set; } 
        public Note note { get; set; } 
        public Company company { get; set; } 
    }

    public class DynamicDown    {
        public string href { get; set; } 
    }

    public class DynamicUp    {
        public string href { get; set; } 
    }

    public class Iterator    {
        public string href { get; set; } 
    }

    public class Self3    {
        public string href { get; set; } 
    }

    public class Image    {
        public string href { get; set; } 
    }

    public class ImageMiddle2    {
        public string href { get; set; } 
    }

    public class Links3    {
        public List<DynamicDown> dynamicDown { get; set; } 
        public List<DynamicUp> dynamicUp { get; set; } 
        public Iterator iterator { get; set; } 
        public Self3 self { get; set; } 
        public List<Image> images { get; set; } 
        public List<ImageMiddle2> image_middle2 { get; set; } 
    }

    public class PriceCzk    {
        public int value_raw { get; set; } 
        public string unit { get; set; } 
        public string name { get; set; } 
    }

    public class Gps    {
        public double lat { get; set; } 
        public double lon { get; set; } 
    }

    public class Estate    {
        public List<List<string>> labelsReleased { get; set; } 
        public int has_panorama { get; set; } 
        public List<string> labels { get; set; } 
        public bool is_auction { get; set; } 
        public List<List<string>> labelsAll { get; set; } 
        public Seo seo { get; set; } 
        public int category { get; set; } 
        public int has_floor_plan { get; set; } 
        public Embedded2 _embedded { get; set; } 
        public int paid_logo { get; set; } 
        public string locality { get; set; } 
        public bool has_video { get; set; } 
        public Links3 _links { get; set; } 
        public bool @new { get; set; } 
        public double auctionPrice { get; set; } 
        public int type { get; set; } 
        public object hash_id { get; set; } 
        public int attractive_offer { get; set; } 
        public int price { get; set; } 
        public PriceCzk price_czk { get; set; } 
        public bool rus { get; set; } 
        public string name { get; set; } 
        public int region_tip { get; set; } 
        public Gps gps { get; set; } 
        public bool has_matterport_url { get; set; } 
    }

    public class Self4    {
        public string href { get; set; } 
    }

    public class Links4    {
        public Self4 self { get; set; } 
    }

    public class IsSaved    {
        public bool email_notification { get; set; } 
        public int notification_advert_count { get; set; } 
        public int stack_id { get; set; } 
        public bool push_notification { get; set; } 
        public Links4 _links { get; set; } 
        public bool removed { get; set; } 
        public bool saved { get; set; } 
    }

    public class Self5    {
        public string profile { get; set; } 
        public string href { get; set; } 
        public string title { get; set; } 
    }

    public class Links5    {
        public Self5 self { get; set; } 
    }

    public class NotPreciseLocationCount    {
        public int result_size_auction { get; set; } 
        public int result_size { get; set; } 
        public Links5 _links { get; set; } 
    }

    public class Embedded    {
        public List<Estate> estates { get; set; } 
        public IsSaved is_saved { get; set; } 
        public NotPreciseLocationCount not_precise_location_count { get; set; } 
    }

    public class Filter    {
        public string category_main_cb { get; set; } 
        public int suggested_districtId { get; set; } 
        public string locality_region_id { get; set; } 
        public string category_type_cb { get; set; } 
        public int suggested_regionId { get; set; } 
    }

    public class Self6    {
        public string href { get; set; } 
    }

    public class ClustersWithBoundingBoxOfFirst10    {
        public string href { get; set; } 
    }

    public class Rss    {
        public string href { get; set; } 
    }

    public class Links6    {
        public Self6 self { get; set; } 
        public ClustersWithBoundingBoxOfFirst10 clusters_with_bounding_box_of_first_10 { get; set; } 
        public Rss rss { get; set; } 
    }

    public class FilterLabels2    {
        public string something_more3_3310 { get; set; } 
        public string furnished_1 { get; set; } 
        public string furnished_2 { get; set; } 
        public string furnished_3 { get; set; } 
        public string building_type_search_1 { get; set; } 
        public string something_more1_3120 { get; set; } 
        public string building_condition_6 { get; set; } 
        public string something_more1_3100 { get; set; } 
        public string something_more1_3090 { get; set; } 
        public string something_more1_3110 { get; set; } 
        public string building_condition_4 { get; set; } 
        public string something_more2_3150 { get; set; } 
        public string something_more3_1820 { get; set; } 
        public string building_type_search_3 { get; set; } 
        public string something_more2_3130 { get; set; } 
        public string building_condition_9 { get; set; } 
        public string building_type_search_2 { get; set; } 
        public string ownership_1 { get; set; } 
        public string something_more2_3140 { get; set; } 
        public string ownership_3 { get; set; } 
        public string ownership_2 { get; set; } 
    }

    public class Root    {
        public string meta_description { get; set; } 
        public int result_size { get; set; } 
        public Embedded _embedded { get; set; } 
        public List<object> filterLabels { get; set; } 
        public string title { get; set; } 
        public Filter filter { get; set; } 
        public Links6 _links { get; set; } 
        public string locality { get; set; } 
        public string locality_dativ { get; set; } 
        public bool logged_in { get; set; } 
        public int per_page { get; set; } 
        public string category_instrumental { get; set; } 
        public int page { get; set; } 
        public FilterLabels2 filterLabels2 { get; set; } 
    }
}