﻿using System;
using System.Collections.Generic;
using Reality.Common.Model;
using Reality.Common.Model.Mixins;
using Reality.SReality.Model.Enums;

namespace Reality.SReality.Model
{
	public class Advert: IEntity, IWithGeoCoordinates, IWithLink, IWithManipulationMeta
	{
		public int Id { get; set; }
		public long ExternalId { get; set; }

		public AdvertType AdvertType { get; set; }
		public AdvertCategory MainCategory { get; set; }
		public int SubCategory { get; set; }

		public string Title { get; set; }

		public IEnumerable<MajorAdvertLabel> MajorAdvertLabels { get; set; }
		public IEnumerable<MinorAdvertLabel> MinorAdvertLabels { get; set; }

		public bool HasPanorama { get; set; }
		public bool IsNew { get; set; }

		public decimal PriceCzk { get; set; }
		public string PriceUnit { get; set; }

		public decimal? Latitude { get; set; }
		public decimal? Longitude { get; set; }
		public string Locality { get; set; }
		public string SeoLocality { get; set; }
		public Uri Link { get; set; }

		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
	}
}