﻿namespace Reality.SReality.Model
{
	public class MinorAdvertLabel
	{
		public int AdvertId { get; set; }
		public Advert Advert { get; set; }

		public string Label { get; set; }
	}
}