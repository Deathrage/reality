﻿namespace Reality.SReality.Model
{
	public class MajorAdvertLabel
	{
		public int AdvertId { get; set; }
		public Advert Advert { get; set; }

		public string Label { get; set; }
	}
}