﻿namespace Reality.SReality.Model.Enums
{
	/// <summary>
	/// category_type_cb
	/// </summary>
	public enum AdvertType
	{
		Sell = 1,
		Rent = 2
	}
}