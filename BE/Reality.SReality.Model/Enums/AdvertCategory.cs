﻿namespace Reality.SReality.Model.Enums
{
	/// <summary>
	/// category_main_cb
	/// </summary>
	public enum AdvertCategory
	{
		Flat = 1,
		House = 2,
		Land = 3,
		Commerce = 4,
		Misc = 5
	}
}