﻿namespace Reality.SReality.Model.Enums
{
	/// <summary>
	///  estate_age
	/// </summary>
	public enum AdvertAge
	{
		Day = 2,
		Week = 8,
		Month = 31 
	}
}