using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Reality.BezRealitky.GraphQL;
using Reality.Common.AzureFunctions.Extensions;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Extensions;
using Reality.Common.Logging;
using Reality.Core.AdvertConverter_;
using Reality.Core.Models.Data;
using BezRealAdvert = Reality.BezRealitky.Models.Advert;

namespace Reality.BezRealitky.AzureFunctions
{
    public class BezRealitky_Http_TestCoreMigration
    {
        private readonly IAdvertConverter<BezRealAdvert> _converterSource;
        private readonly IRepository<BezRealAdvert> _bezRealAdvertRepository;
        private readonly IRepository<Conversion> _conversionRepository;
        private readonly IActivityLogger _activityLogger;

        public BezRealitky_Http_TestCoreMigration(IAdvertConverter<BezRealAdvert> converterSource,
            IRepository<BezRealAdvert> bezRealAdvertRepository, IRepository<Conversion> conversionRepository,
            IActivityLogger activityLogger)
        {
            _converterSource = converterSource ?? throw new ArgumentNullException(nameof(converterSource));
            _bezRealAdvertRepository = bezRealAdvertRepository ??
                                       throw new ArgumentNullException(nameof(bezRealAdvertRepository));
            _conversionRepository =
                conversionRepository ?? throw new ArgumentNullException(nameof(conversionRepository));
            _activityLogger = activityLogger ?? throw new ArgumentNullException(nameof(activityLogger));
        }

        [FunctionName(nameof(BezRealitky_Http_TestCoreMigration))]
        public async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "bez-realitky/test-core-migration")]
            HttpRequest req, [DurableClient] IDurableOrchestrationClient client)
        {
            FinalizeActivityRunLogAsync finisher = _activityLogger.LogActivityRunAsync<BezRealitky_Http_TestCoreMigration>();

            try
            {
                IQueryable<BezRealAdvert> alreadyConverted =
                    _conversionRepository.AsQueryable().Join(_bezRealAdvertRepository.AsQueryable(),
                            conversion => conversion.SourceAdvertId,
                            advert => advert.Id,
                            (conversion, advert) => new {conversion.SourceModule, advert})
                        .Where(temp => temp.SourceModule == _converterSource.ModuleName)
                        .Select(temp => temp.advert);
                ImmutableArray<BezRealAdvert> candidates =
                    await _bezRealAdvertRepository.AsQueryable()
                        .Where(item => item.OfferType == OfferType.Prodej || item.OfferType == OfferType.Pronajem)
                        .Except(alreadyConverted).OrderBy(item => item.Created)
                        .Take(10).ToImmutableArrayAsync();

                string instanceId = await client.StartNewAsync(nameof(BezRealitky_Orchestration_ConvertNewAdverts),
                    new BezRealitky_Orchestration_ConvertNewAdverts.Input
                    {
                        NewIds = candidates.Select(item => item.Id).ToImmutableArray()
                    });

                await finisher(true,
                    $"{candidates.Length} ids -> {nameof(BezRealitky_Orchestration_ConvertNewAdverts)}");

                return client.CreateCheckStatusResponse(req, instanceId);
            }
            catch (Exception ex)
            {
                await finisher(false, null, ex);
                throw;
            }
        }
    }
}