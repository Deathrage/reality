﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

namespace Reality.BezRealitky.AzureFunctions
{
    public class BezRealitky_Http_FillMissingCoordinates 
    {
        [FunctionName(nameof(BezRealitky_Http_FillMissingCoordinates))]
        public async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "bez-realitky/fill-missing-coordinates")] HttpRequest req,
            [DurableClient] IDurableOrchestrationClient client,
            ILogger log)
        {
            StringValues switchCoordinatesRaw = req.Query["switch-bias"];
            string switchCoordinatesRawString = switchCoordinatesRaw.SingleOrDefault();
            bool switchCoordinates = !string.IsNullOrWhiteSpace(switchCoordinatesRawString) && bool.Parse(switchCoordinatesRawString);

            string instanceId = await client.StartNewAsync(nameof(BezRealitky_Orchestration_FillMissingCoordinates),
                new BezRealitky_Orchestration_FillMissingCoordinates.Input()
                {
                    SwitchCoordinatesBias = switchCoordinates
                });

            return await client.WaitForCompletionOrCreateCheckStatusResponseAsync(req, instanceId);
        }
    }
}