﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using Reality.Common.AzureFunctions.Abstractions;
using Reality.Common.AzureFunctions.Extensions;
using Reality.Common.Logging;

namespace Reality.BezRealitky.AzureFunctions
{
    public class BezRealitky_Timer_Crawl : ScrapperFunction
    {
        private readonly IBezRealitkyScrapper _bezRealitkyPerformer;

        public BezRealitky_Timer_Crawl(IActivityLogger loggingProvider, IBezRealitkyScrapper bezRealitkyPerformer) : base(
            loggingProvider)
        {
            _bezRealitkyPerformer =
                bezRealitkyPerformer ?? throw new ArgumentNullException(nameof(bezRealitkyPerformer));
        }

        [FunctionName(nameof(BezRealitky_Timer_Crawl))]
        public async Task RunAsync([TimerTrigger("0 40 1 * * *", RunOnStartup = false)]
            TimerInfo myTimer, ILogger log, [DurableClient] IDurableOrchestrationClient client)
        {
            IEnumerable<int> newIds = await RunPerformerFunction<IBezRealitkyScrapper, BezRealitky_Timer_Crawl>(_bezRealitkyPerformer, log);

            await client.StartNewAsync(nameof(BezRealitky_Orchestration_ConvertNewAdverts),
                new BezRealitky_Orchestration_ConvertNewAdverts.Input
                {
                    NewIds = newIds.ToImmutableArray()
                });
        }
    }
}