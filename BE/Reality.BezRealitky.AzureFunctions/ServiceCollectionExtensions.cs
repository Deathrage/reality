﻿using Microsoft.Extensions.DependencyInjection;

namespace Reality.BezRealitky.AzureFunctions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddBezRealitkyAzureFunctions(this IServiceCollection source)
            => source.AddBezRealitky();
    }
}