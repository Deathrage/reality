using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Reality.BezRealitky.Models;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Extensions;
using Reality.Common.Logging;
using Reality.Common.Geocoder;
using Reality.Common.Geocoder.Results;

namespace Reality.BezRealitky.AzureFunctions
{
    public class BezRealitky_Orchestration_FillMissingCoordinates
    {
        public class Input
        {
            public bool SwitchCoordinatesBias { get; set; }
        }

        private readonly IRepository<Advert> _advertRepository;
        private readonly IGeocoder _geocoder;
        private readonly IActivityLogger _activityLogger;

        public BezRealitky_Orchestration_FillMissingCoordinates(IRepository<Advert> advertRepository,
            IGeocoder geocoder, IActivityLogger activityLogger)
        {
            _advertRepository = advertRepository ?? throw new ArgumentNullException(nameof(advertRepository));
            _geocoder = geocoder ?? throw new ArgumentNullException(nameof(geocoder));
            _activityLogger = activityLogger ?? throw new ArgumentNullException(nameof(activityLogger));
        }

        [FunctionName(nameof(BezRealitky_Orchestration_FillMissingCoordinates))]
        public async Task<string> RunAsync([OrchestrationTrigger] IDurableOrchestrationContext context)
        {
            int fixedCount = await context.CallActivityAsync<int>(
                nameof(BezRealitky_Orchestration_FillMissingCoordinates) + "_Activity", context.GetInput<Input>());

            return  $"Found and fixed {fixedCount}.";
        }
        
        [FunctionName(nameof(BezRealitky_Orchestration_FillMissingCoordinates) + "_Activity")]
        public async Task<int> RunActivityAsync([ActivityTrigger] IDurableActivityContext context, ILogger log)
        {
            FinalizeActivityRunLogAsync finalizer = _activityLogger.LogActivityRunAsync<BezRealitky_Orchestration_FillMissingCoordinates>();

            Input input = context.GetInput<Input>();
            if (input == default)
                throw new ArgumentNullException(nameof(input));
            bool switchCoordinates = input.SwitchCoordinatesBias;

            Advert[] missingGeo = await _advertRepository.AsQueryable()
                .Where(item => item.Latitude == default || item.Longitude == default).Distinct().ToArrayAsync();

            log.LogInformation(
                $"Found {missingGeo.Count(item => item.Latitude == default)} adverts without {nameof(Advert.Latitude)} and {missingGeo.Count(item => item.Longitude == default)} adverts without {nameof(Advert.Longitude)}.");

            foreach (IEnumerable<Advert> batch in missingGeo.ToBatch(200))
            {
                Advert[] batchArray = batch.ToArray();

                log.LogInformation($"Begin work on {batchArray.Length} rows.");

                Task<(int, GeoCoordinatesResult)>[] tasks = batchArray.Select(async item =>
                {
                    GeoCoordinatesResult result =
                        await (switchCoordinates
                            ? _geocoder.GetGeoCoordinatesAsync(item.Locality, item.Longitude, item.Latitude)
                            : _geocoder.GetGeoCoordinatesAsync(item.Locality, item.Latitude, item.Longitude));
                    return (item.Id, result);
                }).ToArray();

                (int, GeoCoordinatesResult)[] codingResults = await Task.WhenAll(tasks);

                foreach (Advert advert in batchArray)
                {
                    GeoCoordinatesResult result = codingResults.Single(item => item.Item1 == advert.Id).Item2;
                    advert.Latitude = result?.Latitude;
                    advert.Longitude = result?.Longitude;
                }
                
                _advertRepository.Update(batchArray);
                log.LogInformation($"Updating {batchArray.Length} rows.");
                await _advertRepository.SaveAsync();
            }

            await finalizer(true, $"Filled coordinates to {missingGeo.Length} adverts");

            return missingGeo.Length;
        }

    }
}
