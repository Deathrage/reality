using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using Reality.Common.Geocoder;
using Reality.Common.Geocoder.Results;
using Reality.Core.Models.Entities.Address_;

namespace Reality.Core.AzureFunctions.Activities
{
    public class Core_Activity_GetAddressFromLocality
    {
        public class Input
        {
            public string Locality { get; set; }
        }

        public class Output
        {
            public decimal Longitude { get; set; }
            public decimal Latitude { get; set; }
            public Address Address { get; set; }
        }

        private readonly IGeocoder _geocoder;
        private readonly IMapper _mapper;

        public Core_Activity_GetAddressFromLocality(IGeocoder geocoder, IMapper mapper)
        {
            _geocoder = geocoder ?? throw new ArgumentNullException(nameof(geocoder));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [FunctionName(nameof(Core_Activity_GetAddressFromLocality))]
        public Task<Result<Output>> RunAsync([ActivityTrigger] Input input, ILogger logger)
            => ExceptionHandler.Handle(logger, async () =>
            {
                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                if (input.Locality == default)
                    throw new ArgumentNullException(nameof(input.Locality));

                GeoCoordinatesResult result =
                    await _geocoder.GetGeoCoordinatesAsync(input.Locality).ConfigureAwait(false);

                Address address = _mapper.Map<Address>(result.Address);
                return new Output()
                {
                    Address = address,
                    Longitude = result.Longitude,
                    Latitude = result.Latitude
                };
            });
    }
}