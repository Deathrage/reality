using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using Reality.Common.Geocoder;
using Reality.Common.Geocoder.Results;
using Reality.Core.Models.Entities.Address_;

namespace Reality.Core.AzureFunctions.Activities
{
    public class Core_Activity_GetAddressFromGeoCoordinates
    {
        public class Input
        {
            public decimal Longitude { get; set; }
            public decimal Latitude { get; set; }
        }
        
        private readonly IGeocoder _geocoder;
        private readonly IMapper _mapper;

        public Core_Activity_GetAddressFromGeoCoordinates(IGeocoder geocoder, IMapper mapper)
        {
            _geocoder = geocoder ?? throw new ArgumentNullException(nameof(geocoder));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [FunctionName(nameof(Core_Activity_GetAddressFromGeoCoordinates))]
        public Task<Result<Address>> RunAsync([ActivityTrigger] Input input, ILogger logger)
            => ExceptionHandler.Handle(logger, async () =>
            {
                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                AddressResult result =
                    await _geocoder.GetAddressAsync(input.Longitude, input.Latitude).ConfigureAwait(false);

                Address address = _mapper.Map<Address>(result);

                return address;
            });
    }
}