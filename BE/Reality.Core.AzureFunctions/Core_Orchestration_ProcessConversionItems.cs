﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using Reality.Common.AzureFunctions.Extensions;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Extensions;
using Reality.Core.AdvertConverter_;
using Reality.Core.Models.Data;
using Reality.Core.Models.Entities.Address_;
using Reality.Core.AzureFunctions;
using Reality.Core.AzureFunctions.Activities;

namespace Reality.Access.Functions.Core
{
    public class Core_Orchestration_ProcessConversionItems
    {
        #region Input types

        public class Input
        {
            public IReadOnlyList<ConversionItem> ConversionItems { get; set; }
        }

        #endregion

        private readonly IRepository<Country> _countryRepository;
        private readonly IRepository<Municipality> _municipalityRepository;
        private readonly IRepository<Street> _streetRepository;
        private readonly IRepository<Address> _addressRepository;
        private readonly IRepository<Conversion> _conversionRepository;

        public Core_Orchestration_ProcessConversionItems(IRepository<Country> countryRepository, 
            IRepository<Municipality> municipalityRepository, IRepository<Street> streetRepository, 
            IRepository<Address> addressRepository,
            IRepository<Conversion> conversionRepository)
        {
            _countryRepository = countryRepository ?? throw new ArgumentNullException(nameof(countryRepository));
            _municipalityRepository =
                municipalityRepository ?? throw new ArgumentNullException(nameof(municipalityRepository));
            _streetRepository = streetRepository ?? throw new ArgumentNullException(nameof(streetRepository));
            _addressRepository = addressRepository ?? throw new ArgumentNullException(nameof(addressRepository));
            _conversionRepository =
                conversionRepository ?? throw new ArgumentNullException(nameof(conversionRepository));
        }

        [FunctionName(nameof(Core_Orchestration_ProcessConversionItems))]
        public async Task RunAsync([OrchestrationTrigger] IDurableOrchestrationContext context)
        {
            DateTime startDate = context.CurrentUtcDateTime;
            
            try
            {
                Input input = context.GetInput<Input>();

                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                IReadOnlyList<ConversionItem> items = input.ConversionItems?.ToImmutableArray();

                if (items?.Count < 1)
                    return;
                
                List<ConversionItem> itemsWithAddresses = new List<ConversionItem>();
                foreach (ConversionItem item in items)
                {
                    Address address = null;

                    if (item.Estate.Latitude.HasValue && item.Estate.Longitude.HasValue)
                    {
                        item.Estate.HasOwnAddress = true;

                        Result<Address> result = await context.CallActivityAsync<Result<Address>>(nameof(Core_Activity_GetAddressFromGeoCoordinates),
                            new Core_Activity_GetAddressFromGeoCoordinates.Input
                            {
                                Longitude = item.Estate.Longitude.Value,
                                Latitude = item.Estate.Latitude.Value
                            });

                        if (!result.Success)
                            continue;
                        address = result.Data;

                    }
                    else if (!string.IsNullOrWhiteSpace(item.Estate.Locality))
                    {
                        item.Estate.HasOwnAddress = false;

                        Result<Core_Activity_GetAddressFromLocality.Output> result =
                            await context.CallActivityAsync<Result<Core_Activity_GetAddressFromLocality.Output>>(
                                nameof(Core_Activity_GetAddressFromLocality),
                                new Core_Activity_GetAddressFromLocality.Input
                                {
                                    // strip "1 - Jozefov" from "Bořvojova, Praha 1 - Jozefov", otherwise it will go wild
                                    Locality =  new Regex(@"(\d+)?\s-\s.+").Replace(item.Estate.Locality, "")
                                });
                        
                        if (!result.Success)
                            continue;
                        item.Estate.Latitude = result.Data.Latitude;
                        item.Estate.Longitude = result.Data.Longitude;
                        address = result.Data.Address;
                    }

                    if (address != default)
                        itemsWithAddresses.Add(item.Fill(estate: item.Estate, address: address, street: address.Street, 
                            municipality: address.Street.Municipality, country: address.Street.Municipality.Country));
                }

                items = itemsWithAddresses;

                items = (await context.CallActivityAsync<Result<IReadOnlyList<ConversionItem>>>(
                    nameof(Core_Orchestration_ProcessConversionItems) + "_" +
                    nameof(ReconcileCountries),
                    items)).Data;
                items = (await context.CallActivityAsync<Result<IReadOnlyList<ConversionItem>>>(
                    nameof(Core_Orchestration_ProcessConversionItems) + "_" +
                    nameof(ReconcileMunicipalities),
                    items)).Data;
                items = (await context.CallActivityAsync<Result<IReadOnlyList<ConversionItem>>>(
                    nameof(Core_Orchestration_ProcessConversionItems) + "_" +
                    nameof(ReconcileStreets),
                    items)).Data;
                items = (await context.CallActivityAsync<Result<IReadOnlyList<ConversionItem>>>(
                    nameof(Core_Orchestration_ProcessConversionItems) + "_" +
                    nameof(ReconcileAddresses),
                    items)).Data;

                ImmutableArray<Conversion> conversions = items.Select(item =>
                {
                    Conversion conversion = item.ConversionMeta;
                    conversion.TargetAdvert = item.Advert;
                    conversion.TargetAdvert.Estate = item.Estate;
                    return conversion;
                }).ToImmutableArray();

                await context.CallActivityAsync(
                    nameof(Core_Orchestration_ProcessConversionItems) + "_" +
                    nameof(SaveConversions), 
                    conversions);

                await context.CallLoggingActivity(nameof(Core_Orchestration_ProcessConversionItems), startDate,
                    $"+{conversions.Length} converted items");
            }
            catch (Exception ex)
            {
                await context.CallLoggingActivity(nameof(Core_Orchestration_ProcessConversionItems), startDate,
                    ex);
                throw;
            }
        }

        [FunctionName(nameof(Core_Orchestration_ProcessConversionItems) + "_" + nameof(ReconcileCountries))]
        public Task<Result<IReadOnlyList<ConversionItem>>> ReconcileCountries(
            [ActivityTrigger] IReadOnlyList<ConversionItem> input, ILogger logger)
            => ExceptionHandler.Handle<IReadOnlyList<ConversionItem>>(logger, async () =>
            {
                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                // Pool together all concerned
                ImmutableArray<Country> concernedCountries =
                    input.Select(item => item.Country).DistinctBy(item => item.Code).ToImmutableArray();

                IEnumerable<string> concernedCountryCodes = input.Select(item => item.Country.Code);

                // Find all countries known in DB
                ImmutableArray<Country> foundCountries = await _countryRepository.AsQueryable()
                    .Where(country => concernedCountryCodes.Contains(country.Code)).ToImmutableArrayAsync();
                ImmutableHashSet<string> foundCountriesCodes =
                    foundCountries.Select(item => item.Code).ToImmutableHashSet();

                // Find countries amongst concerned where were not found in the DB
                ImmutableArray<Country> newCountries = concernedCountries
                    .Where(item => !foundCountriesCodes.Contains(item.Code)).ToImmutableArray();

                await _countryRepository.AddAsync(newCountries);
                await _countryRepository.SaveAsync();

                ImmutableArray<Country> countryPool = foundCountries.Union(newCountries).ToImmutableArray();
                // Fill desired country to each municipality from the pool
                ImmutableArray<ConversionItem> result = input.Select(item =>
                {
                    Country targetCountry = countryPool.SingleOrDefault(country => country.Code == item.Country.Code);

                    item.Municipality.CountryId = targetCountry?.Id ?? default;
                    item.Municipality.Country = default;
                    return item.Fill(country: targetCountry, municipality: item.Municipality);
                }).ToImmutableArray();

                return result;
            });

        [FunctionName(nameof(Core_Orchestration_ProcessConversionItems) + "_" + nameof(ReconcileMunicipalities))]
        public Task<Result<IReadOnlyList<ConversionItem>>> ReconcileMunicipalities(
            [ActivityTrigger] IReadOnlyList<ConversionItem> input, ILogger logger)
            => ExceptionHandler.Handle<IReadOnlyList<ConversionItem>>(logger, async () =>
            {
                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                ImmutableArray<Municipality> concernedMunicipalities = input.Select(item => item.Municipality)
                    .DistinctBy(
                        item =>
                            (item.Name, item.CountrySubdivision, item.CountrySecondarySubdivision, item.CountryId))
                    .ToImmutableArray();

                ImmutableArray<int> concernedMunicipalityCountryIds =
                    concernedMunicipalities.Select(x => x.CountryId).ToImmutableArray();
                ImmutableArray<string> concernedMunicipalityNames =
                    concernedMunicipalities.Select(x => x.Name).ToImmutableArray();
                ImmutableArray<string> concernedMunicipalityCountrySubdivisions =
                    concernedMunicipalities.Select(x => x.CountrySubdivision).ToImmutableArray();
                ImmutableArray<string> concernedMunicipalityCountrySecondarySubdivisions =
                    concernedMunicipalities.Select(x => x.CountrySecondarySubdivision).ToImmutableArray();

                ImmutableArray<Municipality> foundMunicipalities = await _municipalityRepository.AsQueryable()
                    .Where(outer =>
                        concernedMunicipalityCountryIds.Contains(outer.CountryId) &&
                        concernedMunicipalityCountrySubdivisions.Contains(outer.CountrySubdivision) &&
                        concernedMunicipalityCountrySecondarySubdivisions.Contains(outer.CountrySecondarySubdivision) &&
                        concernedMunicipalityNames.Contains(outer.Name)
                    )
                    .ToImmutableArrayAsync();
                ImmutableHashSet<(string Name, string Subdivision, string SecondarySubdivision, int CountryId)>
                    foundMunicipalityKeys =
                        foundMunicipalities.Select(item =>
                                (item.Name, item.CountrySubdivision, item.CountrySecondarySubdivision, item.CountryId))
                            .ToImmutableHashSet();

                ImmutableArray<Municipality> newMunicipalities = concernedMunicipalities.Where(item =>
                    !foundMunicipalityKeys.Contains((item.Name, item.CountrySubdivision,
                        item.CountrySecondarySubdivision,
                        item.CountryId))).ToImmutableArray();

                await _municipalityRepository.AddAsync(newMunicipalities);
                await _municipalityRepository.SaveAsync();

                ImmutableArray<Municipality> municipalityPool =
                    foundMunicipalities.Union(newMunicipalities).ToImmutableArray();

                ImmutableArray<ConversionItem> result = input.Select(item =>
                {
                    Municipality targetMunicipality = municipalityPool.SingleOrDefault(muni =>
                        muni.Name == item.Municipality.Name && muni.CountryId == item.Municipality.CountryId &&
                        muni.CountrySubdivision == item.Municipality.CountrySubdivision &&
                        muni.CountrySecondarySubdivision == item.Municipality.CountrySecondarySubdivision);

                    item.Street.MunicipalityId = targetMunicipality?.Id ?? default;
                    item.Street.Municipality = default;
                    return item.Fill(municipality: targetMunicipality, street: item.Street);
                }).ToImmutableArray();

                return result;
            });

        [FunctionName(nameof(Core_Orchestration_ProcessConversionItems) + "_" + nameof(ReconcileStreets))]
        public Task<Result<IReadOnlyList<ConversionItem>>> ReconcileStreets(
            [ActivityTrigger] IReadOnlyList<ConversionItem> input, ILogger logger)
            => ExceptionHandler.Handle<IReadOnlyList<ConversionItem>>(logger, async () =>
            {
                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                ImmutableArray<Street> concernedStreets = input.Select(item => item.Street).DistinctBy(
                        item => (item.Name, item.MunicipalityId))
                    .ToImmutableArray();

                ImmutableArray<string> concernedStreetNames =
                    concernedStreets.Select(street => street.Name).ToImmutableArray();
                ImmutableArray<int> concernedStreetMunicipalityIds =
                    concernedStreets.Select(street => street.MunicipalityId).ToImmutableArray();

                ImmutableArray<Street> foundStreets = await _streetRepository.AsQueryable()
                    .Where(street =>
                        concernedStreetNames.Contains(street.Name) &&
                        concernedStreetMunicipalityIds.Contains(street.MunicipalityId))
                    .ToImmutableArrayAsync();
                ImmutableArray<(string Name, int MunicipalityId)> foundStreetKeys =
                    foundStreets.Select(street => (street.Name, street.MunicipalityId)).ToImmutableArray();

                ImmutableArray<Street> newStreets = concernedStreets.Where(concernedStreet =>
                        !foundStreetKeys.Contains((concernedStreet.Name, concernedStreet.MunicipalityId)))
                    .ToImmutableArray();

                await _streetRepository.AddAsync(newStreets);
                await _streetRepository.SaveAsync();

                ImmutableArray<Street> streetPool = newStreets.Union(foundStreets).ToImmutableArray();

                ImmutableArray<ConversionItem> result = input.Select(item =>
                {
                    Street targetStreet = streetPool.SingleOrDefault(street =>
                        street.MunicipalityId == item.Street.MunicipalityId && street.Name == item.Street.Name);

                    item.Address.StreetId = targetStreet?.Id ?? default;
                    item.Address.Street = default;
                    return item.Fill(street: targetStreet, address: item.Address);
                }).ToImmutableArray();

                return result;
            });

        [FunctionName(nameof(Core_Orchestration_ProcessConversionItems) + "_" + nameof(ReconcileAddresses))]
        public Task<Result<IReadOnlyList<ConversionItem>>> ReconcileAddresses(
            [ActivityTrigger] IReadOnlyList<ConversionItem> input, ILogger logger)
            => ExceptionHandler.Handle<IReadOnlyList<ConversionItem>>(logger, async () =>
            {
                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                ImmutableArray<Address> concernedAddresses = input.Select(item => item.Address).DistinctBy(
                        item => (item.StreetId, item.StreetNumber))
                    .ToImmutableArray();

                ImmutableArray<string> concernedAddressStreetNumbers =
                    concernedAddresses.Select(item => item.StreetNumber).ToImmutableArray();
                ImmutableArray<int> concernedAddressStreetStreetIds =
                    concernedAddresses.Select(item => item.StreetId).ToImmutableArray();

                ImmutableArray<Address> foundAddresses = await _addressRepository.AsQueryable()
                    .Where(address =>
                        concernedAddressStreetNumbers.Contains(address.StreetNumber) &&
                        concernedAddressStreetStreetIds.Contains(address.StreetId))
                    .ToImmutableArrayAsync();
                ImmutableArray<(int StreetId, string StreetNumber)> foundAddressKeys = foundAddresses
                    .Select(address => (address.StreetId, address.StreetNumber)).ToImmutableArray();

                ImmutableArray<Address> newAddresses = concernedAddresses
                    .Where(address => !foundAddressKeys.Contains((address.StreetId, address.StreetNumber)))
                    .ToImmutableArray();

                await _addressRepository.AddAsync(newAddresses);
                await _addressRepository.SaveAsync();

                ImmutableArray<Address> addressPool = foundAddresses.Union(newAddresses).ToImmutableArray();

                ImmutableArray<ConversionItem> result = input.Select(item =>
                {
                    Address targetAddress = addressPool.SingleOrDefault(street =>
                        street.StreetId == item.Address.StreetId && street.StreetNumber == item.Address.StreetNumber);

                    item.Estate.AddressId = targetAddress?.Id ?? default;
                    item.Estate.Address = default;
                    return item.Fill(address: targetAddress, estate: item.Estate);
                }).ToImmutableArray();

                return result;
            });
        
        [FunctionName(nameof(Core_Orchestration_ProcessConversionItems) + "_" + nameof(SaveConversions))]
        public async Task SaveConversions([ActivityTrigger] IReadOnlyList<Conversion> input, ILogger logger)
        {
            try
            {
                if (input == default)
                    throw new ArgumentNullException(nameof(input));

                await _conversionRepository.AddAsync(input);
                await _conversionRepository.SaveAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error");
                throw;
            }
        }
    }
}