﻿namespace Reality.Core.AzureFunctions
{
    public struct Result<TData>
    {
        public TData Data { get; set; }
        public bool Success { get; set; }

        public static implicit operator TData(Result<TData> result)
            => result.Data;
    }
}