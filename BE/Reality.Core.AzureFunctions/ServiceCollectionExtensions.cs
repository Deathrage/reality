﻿using Microsoft.Extensions.DependencyInjection;
using Reality.Core.Extensions;

namespace Reality.Core.AzureFunctions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCoreAzureFunctions(this IServiceCollection source)
            => source.AddCore();
    }
}