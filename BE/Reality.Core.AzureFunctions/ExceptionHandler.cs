﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Reality.Core.AzureFunctions
{
    public static class ExceptionHandler
    {
        public static Result<TData> Handle<TData>(ILogger logger,Func<TData> runner, bool rethrow = false)
        {
            try
            {
                TData data = runner();
                return new Result<TData>() {Success = true, Data = data};
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error");
                if (rethrow)
                    throw;
                return new Result<TData>() {Success = false};
            }
        }

        public static async Task<Result<TData>> Handle<TData>(ILogger logger, Func<Task<TData>> runner, bool rethrow = false)
        {
            try
            {
                TData data = await runner();
                return new Result<TData>() {Success = true, Data = data};
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error");
                if (rethrow)
                    throw;
                return new Result<TData>() {Success = false};
            }
        }
    }
}