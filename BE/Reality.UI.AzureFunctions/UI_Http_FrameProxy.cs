using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Io;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Primitives;

namespace Reality.UI.AzureFunctions
{
    public class UI_Http_FrameProxy
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IBrowsingContext _browsingContext;

        public UI_Http_FrameProxy(IHttpClientFactory httpClientFactory, IBrowsingContext browsingContext)
        {
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _browsingContext = browsingContext ?? throw new ArgumentNullException(nameof(browsingContext));
        }

        [FunctionName(nameof(UI_Http_FrameProxy))]
        public async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "ui/frame-proxy")]
            HttpRequest req, CancellationToken cancellationToken)
        {
            if (req.Query.TryGetValue("target", out StringValues targets))
            {
                if (targets.Count != 1) return new BadRequestErrorMessageResult($"Found {targets.Count} targets. Specify only one.");

                string target = targets.Single();

                HttpClient client = _httpClientFactory.CreateClient();

                string decodedTarget = HttpUtility.UrlDecode(target);
                Uri targetUri = new Uri(decodedTarget);
                HttpResponseMessage response = await client.GetAsync(targetUri, cancellationToken);

                string html = await response.Content.ReadAsStringAsync();

                IDocument document = await _browsingContext.OpenAsync(req => req.Content(html));
                IHtmlCollection<IElement> hrefs = document.QuerySelectorAll("[href^='/']");
                foreach (IElement href in hrefs)
                {
                    IAttr attr = href.Attributes["href"];
                    attr.Value = new Uri(targetUri, attr.Value).ToString();
                }
                
                IHtmlCollection<IElement> srcs = document.QuerySelectorAll("[src^='/']");
                foreach (IElement src in srcs)
                {
                    IAttr attr = src.Attributes["src"];
                    attr.Value = new Uri(targetUri, attr.Value).ToString();
                }

                string finalHtml = document.ToHtml();
                return new ContentResult()
                    {Content = finalHtml, StatusCode = StatusCodes.Status200OK, ContentType = MimeTypeNames.Html};
            }

            return new BadRequestErrorMessageResult("No target found");
        }

    }
}
