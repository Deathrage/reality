using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Reality.UI.Executor;

namespace Reality.UI.AzureFunctions
{
    public class UI_Http_GraphQl
    {
        private readonly IGraphQlExecutor _graphQlExecutor;

        public UI_Http_GraphQl(IGraphQlExecutor graphQlExecutor)
        {
            _graphQlExecutor = graphQlExecutor ?? throw new ArgumentNullException(nameof(graphQlExecutor));
        }

        [FunctionName(nameof(UI_Http_GraphQl))]
        public async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "ui/graphql")]
            HttpRequest req, CancellationToken cancellationToken)
        {
            await _graphQlExecutor.ExecuteFunctionsQueryAsync(req, cancellationToken);
            return new EmptyResult();
        }

    }
}
