﻿using Microsoft.Extensions.DependencyInjection;
using Reality.UI;

namespace Reality.UI.AzureFunctions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddUIAzureFunctions(this IServiceCollection source)
            => source.AddUI();
    }
}