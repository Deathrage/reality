﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.BezRealitky.Models;
using Reality.Common.DataAccess.Extensions;

namespace Reality.BezRealitky.DataAccess
{
    internal class AdvertConfiguration: IEntityTypeConfiguration<Advert>
    {
        public void Configure(EntityTypeBuilder<Advert> b)
        {
            b.ToTable("Adverts", "bezreal");

            b.ConfigureEntityKey();
            b.Property(p => p.Link).HasUriColumn();
            b.Property(p => p.Price).HasDecimalPriceColumnType();
            b.Property(p => p.Charges).HasDecimalPriceColumnType();
            b.Property(p => p.Deposit).HasDecimalPriceColumnType();
            b.Property(p => p.Longitude).HasDecimalGeoCoordinateColumnType();
            b.Property(p => p.Latitude).HasDecimalGeoCoordinateColumnType();
        }
    }
}
