﻿using Microsoft.EntityFrameworkCore;

namespace Reality.BezRealitky.DataAccess
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder ConfigureBezRealitkyModel(this ModelBuilder builder)
            => builder.ApplyConfiguration(new AdvertConfiguration());
    }
}