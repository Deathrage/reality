﻿using Microsoft.Extensions.DependencyInjection;
using Reality.BezRealitky.Models;
using Reality.Core.AdvertConverter_;

namespace Reality.BezRealitky.Converter
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddBezRealitkyConverter(this IServiceCollection source)
            => source.AddTransient<IAdvertConverter<Advert>, BezRealitkyConverter>();
    }
}