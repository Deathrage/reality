﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reality.Common
{
	public interface IScrapper
	{
		/// <summary>
		/// Returns ids of found items
		/// </summary>
		/// <returns></returns>
		Task<IEnumerable<int>> RunAsync();
	}
}