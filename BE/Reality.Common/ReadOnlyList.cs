﻿using System.Collections.Generic;

namespace Reality.Common
{
    public static class ReadOnlyList
    {
        public static IReadOnlyList<T> Empty<T>()
            => new T[0];
    }
}