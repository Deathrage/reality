﻿using System;
using System.Threading.Tasks;
using Reality.Common.DataAccess.Abstractions;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Model;
using Reality.Common.Serialization;

namespace Reality.Common.Logging
{
	internal class ActivityLogger: IActivityLogger
    {
        private readonly IRepository<ActivityLog> _performerLogRepository;
        private readonly ISerializer _serializer;

        public async Task LogAsync(string source, DateTime start, DateTime ended, bool success, string successData, Exception exception)
        {
            string data;
            if (!success)
            {
                Exception finalException = exception;
                while (finalException?.InnerException != default)
                    finalException = exception.InnerException;

                data = $"{finalException?.Message ?? "Unknown"} at {finalException?.StackTrace ?? "Unknown"}";
                // If the report contains word unknown bundle up whole exception for further inspection
                if (string.IsNullOrWhiteSpace(finalException?.Message) ||
                    string.IsNullOrWhiteSpace(finalException?.StackTrace))
                    data += $"RawData: {_serializer.SerializeException(exception)}";
            }
            else
                data = successData;

            await _performerLogRepository.AddAsync(new ActivityLog()
                {Source = source, BeganAt = start, EndedAt = ended, Data = data, Success = success});
            await _performerLogRepository.SaveAsync();
        }

        public ActivityLogger(IRepository<ActivityLog> performerLogRepository, ISerializer serializer)
        {
            _performerLogRepository =
                performerLogRepository ?? throw new ArgumentNullException(nameof(performerLogRepository));
            _serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
        }
    }
}