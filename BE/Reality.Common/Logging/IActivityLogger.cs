﻿using System;
using System.Threading.Tasks;

namespace Reality.Common.Logging
{
    public interface IActivityLogger
    {
        Task LogAsync(string source, DateTime start, DateTime ended, bool success, string successData,
            Exception exception);
    }
}