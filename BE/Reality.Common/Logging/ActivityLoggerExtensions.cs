﻿using System;
using System.Threading.Tasks;

namespace Reality.Common.Logging
{
    public delegate Task FinalizeScrapperRunLogAsync(bool success, int addedRecords, Exception exception = null);
        
    public delegate Task FinalizeActivityRunLogAsync(bool success, string data, Exception exception = null, DateTime? ended = null);

    public static class ActivityLoggerExtensions
    {
        public static Task LogAsync<TSource>(this IActivityLogger logger, DateTime start, DateTime ended, bool success, string successData,
            Exception exception)
        {
            return logger.LogAsync(typeof(TSource).Name, start, ended, success, successData, exception);
        }

        public static FinalizeScrapperRunLogAsync LogScrapperRunAsync<TSource, TScrapper>(this IActivityLogger provider)
            where TScrapper: IScrapper
        {
            FinalizeActivityRunLogAsync finalize = provider.LogActivityRunAsync<TSource>();
            return (success, addedRecords, exception) => finalize(success, $"+{addedRecords} records via {typeof(TScrapper).Name}", exception);
        }  

        public static FinalizeActivityRunLogAsync LogActivityRunAsync<TSource>(this IActivityLogger provider, DateTime? beganAt = null)
        {
            DateTime started = beganAt ?? DateTime.Now;

            return (success, message, exception, endedAt) =>
                provider.LogAsync(typeof(TSource).Name, started, endedAt ?? DateTime.Now, success, message,
                    exception);
        }
    }
}