﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Reality.Common.Extensions
{
	public static class EnumerableExtensions
	{
		public static IEnumerable<TSource> DistinctBy<TSource, TKey>
			(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			HashSet<TKey> seenKeys = new HashSet<TKey>();
			foreach (TSource element in source)
			{
				if (seenKeys.Add(keySelector(element)))
				{
					yield return element;
				}
			}
		}

        public static IEnumerable<IEnumerable<TSource>> ToBatch<TSource>(this IEnumerable<TSource> source, int batchSize)
        {
			if (batchSize < 2)
				throw new ArgumentException($"Batch size has to be above 1. Supplied was {batchSize}.");

            List<TSource> elements = source.ToList();

            if (elements.Count == 0)
                yield return new TSource[0];
            else
            {
                for (int currentIndex = 0; currentIndex < elements.Count; currentIndex += batchSize) 
                { 
                    yield return elements.GetRange(currentIndex, Math.Min(batchSize, elements.Count - currentIndex)); 
                }
            }
        }
    }
}