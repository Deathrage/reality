﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Reality.Common.Logging;
using Reality.Common.Geocoder;
using Reality.Common.Serialization;

namespace Reality.Common.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCommon(this IServiceCollection source, string azureMapsKey,
            Func<IServiceProvider, ISerializer> serializerFactory)
            => source
                .AddScoped<IActivityLogger, ActivityLogger>()
                .AddGeocoder(azureMapsKey)
                .AddSingleton(serializerFactory);
    }
}