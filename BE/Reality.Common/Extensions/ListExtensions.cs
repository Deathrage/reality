﻿using System.Collections.Generic;

namespace Reality.Common.Extensions
{
    public static class ListExtensions
    {
        public static T GetSafe<T>(this IList<T> list, int index)
        {
            if (index == -1)
                return default;
            if (index > list.Count - 1)
                return default;
            return list[index];
        }
    }
}