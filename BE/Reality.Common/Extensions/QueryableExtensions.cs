﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Reality.Common.Extensions
{
    public static class QueryableExtensions
    {
        public static async Task<ImmutableArray<TSource>> ToImmutableArrayAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            IEnumerable<TSource> result = await source.ToListAsync(cancellationToken);
            return result.ToImmutableArray();
        }
    }
}