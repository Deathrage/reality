﻿using System;
using System.Linq;

namespace Reality.Common.Extensions
{
	public static class StringExtensions
	{
		public static string ToPascalCase(this string source)
		{
            if (string.IsNullOrWhiteSpace(source))
                return source;

			return source.Split(new[] {"_"}, StringSplitOptions.RemoveEmptyEntries)
				.Select(s => char.ToUpperInvariant(s[0]) + s.Substring(1, s.Length - 1))
				.Aggregate(string.Empty, (s1, s2) => s1 + s2);
		}

        public static string ToCamelCase(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return source;

            string pascal = source.ToPascalCase();
            return char.ToLowerInvariant(pascal.First()) + pascal.Substring(1, pascal.Length - 1);
        }
	}
}