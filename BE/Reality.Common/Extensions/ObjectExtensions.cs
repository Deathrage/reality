﻿using System.Linq;

namespace Reality.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static bool In<TSource>(this TSource source, params TSource[] possibilities)
            => possibilities.Contains(source);
    }
}