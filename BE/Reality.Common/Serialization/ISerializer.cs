﻿using System;

namespace Reality.Common.Serialization
{
    public interface ISerializer
    {
        public T Deserialize<T>(string input);

        public string Serialize(object input);

        public string SerializeException(Exception ex);
    }
}