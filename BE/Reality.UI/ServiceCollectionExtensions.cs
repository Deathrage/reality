﻿using System;
using System.Collections.Immutable;
using System.Linq;
using HotChocolate.Execution.Configuration;
using HotChocolate.Execution.Options;
using HotChocolate.Types;
using Microsoft.Extensions.DependencyInjection;
using Reality.UI.Executor;
using Reality.UI.GraphQl;
using Reality.UI.GraphQl.DetailedEstate_;

namespace Reality.UI
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddUI(this IServiceCollection services)
        {
            IRequestExecutorBuilder builder = services.AddGraphQLServer()
                .ModifyRequestOptions(options =>
                {
                    options.TracingPreference = TracingPreference.OnDemand;
                })
                .AddProjections()
                .AddFiltering()
                .AddSorting()
                .AddErrorFilter(err => err)
                .AddQueryType<QueriesType>()
                .AddType<DetailedHouseType>()
                .AddType<DetailedFlatType>()
                .AddType<DetailedLandType>()
                .AddType<DetailedBusinessType>()
                .AddType<DetailedEstateType>();

            ImmutableArray<Type> typeExtensions = typeof(QueriesType).Assembly.GetTypes()
                .Where(type => type.IsDefined(typeof(ExtendObjectTypeAttribute), true)).ToImmutableArray();

            foreach (Type typeExtension in typeExtensions)
                builder.AddTypeExtension(typeExtension);

            services.AddSingleton<IGraphQlExecutor, GraphQlExecutor>();
            
            return services;
        }
    }
}