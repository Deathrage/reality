﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HotChocolate.Execution;
using HotChocolate.Language;
using System.Threading;
using System.Threading.Tasks;
using HotChocolate.AspNetCore.Serialization;
using Microsoft.AspNetCore.Http;
using HotChocolate.Execution.Serialization;

namespace Reality.UI.Executor
{
    internal class GraphQlExecutor : IGraphQlExecutor
    {
        public IRequestExecutorResolver ExecutorResolver { get; }
        public IHttpRequestParser Parser { get; }
        private readonly JsonQueryResultSerializer _jsonQueryResultSerializer;

        public GraphQlExecutor(IRequestExecutorResolver executorResolver,
            IHttpRequestParser parser)
        {
            ExecutorResolver = executorResolver ?? throw new ArgumentNullException(nameof(executorResolver));
            Parser = parser ?? throw new ArgumentNullException(nameof(parser));

            _jsonQueryResultSerializer = new JsonQueryResultSerializer();
        }

        public async Task ExecuteFunctionsQueryAsync(
            HttpRequest request,
            CancellationToken cancellationToken)
        {
            await using Stream stream = request.HttpContext.Request.Body;

            IReadOnlyList<GraphQLRequest> requestQuery = await Parser
                .ReadJsonRequestAsync(stream, cancellationToken)
                .ConfigureAwait(false);

            QueryRequestBuilder builder = QueryRequestBuilder.New();

            if (requestQuery.Any())
            {
                GraphQLRequest firstQuery = requestQuery.First();

                builder
                    .SetQuery(firstQuery.Query)
                    .SetOperation(firstQuery.OperationName)
                    .SetQueryId(firstQuery.QueryId);

                if (firstQuery.Variables?.Any() == true)
                    builder.SetVariableValues(firstQuery.Variables);
            }

            IRequestExecutor executor =
                await ExecutorResolver.GetRequestExecutorAsync(cancellationToken: cancellationToken);

            IExecutionResult result = await executor.ExecuteAsync(builder.Create(), cancellationToken);
            await _jsonQueryResultSerializer.SerializeAsync((IReadOnlyQueryResult) result,
                request.HttpContext.Response.Body, cancellationToken);
        }
    }
}