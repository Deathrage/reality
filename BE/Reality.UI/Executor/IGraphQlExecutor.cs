﻿using HotChocolate.Execution;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using HotChocolate.AspNetCore.Serialization;

namespace Reality.UI.Executor
{
    public interface IGraphQlExecutor
    {
        IRequestExecutorResolver ExecutorResolver { get; }
        IHttpRequestParser Parser { get; }

        Task ExecuteFunctionsQueryAsync(HttpRequest request, CancellationToken cancellationToken);
    }
}