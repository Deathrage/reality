﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_;
using Reality.UI.GraphQl.CommonEnums;

namespace Reality.UI.GraphQl.DetailedEstate_
{
    public class DetailedFlatType: ObjectType<Flat>
    {
        public new const string Name = "DetailedFlat";

        protected override void Configure(IObjectTypeDescriptor<Flat> descriptor)
        {
            descriptor.Name(Name).BindFieldsExplicitly().ConfigureEstateMembers();
            
            descriptor.Field(_ => _.Furnishment).Type<FurnishmentType>();
            descriptor.Field(_ => _.Construction).Type<ConstructionMaterialType>();
            descriptor.Field(_ => _.Disposition).Type<FlatDispositionType>();

            descriptor.Field(_ => _.UsableSurface).Type<NonNullType<IntType>>();
            descriptor.Field(_ => _.Floor).Type<IntType>();

            descriptor.Field(_ => _.Balcony).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.Terrace).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.Garage).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.Parking).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.Lift).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.LowEnergy).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.NewBuilding).Type<NonNullType<BooleanType>>();
        }
    }
}