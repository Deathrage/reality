﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_;
using Reality.UI.GraphQl.CommonEnums;

namespace Reality.UI.GraphQl.DetailedEstate_
{
    public class DetailedHouseType: ObjectType<House>
    {
        public new const string Name = "DetailedHouse";

        protected override void Configure(IObjectTypeDescriptor<House> descriptor)
        {
            descriptor.Name(Name).BindFieldsExplicitly().ConfigureEstateMembers();

            descriptor.Field(_ => _.Furnishment).Type<FurnishmentType>();

            descriptor.Field(_ => _.UsableSurface).Type<NonNullType<IntType>>();
            descriptor.Field(_ => _.LandSurface).Type<NonNullType<IntType>>();

            descriptor.Field(_ => _.Balcony).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.Terrace).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.Garage).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.LowEnergy).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.NewBuilding).Type<NonNullType<BooleanType>>();
        }
    }
}