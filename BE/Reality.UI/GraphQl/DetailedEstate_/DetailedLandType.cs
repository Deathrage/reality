﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_;
using Reality.UI.GraphQl.CommonEnums;

namespace Reality.UI.GraphQl.DetailedEstate_
{
    public class DetailedLandType: ObjectType<Land>
    {
        public new const string Name = "DetailedLand";

        protected override void Configure(IObjectTypeDescriptor<Land> descriptor)
        {
            descriptor.Name(Name).BindFieldsExplicitly().ConfigureEstateMembers();

            descriptor.Field(_ => _.LandType).Type<LandTypeType>();
            descriptor.Field(_ => _.LandSurface).Type<NonNullType<IntType>>();
        }
    }
}