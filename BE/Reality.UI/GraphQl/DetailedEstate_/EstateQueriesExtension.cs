﻿using System.Threading;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_;
using Reality.UI.DataLoaders;

namespace Reality.UI.GraphQl.DetailedEstate_
{
    [ExtendObjectType(Name = QueriesType.Name)]
    public class EstateQueriesExtension
    {
        [GraphQLType(typeof(NonNullType<GeneralEstateType>))]
        public Task<Estate> GetEstateAsync(int id, [DataLoader] EntityLoader<Estate> dataLoader,
            CancellationToken token)
            => dataLoader.LoadAsync(id, token);
    }
}