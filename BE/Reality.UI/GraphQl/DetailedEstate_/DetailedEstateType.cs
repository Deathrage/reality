﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_;
using Reality.UI.GraphQl.CommonEnums;
using Reality.UI.GraphQl.CommonTypes;

namespace Reality.UI.GraphQl.DetailedEstate_
{
    public static class DetailedEstateMixin
    {
        public static void ConfigureEstateMembers<TEstate>(this IObjectTypeDescriptor<TEstate> descriptor) where TEstate : Estate
        {
            descriptor
                .Implements<NamedEntityType>()
                .Implements<GeneralEstateType>();

            descriptor.Field(_ => _.Id).Type<NonNullType<IntType>>();
            descriptor.Field(_ => _.Name).Type<StringType>();
            
            descriptor.Field(_ => _.EstateType).Type<NonNullType<EstateTypeType>>();
            descriptor.Field(_ => _.Ownership).Type<OwnershipType>();

            descriptor.Field<EstateResolver>(_ => _.GetAddressAsync(default, default, default));

            descriptor.Field(_ => _.Longitude).Type<DecimalType>();
            descriptor.Field(_ => _.Latitude).Type<DecimalType>();

            descriptor.Field(_ => _.HasOwnAddress).Type<NonNullType<BooleanType>>();
            descriptor.Field(_ => _.Locality).Type<StringType>();
        }
    }


    public class DetailedEstateType: ObjectType<Estate>
    {
        public new const string Name = "DetailedEstate";

        protected override void Configure(IObjectTypeDescriptor<Estate> descriptor)
        {
            descriptor.Name(Name).BindFieldsExplicitly().ConfigureEstateMembers();
        }
    }
}