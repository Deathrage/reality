﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_;
using Reality.UI.GraphQl.CommonEnums;

namespace Reality.UI.GraphQl.DetailedEstate_
{
    public class DetailedBusinessType: ObjectType<Business>
    {
        public new const string Name = "DetailedBusiness";

        protected override void Configure(IObjectTypeDescriptor<Business> descriptor)
        {
            descriptor.Name(Name).BindFieldsExplicitly().ConfigureEstateMembers();

            descriptor.Field(_ => _.BusinessType).Type<BusinessTypeType>();

            descriptor.Field(_ => _.UsableSurface).Type<NonNullType<IntType>>();
        }
    }
}