﻿using System.Threading;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_;
using Reality.Core.Models.View;
using Reality.UI.DataLoaders;
using Reality.UI.GraphQl.DetailedAddress_;

namespace Reality.UI.GraphQl.DetailedEstate_
{
    public class EstateResolver
    {
        [GraphQLType(typeof(NonNullType<DetailedAddressType>))]
        public Task<FullAddress> GetAddressAsync([Parent] Estate estate, [DataLoader] EntityLoader<FullAddress> loader,
            CancellationToken token)
            => loader.LoadAsync(estate.AddressId, token);
    }
}