﻿using HotChocolate.Types;
using Reality.UI.GraphQl.CommonEnums;
using Reality.UI.GraphQl.DetailedAddress_;

namespace Reality.UI.GraphQl.DetailedEstate_
{
    public class GeneralEstateType: InterfaceType
    {
        public new const string Name = "GeneralEstate";

        protected override void Configure(IInterfaceTypeDescriptor descriptor)
        {
            descriptor.Name(Name);

            descriptor.Field("id").Type<NonNullType<IntType>>();
            
            descriptor.Field("name").Type<StringType>();

            descriptor.Field("estateType").Type<NonNullType<EstateTypeType>>();
            descriptor.Field("ownership").Type<OwnershipType>();

            descriptor.Field("address").Type<NonNullType<DetailedAddressType>>();

            descriptor.Field("longitude").Type<DecimalType>();
            descriptor.Field("latitude").Type<DecimalType>();

            descriptor.Field("hasOwnAddress").Type<NonNullType<BooleanType>>();
            descriptor.Field("locality").Type<StringType>();
        }
    }
}