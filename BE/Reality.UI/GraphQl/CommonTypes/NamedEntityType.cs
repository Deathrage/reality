﻿using HotChocolate.Types;
using Reality.Common.Model;

namespace Reality.UI.GraphQl.CommonTypes
{
    public class NamedEntityType: InterfaceType<INamedEntity>
    {
        public new const string Name = "NamedEntity";

        protected override void Configure(IInterfaceTypeDescriptor<INamedEntity> descriptor)
        {
            descriptor.Name(Name);
            descriptor.Field(_ => _.Id).Type<NonNullType<IntType>>();
            descriptor.Field(_ => _.Name).Type<StringType>();
        }
    }
}