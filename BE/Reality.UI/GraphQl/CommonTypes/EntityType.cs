﻿using HotChocolate.Types;
using Reality.Common.Model;

namespace Reality.UI.GraphQl.CommonTypes
{
    public class EntityType: InterfaceType<IEntity>
    {
        public new const string Name = "Entity";

        protected override void Configure(IInterfaceTypeDescriptor<IEntity> descriptor)
        {
            descriptor.Name(Name);
            descriptor.Field(_ => _.Id).Type<NonNullType<IntType>>();
        }
    }
}