﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_;
using Reality.UI.GraphQl.CommonEnums;
using Reality.UI.GraphQl.CommonTypes;

namespace Reality.UI.GraphQl.ListableEstate_
{
    public class ListableEstateType: ObjectType<Estate>
    {
        public new const string Name = "ListableEstate";

        protected override void Configure(IObjectTypeDescriptor<Estate> descriptor)
        {
            descriptor.Name(Name).BindFieldsExplicitly().Implements<NamedEntityType>();

            descriptor.Field(_ => _.Id).Type<NonNullType<IntType>>();

            descriptor.Field(_ => _.Name).Type<StringType>();

            descriptor.Field(_ => _.EstateType).Type<EstateTypeType>();

            descriptor.Field(_ => _.Created).Type<NonNullType<DateTimeType>>();
            descriptor.Field(_ => _.Modified).Type<NonNullType<DateTimeType>>();

            descriptor.Field(_ => _.Latitude).Type<DecimalType>();
            descriptor.Field(_ => _.Longitude).Type<DecimalType>();
        }
    }
}