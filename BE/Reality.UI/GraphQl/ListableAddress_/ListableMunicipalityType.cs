﻿using HotChocolate.Types;
using Reality.UI.GraphQl.CommonTypes;
using Reality.UI.Model.ListableAddress_;

namespace Reality.UI.GraphQl.ListableAddress_
{
    public class ListableMunicipalityType: ObjectType<ListableMunicipality>
    {
        protected override void Configure(IObjectTypeDescriptor<ListableMunicipality> descriptor)
        {
            descriptor.BindFieldsExplicitly().Implements<NamedEntityType>();
            
            descriptor.Field(_ => _.Id).Type<NonNullType<IntType>>();
            descriptor.Field(_ => _.Name).Type<StringType>();
        }
    }
}