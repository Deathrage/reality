﻿using HotChocolate.Types;
using Reality.UI.GraphQl.CommonTypes;
using Reality.UI.Model.ListableAddress_;

namespace Reality.UI.GraphQl.ListableAddress_
{
    public class ListableStreetType: ObjectType<ListableStreet>
    {
        protected override void Configure(IObjectTypeDescriptor<ListableStreet> descriptor)
        {
            descriptor.BindFieldsExplicitly().Implements<NamedEntityType>();
            
            descriptor.Field(_ => _.Id).Type<NonNullType<IntType>>();
            descriptor.Field(_ => _.Name).Type<StringType>();
        }
    }
}