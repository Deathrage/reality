﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.UI.GraphQl.CommonEnums
{
    public class FurnishmentType: EnumType<Furnishment>
    {
        protected override void Configure(IEnumTypeDescriptor<Furnishment> descriptor)
        {
            descriptor.BindValuesImplicitly();
        }
    }
}