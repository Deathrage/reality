﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.UI.GraphQl.CommonEnums
{
    public class LandTypeType: EnumType<LandType>
    {
        protected override void Configure(IEnumTypeDescriptor<LandType> descriptor)
        {
            descriptor.BindValuesImplicitly();
        }
    }
}