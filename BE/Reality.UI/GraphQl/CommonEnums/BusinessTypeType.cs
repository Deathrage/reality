﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.UI.GraphQl.CommonEnums
{
    public class BusinessTypeType: EnumType<BusinessType>
    {
        protected override void Configure(IEnumTypeDescriptor<BusinessType> descriptor)
        {
            descriptor.BindValuesImplicitly();
        }
    }
}