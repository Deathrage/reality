﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.UI.GraphQl.CommonEnums
{
    public class EstateTypeType: EnumType<EstateType>
    {
        protected override void Configure(IEnumTypeDescriptor<EstateType> descriptor)
        {
            descriptor.BindValuesImplicitly();
        }
    }
}