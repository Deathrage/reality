﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.UI.GraphQl.CommonEnums
{
    class OwnershipType: EnumType<Ownership>
    {
        protected override void Configure(IEnumTypeDescriptor<Ownership> descriptor)
        {
            descriptor.BindValuesImplicitly();
        }
    }
}
