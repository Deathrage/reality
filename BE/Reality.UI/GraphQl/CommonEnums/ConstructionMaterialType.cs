﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.UI.GraphQl.CommonEnums
{
    public class ConstructionMaterialType: EnumType<ConstructionMaterial>
    {
        protected override void Configure(IEnumTypeDescriptor<ConstructionMaterial> descriptor)
        {
            descriptor.BindValuesImplicitly();
        }
    }
}