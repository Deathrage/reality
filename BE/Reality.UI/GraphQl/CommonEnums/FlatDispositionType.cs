﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.UI.GraphQl.CommonEnums
{
    public class FlatDispositionType: EnumType<FlatDisposition>
    {
        protected override void Configure(IEnumTypeDescriptor<FlatDisposition> descriptor)
        {
            descriptor.BindValuesImplicitly();
        }
    }
}