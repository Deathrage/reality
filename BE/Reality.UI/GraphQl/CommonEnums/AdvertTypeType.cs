﻿using HotChocolate.Types;
using Reality.Core.Models.Entities.Advert_;

namespace Reality.UI.GraphQl.CommonEnums
{
    public class AdvertTypeType: EnumType<AdvertType>
    {
        protected override void Configure(IEnumTypeDescriptor<AdvertType> descriptor)
        {
            descriptor.BindValuesImplicitly();
        }
    }
}