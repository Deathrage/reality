﻿using System;
using HotChocolate.Types;
using Reality.Core.Models.Entities.Advert_;
using Reality.UI.GraphQl.CommonEnums;
using Reality.UI.GraphQl.CommonTypes;

namespace Reality.UI.GraphQl.DetailedAdvert_
{
    public class DetailedAdvertType: ObjectType<Advert>
    {
        public new const string Name = "DetailedAdvert";

        protected override void Configure(IObjectTypeDescriptor<Advert> descriptor)
        {
            descriptor.Name(Name).BindFieldsExplicitly().Implements<NamedEntityType>();

            descriptor.Field(_ => _.Id).Type<NonNullType<IntType>>();

            descriptor.Field(_ => _.Name).Type<StringType>();
            
            descriptor.Field(_ => _.PriceCzk).Name("price").Type<NonNullType<DecimalType>>();
            descriptor.Field<DetailedAdvertExtension>(_ => _.GetCurrency()).Type<NonNullType<StringType>>();

            descriptor.Field(_ => _.Created).Type<NonNullType<DateTimeType>>();
            descriptor.Field(_ => _.Modified).Type<NonNullType<DateTimeType>>();

            descriptor.Field(_ => _.AdvertType).Type<NonNullType<AdvertTypeType>>();
            
            descriptor.Field(_ => _.Link).Resolve(ctx => ctx.Parent<Advert>().Link.ToString());
        }
    }
}