﻿using HotChocolate.Types;
using Reality.Core.Models.Data;

namespace Reality.UI.GraphQl.DetailedAdvert_
{
    public class DetailedAdvertConversionType: ObjectType<Conversion>
    {
        public new const string Name = nameof(Conversion);

        protected override void Configure(IObjectTypeDescriptor<Conversion> descriptor)
        {
            descriptor.Name(Name).BindFieldsImplicitly();
            descriptor.Field(_ => _.SourceModule).Type<NonNullType<StringType>>();
        }
    }
}