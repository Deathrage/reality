﻿using System.Threading;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Reality.Core.Models.Entities.Advert_;
using Reality.UI.DataLoaders;

namespace Reality.UI.GraphQl.DetailedAdvert_
{
    [ExtendObjectType(Name = QueriesType.Name)]
    public class DetailedAdvertQueriesExtension
    {
        [GraphQLType(typeof(NonNullType<DetailedAdvertType>))]
        public Task<Advert> GetAdvertAsync(int id, [DataLoader] EntityLoader<Advert> dataLoader,
            CancellationToken token)
            => dataLoader.LoadAsync(id, token);
    }
}