﻿using System.Threading;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Reality.Core.Models.Entities.Advert_;
using Reality.Core.Models.Entities.Estate_;
using Reality.UI.DataLoaders;
using Reality.UI.GraphQl.DetailedEstate_;

namespace Reality.UI.GraphQl.DetailedAdvert_
{
    [ExtendObjectType(Name = DetailedAdvertType.Name)]
    public class DetailedAdvertExtension
    {
        [GraphQLType(typeof(NonNullType<StringType>))]
        public string GetCurrency() => "CZK";

        [GraphQLType(typeof(NonNullType<GeneralEstateType>))]
        public Task<Estate> GetEstateAsync([Parent] Advert advert, [DataLoader] EntityLoader<Estate> loader,
            CancellationToken token)
            => loader.LoadAsync(advert.EstateId, token);
        
        [GraphQLType(typeof(NonNullType<StringType>))]
        public async Task<string> GetSource([Parent] Advert advert, [DataLoader] ConversionByAdvertLoader loader,
            CancellationToken token)
            => (await loader.LoadAsync(advert.EstateId, token)).SourceModule;
    }
}