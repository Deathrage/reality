﻿using HotChocolate.Types;

namespace Reality.UI.GraphQl
{
    public class QueriesType : ObjectType
    {
        public const string Name = "Queries";

        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor.Name(Name);
        }
    }
}