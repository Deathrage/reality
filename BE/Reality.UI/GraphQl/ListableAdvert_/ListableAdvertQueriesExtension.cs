﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using Microsoft.EntityFrameworkCore;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Core.Models.Entities.Advert_;
using Reality.Core.Models.Entities.Estate_;
using Reality.Core.Models.Entities.Estate_.Enums;
using Reality.UI.Model;
using Reality.UI.Model.EstateInBoundary_;

namespace Reality.UI.GraphQl.ListableAdvert_
{
    [ExtendObjectType(Name = QueriesType.Name)]
    public class ListableAdvertQueriesExtension
    {
        public class AdvertEstate
        {
            public ListableAdvert Advert { get; set; }
            public Estate Estate { get; set; }
        }

        public class LatLon
        {
            public decimal Lat { get; set; }
            public decimal Lon { get; set; }
        }

        //interface IHouseFilter
        //{
        //    public Furnishment? Furnishment { get; }
        //    public bool Balcony { get; }
        //    public bool Terrace { get; }
        //    public bool Garage { get; }
        //    public bool NewBuilding { get; }
        //    public bool LowEnergy { get; }
        //}

        //interface IFlatFilter
        //{
        //    public FlatDisposition? Disposition { get; }
        //    public Furnishment? Furnishment { get; }
        //    public ConstructionMaterial? Construction { get; }
        //    public bool Parking { get; }
        //    public bool Lift { get; }
        //    public bool Balcony { get; }
        //    public bool Terrace { get; }
        //    public bool Garage { get; }
        //    public bool NewBuilding { get; }
        //    public bool LowEnergy { get; }
        //}

        //interface IBusinessFilter
        //{
        //    public BusinessType? BusinessType { get; }
        //}

        //interface ILandFilter
        //{
        //    public LandType? LandType { get; }
        //}

        public class ListableAdvertFilter// : IHouseFilter, IFlatFilter, IBusinessFilter, ILandFilter
        {
            public DateTime From { get; set; }
            public DateTime To { get; set; }
            public string Source { get; set; }
            public EstateType? EstateType { get; set; }
            public AdvertType? AdvertType { get; set; }
            public IEnumerable<LatLon> Boundary { get; set; }
            public Ownership? Ownership { get; set; }
            //public FlatDisposition? Disposition { get; set; }
            //public Furnishment? Furnishment { get; set; }
            //public ConstructionMaterial? Construction { get; set; }
            //public BusinessType? BusinessType { get; set; }
            //public LandType? LandType { get; set; }
            //public bool Parking { get; set; }
            //public bool Lift { get; set; }
            //public bool Balcony { get; set; }
            //public bool Terrace { get; set; }
            //public bool Garage { get; set; }
            //public bool NewBuilding { get; set; }
            //public bool LowEnergy { get; set; }
        }

        [GraphQLType(typeof(NonNullType<IntType>))]
        public Task<int> CountAdvertsAsync(ListableAdvertFilter filter,
            [Service] IDataProvider<EstateInBoundary, EstatesInBoundaryParameter> estatesInBoundaryProvider,
            [Service] IRepository<ListableAdvert> advertRepository,
            [Service] IRepository<Estate> estateRepository)
            => GetAdverts(filter, estatesInBoundaryProvider, advertRepository, estateRepository).CountAsync();

        [GraphQLType(typeof(NonNullType<ListType<NonNullType<ListableAdvertType>>>))]
        [UseProjection]
        public IQueryable<ListableAdvert> GetAdverts(ListableAdvertFilter filter,
            [Service] IDataProvider<EstateInBoundary, EstatesInBoundaryParameter> estatesInBoundaryProvider,
            [Service] IRepository<ListableAdvert> advertRepository,
            [Service] IRepository<Estate> estateRepository)
        {
            IQueryable<ListableAdvert> query = GetQueryRoot(filter, estatesInBoundaryProvider, advertRepository)
                .Where(row => row.Created >= filter.From && row.Created <= filter.To);

            if (filter.AdvertType.HasValue)
                query = query.Where(row => row.AdvertType == filter.AdvertType);

            if (!string.IsNullOrWhiteSpace(filter.Source))
                query = query.Where(row => row.Source == filter.Source);

            query = ApplyEstateFilters(query, filter, estateRepository);

            return query;
        }

        private IQueryable<ListableAdvert> ApplyEstateFilters(
            IQueryable<ListableAdvert> query,
            ListableAdvertFilter filter,
            IRepository<Estate> estateRepository)
        {
            IQueryable<AdvertEstate> queryWithEstate = query
                .Join(estateRepository.AsQueryable(), advert => advert.EstateId, estate => estate.Id,
                    (advert, estate) => new AdvertEstate {Advert = advert, Estate = estate});

            if (filter.EstateType.HasValue)
                queryWithEstate = queryWithEstate.Where(row => row.Estate.EstateType == filter.EstateType);

            if (filter.Ownership.HasValue)
                queryWithEstate = queryWithEstate.Where(row => row.Estate.Ownership == filter.Ownership);
            
            return queryWithEstate.Select(row => row.Advert);
        }

        private IQueryable<ListableAdvert> GetQueryRoot(ListableAdvertFilter filter,
            IDataProvider<EstateInBoundary, EstatesInBoundaryParameter> estatesInBoundaryProvider,
            IRepository<ListableAdvert> advertRepository)
        {
            if (filter?.Boundary is null)
                return advertRepository.AsQueryable();

            EstatesInBoundaryParameter parameter = new EstatesInBoundaryParameter()
            {
                Boundary = filter.Boundary.Select(boundary => new EstatesInBoundaryCoordinates()
                    {Lat = boundary.Lat, Lon = boundary.Lon})
            };

            IQueryable<ListableAdvert> root = estatesInBoundaryProvider.AsQueryable(parameter)
                .Join(advertRepository.AsQueryable(), estate => estate.EstateId, advert => advert.EstateId,
                    (_, advert) => advert);

            return root;
        }
    }
}