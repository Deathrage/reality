﻿using HotChocolate.Types;
using Reality.UI.GraphQl.CommonEnums;
using Reality.UI.GraphQl.CommonTypes;
using Reality.UI.GraphQl.ListableEstate_;
using Reality.UI.Model;

namespace Reality.UI.GraphQl.ListableAdvert_
{
    public class ListableAdvertType: ObjectType<ListableAdvert>
    {
        public new const string Name = nameof(ListableAdvert);

        protected override void Configure(IObjectTypeDescriptor<ListableAdvert> descriptor)
        {
            descriptor.Name(Name).BindFieldsExplicitly().Implements<NamedEntityType>();

            descriptor.Field(_ => _.Id).Type<NonNullType<IntType>>();

            descriptor.Field(_ => _.Name).Type<StringType>();
            
            descriptor.Field(_ => _.Source).Type<NonNullType<StringType>>();

            descriptor.Field(_ => _.PriceCzk).Name("price").Type<NonNullType<DecimalType>>();

            descriptor.Field(_ => _.Created).Type<NonNullType<DateTimeType>>();
            descriptor.Field(_ => _.Modified).Type<NonNullType<DateTimeType>>();

            descriptor.Field(_ => _.AdvertType).Type<NonNullType<AdvertTypeType>>();

            descriptor.Field(_ => _.Estate).Type<ListableEstateType>();
        }
    }
}