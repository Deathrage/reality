﻿using HotChocolate;
using HotChocolate.Types;

namespace Reality.UI.GraphQl.ListableAdvert_
{
    [ExtendObjectType(Name = ListableAdvertType.Name)]
    public class ListableAdvertExtension
    {
        [GraphQLType(typeof(NonNullType<StringType>))]
        public string GetCurrency() => "CZK";
    }
}