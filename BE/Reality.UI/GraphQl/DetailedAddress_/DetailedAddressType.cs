﻿using HotChocolate.Types;
using Reality.Core.Models.View;
using Reality.UI.GraphQl.CommonTypes;

namespace Reality.UI.GraphQl.DetailedAddress_
{
    public class DetailedAddressType: ObjectType<FullAddress>
    {
        public new const string Name = "DetailedAddress";

        protected override void Configure(IObjectTypeDescriptor<FullAddress> descriptor)
        {
            descriptor.Name(Name).BindFieldsExplicitly().Implements<EntityType>();

            descriptor.Field(_ => _.Id).Type<NonNullType<IntType>>();

            descriptor.Field(_ => _.StreetNumber).Type<StringType>();
            descriptor.Field(_ => _.Street).Type<StringType>();
            descriptor.Field(_ => _.Country).Type<StringType>();
            descriptor.Field(_ => _.CountryCode).Type<StringType>();
            descriptor.Field(_ => _.Municipality).Type<StringType>();
            descriptor.Field(_ => _.CountrySubdivision).Type<StringType>();
            descriptor.Field(_ => _.CountrySecondarySubdivision).Type<StringType>();
        }
    }
}