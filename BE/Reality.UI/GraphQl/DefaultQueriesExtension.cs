﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Microsoft.Extensions.Caching.Memory;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Extensions;
using Reality.Core.Models.Data;
using Reality.Core.Models.Entities.Address_;
using Reality.UI.GraphQl.ListableAddress_;
using Reality.UI.Model.ListableAddress_;

namespace Reality.UI.GraphQl
{
    [ExtendObjectType(Name = QueriesType.Name)]
    public class DefaultQueriesExtension
    {
        //[UseProjection]
        //[UseFiltering]
        //[UseSorting]
        //public IQueryable<ListableEstate> GetEstatesInBoundary([Service] IEstatesInBoundaryProvider provider,
        //    IEnumerable<EstatesInBoundaryCoordinates> boundary)
        //    => provider.AsQueryable(new EstatesInBoundaryParameter() {Boundary = boundary})
        //        .Include(s => s.Estate)
        //        .Select(s => s.Estate);

        [GraphQLType(typeof(NonNullType<ListType<NonNullType<StringType>>>))]
        public async Task<IEnumerable<string>> GetSourceModuleNamesAsync(
            [Service] IRepository<Conversion> conversionRepository, [Service] IMemoryCache memoryCache)
        {
            IEnumerable<string> cachedModules = await memoryCache.GetOrCreateAsync("moduleNames", async entry =>
            {
                IEnumerable<string> modules = await conversionRepository.AsQueryable()
                    .Select(row => row.SourceModule)
                    .Distinct()
                    .ToImmutableArrayAsync();

                return modules;
            });

            return cachedModules;
        }

        [GraphQLType(typeof(NonNullType<ListType<NonNullType<ListableCountryType>>>))]
        public async Task<IEnumerable<ListableCountry>> GetCountriesAsync(
            [Service] IRepository<ListableCountry> countryRepository, [Service] IMemoryCache memoryCache)
        {
            IEnumerable<ListableCountry> cachedCities = await memoryCache.GetOrCreateAsync("listableCountries",
                async entry =>
                {
                    IEnumerable<ListableCountry> modules = await countryRepository.AsQueryable()
                        .Where(row => row.Name != null)
                        .Select(row => new ListableCountry() {Id = row.Id, Name = row.Name})
                        .ToImmutableArrayAsync();

                    return modules;
                });

            return cachedCities;
        }

        [GraphQLType(typeof(NonNullType<ListType<NonNullType<ListableMunicipalityType>>>))]
        public async Task<IEnumerable<ListableMunicipality>> GetMunicipalitiesAsync(int countryId,
            [Service] IRepository<Municipality> municipalityRepository, [Service] IMemoryCache memoryCache)
        {
            IEnumerable<ListableMunicipality> cachedMunicipalities = await memoryCache.GetOrCreateAsync(
                "listableMunicipality", async entry =>
                {
                    IEnumerable<ListableMunicipality> modules = await municipalityRepository.AsQueryable()
                        .Where(row => row.Name != null && row.CountryId == countryId)
                        .Select(row => new ListableMunicipality() {Id = row.Id, Name = row.Name})
                        .ToImmutableArrayAsync();

                    return modules;
                });

            return cachedMunicipalities;
        }

        [GraphQLType(typeof(NonNullType<ListType<NonNullType<ListableStreetType>>>))]
        public async Task<IEnumerable<ListableStreet>> GetStreetsAsync(int municipalityId,
            [Service] IRepository<Street> streetRepository, [Service] IMemoryCache memoryCache)
        {
            IEnumerable<ListableStreet> cachedStreets = await memoryCache.GetOrCreateAsync("listableStreets", async entry =>
            {
                IEnumerable<ListableStreet> modules = await streetRepository.AsQueryable()
                    .Where(row => row.Name != null && row.MunicipalityId == municipalityId)
                    .Select(row => new ListableStreet() {Id = row.Id, Name = row.Name})
                    .ToImmutableArrayAsync();
                
                return modules;
            });

            return cachedStreets;
        }
    }
}