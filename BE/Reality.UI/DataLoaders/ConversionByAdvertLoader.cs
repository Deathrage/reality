﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GreenDonut;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Core.Models.Data;
using Reality.Common.Extensions;

namespace Reality.UI.DataLoaders
{
    public class ConversionByAdvertLoader : DataLoaderBase<int, Conversion>
    {
        private readonly IServiceProvider _provider;

        public ConversionByAdvertLoader(IBatchScheduler batchScheduler, IServiceProvider provider) : base(
            batchScheduler)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
        }

        public ConversionByAdvertLoader(IBatchScheduler batchScheduler, DataLoaderOptions<int>? options,
            IServiceProvider provider) : base(batchScheduler, options)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
        }

        protected override async ValueTask<IReadOnlyList<Result<Conversion>>> FetchAsync(IReadOnlyList<int> keys,
            CancellationToken cancellationToken)
        {
            IRepository<Conversion> repository = _provider.GetRequiredService<IRepository<Conversion>>().Clone();

            IReadOnlyList<Conversion> entities = await repository.AsQueryable().AsNoTracking()
                .Where(row => keys.Contains(row.TargetAdvertId)).ToImmutableArrayAsync(cancellationToken);

            IReadOnlyList<Result<Conversion>> results = keys.Select(id =>
            {
                Conversion conversion = entities.SingleOrDefault(row => row.TargetAdvertId == id);
                if (conversion == default)
                    return Result<Conversion>.Reject(
                        new KeyNotFoundException(
                            $"{typeof(Conversion).FullName} was not found for target advert {id}"));

                return Result<Conversion>.Resolve(conversion);
            }).ToImmutableArray();

            return results;
        }
    }
}