﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GreenDonut;
using Microsoft.Extensions.DependencyInjection;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Model;

namespace Reality.UI.DataLoaders
{
    public class EntityLoader<TEntity> : DataLoaderBase<int, TEntity>
        where TEntity : class, IEntity
    {
        private readonly IServiceProvider _provider;

        public EntityLoader(IBatchScheduler batchScheduler, IServiceProvider provider) : base(batchScheduler)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
        }

        public EntityLoader(IBatchScheduler batchScheduler, DataLoaderOptions<int>? options, IServiceProvider provider)
            : base(batchScheduler, options)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
        }

        protected override async ValueTask<IReadOnlyList<Result<TEntity>>> FetchAsync(IReadOnlyList<int> keys,
            CancellationToken cancellationToken)
        {
            IEntityRepository<TEntity> repository = _provider.GetRequiredService<IEntityRepository<TEntity>>().Clone();

            IReadOnlyList<TEntity> entities = await repository.GetByIdsAsync(keys, cancellationToken, false);

            return entities.Select((entity, index) =>
            {
                if (entity == default)
                    return Result<TEntity>.Reject(
                        new KeyNotFoundException(
                            $"Entity {typeof(TEntity).FullName} was not found for id {keys[index]}"));

                return Result<TEntity>.Resolve(entity);
            }).ToImmutableArray();
        }
    }
}