using AutoMapper;
using NUnit.Framework;
using Reality.BezRealitky.Mapping;

namespace Reality.BezRealitky.Tests
{
    public class MappingTests
    {
        [Test]
        public void Mapping_ValidateConfiguration()
        {
            var configuration = new MapperConfiguration(opts => opts.AddProfile(new BezRealitkyMapping()));

            configuration.AssertConfigurationIsValid();
        }
    }
}