﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.SReality.Model;

namespace Reality.SReality.DataAccess
{
    internal class MajorAdvertLabelConfiguration: IEntityTypeConfiguration<MajorAdvertLabel>
    {
        public void Configure(EntityTypeBuilder<MajorAdvertLabel> b)
        {
            b.ToTable("MajorAdvertLabels", "sreality");

            b.HasKey(obj => new {obj.AdvertId, obj.Label});
        }
    }
}