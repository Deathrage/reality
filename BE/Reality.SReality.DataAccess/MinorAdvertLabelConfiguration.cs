﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.SReality.Model;

namespace Reality.SReality.DataAccess
{
    internal class MinorAdvertLabelConfiguration: IEntityTypeConfiguration<MinorAdvertLabel>
    {
        public void Configure(EntityTypeBuilder<MinorAdvertLabel> b)
        {
            b.ToTable("MinorAdvertLabels", "sreality");

            b.HasKey(obj => new {obj.AdvertId, obj.Label});
        }
    }
}