﻿using Microsoft.EntityFrameworkCore;

namespace Reality.SReality.DataAccess
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder ConfigureSRealityModel(this ModelBuilder builder)
            => builder.ApplyConfiguration(new AdvertConfiguration())
                .ApplyConfiguration(new MajorAdvertLabelConfiguration())
                .ApplyConfiguration(new MinorAdvertLabelConfiguration());
    }
}