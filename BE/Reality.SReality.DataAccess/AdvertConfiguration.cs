﻿using Microsoft.EntityFrameworkCore;
using Reality.SReality.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reality.Common.DataAccess.Extensions;

namespace Reality.SReality.DataAccess
{
    internal class AdvertConfiguration: IEntityTypeConfiguration<Advert>
    {
        public void Configure(EntityTypeBuilder<Advert> b)
        {
            b.ToTable("Adverts", "sreality");

            b.ConfigureEntityKey();
            b.HasMany(p => p.MajorAdvertLabels).WithOne(p => p.Advert);
            b.HasMany(p => p.MinorAdvertLabels).WithOne(p => p.Advert);
            b.Property(p => p.Link).HasUriColumn();
            b.Property(p => p.PriceCzk).HasDecimalPriceColumnType();
            b.Property(p => p.Longitude).HasDecimalGeoCoordinateColumnType();
            b.Property(p => p.Latitude).HasDecimalGeoCoordinateColumnType();
        }
    }
}
