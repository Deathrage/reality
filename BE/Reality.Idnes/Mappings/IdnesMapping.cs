﻿using System;
using AutoMapper;
using Reality.Core.Models.Entities.Advert_;
using Reality.Core.Models.Entities.Estate_;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.Idnes.Mappings
{
    public class IdnesMapping: Profile
    {
        public IdnesMapping()
        {
            #region Estate

             CreateMap<Idnes.Model.Advert, Estate>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.GeoPoint, opts => opts.Ignore())
                .ForMember(dst => dst.AddressId, opts => opts.Ignore())
                .ForMember(dst => dst.Address, opts => opts.Ignore())
                .ForMember(dst => dst.Adverts, opts => opts.Ignore())
                .ForMember(dst => dst.HasOwnAddress, opts => opts.Ignore())
                .ForMember(dst => dst.Name, opts => opts.MapFrom((src, dst) =>
                {
                    string objectTypeName = src.EstateType switch
                    {
                        Idnes.Model.EstateType.Flat => "Byt",
                        Idnes.Model.EstateType.House => "Dům",
                        Idnes.Model.EstateType.Land => "Pozemek",
                        Idnes.Model.EstateType.Commerce => "Nebytový prostor",
                        _ => "Objekt"
                    };

                    return $"{objectTypeName} {src.UsableSurface} m2, {src.Locality}";
                }))
                .ForMember(dst => dst.EstateType, opts => opts.MapFrom((src, dst) =>
                {
                    return src.EstateType switch
                    {
                        Idnes.Model.EstateType.Flat => EstateType.Flat,
                        Idnes.Model.EstateType.House => EstateType.House,
                        Idnes.Model.EstateType.Land => EstateType.Land,
                        Idnes.Model.EstateType.Commerce => EstateType.Business,
                        _ => EstateType.Miscellaneous
                    };
                }))
                .ForMember(dst => dst.Ownership, opts => opts.MapFrom((src, dst) =>
                {
                    return src.Ownership switch
                    {
                        "osobní" => Ownership.Personal,
                        "s.r.o." => Ownership.Personal,
                        "družstevní" => Ownership.Collective,
                        _ => default
                    };
                }));

             CreateMap<Idnes.Model.Advert, Flat>(MemberList.Destination)
                 .IncludeBase<Idnes.Model.Advert, Estate>()
                 .ForMember(dst => dst.UsableSurface, opts => opts.MapFrom(src => src.UsableSurface))
                 .ForMember(dst => dst.Furnishment, opts => opts.MapFrom((src, dst) =>
                 {
                     return src.Furnishment switch
                     {
                         "nezařízený" => Furnishment.NotFurnished,
                         "částečně zařízený" => Furnishment.PartiallyFurnished,
                         "zařízený" => Furnishment.FullyFurnished,
                         _ => default
                     };
                 }))
                 .ForMember(dst => dst.Construction, opts => opts.MapFrom((src, dst) =>
                 {
                     return src.Construction switch
                     {
                         "cihlová" => ConstructionMaterial.Brick,
                         "panelová" => ConstructionMaterial.Panel,
                         "dřevěná" => ConstructionMaterial.Wood,
                         "smíšená" => ConstructionMaterial.Other,
                         "kamenná" => ConstructionMaterial.Other,
                         "montovaná" => ConstructionMaterial.Other,
                         "skeletová" => ConstructionMaterial.Other,
                         _ => default
                     };
                 }))
                 .ForMember(dst => dst.Disposition, opts => opts.MapFrom((src, dst) =>
                 {
                     return src.Disposition switch
                     {
                         "1+kk" => FlatDisposition.Dispition1_KK,
                         "1+1" => FlatDisposition.Dispition1_1,

                         "2+kk" => FlatDisposition.Dispition2_KK,
                         "2+1" => FlatDisposition.Dispition2_1,

                         "3+kk" => FlatDisposition.Dispition3_KK,
                         "3+1" => FlatDisposition.Dispition3_1,

                         "4+kk" => FlatDisposition.Dispition4_KK,
                         "4+1" => FlatDisposition.Dispition4_1,

                         "5+kk" => FlatDisposition.Dispition5_KK,
                         "5+1" => FlatDisposition.Dispition5_1,


                         "atypický" => FlatDisposition.Other,

                         _ => default
                     };
                 }))
                 .ForMember(dst => dst.Floor, opts => opts.MapFrom(src => src.Floor))
                 .ForMember(dst => dst.LowEnergy, opts => opts.MapFrom((src, dst) => src.Penb?.StartsWith("A") == true))
                 .ForMember(dst => dst.NewBuilding, opts => opts.MapFrom(src => src.State == "novostavba"));

            #endregion

            #region Advert

            CreateMap<Idnes.Model.Advert, Advert>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.Conversion, opts => opts.Ignore())
                .ForMember(dst => dst.EstateId, opts => opts.Ignore())
                .ForMember(dst => dst.Estate, opts => opts.MapFrom((src, dst, prop, ctx) =>
                {
                    switch (src.EstateType)
                    {
                        case Model.EstateType.Flat: return ctx.Mapper.Map<Flat>(src);
                        default:
                            throw new NotImplementedException();
                    }
                }))
                .ForMember(dst => dst.Conversion, opts => opts.Ignore());

            #endregion
        }
    }
}