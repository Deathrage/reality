﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Reality.Core.AdvertConverter_;
using Reality.Core.Models.Data;
using Reality.Idnes.Model;

namespace Reality.Idnes.Mappings
{
    internal class IdnesConverter: IAdvertConverter<Advert>
    {
        private readonly IMapper _mapper;

        public string ModuleName => "idnes";

        public IdnesConverter(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public IReadOnlyList<ConversionItem> Convert(IReadOnlyList<Advert> sources)
        {
            return sources
                .Select(advert =>
                {
                    Conversion conversion = new Conversion() {SourceModule = ModuleName, SourceAdvertId = advert.Id};
                    Core.Models.Entities.Advert_.Advert coreAdvert = _mapper.Map<Core.Models.Entities.Advert_.Advert>(advert);

                    return new ConversionItem(conversion, coreAdvert, coreAdvert.Estate, default, default, default,
                        default);
                })
                .ToArray();
        }
    }
}