﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using Newtonsoft.Json;
using Reality.Common.Extensions;
using Reality.Idnes.Extensions;
using Reality.Idnes.FloorExtractor_;
using Reality.Idnes.LinkExtractor_;
using Reality.Idnes.Model;

namespace Reality.Idnes
{
    internal class FlatCrawler
    {
        private static readonly string _flatRentUrl =
            "https://reality.idnes.cz/s/pronajem/byty/praha/?s-qc%5BarticleAge%5D=1";
        private static readonly string _flatSellUrl =
            "https://reality.idnes.cz/s/prodej/byty/praha/?s-qc%5BarticleAge%5D=1";

        private readonly ILinkExtractor _linkExtractor;
        private readonly IBrowsingContext _browsingContext;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IFloorExtractor _floorExtractor;

        public FlatCrawler(ILinkExtractor linkExtractor, IBrowsingContext browsingContext,
            IHttpClientFactory httpClientFactory, IFloorExtractor floorExtractor)
        {
            _linkExtractor = linkExtractor ?? throw new ArgumentNullException(nameof(linkExtractor));
            _browsingContext = browsingContext ?? throw new ArgumentNullException(nameof(browsingContext));
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _floorExtractor = floorExtractor ?? throw new ArgumentNullException(nameof(floorExtractor));
        }

        public FlatCrawler(ILinkExtractor linkExtractor)
        {
            _linkExtractor = linkExtractor ?? throw new ArgumentNullException(nameof(linkExtractor));
        }

        public async Task<IEnumerable<Advert>> GetDailyAdvertsAsync()
        {
            List<Advert> adverts = new List<Advert>();
            
            IEnumerable<string> allRentLinks = await _linkExtractor.ExtractFromPageAndAllNextPagesAsync(_flatRentUrl);
            // Has to be done serially in order not to get Too Many Requests
            foreach (string link in allRentLinks)
            {
                Advert newAdvert = await ExtractAdvertAsync(link, AdvertType.Rent);
                if (newAdvert == default) continue;

                adverts.Add(newAdvert);
            }

            IEnumerable<string> allSellLinks = await _linkExtractor.ExtractFromPageAndAllNextPagesAsync(_flatSellUrl);
            // Has to be done serially in order not to get Too Many Requests
            foreach (string link in allSellLinks)
            {
                Advert newAdvert = await ExtractAdvertAsync(link, AdvertType.Sell);
                if (newAdvert == default) continue;

                adverts.Add(newAdvert);
            }

            return adverts;
        }

        protected async Task<Advert> ExtractAdvertAsync(string url, AdvertType advertType)
        {
            IDocument listPageDocument =
                await _browsingContext.OpenUsingHttpClientAsync(new Url(url), _httpClientFactory.CreateClient());

            string name = listPageDocument.QuerySelector(".b-detail__title")?.Text();

            if (string.IsNullOrWhiteSpace(name))
                return default;

            string locality = listPageDocument.QuerySelector(".b-detail__info")?.Text();
            string price = listPageDocument.QuerySelector(".b-detail__price")?.Text();

            ImmutableList<IElement> dts = listPageDocument.QuerySelectorAll<IElement>(".b-definition-columns dt").ToImmutableList();
            ImmutableList<IElement> dds = listPageDocument.QuerySelectorAll<IElement>(".b-definition-columns dd").ToImmutableList();

            int constructionIndex = GetIndexByText(dts, "Konstrukce budovy");
            int stateIndex = GetIndexByText(dts, "Stav bytu");
            if (stateIndex == -1)
                stateIndex = GetIndexByText(dts, "Stav budovy");
            int ownershipIndex = GetIndexByText(dts, "Vlastnictví");

            int surfaceIndex = GetIndexByText(dts, "Užitná plocha");
            string surface = dds.GetSafe(surfaceIndex)?.Text();

            int floorIndex = GetIndexByText(dts, "Podlaží");
            
            int cellarIndex = GetIndexByText(dts, "Sklep");
            int liftIndex = GetIndexByText(dts, "Výtah");
            int balconyIndex = GetIndexByText(dts, "Balkon");
            int terraceIndex = GetIndexByText(dts, "Terasa");
            int internetIndex = GetIndexByText(dts, "Internet");
            int landlineIndex = GetIndexByText(dts, "Telefon");
            int televisionIndex = GetIndexByText(dts, "Televize");
            
            int parkingIndex = GetIndexByText(dts, "Parkování");

            int furnishmentIndex = GetIndexByText(dts, "Vybavení");

            int penbIndex = GetIndexByText(dts, "PENB");

            int loggiaIndex = GetIndexByText(dts, "Lodžie");

            int easyAccessIndex = GetIndexByText(dts, "Bezbariérový přístup");

            string[] urlSplit = url.Split("/");
            string externalId = urlSplit[new Index(2, true)];

            string disposition = null;
            if (name.Contains("atypického"))
                disposition = "atypický";
            else
            {
                Regex dispositionRegex = new Regex(@"\d\+(\d|kk)");
                disposition = dispositionRegex.Match(name)?.Value;
            }

            string mapFunc = listPageDocument.QuerySelectorAll<IHtmlScriptElement>("script")
                .SingleOrDefault(x => x.InnerHtml.Contains("var mtMap; function runMap()"))?.InnerHtml;
            string locationMatch = new Regex(@"\[\d+\.\d+,\d+\.\d+\]").Match(mapFunc ?? "")?.Value;
            decimal? longitude = null;
            decimal? latitude = null;
            if (!string.IsNullOrWhiteSpace(locationMatch))
            {
                decimal[] geolocs = JsonConvert.DeserializeObject<decimal[]>(locationMatch);
                if (geolocs.Length == 2)
                {
                    longitude = geolocs.First();
                    latitude = geolocs.Last();
                }
            }

            return new Advert()
            {
                AdvertType = advertType,
                EstateType = EstateType.Flat,
                ExternalId = externalId,
                Link = new Uri(url),
                Name = name,
                Locality = locality?.Trim(),
                PriceCzk = decimal.TryParse(string.Join("", new Regex(@"\d+").Matches(price ?? "")), out decimal result) ? result : default,
                Construction = dds.GetSafe(constructionIndex)?.Text(),
                State = dds.GetSafe(stateIndex)?.Text(),
                Ownership = dds.GetSafe(ownershipIndex)?.Text(),
                UsableSurface = string.IsNullOrWhiteSpace(surface) ? default : int.TryParse(new Regex(@"\d+").Match(surface).Value, out int resultSurface) ? resultSurface : default,
                Floor = _floorExtractor.Get(dds.GetSafe(floorIndex)?.Text()),
                Lift = liftIndex > -1,
                Cellar = cellarIndex > -1,
                Balcony = balconyIndex > -1,
                Terrace = terraceIndex > -1,
                Parking = parkingIndex > -1,
                Garage = dds.GetSafe(parkingIndex)?.Text() == "garáž",
                Internet = internetIndex > -1,
                Landline = landlineIndex > -1,
                CableTelevision =  televisionIndex > -1,
                Furnishment =  dds.GetSafe(furnishmentIndex)?.Text(),
                Penb = dds.GetSafe(penbIndex)?.Text(),
                Loggia = loggiaIndex > -1,
                EasyAccess = easyAccessIndex > -1,
                Disposition = disposition,
                Longitude = longitude,
                Latitude = latitude
            };
        }

        private static int GetIndexByText(ImmutableList<IElement> elements, string text) 
            => elements.FindIndex(item => item.Text().Contains(text));
        
    }
}