﻿using AngleSharp;
using Microsoft.Extensions.DependencyInjection;
using Reality.Core.AdvertConverter_;
using Reality.Idnes.FloorExtractor_;
using Reality.Idnes.LinkExtractor_;
using Reality.Idnes.Mappings;
using Reality.Idnes.Model;

namespace Reality.Idnes.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddIdnes(this IServiceCollection services) =>
            services.AddTransient(provider => BrowsingContext.New(Configuration.Default))
                .AddTransient<ILinkExtractor, LinkExtractor>()
                .AddTransient<IFloorExtractor, FloorExtractor>()
                .AddTransient<FlatCrawler>()
                .AddTransient<IIdnesScrapper, IdnesScrapper>()
                .AddTransient<IAdvertConverter<Advert>, IdnesConverter>();
    }
}