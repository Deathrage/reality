﻿using System.Net.Http;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;

namespace Reality.Idnes.Extensions
{
    public static class BrowsingContextExtensions
    {
        public static async Task<IDocument> OpenUsingHttpClientAsync(this IBrowsingContext context, Url link,
            HttpClient client)
        {
            string response = await client.GetStringAsync(link.Href);

            return await context.OpenAsync(res => res.Content(response).Address($"{link.Scheme}://{link.Host}"));
        }
    }
}