﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Reality.Common;
using Reality.Common.DataAccess.Abstractions.Interfaces;
using Reality.Common.Extensions;
using Reality.Idnes.Model;

namespace Reality.Idnes
{
    public interface IIdnesScrapper: IScrapper
    {

    }

    internal class IdnesScrapper: IIdnesScrapper
    {
        private readonly FlatCrawler _flatRentsCrawler;
        private readonly IRepository<Advert> _advertRepository;

        public IdnesScrapper(FlatCrawler flatRentsCrawler, IRepository<Advert> advertRepository)
        {
            _flatRentsCrawler = flatRentsCrawler ?? throw new ArgumentNullException(nameof(flatRentsCrawler));
            _advertRepository = advertRepository ?? throw new ArgumentNullException(nameof(advertRepository));
        }

        public async Task<IEnumerable<int>> RunAsync()
        {
            ImmutableArray<Advert> adverts = (await _flatRentsCrawler.GetDailyAdvertsAsync()).ToImmutableArray();
            ImmutableArray<string> externalIds = adverts.Select(i => i.ExternalId).ToImmutableArray();

            ImmutableArray<string> externalIdsInDb = await _advertRepository.AsQueryable().Select(item => item.ExternalId)
                .Where(item => externalIds.Contains(item))
                .ToImmutableArrayAsync();

            ImmutableArray<Advert> finalAdverts = adverts.Where(a => !externalIdsInDb.Contains(a.ExternalId)).ToImmutableArray();

            await _advertRepository.AddAsync(finalAdverts);
            await _advertRepository.SaveAsync();

            return finalAdverts.Select(item => item.Id);
        }
    }
}