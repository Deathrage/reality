﻿using System.Text.RegularExpressions;
using Reality.Common.Extensions;

namespace Reality.Idnes.FloorExtractor_
{
    public class FloorExtractor : IFloorExtractor
    {
        public int? Get(string floorString)
        {
            if (string.IsNullOrWhiteSpace(floorString))
                return default;
            if (floorString.In("20. patro a vyšší", "-3. patro a nižší"))
                return default;

            Regex floorPartRegex = new Regex(@"\d+.\s(N|P)P");
            string match = floorPartRegex.Match(floorString)?.Value;
            if (string.IsNullOrWhiteSpace(match))
                return default;

            Regex numberRegex = new Regex(@"\d+");
            bool success = int.TryParse(numberRegex.Match(match)?.Value, out int floorResult);

            if (!success)
                return default;

            return match.Contains("PP") ? -1 * floorResult : floorResult;
        }
    }
}