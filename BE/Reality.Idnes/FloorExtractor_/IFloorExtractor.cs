﻿namespace Reality.Idnes.FloorExtractor_
{
    public interface IFloorExtractor
    {
        public int? Get(string floorString);
    }
}