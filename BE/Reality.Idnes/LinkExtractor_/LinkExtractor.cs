﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using Reality.Idnes.Extensions;

namespace Reality.Idnes.LinkExtractor_
{
    internal class LinkExtractor: ILinkExtractor
    {
        private readonly IBrowsingContext _browsingContext;
        private readonly IHttpClientFactory _httpClientFactory; 

        protected async Task<IEnumerable<string>> GetItemLinksFromAllPagesAsync(Url startingPoint)
        {
            List<string> foundItemLinks = new List<string>();

            Url currentLink = startingPoint;
            while (currentLink != null)
            {
                (IEnumerable<string> newItemLinks, Url nextPageLink) =
                    await ExtractItemLinksFromListPageAsync(currentLink);

                foundItemLinks.AddRange(newItemLinks);
                currentLink = nextPageLink;
            }

            return foundItemLinks;
        }

        protected async Task<(IEnumerable<string> FoundItemLinks, Url NextPageLink)> ExtractItemLinksFromListPageAsync(Url link)
        {
            IDocument listPageDocument =
                await _browsingContext.OpenUsingHttpClientAsync(link, _httpClientFactory.CreateClient());

            IEnumerable<string> links = GetLinksInTheList(listPageDocument);

            string nextPage = GetNextPageLink(listPageDocument);
            Url nextPageLink = string.IsNullOrWhiteSpace(nextPage) ? default : new Url(nextPage);

            return (links, nextPageLink);
        }

        protected string GetNextPageLink(IDocument page)
        {
            IEnumerable<IElement> pagingElements = page.QuerySelectorAll(".paging__item");

            IElement activeElement = pagingElements.SingleOrDefault(element => element.ClassList.Contains("btn--grey"));
            if (activeElement == default)
                return default;

            int activePage = int.Parse(activeElement.Text());

            IElement nextActiveElement = pagingElements.SingleOrDefault(element =>
                int.TryParse(element.Text(), out int pageNumber) && pageNumber == (activePage + 1));
            return ((IHtmlAnchorElement) nextActiveElement)?.Href;
        }

        protected IEnumerable<string> GetLinksInTheList(IDocument page)
        {
            IEnumerable<IElement> matchElements = page.QuerySelectorAll(".c-list-products__link");
            ImmutableArray<string> links = matchElements.Where(element => element is IHtmlAnchorElement).Cast<IHtmlAnchorElement>()
                .Select(anchor => anchor.Href).ToImmutableArray();

            return links;
        }

        public Task<IEnumerable<string>> ExtractFromPageAndAllNextPagesAsync(string url)
            => GetItemLinksFromAllPagesAsync(new Url(url));

        public LinkExtractor(IBrowsingContext browsingContext, IHttpClientFactory httpClientFactory)
        {
            _browsingContext = browsingContext ?? throw new ArgumentNullException(nameof(browsingContext));
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
        }
    }
}