﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reality.Idnes.LinkExtractor_
{
    internal interface ILinkExtractor
    {
        Task<IEnumerable<string>> ExtractFromPageAndAllNextPagesAsync(string url);
    }
}