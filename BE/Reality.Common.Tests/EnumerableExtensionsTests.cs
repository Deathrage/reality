using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Reality.Common.Extensions;

namespace Reality.Common.Tests
{
    public class EnumerableExtensionsTests
    {
        [Test]
        public void EnumerableExtensions_ToBatch_Full()
        {
            int[] source = new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            IEnumerable<IEnumerable<int>> batches = source.ToBatch(5);

            CollectionAssert.AreEqual(new int[][] {new [] {1, 2, 3, 4, 5}, new [] {6, 7, 8, 9, 10}}, batches);
        }

        [Test]
        public void EnumerableExtensions_ToBatch_Partial()
        {
            int[] source = new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            IEnumerable<IEnumerable<int>> batches = source.ToBatch(6);

            CollectionAssert.AreEqual(new int[][] {new [] {1, 2, 3, 4, 5,6}, new [] {7, 8, 9, 10}}, batches);
        }

        [Test]
        public void EnumerableExtensions_ToBatch_Empty()
        {
            int[] source = new int[0];

            IEnumerable<IEnumerable<int>> batches = source.ToBatch(6);

            CollectionAssert.AreEqual(new int[][] { new int[0] }, batches);
        }

        [Test]
        public void EnumerableExtensions_ToBatch_OneBatch()
        {
            int[] source = new int[] { 1,2,3,4};

            Assert.Throws<ArgumentException>(() =>
            {
                source.ToBatch(1).ToArray();
            });
        }
    }
}