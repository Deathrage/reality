using AutoMapper;
using NUnit.Framework;
using Reality.Idnes.Mappings;

namespace Reality.Idnes.Tests
{
    public class MappingTests
    {
        [Test]
        public void Mapping_ValidateConfiguration()
        {
            var configuration = new MapperConfiguration(opts => opts.AddProfile(new IdnesMapping()));

            configuration.AssertConfigurationIsValid();
        }
    }
}