using NUnit.Framework;
using Reality.Idnes.FloorExtractor_;

namespace Reality.Idnes.Tests
{
    public class FloorExtractorTests
    {
        public static readonly object[] Data = new object[]
        {
            new object[] {"20. patro a vy���", null},
            new object[] {"sn�en� p��zem� (1. PP = podzemn� podla��)", -1},
            new object[] {"p��zem� (1. NP = nadzemn� podla��)", 1},
            new object[] {"-3. patro a ni���", null},
            new object[] {"12. patro (13. NP)", 13},
            new object[] {"1. patro (2. NP)", 2}
        };

        [Test]
        [TestCaseSource(nameof(Data))]
        public void FloorExtractor_Extract(string input, int? output)
        {
            FloorExtractor sut = new FloorExtractor();
            int? result = sut.Get(input);

            Assert.AreEqual(output, result);
        }
    }
}