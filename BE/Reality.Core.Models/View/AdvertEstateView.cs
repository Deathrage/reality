﻿using Reality.Common.Model;
using System;
using Reality.Core.Models.Entities.Advert_;
using Reality.Core.Models.Entities.Estate_;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.Core.Models.View
{
    public class AdvertEstateView: INamedEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int EstateId { get; set; }
        public string EstateName { get; set; }
        public AdvertType AdvertType { get; set; }

        public decimal PriceCzk { get; set; }

        public EstateType EstateType { get; set; }
        public Ownership? Ownership { get; set; }

        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        
        public int LandSurface { get; set; }
        public int UsableSurface { get; set; }
		
        public Furnishment? Furnishment { get; set; }
        public ConstructionMaterial? Construction { get; set; }
        public FlatDisposition? Disposition { get; set; }

        public int? FlatFloor { get; set; }

        public bool Balcony { get; set; }
        public bool Terrace { get; set; }
        public bool Garage { get; set; }
        public bool Parking { get; set; }
        public bool Lift { get; set; }
        public bool LowEnergy { get; set; }
        public bool NewBuilding { get; set; }
        
        public LandType? LandType { get; set; }
        
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        
        public DateTime EstateCreated { get; set; }
        public DateTime EstateModified { get; set; }
        
        public int MunicipalityId { get; set; }
        public string MunicipalityName { get; set; }
        public int StreetId { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; } 
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }

        public Uri Link { get; set; }
    }
}