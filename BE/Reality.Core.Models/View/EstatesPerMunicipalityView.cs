﻿namespace Reality.Core.Models.View
{
    public class EstatesPerMunicipalityView
    {
        public int EstateCount { get; set; }
        public int MunicipalityId { get; set; }
        public string MunicipalityName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string CountrySecondarySubdivision { get; set; }
        public string CountrySecondary { get; set; }
    }
}