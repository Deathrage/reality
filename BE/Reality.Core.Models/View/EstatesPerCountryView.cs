﻿namespace Reality.Core.Models.View
{
    public class EstatesPerCountryView
    {
        public int EstateCount { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }
}