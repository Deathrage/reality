﻿using Reality.Common.Model;

namespace Reality.Core.Models.View
{
    public class FullAddress: IEntity
    {
        public int Id { get; set; }
        public int StreetId { get; set; }

        public string StreetNumber { get; set; } 
        public string Street { get; set; }

        public int MunicipalityId { get; set; }
        public string Municipality { get; set; }
		
        public int CountryId { get; set; }
        public string CountrySubdivision { get; set; }
        public string CountrySecondarySubdivision { get; set; }

        public string Country { get; set; }
        public string CountryCode { get; set; }
    }
}