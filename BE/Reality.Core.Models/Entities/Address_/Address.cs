﻿using System;
using System.Collections.Generic;
using Reality.Common.Model;
using Reality.Common.Model.Mixins;
using Reality.Core.Models.Entities.Estate_;

namespace Reality.Core.Models.Entities.Address_
{
    public class Address: IEntity, IWithManipulationMeta
    {
        public int Id { get; set; }
        
        public int StreetId { get; set; }
        public string StreetNumber { get; set; } 

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public Street Street { get; set; }
        public IEnumerable<Estate> Estates { get; set; }
    }
}