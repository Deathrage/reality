﻿using System;
using System.Collections.Generic;
using Reality.Common.Model;
using Reality.Common.Model.Mixins;

namespace Reality.Core.Models.Entities.Address_
{
	public class Municipality: INamedEntity, IWithManipulationMeta
	{
		public int Id { get; set;  }
		public string Name { get; set; }
		
        public int CountryId { get; set; }
		public string CountrySubdivision { get; set; }
		public string CountrySecondarySubdivision { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

		public Country Country { get; set; }
        public IEnumerable<Street> Streets { get; set; }
    }
}