﻿using System;
using System.Collections.Generic;
using Reality.Common.Model;
using Reality.Common.Model.Mixins;

namespace Reality.Core.Models.Entities.Address_
{
	public class Street: INamedEntity, IWithManipulationMeta
	{
		public int Id { get; set; }
        public string Name { get; set; }
        public int MunicipalityId { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

		public Municipality Municipality { get; set; }
        public IEnumerable<Address> Addresses { get; set; }
    }
}