﻿using System;
using System.Collections.Generic;
using Reality.Common.Model;
using Reality.Common.Model.Mixins;

namespace Reality.Core.Models.Entities.Address_
{
	public class Country: INamedEntity, IWithManipulationMeta
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Code { get; set; }
		
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

		public IEnumerable<Municipality> Municipalities { get; set; }
    }
}