﻿using System;
using Reality.Common.Model;
using Reality.Common.Model.Mixins;
using Reality.Core.Models.Data;
using Reality.Core.Models.Entities.Estate_;

namespace Reality.Core.Models.Entities.Advert_
{
	public class Advert: INamedEntity, IWithManipulationMeta, IWithLink
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int EstateId { get; set; }
        public AdvertType AdvertType { get; set; }

		public decimal PriceCzk { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

		public Uri Link { get; set; }

		public Estate Estate { get; set; }

		public Conversion Conversion { get; set; }
    }
}