﻿using System;
using Reality.Common.Model.Mixins;
using Reality.Core.Models.Entities.Advert_;

namespace Reality.Core.Models.Data
{
    public class Conversion: IWithManipulationMeta
    {
        public int TargetAdvertId { get; set; }
        public int SourceAdvertId { get; set; }
        public string SourceModule { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public Advert TargetAdvert { get; set; }
    }
}