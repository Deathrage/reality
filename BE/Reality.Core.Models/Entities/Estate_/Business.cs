﻿using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.Core.Models.Entities.Estate_
{
    public class Business: Estate
    {
        public int UsableSurface { get; set; }
        public BusinessType BusinessType { get; set; }
    }
}