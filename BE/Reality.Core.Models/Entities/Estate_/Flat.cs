﻿using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.Core.Models.Entities.Estate_
{
	public class Flat: Estate
	{
		public int UsableSurface { get; set; }
		
        public Furnishment? Furnishment { get; set; }
		public ConstructionMaterial? Construction { get; set; }
		public FlatDisposition? Disposition { get; set; }

		public int? Floor { get; set; }

		public bool Balcony { get; set; }
		public bool Terrace { get; set; }
        public bool Garage { get; set; }
		public bool Parking { get; set; }
		public bool Lift { get; set; }
        public bool LowEnergy { get; set; }
        public bool NewBuilding { get; set; }
	}
}