﻿namespace Reality.Core.Models.Entities.Estate_.Enums
{
	public enum EstateType
	{
        House,
		Flat,
		Land,
		Business,
        Miscellaneous
	}
}