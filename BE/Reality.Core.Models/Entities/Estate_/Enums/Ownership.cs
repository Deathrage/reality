﻿namespace Reality.Core.Models.Entities.Estate_.Enums
{
    public enum Ownership
    {
        Personal,
        Collective,
        State
    }
}