﻿namespace Reality.Core.Models.Entities.Estate_.Enums
{
    public enum Furnishment
    {
        NotFurnished,
        PartiallyFurnished,
        FullyFurnished
    }
}