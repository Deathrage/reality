﻿namespace Reality.Core.Models.Entities.Estate_.Enums
{
    public enum FlatDisposition
    {
        Dispition1_KK,
        Dispition1_1,
        Dispition2_KK,
        Dispition2_1,
        Dispition3_KK,
        Dispition3_1,
        Dispition4_KK,
        Dispition4_1,
        Dispition5_KK,
        Dispition5_1,
        Dispition6_OrMore,
        Other
    }
}