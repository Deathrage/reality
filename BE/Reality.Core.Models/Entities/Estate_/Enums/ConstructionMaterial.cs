﻿namespace Reality.Core.Models.Entities.Estate_.Enums
{
    public enum ConstructionMaterial
    {
        Brick,
        Panel,
        Wood,
        Other
    }
}