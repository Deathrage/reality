﻿namespace Reality.Core.Models.Entities.Estate_.Enums
{
    public enum LandType
    {
        Housing,
        Forest,
        Field,
        Pond,
        Commerce,
        Garden
    }
}