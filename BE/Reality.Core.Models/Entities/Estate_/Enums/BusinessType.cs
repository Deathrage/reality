﻿namespace Reality.Core.Models.Entities.Estate_.Enums
{
    public enum BusinessType
    {
        Office,
        Recreational,
        Manufacturing,
        Other
    }
}