﻿using System;
using System.Collections.Generic;
using Reality.Common.Model;
using Reality.Common.Model.Mixins;
using Reality.Core.Models.Entities.Address_;
using Reality.Core.Models.Entities.Advert_;
using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.Core.Models.Entities.Estate_
{
	public class Estate: INamedEntity, IWithGeoCoordinates, IWithManipulationMeta
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int AddressId { get; set; }

		public EstateType EstateType { get; set; }
		public Ownership? Ownership { get; set; }

		public decimal? Longitude { get; set; }
		public decimal? Latitude { get; set; }

        public GeoPoint GeoPoint
        {
            get
            {
                if (Longitude.HasValue && Latitude.HasValue)
                    return new GeoPoint(Latitude.Value, Longitude.Value);
                return null;
            }
			private set {}
        }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

		public string Locality { get; set; }

		public bool HasOwnAddress { get; set; }

		public Address Address { get; set; }
		public IEnumerable<Advert> Adverts { get; set; }
    }
}