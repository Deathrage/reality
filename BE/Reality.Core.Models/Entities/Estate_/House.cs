﻿using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.Core.Models.Entities.Estate_
{
	public class House: Estate
	{
		public int LandSurface { get; set; }
		public int UsableSurface { get; set; }

		public Furnishment? Furnishment { get; set; }

        public bool Balcony { get; set; }
		public bool Terrace { get; set; }
		public bool Garage { get; set; }
        public bool LowEnergy { get; set; }
		public bool NewBuilding { get; set; }
	}
}