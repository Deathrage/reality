﻿using Reality.Core.Models.Entities.Estate_.Enums;

namespace Reality.Core.Models.Entities.Estate_
{
	public class Land: Estate
	{
		public LandType? LandType { get; set; }
		public int LandSurface { get; set; }
    }
}