﻿using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Microsoft.Extensions.DependencyInjection;
using Reality.BezRealitky.Mapping;
using Reality.BezRealitky.Models;
using Reality.Core.AdvertConverter_;

namespace Reality.BezRealitky
{
	public static class ServiceCollectionExtensions
    {
        public static IServiceCollection
            AddBezRealitky(this IServiceCollection source) =>
            source
                .AddTransient(_ =>
                    new GraphQLHttpClient("https://www.bezrealitky.cz/webgraphql", new NewtonsoftJsonSerializer()))
                .AddTransient<IBezRealitkyScrapper, BezRealitkyScrapper>()
                .AddTransient<IAdvertConverter<Advert>, BezRealitkyConverter>();
    }
}