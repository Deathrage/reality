﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Reality.Core.AdvertConverter_;
using Reality.Core.Models.Data;
using Reality.Core.Models.Entities.Advert_;

namespace Reality.BezRealitky.Mapping
{
    internal class BezRealitkyConverter: IAdvertConverter<BezRealitky.Models.Advert>
    {
        private readonly IMapper _mapper;

        public string ModuleName => "bezreal";

        public BezRealitkyConverter(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public IReadOnlyList<ConversionItem> Convert(
            IReadOnlyList<BezRealitky.Models.Advert> adverts)
        {
            return adverts
                .Select(advert =>
                {
                    Conversion conversion = new Conversion() {SourceModule = ModuleName, SourceAdvertId = advert.Id};
                    Advert coreAdvert = _mapper.Map<Advert>(advert);

                    return new ConversionItem(conversion, coreAdvert, coreAdvert.Estate, default, default, default,
                        default);
                })
                .ToArray();
        }
    }
}