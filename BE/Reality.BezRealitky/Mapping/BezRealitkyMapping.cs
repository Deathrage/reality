﻿using System;
using AutoMapper;
using Reality.BezRealitky.GraphQL;
using Reality.Core.Models.Entities.Estate_;
using Reality.Core.Models.Entities.Estate_.Enums;
using Advert = Reality.Core.Models.Entities.Advert_.Advert;
using AdvertType = Reality.Core.Models.Entities.Advert_.AdvertType;
using EstateType = Reality.BezRealitky.GraphQL.EstateType;
using LandType = Reality.Core.Models.Entities.Estate_.Enums.LandType;
using Ownership = Reality.Core.Models.Entities.Estate_.Enums.Ownership;

namespace Reality.BezRealitky.Mapping
{
	public class BezRealitkyMapping : Profile
	{
		public BezRealitkyMapping()
		{
            Random moveBackSource = new Random();

			CreateMap<GraphQL.AdvertEstateOffer, Models.Advert>(MemberList.Destination)
				.ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.Created, opts => opts.MapFrom((src, dst) =>
                {
                    int? moveBack = src?.Age switch
                    {
                        Age.Between110 => moveBackSource.Next(1, 11),
                        Age.Between1030 => moveBackSource.Next(10, 31),
                        Age.Between3050 => moveBackSource.Next(30, 51),
                        _ => null
                    };

                    if (!moveBack.HasValue)
                        return (DateTime?) null;

                    return DateTime.Now.AddDays(-1 * moveBack.Value);
                }))
                .ForMember(dst => dst.ExternalId, opts => opts.Ignore())
                .ForMember(dst => dst.Title, opts => opts.Ignore())
                .ForMember(dst => dst.Active, opts => opts.Ignore())
                .ForMember(dst => dst.Archived, opts => opts.Ignore())
                .ForMember(dst => dst.Link, opts => opts.Ignore())
                .ForMember(dst => dst.Modified, opts => opts.Ignore())
				.ForMember(dst => dst.Locality, opts => opts.MapFrom(src => src.Address))
				.ForMember(dst => dst.Longitude, opts => opts.MapFrom(src => src.Gps.Lng))
				.ForMember(dst => dst.Latitude, opts => opts.MapFrom(src => src.Gps.Lat));

			CreateMap<GraphQL.Advert, Models.Advert>(MemberList.Destination)
				.ForMember(dst => dst.Id, opts => opts.Ignore())
				.ForMember(dst => dst.Modified, opts => opts.Ignore())
                .ForMember(dst => dst.Created, opts => opts.Ignore())
                .ForMember(dst => dst.EstateType, opts => opts.Ignore())
                .ForMember(dst => dst.Price, opts => opts.Ignore())
                .ForMember(dst => dst.Currency, opts => opts.Ignore())
                .ForMember(dst => dst.Charges, opts => opts.Ignore())
                .ForMember(dst => dst.Deposit, opts => opts.Ignore())
                .ForMember(dst => dst.Surface, opts => opts.Ignore())
                .ForMember(dst => dst.SurfaceLand, opts => opts.Ignore())
                .ForMember(dst => dst.SurfaceUsable, opts => opts.Ignore())
                .ForMember(dst => dst.Disposition, opts => opts.Ignore())
                .ForMember(dst => dst.Construction, opts => opts.Ignore())
                .ForMember(dst => dst.Condition, opts => opts.Ignore())
                .ForMember(dst => dst.LandType, opts => opts.Ignore())
                .ForMember(dst => dst.Equipped, opts => opts.Ignore())
                .ForMember(dst => dst.Ownership, opts => opts.Ignore())
                .ForMember(dst => dst.Floor, opts => opts.Ignore())
                .ForMember(dst => dst.Etage, opts => opts.Ignore())
                .ForMember(dst => dst.Balcony, opts => opts.Ignore())
                .ForMember(dst => dst.Penb, opts => opts.Ignore())
                .ForMember(dst => dst.Mirador, opts => opts.Ignore())
                .ForMember(dst => dst.Terrace, opts => opts.Ignore())
                .ForMember(dst => dst.Garage, opts => opts.Ignore())
                .ForMember(dst => dst.Parking, opts => opts.Ignore())
                .ForMember(dst => dst.NewBuilding, opts => opts.Ignore())
                .ForMember(dst => dst.Lift, opts => opts.Ignore())
                .ForMember(dst => dst.Heating, opts => opts.Ignore())
                .ForMember(dst => dst.FrontGarden, opts => opts.Ignore())
                .ForMember(dst => dst.Reconstruction, opts => opts.Ignore())
                .ForMember(dst => dst.Water, opts => opts.Ignore())
                .ForMember(dst => dst.Sewage, opts => opts.Ignore())
                .ForMember(dst => dst.Locality, opts => opts.Ignore())
                .ForMember(dst => dst.Latitude, opts => opts.Ignore())
                .ForMember(dst => dst.Longitude, opts => opts.Ignore())
				.ForMember(dst => dst.ExternalId, opts => opts.MapFrom(src => src.Id))
				.ForMember(dst => dst.Link, opts => opts.MapFrom(src => new Uri(src.AbsoluteUrl)))
				.ForMember(dst => dst.Title, opts => opts.MapFrom(src => src.ShortDescription))
				.AfterMap((sourceAdvert, targetAdvert, ctx) => ctx.Mapper.Map(sourceAdvert.AdvertObject, targetAdvert));

			#region Estates
            
            CreateMap<BezRealitky.Models.Advert, Estate>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.GeoPoint, opts => opts.Ignore())
                .ForMember(dst => dst.AddressId, opts => opts.Ignore())
                .ForMember(dst => dst.Address, opts => opts.Ignore())
                .ForMember(dst => dst.Adverts, opts => opts.Ignore())
                .ForMember(dst => dst.HasOwnAddress, opts => opts.Ignore())
                .ForMember(dst => dst.Name, opts => opts.MapFrom((src, dst) =>
                {
                    string objectTypeName = src.EstateType switch
                    {
                        BezRealitky.GraphQL.EstateType.Byt => "Byt",
                        BezRealitky.GraphQL.EstateType.Dum => "Dům",
                        BezRealitky.GraphQL.EstateType.Pozemek => "Pozemek",
                        BezRealitky.GraphQL.EstateType.Garaz => "Garáž",
                        BezRealitky.GraphQL.EstateType.Kancelar => "Kancelář",
                        BezRealitky.GraphQL.EstateType.NebytovyProstor => "Nebytový prostor",
                        BezRealitky.GraphQL.EstateType.RekreacniObjekt => "Rekreační objekt",
                        _ => "Objekt"
                    };

                    return $"{objectTypeName} {src.SurfaceUsable} m2, {src.Locality}";
                }))
                .ForMember(dst => dst.EstateType, opts => opts.MapFrom((src, dst) =>
                {
                    return src.EstateType switch
                    {
                        BezRealitky.GraphQL.EstateType.Dum => Core.Models.Entities.Estate_.Enums.EstateType.House,
                        BezRealitky.GraphQL.EstateType.RekreacniObjekt => Core.Models.Entities.Estate_.Enums.EstateType.House,
                        BezRealitky.GraphQL.EstateType.Byt => Core.Models.Entities.Estate_.Enums.EstateType.Flat,
                        BezRealitky.GraphQL.EstateType.Pozemek => Core.Models.Entities.Estate_.Enums.EstateType.Land,
                        BezRealitky.GraphQL.EstateType.Kancelar => Core.Models.Entities.Estate_.Enums.EstateType.Business,
                        _ => Core.Models.Entities.Estate_.Enums.EstateType.Miscellaneous
                    };
                }))
                .ForMember(dst => dst.Ownership, opts => opts.MapFrom((src, dst) =>
                {
                    return src.Ownership switch
                    {
                        BezRealitky.GraphQL.Ownership.Osobni => Ownership.Personal,
                        BezRealitky.GraphQL.Ownership.Druzstevni => Ownership.Collective,
                        BezRealitky.GraphQL.Ownership.Obecni => Ownership.State,
                        _ => default
                    };
                }));

            CreateMap<BezRealitky.Models.Advert, Land>(MemberList.Destination)
                .IncludeBase<BezRealitky.Models.Advert, Estate>()
                .ForMember(dst => dst.LandSurface, opts => opts.MapFrom(src => src.Surface))
                .ForMember(dst => dst.LandType, opts => opts.MapFrom((src, dst) =>
                {
                    return src.LandType switch
                    {
                        BezRealitky.GraphQL.LandType.Bydleni => LandType.Housing,
                        BezRealitky.GraphQL.LandType.Les => LandType.Forest,
                        BezRealitky.GraphQL.LandType.Pole => LandType.Field,
                        BezRealitky.GraphQL.LandType.Louka => LandType.Field,
                        BezRealitky.GraphQL.LandType.Zahrada => LandType.Garden,
                        BezRealitky.GraphQL.LandType.Rybnik => LandType.Pond,
                        BezRealitky.GraphQL.LandType.Komercni => LandType.Commerce,
                        _ => default
                    };
                }));

            CreateMap<BezRealitky.Models.Advert, House>(MemberList.Destination)
                .IncludeBase<BezRealitky.Models.Advert, Estate>()
                .ForMember(dst => dst.LandSurface, opts => opts.MapFrom(src => src.SurfaceLand))
                .ForMember(dst => dst.UsableSurface, opts => opts.MapFrom(src => src.SurfaceUsable))
                .ForMember(dst => dst.Furnishment, opts => opts.MapFrom((src, dst) =>
                {
                    return src.Equipped switch
                    {
                        BezRealitky.GraphQL.Equipped.Nevybaveny => Furnishment.NotFurnished,
                        BezRealitky.GraphQL.Equipped.Castecne => Furnishment
                            .PartiallyFurnished,
                        BezRealitky.GraphQL.Equipped.Vybaveny => Furnishment.FullyFurnished,
                        _ => default
                    };
                }))
                .ForMember(dst => dst.LowEnergy, opts => opts.MapFrom(src => src.Penb == Penb.A));

            CreateMap<BezRealitky.Models.Advert, Flat>(MemberList.Destination)
                .IncludeBase<BezRealitky.Models.Advert, Estate>()
                .ForMember(dst => dst.UsableSurface, opts => opts.MapFrom(src => src.SurfaceUsable))
                .ForMember(dst => dst.Furnishment, opts => opts.MapFrom((src, dst) =>
                {
                    return src.Equipped switch
                    {
                        BezRealitky.GraphQL.Equipped.Nevybaveny => Furnishment.NotFurnished,
                        BezRealitky.GraphQL.Equipped.Castecne => Furnishment
                            .PartiallyFurnished,
                        BezRealitky.GraphQL.Equipped.Vybaveny => Furnishment.FullyFurnished,
                        _ => default
                    };
                }))
                .ForMember(dst => dst.Construction, opts => opts.MapFrom((src, dst) =>
                {
                    return src.Construction switch
                    {
                        BezRealitky.GraphQL.Construction.Cihla => ConstructionMaterial.Brick,
                        BezRealitky.GraphQL.Construction.Panel => ConstructionMaterial.Panel,
                        BezRealitky.GraphQL.Construction.Drevostavba => ConstructionMaterial
                            .Wood,
                        BezRealitky.GraphQL.Construction.Nizkoenergeticky => ConstructionMaterial.Other,
                        BezRealitky.GraphQL.Construction.Ostatni =>
                        ConstructionMaterial.Other,
                        _ => default
                    };
                }))
                .ForMember(dst => dst.Disposition, opts => opts.MapFrom((src, dst) =>
                {
                    return src.Disposition switch
                    {
                        BezRealitky.GraphQL.Disposition.Garsoniera => FlatDisposition
                            .Dispition1_KK,
                        BezRealitky.GraphQL.Disposition.Disp1Izb => FlatDisposition
                            .Dispition1_KK,
                        BezRealitky.GraphQL.Disposition.Disp1Kk => FlatDisposition
                            .Dispition1_KK,
                        BezRealitky.GraphQL.Disposition.Disp11 =>
                        FlatDisposition.Dispition1_1,

                        BezRealitky.GraphQL.Disposition.Disp2Izb => FlatDisposition
                            .Dispition2_KK,
                        BezRealitky.GraphQL.Disposition.Disp2Kk => FlatDisposition
                            .Dispition2_KK,
                        BezRealitky.GraphQL.Disposition.Disp21 =>
                        FlatDisposition.Dispition2_1,

                        BezRealitky.GraphQL.Disposition.Disp3Izb => FlatDisposition
                            .Dispition3_KK,
                        BezRealitky.GraphQL.Disposition.Disp3Kk => FlatDisposition
                            .Dispition3_KK,
                        BezRealitky.GraphQL.Disposition.Disp31 =>
                        FlatDisposition.Dispition3_1,

                        BezRealitky.GraphQL.Disposition.Disp4Izb => FlatDisposition
                            .Dispition4_KK,
                        BezRealitky.GraphQL.Disposition.Disp4Kk => FlatDisposition
                            .Dispition4_KK,
                        BezRealitky.GraphQL.Disposition.Disp41 =>
                        FlatDisposition.Dispition4_1,

                        BezRealitky.GraphQL.Disposition.Disp5Izb => FlatDisposition
                            .Dispition5_KK,
                        BezRealitky.GraphQL.Disposition.Disp5Kk => FlatDisposition
                            .Dispition5_KK,
                        BezRealitky.GraphQL.Disposition.Disp51 =>
                        FlatDisposition.Dispition5_1,

                        BezRealitky.GraphQL.Disposition.Disp61 => FlatDisposition
                            .Dispition6_OrMore,
                        BezRealitky.GraphQL.Disposition.Disp6Izb => FlatDisposition
                            .Dispition6_OrMore,
                        BezRealitky.GraphQL.Disposition.Disp6Kk => FlatDisposition
                            .Dispition6_OrMore,
                        BezRealitky.GraphQL.Disposition.Disp7Izb => FlatDisposition
                            .Dispition6_OrMore,
                        BezRealitky.GraphQL.Disposition.Disp7Kk => FlatDisposition
                            .Dispition6_OrMore,
                        BezRealitky.GraphQL.Disposition.Disp71 => FlatDisposition
                            .Dispition6_OrMore,

                        BezRealitky.GraphQL.Disposition.Ostatni => FlatDisposition.Other,

                        _ => default
                    };
                }))
                .ForMember(dst => dst.Floor, opts => opts.MapFrom(src => src.Etage))
                .ForMember(dst => dst.LowEnergy, opts => opts.MapFrom(src => src.Penb == Penb.A));

            CreateMap<BezRealitky.Models.Advert, Business>(MemberList.Destination)
                .IncludeBase<BezRealitky.Models.Advert, Estate>()
                .ForMember(dst => dst.BusinessType, opts => opts.MapFrom((src, dst) =>
                {
                    return src.EstateType switch
                    {
                        EstateType.Kancelar => BusinessType.Office,
                        _ => BusinessType.Other
                    };
                }))
                .ForMember(dst => dst.UsableSurface,
                    opts => opts.MapFrom(src => src.SurfaceUsable ?? src.Surface ?? src.SurfaceLand));
            
            #endregion

            #region Advert

            CreateMap<BezRealitky.Models.Advert, Advert>(MemberList.Destination)
                .ForMember(dst => dst.Id, opts => opts.Ignore())
                .ForMember(dst => dst.Conversion, opts => opts.Ignore())
                .ForMember(dst => dst.EstateId, opts => opts.Ignore())
                .ForMember(dst => dst.Name, opts => opts.MapFrom(src => src.Title))
                .ForMember(dst => dst.PriceCzk, opts => opts.MapFrom(src => src.Price))
                .ForMember(dst => dst.AdvertType, opts => opts.MapFrom((src, dst) =>
                {
                    switch (src.OfferType)
                    {
                        case OfferType.Prodej: return AdvertType.Sell;
                        case OfferType.Pronajem: return AdvertType.Rent;
                        default:
                            throw new Exception($"Cannot map {src.OfferType} to {nameof(GraphQL.AdvertType)}");
                    }
                }))
                .ForMember(dst => dst.Estate, opts => opts.MapFrom((src, dst, prop, ctx) =>
                {
                    switch (src.EstateType)
                    {
                        case EstateType.Dum: return ctx.Mapper.Map<House>(src);
                        case EstateType.RekreacniObjekt: return ctx.Mapper.Map<House>(src);
                        case EstateType.Byt: return ctx.Mapper.Map<Flat>(src); 
                        case EstateType.Pozemek: return ctx.Mapper.Map<Land>(src);
                        case EstateType.Kancelar: return ctx.Mapper.Map<Business>(src);
                        default:
                            return ctx.Mapper.Map<Estate>(src);
                    }
                }))
                .ForMember(dst => dst.Conversion, opts => opts.Ignore());

            #endregion
		}
	}
}