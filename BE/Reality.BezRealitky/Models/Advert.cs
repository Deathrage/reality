﻿using System;
using Reality.BezRealitky.GraphQL;
using Reality.Common.Model;
using Reality.Common.Model.Mixins;

namespace Reality.BezRealitky.Models
{
	public class Advert: IEntity, IWithGeoCoordinates, IWithLink, IWithManipulationMeta
	{
		public int Id { get; set; }
		public long ExternalId { get; set; }

		public string Title { get; set; }
		public bool Active { get; set; }
		public bool Archived { get; set; }

		public EstateType EstateType { get; set; }
		public OfferType OfferType { get; set; }

		public decimal Price { get; set; }
		public Currency Currency { get; set; }
		public decimal? Charges { get; set; }
		public decimal? Deposit { get; set; }

		public int? Surface { get; set; }
		public int? SurfaceLand { get; set; }
		public int? SurfaceUsable { get; set; }

		public Disposition? Disposition { get; set; }
		public Construction? Construction { get; set; }
		public Condition? Condition { get; set; }
		public LandType? LandType { get; set; }
		public Equipped? Equipped { get; set; }
		public Ownership? Ownership { get; set; }

		public Floor? Floor { get; set; }
		public int? Etage { get; set; }
		public bool Balcony { get; set; }
		public Penb? Penb { get; set; }
		public bool Mirador { get; set; }
		public bool Terrace { get; set; }
		public bool Garage { get; set; }
        public bool Parking { get; set; }
		public bool NewBuilding { get; set; }
		public bool Lift { get; set; }
		public Heating? Heating { get; set; }
		public int? FrontGarden { get; set; }
		public Reconstruction? Reconstruction { get; set; }
		public Water? Water { get; set; }
		public Sewage? Sewage { get; set; }

		public string Locality { get; set; }

		public Uri Link { get; set; }

		public decimal? Latitude { get; set; }
		public decimal? Longitude { get; set; }

		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
	}
}