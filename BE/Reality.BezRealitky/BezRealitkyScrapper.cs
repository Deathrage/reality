﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GraphQL;
using Reality.BezRealitky.GraphQL;
using GraphQL.Client.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Reality.Common;
using Reality.Common.DataAccess.Abstractions;
using Reality.Common.DataAccess.Abstractions.Interfaces;

namespace Reality.BezRealitky
{
    public interface IBezRealitkyScrapper : IScrapper
    {

    }

	internal class BezRealitkyScrapper : IBezRealitkyScrapper
	{
		private readonly GraphQLHttpClient _graphQlHttpClient;
		private readonly IMapper _mapper;
		private readonly IRepository<Models.Advert> _advertRepository;
		private readonly ILogger<BezRealitkyScrapper> _logger;
		
		public BezRealitkyScrapper(GraphQLHttpClient graphQlHttpClient, IMapper mapper,
			IRepository<Models.Advert> advertRepository, ILogger<BezRealitkyScrapper> logger)
		{
			_graphQlHttpClient = graphQlHttpClient ?? throw new ArgumentNullException(nameof(graphQlHttpClient));
			_mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _advertRepository = advertRepository ?? throw new ArgumentNullException(nameof(advertRepository));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		public async Task<IEnumerable<int>> RunAsync()
		{
			int limit = 500;
			int offset = 0;

			int addedRows = 0;

			List<Models.Advert> gatheredAdverts = new List<Models.Advert>();
			while (true)
			{
				IReadOnlyList<Advert> results = await GetRawAdvertsAsync(limit, offset);
				// Only Czech offers are interesting
                IEnumerable<Advert> czechOffers = results
                    .Where(x => x.Id.HasValue && x.AdvertType == AdvertType.Offer && x.Locale == Locale.Cs).ToArray();
				// Find all adverts that are already in the DB
				IReadOnlyList<long> uniqExternalIds = czechOffers.Select(x => (long)x.Id.Value).Distinct().ToList();
				IReadOnlyList<long> alreadySeenExternalIds = await _advertRepository.AsQueryable()
					.Where(row => uniqExternalIds.Contains(row.ExternalId)).Select(row => row.ExternalId).ToListAsync();
				// Filter out those that are already in the DB
				IEnumerable<Advert> notSeenAdverts =
					czechOffers.Where(item => !alreadySeenExternalIds.Contains((long)item.Id));
				// Map and save noew items
				IReadOnlyList<Models.Advert> mappedAdverts = _mapper.Map<IReadOnlyList<Models.Advert>>(notSeenAdverts);

                gatheredAdverts.AddRange(mappedAdverts);

				// As we are going from newest to oldest it makes no sense to continue to the next loop if we have already seen item from the DB
				if (results.Count < limit || alreadySeenExternalIds.Any())
					break;

				offset += limit;

				await Task.Delay(500);
			}
			
			_logger.LogInformation($"Writing {_advertRepository.TrackedSpecifiedEntities} records to the DB.");
            await _advertRepository.AddAsync(gatheredAdverts);
			await _advertRepository.SaveAsync();

            return gatheredAdverts.Select(item => item.Id);
        }
		

		private async Task<IReadOnlyList<Advert>> GetRawAdvertsAsync(int limit, int offset)
		{
			GraphQLRequest request = new GraphQLRequest(_query, new {limit, offset, boundaryPoints = _pragueBoundary});

			GraphQLResponse<Query> response = await _graphQlHttpClient.SendQueryAsync<Query>(request);

			IReadOnlyList<Advert> results = response.Data.AdvertList.List.ToList();;

			_logger.LogInformation(
				$"Recovered {results.Count} records.");

			return results;
		}
		
		private readonly string _query = @"
			query Foo($limit: Int!, $offset: Int!, $boundaryPoints: [GPSPointInput]) {
			  advertList(limit: $limit, offset: $offset, order: TIMEORDER_DESC, boundaryPoints: $boundaryPoints) {
			    totalCount
			    list {
			      id
			      address
			      active
				  shortDescription
			      archived
			      absoluteUrl
			      advertType
			      advertObject {
			        id
			        address
			        estateType
			        offerType
			        ... on AdvertEstateOffer {
                      id
			          price
                      age
			          currency
			          gps {
			            lat
			            lng
			          }
			          
			          charges
			          deposit
			          
			          surface
			          surfaceLand
			          surfaceUsable
			          
			          disposition
			          construction
			          condition
			          landType
			          equipped
			          
			          ownership
			          
			          floor
					  etage          
			          
			          balcony
			          penb
			          mirador
			          terrace
			          garage
                      parking
			          newBuilding
			          lift
			          heating
			          frontGarden
			          reconstruction
			          water
			          sewage
			                    
			          city
			          street
			          houseNumber
			          zip
			        }
			      }
			      locale
			    }
			  }
			}
		";

		private readonly object[] _pragueBoundary = {
			new { lat = 50.1029963, lng = 14.2244355}, new { lat = 50.0771366, lng = 14.2893735},
			new { lat = 50.0583091, lng = 14.2480852}, new { lat = 50.0235953, lng = 14.3158709},
			new { lat = 50.0022104, lng = 14.2948377}, new { lat = 49.9906305, lng = 14.3427557},
			new { lat = 49.9718584, lng = 14.3268139}, new { lat = 49.9676279, lng = 14.344916},
			new { lat = 49.9572087, lng = 14.3254392}, new { lat = 49.9419006, lng = 14.395562},
			new { lat = 49.9706693, lng = 14.4006472}, new { lat = 49.9707711, lng = 14.4622402},
			new { lat = 50.0108381, lng = 14.5274725}, new { lat = 50.0163634, lng = 14.582393},
			new { lat = 49.9944411, lng = 14.6401518}, new { lat = 50.0188407, lng = 14.669537},
			new { lat = 50.0568947, lng = 14.6401916}, new { lat = 50.0721289, lng = 14.6999919},
			new { lat = 50.0870194, lng = 14.7067867}, new { lat = 50.1065009, lng = 14.6574355},
			new { lat = 50.1226041, lng = 14.6591154}, new { lat = 50.129523, lng = 14.6005164},
			new { lat = 50.1452435, lng = 14.5877286}, new { lat = 50.1541328, lng = 14.5990028},
			new { lat = 50.1501702, lng = 14.5632297}, new { lat = 50.1661221, lng = 14.5505391},
			new { lat = 50.161565, lng = 14.5342354}, new { lat = 50.1771831, lng = 14.5324832},
			new { lat = 50.1774301, lng = 14.5268551}, new { lat = 50.141429, lng = 14.3949016},
			new { lat = 50.1480311, lng = 14.3657001}, new { lat = 50.1160189, lng = 14.3607835},
			new { lat = 50.1152415, lng = 14.3206068}, new { lat = 50.1300768, lng = 14.3024335},
			new { lat = 50.1029963, lng = 14.2244355}
		};
	}
}