const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: ['./src/index.ts'],
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: 'reality-client.js',
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.txt', '.graphql'],
    alias: {
      '@components': path.resolve(__dirname, './src/components'),
      '@workspaceComponents': path.resolve(__dirname, './src/workspaceComponents'),
      '@pages': path.resolve(__dirname, './src/pages'),
      '@helpers': path.resolve(__dirname, './src/helpers'),
      'global-types': path.resolve(__dirname, './__generated__/globalTypes.ts'),
      'schema': path.resolve(__dirname, './schema.json'),
      '@localization': path.resolve(__dirname, './src/localization'),
    },
  },
  module: {
    rules: [
      {
        test: /\.txt$/i,
        use: 'raw-loader',
      },
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader',
        options: {
          compiler: 'ttypescript',
        },
      },
      {
        test: /\.html$/i,
        loader: 'html-loader',
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      }
    ],
  },
  plugins: [
    new HTMLWebpackPlugin({
      title: 'Reality Client',
      template: './static/index.html'
    }),
    new HTMLWebpackPlugin({
      title: 'Reality Client Sub Window',
      template: './static/sub.html',
      filename: 'sub.html'
    }),
    new CopyPlugin({
      patterns: [
        { from: './static/mapMarkers', to: 'map-markers' }
      ]
    })
  ],
};
