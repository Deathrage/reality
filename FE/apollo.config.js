const dotenv = require("dotenv");
dotenv.config();

module.exports = {
  client: {
    service: {
      name: 'dev',
      url: process.env.GRAPHQL_URI
    }
  }
};