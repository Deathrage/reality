/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum AdvertType {
  RENT = "RENT",
  SELL = "SELL",
}

export enum BusinessType {
  MANUFACTURING = "MANUFACTURING",
  OFFICE = "OFFICE",
  OTHER = "OTHER",
  RECREATIONAL = "RECREATIONAL",
}

export enum ConstructionMaterial {
  BRICK = "BRICK",
  OTHER = "OTHER",
  PANEL = "PANEL",
  WOOD = "WOOD",
}

export enum EstateType {
  BUSINESS = "BUSINESS",
  FLAT = "FLAT",
  HOUSE = "HOUSE",
  LAND = "LAND",
  MISCELLANEOUS = "MISCELLANEOUS",
}

export enum FlatDisposition {
  DISPITION1_1 = "DISPITION1_1",
  DISPITION1__KK = "DISPITION1__KK",
  DISPITION2_1 = "DISPITION2_1",
  DISPITION2__KK = "DISPITION2__KK",
  DISPITION3_1 = "DISPITION3_1",
  DISPITION3__KK = "DISPITION3__KK",
  DISPITION4_1 = "DISPITION4_1",
  DISPITION4__KK = "DISPITION4__KK",
  DISPITION5_1 = "DISPITION5_1",
  DISPITION5__KK = "DISPITION5__KK",
  DISPITION6__OR_MORE = "DISPITION6__OR_MORE",
  OTHER = "OTHER",
}

export enum Furnishment {
  FULLY_FURNISHED = "FULLY_FURNISHED",
  NOT_FURNISHED = "NOT_FURNISHED",
  PARTIALLY_FURNISHED = "PARTIALLY_FURNISHED",
}

export enum LandType {
  COMMERCE = "COMMERCE",
  FIELD = "FIELD",
  FOREST = "FOREST",
  GARDEN = "GARDEN",
  HOUSING = "HOUSING",
  POND = "POND",
}

export enum Ownership {
  COLLECTIVE = "COLLECTIVE",
  PERSONAL = "PERSONAL",
  STATE = "STATE",
}

export interface LatLonInput {
  lat: any;
  lon: any;
}

export interface ListableAdvertFilterInput {
  from: any;
  to: any;
  source?: string | null;
  estateType?: EstateType | null;
  advertType?: AdvertType | null;
  boundary?: (LatLonInput | null)[] | null;
  ownership?: Ownership | null;
  disposition?: FlatDisposition | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
