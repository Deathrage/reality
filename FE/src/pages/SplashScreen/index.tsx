import React, { FC, useContext } from 'react';
import { useEnvironmentContext } from '@components/Environment/hooks';

export default (() => {
  const {
    version: { client, api },
  } = useEnvironmentContext();

  return (
    <div>
      Client: {client}
      API: {api}
    </div>
  );
}) as FC;
