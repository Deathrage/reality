import React, { FC } from 'react';

export default (() => <div>404 Not Found</div>) as FC;
