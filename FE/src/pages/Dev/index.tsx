import React, { FC } from 'react';
import AdvertSelector from '@workspaceComponents/AdvertSelector';
import Navbar from '@components/Navbar';
import Workspace from '@components/Workspace';
import AdvertDetail from '@workspaceComponents/AdvertDetail';
import Dataset from '@workspaceComponents/Dataset';

export default (() => {
  return (
    <>
      <Navbar />
      <Workspace
        initialToolbox={[Dataset.workspaceItem()]}
        initialActive={[AdvertSelector.workspaceItem()/* , AdvertDetail.workspaceItem(34351) */]}
      />
    </>
  );
}) as FC;
