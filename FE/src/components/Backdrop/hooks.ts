import { useContext } from 'react';
import context from './context';
import { BackdropContext } from './types';

export const useBackdrop = (): BackdropContext => useContext(context);
