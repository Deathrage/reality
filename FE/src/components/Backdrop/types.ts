export interface BackdropContext {
  open: () => void;
  close: () => void;
  isVisible: boolean;
  isHidden: boolean;
}
