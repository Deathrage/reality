import { createContext } from 'react';
import { BackdropContext } from './types';
import emptyContext from '@helpers/emptyContext';

export default createContext<BackdropContext>(emptyContext(nameof<BackdropContext>()));
