import React, { FC, useState, useMemo, useCallback } from 'react';
import Context from './context';
import { BackdropContext } from './types';
import { Intent, Overlay, Spinner } from '@blueprintjs/core';

import './style.scss';

export default (({ children }) => {
  const [visible, setVisible] = useState(false);

  const open = useCallback(() => setVisible(true), []);
  const close = useCallback(() => setVisible(false), []);

  const ctx = useMemo<BackdropContext>(
    () => ({
      isVisible: visible,
      isHidden: !visible,
      open,
      close,
    }),
    [close, open, visible],
  );

  return (
    <Context.Provider value={ctx}>
      {children}
      {visible && (
        <Overlay isOpen={true} className="backdrop">
          <Spinner intent={Intent.PRIMARY} size={200} className="backdrop__spinner" />
        </Overlay>
      )}
    </Context.Provider>
  );
}) as FC;
