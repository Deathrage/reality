import React, { ReactElement, ReactNode } from 'react';
import { Case } from 'react-when-then';

// TODO: Fix
export default function Postpone<TData = unknown>({
  data,
  children,
}: {
  data: TData | undefined | null;
  children: (data: TData) => ReactNode;
}): ReactElement {
  return <Case when={!!data}>{children && children(data!)}</Case>;
}
