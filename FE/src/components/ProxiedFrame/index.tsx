import { Button, Callout, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { useEndpoints } from '@components/Environment/hooks';
import React, { useCallback, useState } from 'react';
import { FC } from 'react';
import { Falsy, Ternary, Truthy } from 'react-ternary-operator';
import { Case } from 'react-when-then';
import { v4 } from 'uuid';
import './style.scss';

export default (({ link, noProxy = false }) => {
  const { FRAME_PROXY } = useEndpoints();

  const [key, setKey] = useState(v4());

  const onRefresh = useCallback(() => setKey(v4()), []);

  return (
    <>
      <Ternary condition={!!link}>
        <Truthy>
          <Callout intent={Intent.WARNING} hidden={noProxy}>
            Embedded content has been proxied and may not be fully functional. Use the external link to visit the
            website.
          </Callout>
          <div className="proxied-frame">
            <Button icon={IconNames.REFRESH} className="proxied-frame__button" onClick={onRefresh} />
            <iframe
              key={key}
              className="advert__frame"
              src={noProxy ? link! : `${FRAME_PROXY}?target=${encodeURIComponent(link!)}`}
            />
          </div>
        </Truthy>
        <Falsy>
          <Callout intent={Intent.DANGER}>No link specified!</Callout>
        </Falsy>
      </Ternary>
    </>
  );
}) as FC<{ link: string | undefined | null; noProxy?: boolean }>;
