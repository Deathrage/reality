import { MenuItem } from '@blueprintjs/core';
import React, { FC } from 'react';
import { ToolboxItemProps } from './types';

export default (({ title, icon, onClick }) => {
  return (
    <MenuItem
      icon={icon}
      text={title}
      className="toolbox__item"
      textClassName="text"
      labelClassName="text"
      onClick={onClick}
    />
  );
}) as FC<ToolboxItemProps>;
