import { IconName } from '@blueprintjs/core';
import { ReactText } from 'react';

export interface ToolboxItemProps {
  title: ReactText;
  icon: IconName;
  onClick: () => void;
}
