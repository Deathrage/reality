import { Button, Collapse, Intent, Menu, MenuDivider, MenuItem } from '@blueprintjs/core';
import React, { Children, FC, useCallback, useState } from 'react';
import ToolboxItem from './ToolboxItem';
import './style.scss';
import { IconNames } from '@blueprintjs/icons';

const Toolbox: FC & { Item: typeof ToolboxItem } = ({ children }) => {
  const [show, setShow] = useState(false);

  const onClick = useCallback(() => setShow(prev => !prev), []);

  return (
    <Menu className="toolbox">
      <Collapse isOpen={show}>
        <div className="toolbox__zone">
          {Children.count(children) ? children : <MenuItem icon={IconNames.INBOX} text="Empty" disabled />}
        </div>
      </Collapse>
      <Button className="toolbox__toggle" intent={Intent.PRIMARY} onClick={onClick}>
        {show ? 'Hide Toolbox' : 'Show Toolbox'}
      </Button>
    </Menu>
  );
};

Toolbox.Item = ToolboxItem;

export default Toolbox;
