import { Furnishment } from 'global-types';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { EstateFeaturesHouseMetaFragment } from './__generated__/EstateFeaturesHouseMetaFragment';

export default (({ data }) => {
  const { t } = useTranslation(nameof<Furnishment>());
  return (
    <>
      <tr>
        <td>Usable surface:</td>
        <td>{`${data.usableSurface || '??'} m2`}</td>
      </tr>
      <tr>
        <td>Land surface:</td>
        <td>{`${data.landSurface || '??'} m2`}</td>
      </tr>
      <tr>
        <td>Furnishment:</td>
        <td>{data.furnishment ? t(data.furnishment) : '??'}</td>
      </tr>
    </>
  );
}) as FC<{ data: EstateFeaturesHouseMetaFragment }>;
