import { Classes, HTMLTable } from '@blueprintjs/core';
import filter from '@helpers/filter';
import classNames from 'classnames';
import { EstateType, Ownership } from 'global-types';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Case } from 'react-when-then';
import BusinessMetaInfo from './BusinessMetaInfo';
import FlatMetaInfo from './FlatMetaInfo';
import HouseMetaInfo from './HouseMetaInfo';
import LandMetaInfo from './LandMetaInfo';
import { MetaInfoProps } from './type';
import { EstateFeaturesBusinessMetaFragment } from './__generated__/EstateFeaturesBusinessMetaFragment';
import { EstateFeaturesFlatMetaFragment } from './__generated__/EstateFeaturesFlatMetaFragment';
import { EstateFeaturesHouseMetaFragment } from './__generated__/EstateFeaturesHouseMetaFragment';
import { EstateFeaturesLandMetaFragment } from './__generated__/EstateFeaturesLandMetaFragment';

export default (({ loading, data }) => {
  const { t } = useTranslation(nameof<Ownership>());

  return (
    <HTMLTable className={classNames('estate-features__meta-table', { [Classes.SKELETON]: loading })}>
      <thead>
        <tr>
          <td>Ownership:</td>
          <td>{data?.ownership ? t(data.ownership) : '??'}</td>
        </tr>
        <Case when={data?.estateType === EstateType.FLAT}>
          <FlatMetaInfo data={data as EstateFeaturesFlatMetaFragment} />
        </Case>
        <Case when={data?.estateType === EstateType.HOUSE}>
          <HouseMetaInfo data={data as EstateFeaturesHouseMetaFragment} />
        </Case>
        <Case when={data?.estateType === EstateType.BUSINESS}>
          <BusinessMetaInfo data={data as EstateFeaturesBusinessMetaFragment} />
        </Case>
        <Case when={data?.estateType === EstateType.LAND}>
          <LandMetaInfo data={data as EstateFeaturesLandMetaFragment} />
        </Case>
      </thead>
    </HTMLTable>
  );
}) as FC<MetaInfoProps>;
