import { EstateFeaturesMetaFragment } from './__generated__/EstateFeaturesMetaFragment';

export interface MetaInfoProps {
  loading: boolean;
  data: EstateFeaturesMetaFragment | undefined;
}
