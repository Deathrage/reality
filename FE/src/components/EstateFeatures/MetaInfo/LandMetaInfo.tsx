import { LandType } from 'global-types';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { EstateFeaturesLandMetaFragment } from './__generated__/EstateFeaturesLandMetaFragment';

export default (({ data }) => {
  const { t } = useTranslation(nameof<LandType>());
  return (
    <>
      <tr>
        <td>Land type:</td>
        <td>{data.landType ? t(data.landType) : '??'}</td>
      </tr>
      <tr>
        <td>Land surface:</td>
        <td>{`${data.landSurface || '??'} m2`}</td>
      </tr>
    </>
  );
}) as FC<{ data: EstateFeaturesLandMetaFragment }>;
