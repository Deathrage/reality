/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { EstateType, Ownership, Furnishment, ConstructionMaterial, FlatDisposition, BusinessType, LandType } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL fragment: EstateFeaturesMetaFragment
// ====================================================

export interface EstateFeaturesMetaFragment_DetailedEstate {
  estateType: EstateType;
  ownership: Ownership | null;
}

export interface EstateFeaturesMetaFragment_DetailedHouse {
  estateType: EstateType;
  ownership: Ownership | null;
  landSurface: number;
  usableSurface: number;
  furnishment: Furnishment | null;
}

export interface EstateFeaturesMetaFragment_DetailedFlat {
  estateType: EstateType;
  ownership: Ownership | null;
  usableSurface: number;
  furnishment: Furnishment | null;
  floor: number | null;
  construction: ConstructionMaterial | null;
  disposition: FlatDisposition | null;
}

export interface EstateFeaturesMetaFragment_DetailedBusiness {
  estateType: EstateType;
  ownership: Ownership | null;
  businessType: BusinessType | null;
  usableSurface: number;
}

export interface EstateFeaturesMetaFragment_DetailedLand {
  estateType: EstateType;
  ownership: Ownership | null;
  landType: LandType | null;
  landSurface: number;
}

export type EstateFeaturesMetaFragment = EstateFeaturesMetaFragment_DetailedEstate | EstateFeaturesMetaFragment_DetailedHouse | EstateFeaturesMetaFragment_DetailedFlat | EstateFeaturesMetaFragment_DetailedBusiness | EstateFeaturesMetaFragment_DetailedLand;
