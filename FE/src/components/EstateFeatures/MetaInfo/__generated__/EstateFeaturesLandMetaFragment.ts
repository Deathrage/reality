/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LandType } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL fragment: EstateFeaturesLandMetaFragment
// ====================================================

export interface EstateFeaturesLandMetaFragment {
  landType: LandType | null;
  landSurface: number;
}
