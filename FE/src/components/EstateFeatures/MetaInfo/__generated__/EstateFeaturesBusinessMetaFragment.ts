/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { BusinessType } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL fragment: EstateFeaturesBusinessMetaFragment
// ====================================================

export interface EstateFeaturesBusinessMetaFragment {
  businessType: BusinessType | null;
  usableSurface: number;
}
