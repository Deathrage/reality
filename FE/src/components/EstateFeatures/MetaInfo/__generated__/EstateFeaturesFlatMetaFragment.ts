/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Furnishment, ConstructionMaterial, FlatDisposition } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL fragment: EstateFeaturesFlatMetaFragment
// ====================================================

export interface EstateFeaturesFlatMetaFragment {
  usableSurface: number;
  furnishment: Furnishment | null;
  floor: number | null;
  construction: ConstructionMaterial | null;
  disposition: FlatDisposition | null;
}
