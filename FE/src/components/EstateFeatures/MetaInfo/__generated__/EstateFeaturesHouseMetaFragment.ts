/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Furnishment } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL fragment: EstateFeaturesHouseMetaFragment
// ====================================================

export interface EstateFeaturesHouseMetaFragment {
  landSurface: number;
  usableSurface: number;
  furnishment: Furnishment | null;
}
