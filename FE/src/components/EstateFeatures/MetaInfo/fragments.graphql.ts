import gql from 'graphql-tag';

export const ESTATE_FEATURES_HOUSE_META_FRAGMENT = gql`
  fragment EstateFeaturesHouseMetaFragment on DetailedHouse {
    landSurface
    usableSurface
    furnishment
  }
`;

export const ESTATE_FEATURES_FLAT_META_FRAGMENT = gql`
  fragment EstateFeaturesFlatMetaFragment on DetailedFlat {
    usableSurface
    furnishment
    floor
    construction
    disposition
  }
`;

export const ESTATE_FEATURES_BUSINESS_META_FRAGMENT = gql`
  fragment EstateFeaturesBusinessMetaFragment on DetailedBusiness {
    businessType
    usableSurface
  }
`;

export const ESTATE_FEATURES_LAND_META_FRAGMENT = gql`
  fragment EstateFeaturesLandMetaFragment on DetailedLand {
    landType
    landSurface
  }
`;

export const ESTATE_FEATURES_META_FRAGMENT = gql`
  fragment EstateFeaturesMetaFragment on GeneralEstate {
    estateType
    ownership
    ...EstateFeaturesHouseMetaFragment
    ...EstateFeaturesFlatMetaFragment
    ...EstateFeaturesBusinessMetaFragment
    ...EstateFeaturesLandMetaFragment
  }
  ${ESTATE_FEATURES_HOUSE_META_FRAGMENT}
  ${ESTATE_FEATURES_FLAT_META_FRAGMENT}
  ${ESTATE_FEATURES_BUSINESS_META_FRAGMENT}
  ${ESTATE_FEATURES_LAND_META_FRAGMENT}
`;
