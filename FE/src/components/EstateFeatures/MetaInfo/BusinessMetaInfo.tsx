import { BusinessType } from 'global-types';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { EstateFeaturesBusinessMetaFragment } from './__generated__/EstateFeaturesBusinessMetaFragment';

export default (({ data }) => {
  const { t } = useTranslation(nameof<BusinessType>());
  return (
    <>
      <tr>
        <td>Business type:</td>
        <td>{data.businessType ? t(data.businessType) : '??'}</td>
      </tr>
      <tr>
        <td>Usable surface:</td>
        <td>{`${data.usableSurface || '??'} m2`}</td>
      </tr>
    </>
  );
}) as FC<{ data: EstateFeaturesBusinessMetaFragment }>;
