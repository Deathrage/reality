import { ConstructionMaterial, FlatDisposition, Furnishment } from 'global-types';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { EstateFeaturesFlatMetaFragment } from './__generated__/EstateFeaturesFlatMetaFragment';

export default (({ data }) => {
  const { t: furnishmentT } = useTranslation(nameof<Furnishment>());
  const { t: constructionT } = useTranslation(nameof<ConstructionMaterial>());
  const { t: dispositionT } = useTranslation(nameof<FlatDisposition>());

  return (
    <>
      <tr>
        <td>Disposition:</td>
        <td>{data.disposition ? dispositionT(data.disposition) : '??'}</td>
      </tr>
      <tr>
        <td>Usable surface:</td>
        <td>{`${data.usableSurface || '??'} m2`}</td>
      </tr>
      <tr>
        <td>Floor:</td>
        <td>{data.floor || '??'}</td>
      </tr>
      <tr>
        <td>Furnishment:</td>
        <td>{data.furnishment ? furnishmentT(data.furnishment) : '??'}</td>
      </tr>
      <tr>
        <td>Construction:</td>
        <td>{data.construction ? constructionT(data.construction) : '??'}</td>
      </tr>
    </>
  );
}) as FC<{ data: EstateFeaturesFlatMetaFragment }>;
