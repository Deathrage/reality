import React, { FC } from 'react';
import { getItem } from './helpers';
import { EstateFeaturesHouseListFragment } from './__generated__/EstateFeaturesHouseListFragment';

export default (({ data }) => {
  return (
    <>
      {getItem('Balcony', data?.balcony)}
      {getItem('Terrace', data?.terrace)}
      {getItem('Garage', data?.garage)}
      {getItem('New building', data?.newBuilding)}
      {getItem('Low energy', data?.lowEnergy)}
    </>
  );
}) as FC<{ data: EstateFeaturesHouseListFragment }>;
