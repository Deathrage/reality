import { EstateType } from 'global-types';
import React, { FC } from 'react';
import { Case } from 'react-when-then';
import FlatFeatureList from './FlatFeatureList';
import HouseFeatureList from './HouseFeatureList';
import { EstateFeaturesFlatListFragment } from './__generated__/EstateFeaturesFlatListFragment';
import { EstateFeaturesHouseListFragment } from './__generated__/EstateFeaturesHouseListFragment';
import { EstateFeaturesListFragment } from './__generated__/EstateFeaturesListFragment';

export default (({ data }) => {
  return (
    <div className="estate-features__feature-tables">
      <Case when={data?.estateType === EstateType.FLAT}>
        <FlatFeatureList data={data as EstateFeaturesFlatListFragment} />
      </Case>
      <Case when={data?.estateType === EstateType.HOUSE}>
        <HouseFeatureList data={data as EstateFeaturesHouseListFragment} />
      </Case>
    </div>
  );
}) as FC<{ data: EstateFeaturesListFragment | undefined }>;
