/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { EstateType } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL fragment: EstateFeaturesListFragment
// ====================================================

export interface EstateFeaturesListFragment_DetailedLand {
  estateType: EstateType;
}

export interface EstateFeaturesListFragment_DetailedFlat {
  estateType: EstateType;
  balcony: boolean;
  terrace: boolean;
  garage: boolean;
  parking: boolean;
  lift: boolean;
  lowEnergy: boolean;
  newBuilding: boolean;
}

export interface EstateFeaturesListFragment_DetailedHouse {
  estateType: EstateType;
  balcony: boolean;
  terrace: boolean;
  garage: boolean;
  lowEnergy: boolean;
  newBuilding: boolean;
}

export type EstateFeaturesListFragment = EstateFeaturesListFragment_DetailedLand | EstateFeaturesListFragment_DetailedFlat | EstateFeaturesListFragment_DetailedHouse;
