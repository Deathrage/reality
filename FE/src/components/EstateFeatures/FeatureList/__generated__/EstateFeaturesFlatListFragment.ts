/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: EstateFeaturesFlatListFragment
// ====================================================

export interface EstateFeaturesFlatListFragment {
  balcony: boolean;
  terrace: boolean;
  garage: boolean;
  parking: boolean;
  lift: boolean;
  lowEnergy: boolean;
  newBuilding: boolean;
}
