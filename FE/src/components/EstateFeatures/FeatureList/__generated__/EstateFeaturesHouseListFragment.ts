/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: EstateFeaturesHouseListFragment
// ====================================================

export interface EstateFeaturesHouseListFragment {
  balcony: boolean;
  terrace: boolean;
  garage: boolean;
  lowEnergy: boolean;
  newBuilding: boolean;
}
