import React, { FC } from 'react';
import { getItem } from './helpers';
import { EstateFeaturesFlatListFragment } from './__generated__/EstateFeaturesFlatListFragment';

export default (({ data }) => {
  return (
    <>
      {getItem('Balcony', data?.balcony)}
      {getItem('Terrace', data?.terrace)}
      {getItem('Parking', data?.parking)}
      {getItem('Garage', data?.garage)}
      {getItem('Lift', data?.lift)}
      {getItem('New building', data?.newBuilding)}
      {getItem('Low energy', data?.lowEnergy)}
    </>
  );
}) as FC<{ data: EstateFeaturesFlatListFragment }>;
