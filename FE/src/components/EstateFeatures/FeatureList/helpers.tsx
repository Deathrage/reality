import { HTMLTable, Icon } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import React, { ReactNode } from 'react';

const getIcon = (value: boolean | undefined) => <Icon icon={value ? IconNames.TICK : IconNames.CROSS} />;

export const getItem = (title: string, value: boolean | undefined): ReactNode => (
  <HTMLTable className="estate-features__feature-table">
    <thead>
      <tr>
        <th>{title}</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{getIcon(value)}</td>
      </tr>
    </tbody>
  </HTMLTable>
);
