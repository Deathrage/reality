import gql from 'graphql-tag';

export const ESTATE_FEATURES_HOUSE_LIST_FRAGMENT = gql`
  fragment EstateFeaturesHouseListFragment on DetailedHouse {
    balcony
    terrace
    garage
    lowEnergy
    newBuilding
  }
`;

export const ESTATE_FEATURES_FLAT_LIST_FRAGMENT = gql`
  fragment EstateFeaturesFlatListFragment on DetailedFlat {
    balcony
    terrace
    garage
    parking
    lift
    lowEnergy
    newBuilding
  }
`;

export const ESTATE_FEATURES_LIST_FRAGMENT = gql`
  fragment EstateFeaturesListFragment on GeneralEstate {
    estateType
    ...EstateFeaturesFlatListFragment
    ...EstateFeaturesHouseListFragment
  }
  ${ESTATE_FEATURES_FLAT_LIST_FRAGMENT}
  ${ESTATE_FEATURES_HOUSE_LIST_FRAGMENT}
`;
