import gql from 'graphql-tag';
import { ESTATE_FEATURES_LIST_FRAGMENT } from './FeatureList/fragments.graphql';
import { ESTATE_FEATURES_META_FRAGMENT } from './MetaInfo/fragments.graphql';

export const ESTATE_FEATURES_QUERY = gql`
  query EstateFeaturesQuery($id: Int!) {
    estate(id: $id) {
      id
      estateType
      ...EstateFeaturesMetaFragment
      ...EstateFeaturesListFragment
    }
  }
  ${ESTATE_FEATURES_META_FRAGMENT}
  ${ESTATE_FEATURES_LIST_FRAGMENT}
`;
