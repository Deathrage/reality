import { useQuery } from '@apollo/client';
import { Callout, Card, Intent } from '@blueprintjs/core';
import { EstateType } from 'global-types';
import React, { FC } from 'react';
import { Case } from 'react-when-then';
import MetaInfo from './MetaInfo';
import { ESTATE_FEATURES_QUERY } from './queries.graphql';
import { EstateFeaturesProps } from './types';
import { EstateFeaturesQuery, EstateFeaturesQueryVariables } from './__generated__/EstateFeaturesQuery';
import './style.scss';
import FeatureList from './FeatureList';

export default (({ estateId }) => {
  const { data, loading } = useQuery<EstateFeaturesQuery, EstateFeaturesQueryVariables>(ESTATE_FEATURES_QUERY, {
    variables: {
      id: estateId!,
    },
    skip: !estateId,
  });

  return (
    <Card className="estate-features">
      <div className="estate-features__panel">
        <MetaInfo loading={loading} data={data?.estate} />
        <Case when={!loading}>
          <FeatureList data={data?.estate} />
        </Case>
      </div>
      <Case when={data?.estate.estateType === EstateType.MISCELLANEOUS}>
        <Callout className="estate-features__callout" intent={Intent.WARNING}>
          This estate is of miscellaneous type and no additional features are known. Try visiting the original advert.
        </Callout>
      </Case>
    </Card>
  );
}) as FC<EstateFeaturesProps>;
