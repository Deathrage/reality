/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { EstateType, Ownership, Furnishment, ConstructionMaterial, FlatDisposition, BusinessType, LandType } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: EstateFeaturesQuery
// ====================================================

export interface EstateFeaturesQuery_estate_DetailedEstate {
  id: number;
  estateType: EstateType;
  ownership: Ownership | null;
}

export interface EstateFeaturesQuery_estate_DetailedHouse {
  id: number;
  estateType: EstateType;
  ownership: Ownership | null;
  landSurface: number;
  usableSurface: number;
  furnishment: Furnishment | null;
  balcony: boolean;
  terrace: boolean;
  garage: boolean;
  lowEnergy: boolean;
  newBuilding: boolean;
}

export interface EstateFeaturesQuery_estate_DetailedFlat {
  id: number;
  estateType: EstateType;
  ownership: Ownership | null;
  usableSurface: number;
  furnishment: Furnishment | null;
  floor: number | null;
  construction: ConstructionMaterial | null;
  disposition: FlatDisposition | null;
  balcony: boolean;
  terrace: boolean;
  garage: boolean;
  parking: boolean;
  lift: boolean;
  lowEnergy: boolean;
  newBuilding: boolean;
}

export interface EstateFeaturesQuery_estate_DetailedBusiness {
  id: number;
  estateType: EstateType;
  ownership: Ownership | null;
  businessType: BusinessType | null;
  usableSurface: number;
}

export interface EstateFeaturesQuery_estate_DetailedLand {
  id: number;
  estateType: EstateType;
  ownership: Ownership | null;
  landType: LandType | null;
  landSurface: number;
}

export type EstateFeaturesQuery_estate = EstateFeaturesQuery_estate_DetailedEstate | EstateFeaturesQuery_estate_DetailedHouse | EstateFeaturesQuery_estate_DetailedFlat | EstateFeaturesQuery_estate_DetailedBusiness | EstateFeaturesQuery_estate_DetailedLand;

export interface EstateFeaturesQuery {
  estate: EstateFeaturesQuery_estate;
}

export interface EstateFeaturesQueryVariables {
  id: number;
}
