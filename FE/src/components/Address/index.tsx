import { Callout, Classes, Intent } from '@blueprintjs/core';
import classNames from 'classnames';
import React, { FC } from 'react';
import { AddressProps } from './types';
import './style.scss';

export default (({
  loading,
  street,
  streetNumber,
  municipality,
  countrySecondarySubdivision,
  countrySubdivision,
  country,
  accurate,
}) => {
  const commonClassName = classNames('address__paragraph', { [Classes.SKELETON]: loading });

  return (
    <Callout className="address" title="Address">
      <p className={commonClassName}>
        {street}
        {streetNumber ? ` ${streetNumber}` : ''}, {municipality}
      </p>
      <p className={commonClassName}>
        {countrySecondarySubdivision === countrySubdivision
          ? countrySubdivision
          : `${countrySecondarySubdivision}, ${countrySubdivision}`}
      </p>
      <p className={commonClassName}>{country}</p>
      {!accurate && <Callout intent={Intent.WARNING}>Might be inaccurate</Callout>}
    </Callout>
  );
}) as FC<AddressProps>;
