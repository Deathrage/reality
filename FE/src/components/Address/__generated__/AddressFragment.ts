/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AddressFragment
// ====================================================

export interface AddressFragment {
  streetNumber: string | null;
  street: string | null;
  municipality: string | null;
  countrySubdivision: string | null;
  countrySecondarySubdivision: string | null;
  country: string | null;
}
