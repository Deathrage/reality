import { AddressFragment } from './__generated__/AddressFragment';

export interface AddressProps extends Partial<AddressFragment> {
  loading: boolean;
  accurate: boolean;
}
