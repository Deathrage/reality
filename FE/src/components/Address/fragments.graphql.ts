import gql from 'graphql-tag';

export const ADDRESS_FRAGMENT = gql`
  fragment AddressFragment on DetailedAddress {
    streetNumber
    street
    municipality
    countrySubdivision
    countrySecondarySubdivision
    country
  }
`;
