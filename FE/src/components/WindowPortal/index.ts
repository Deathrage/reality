import { PureComponent, ReactNode } from 'react';
import { createPortal } from 'react-dom';
import { WindowPortalProps, WindowPortalState } from './type';
import { v4 } from 'uuid';

export default class WindowPortal extends PureComponent<WindowPortalProps, WindowPortalState> {
  private window: Window | null = null;
  private container: HTMLElement | null = null;
  private unloaded = false;
  private windowCheckerInterval: number | null = null;

  private unload() {
    if (this.unloaded) return;
    this.unloaded = true;

    this.window?.close();

    if (this.windowCheckerInterval) clearInterval(this.windowCheckerInterval);

    const { onUnload } = this.props;
    this.setState({ mounted: false }, () => {
      if (typeof onUnload === 'function') onUnload();
    });
  }

  private load() {
    const { title, width, height, onLoad } = this.props;

    this.window = window.open(`${window.location.href}sub.html`, v4(), `width=${width}px height=${height}px`);
    this.window?.addEventListener('load', () => {
      if (!this.window) return;

      this.container = this.window.document.getElementById('sub-root') ?? null;
      this.window.document.title = title;

      this.setState({ mounted: true }, () => {
        if (typeof onLoad === 'function' && this.window) onLoad(this.window);
      });
    });

    window.addEventListener('beforeunload', () => this.unload());
    this.window?.addEventListener('beforeunload', () => this.unload());
    this.windowCheckerInterval = window.setInterval(() => {
      if (!this.window || this.window.closed) {
        this.unload();
      }
    }, 100);
  }

  constructor(props: WindowPortalProps) {
    super(props);

    this.state = { mounted: false };
  }

  focus(): void {
    this.window?.focus();
  }

  componentDidMount(): void {
    this.load();
  }

  componentWillUnmount(): void {
    this.unload();
  }

  render(): ReactNode {
    if (!this.state.mounted || !this.container) return null;
    return createPortal(this.props.children, this.container);
  }
}
