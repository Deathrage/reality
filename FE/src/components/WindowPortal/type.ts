import { ReactNode } from 'react';

export interface WindowPortalProps {
  title: string;
  width: number;
  height: number;
  onUnload?: () => void;
  onLoad?: (window: Window) => void;
}

export interface WindowPortalState {
  mounted: boolean;
}
