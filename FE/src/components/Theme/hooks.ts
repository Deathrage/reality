import { useContext } from 'react';
import context from './context';
import { ThemeContext } from './types';

export const useThemeContext = (): ThemeContext => useContext(context);

export const useThemeClassName = (): string => {
  const { themeClassName } = useThemeContext();
  return themeClassName;
};
