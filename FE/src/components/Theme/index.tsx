import React, { FC, useLayoutEffect, useMemo } from 'react';
import { Classes } from '@blueprintjs/core';
import './style.scss';
import Context from './context';
import { ThemeContext } from './types';

export default (({ children }) => {
  const value = useMemo<ThemeContext>(
    () => ({
      themeClassName: Classes.DARK,
    }),
    [],
  );

  useLayoutEffect(() => {
    document.body.classList.add(Classes.DARK, 'theme');
  }, []);

  return <Context.Provider value={value}>{children}</Context.Provider>;
}) as FC;
