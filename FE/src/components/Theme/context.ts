import emptyContext from '@helpers/emptyContext';
import { createContext } from 'react';
import { ThemeContext } from './types';

export default createContext(emptyContext<ThemeContext>(nameof<ThemeContext>()));
