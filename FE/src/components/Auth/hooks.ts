import { useContext } from 'react';
import context from './context';
import { AuthContext } from './types';

export const useAuthContext = (): AuthContext => useContext(context);
export const useAuthPromp = (): (() => void) => {
  const { prompt } = useAuthContext();
  return prompt;
};
