import { Alert, H3, Icon, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { useBackdrop } from '@components/Backdrop/hooks';
import { useEndpoints } from '@components/Environment/hooks';
import React, { FC, useCallback, useEffect, useMemo, useState } from 'react';
import Context from './context';
import './style.scss';
import { AuthContext, Me } from './types';

export default (({ children }) => {
  const [isOpen, setIsOpen] = useState(false);

  const [loading, setLoading] = useState(false);
  const [authenticated, setAuthenticated] = useState<boolean | undefined>(undefined);
  const [userName, setUserName] = useState<string | undefined>(undefined);

  const { ME, SIGN_OUT, SIGN_IN } = useEndpoints();
  const { open, close } = useBackdrop();

  useEffect(() => {
    if (loading || typeof authenticated === 'boolean') return;

    open();
    setLoading(true);

    fetch(ME, { method: 'GET', credentials: 'include' }).then(response => {
      setAuthenticated(response.status === 200);

      response.json().then((mes: Me[]) => {
        const userName = (mes ?? [])[0]?.user_id;
        setUserName(userName);
      });

      close();
      setLoading(false);
    });
  }, [ME, authenticated, close, loading, open]);

  const prompt = useCallback(() => setIsOpen(true), []);

  const signOut = useCallback(() => {
    const self = encodeURIComponent(document.location.href);
    document.location.href = `${SIGN_OUT}?post_logout_redirect_uri=${self}`;
  }, [SIGN_OUT]);

  const value = useMemo<AuthContext>(
    () => ({
      prompt,
      authenticated: authenticated ?? false,
      signOut,
      userName,
    }),
    [authenticated, signOut, prompt, userName],
  );

  const onConfirm = useCallback(() => {
    const self = encodeURIComponent(document.location.href);
    const url = authenticated
      ? `${SIGN_OUT}?post_logout_redirect_uri=${self}`
      : `${SIGN_IN}?post_login_redirect_url=${self}`;

    window.location.href = url;
  }, [SIGN_IN, SIGN_OUT, authenticated]);

  return (
    <Context.Provider value={value}>
      {!loading && typeof authenticated === 'boolean' && children}
      <Alert
        isOpen={isOpen}
        intent={Intent.WARNING}
        confirmButtonText={authenticated ? 'Sign out' : 'Sign in'}
        onConfirm={onConfirm}
        className="auth"
      >
        <div className="auth__grid">
          <Icon icon={IconNames.LOCK} intent={Intent.WARNING} iconSize={40} className="auth__icon" />
          <H3>
            You are not {authenticated ? 'authorized to access this resource' : 'authenticated'}. Please sign in
            {authenticated ? ' using an authorized account' : ''}.
          </H3>
        </div>
        {!authenticated && (
          <>
            <p>
              Some browsers might evaluate the authentication cookie as a cross site tracker and block it in CORS
              requests. If you are repeatedly seeing this window:
            </p>
            <ul>
              <li>Chrome - try visiting the site outside incognito mode</li>
              <li>Brave - try lowering the Brave Shield</li>
            </ul>
          </>
        )}
      </Alert>
    </Context.Provider>
  );
}) as FC;
