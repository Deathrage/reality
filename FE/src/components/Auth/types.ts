export interface AuthContext {
  prompt: () => void;
  authenticated: boolean;
  userName: string | undefined;
  signOut: () => void;
}

export interface Me {
  user_id: string;
}
