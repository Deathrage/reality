import emptyContext from '@helpers/emptyContext';
import { createContext } from 'react';
import { AuthContext } from './types';

export default createContext<AuthContext>(emptyContext(nameof<AuthContext>()));
