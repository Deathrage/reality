import React, { FC } from 'react';

import './style.scss';

export default (({ children }) => {
  return (
    <div className="page">
      <>{children}</>
    </div>
  );
}) as FC;
