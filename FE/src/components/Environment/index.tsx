import React, { FC, useState, useEffect, useMemo } from 'react';
import Context from './context';
import { EnvironmentProps, EnvironmentVariables } from './types';
import { parse } from 'js-ini';
import endpointsFactory, { EndpointsDictionary } from './endpointsFactory';
import { EnvironmentContext as EnvironmentContextType } from './types';
import emptyContext from '@helpers/emptyContext';
import { useBackdrop } from '@components/Backdrop/hooks';

export default (({ children, clientVersion }) => {
  const [envConfig, setEnvConfig] = useState<EnvironmentVariables>();
  const [apiVersion, setApiVersion] = useState<string>();
  const [loading, setLoading] = useState<boolean>(true);

  const { open, close } = useBackdrop();
  useEffect(() => {
    if (loading) open();
    else close();
  }, [close, loading, open]);

  useEffect(() => {
    fetch('/.env')
      .then(response => response.text())
      .then(
        text =>
          // ini parse is not generic and returns dictionary
          (parse(text) as unknown) as EnvironmentVariables,
      )
      .then(config => setEnvConfig(config));
  }, []);

  const endpoints = useMemo(() => envConfig && endpointsFactory(envConfig.API_URL), [envConfig]);

  useEffect(() => {
    if (!endpoints) return;
    fetch(endpoints.VERSION)
      .then(response => response.text())
      .then(version => setApiVersion(version))
      .then(() => setLoading(false));
  }, [endpoints]);

  const context = useMemo<EnvironmentContextType | null>(
    () =>
      !loading
        ? {
            version: { client: clientVersion, api: apiVersion ?? null },
            endpoints: endpoints as EndpointsDictionary,
            azureMapsKey: envConfig?.AZURE_MAPS_KEY as string,
          }
        : null,
    [apiVersion, clientVersion, endpoints, loading, envConfig],
  );

  return (
    <Context.Provider
      value={
        loading
          ? emptyContext<EnvironmentContextType>(nameof<EnvironmentContextType>())
          : (context as EnvironmentContextType)
      }
    >
      {!loading && children}
    </Context.Provider>
  );
}) as FC<EnvironmentProps>;
