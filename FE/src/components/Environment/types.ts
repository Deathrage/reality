import { EndpointsDictionary } from './endpointsFactory';

export interface EnvironmentVariables {
  API_URL: string;
  AZURE_MAPS_KEY: string;
}

export interface EnvironmentProps {
  clientVersion: string;
}

export interface EnvironmentContextVersions {
  client: string | null;
  api: string | null;
}

export interface EnvironmentContext {
  version: EnvironmentContextVersions;
  endpoints: EndpointsDictionary;
  azureMapsKey: string;
}
