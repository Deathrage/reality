import React, { createContext } from 'react';
import { EnvironmentContext } from './types';
import emptyContext from '@helpers/emptyContext';

export default createContext<EnvironmentContext>(emptyContext(nameof<EnvironmentContext>()));
