export type Endpoints = 'VERSION' | 'TEST' | 'GRAPHQL' | 'ME' | 'SIGN_OUT' | 'SIGN_IN' | 'FRAME_PROXY';

export type EndpointsDictionary = {
  [Endpoint in Endpoints]: string;
};

export default (basePath: string): EndpointsDictionary => ({
  ME: `${basePath}/.auth/me`,
  SIGN_IN: `${basePath}/.auth/login/aad`,
  SIGN_OUT: `${basePath}/.auth/logout`,
  VERSION: `${basePath}/api/version`,
  GRAPHQL: `${basePath}/api/ui/graphql`,
  FRAME_PROXY: `${basePath}/api/ui/frame-proxy`,
  TEST: 'test',
});
