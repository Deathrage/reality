import { useContext } from 'react';
import context from './context';
import { EnvironmentContext, EnvironmentContextVersions } from './types';
import { EndpointsDictionary } from './endpointsFactory';

export const useEnvironmentContext = (): EnvironmentContext => useContext(context);

export const useApplicationVersion = (): EnvironmentContextVersions => {
  const { version } = useEnvironmentContext();
  return version;
};

export const useEndpoints = (): EndpointsDictionary => {
  const { endpoints } = useEnvironmentContext();
  return endpoints;
};

export const useAzureMapsKey = (): string => {
  const { azureMapsKey } = useEnvironmentContext();
  return azureMapsKey;
};
