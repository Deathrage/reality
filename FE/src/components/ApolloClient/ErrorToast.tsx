import React, { FC } from 'react';

export default (({ data }) => {
  return (
    <div className="error-toast">
      {(data ?? []).map(message => (
        <p key={message} className="error-toast__message">
          {message}
        </p>
      ))}
    </div>
  );
}) as FC<{
  data: string[];
}>;
