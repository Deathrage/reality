import React, { FC, useMemo, useRef } from 'react';
import { useEndpoints } from '@components/Environment/hooks';
import { ApolloClient, InMemoryCache, ApolloProvider, from, HttpLink } from '@apollo/client';
import { ErrorLink } from '@apollo/client/link/error';
import { Toaster } from '@blueprintjs/core';
import { errorHandlerFactory } from './helpers';
import './style.scss';
import { useAuthPromp } from '@components/Auth/hooks';
import schema from 'schema';

const possibleTypeEntries: [string, string[]][] = schema.__schema.types
  .filter(type => type.possibleTypes)
  .map(type => [type.name, type.possibleTypes!.map(possibleType => possibleType.name)]);
const possibleTypes: Record<string, string[]> = Object.fromEntries(possibleTypeEntries);

const cache = new InMemoryCache({ possibleTypes });

export default (({ children }) => {
  const toaster = useRef<Toaster>();

  const { GRAPHQL } = useEndpoints();
  const authPromp = useAuthPromp();

  const client = useMemo(
    () =>
      new ApolloClient({
        cache,
        link: from([
          new ErrorLink(errorHandlerFactory(toaster, authPromp)),
          new HttpLink({ uri: GRAPHQL, credentials: 'include' }),
        ]),
        defaultOptions: {
          query: {
            fetchPolicy: 'network-only',
          },
        },
        connectToDevTools: true,
      }),
    [GRAPHQL, authPromp],
  );

  return (
    <ApolloProvider client={client}>
      <Toaster
        ref={ref => {
          toaster.current = ref ?? undefined;
        }}
      />
      {children}
    </ApolloProvider>
  );
}) as FC;
