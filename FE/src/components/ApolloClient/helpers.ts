import { ServerError } from '@apollo/client';
import { ErrorLink } from '@apollo/client/link/error';
import { Intent, Toaster } from '@blueprintjs/core';
import { createElement, RefObject } from 'react';
import ErrorToast from './ErrorToast';

export const errorHandlerFactory = (
  toasterRef: RefObject<Toaster | undefined>,
  onUnauthorized: () => void,
): ErrorLink.ErrorHandler => ({ networkError, graphQLErrors }) => {
  if (!toasterRef?.current) return;
  const { current: toaster } = toasterRef;

  const serverError = networkError as ServerError;
  if (serverError?.statusCode === 401) return onUnauthorized();

  const messages =
    graphQLErrors?.map(({ message, path, extensions }) => {
      const errorMessage = `${message} at path ${(path ?? []).join('.')}`;
      console.error(errorMessage, extensions);
      return extensions?.message || errorMessage;
    }) ?? [];

  if (networkError) {
    messages.push(networkError.message);
    console.error(`${networkError.message} at ${networkError.stack}`);
  }

  toaster.show({ message: createElement(ErrorToast, { data: messages }), intent: Intent.DANGER, timeout: 20000 });
};
