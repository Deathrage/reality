import { useContext } from 'react';
import context from './context';
import { IntlContext } from './types';

export const useIntl = (): IntlContext => useContext(context);

export const useCurrentCulture = (): string => {
  const { culture } = useIntl();
  return culture;
};
