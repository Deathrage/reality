import moment from 'moment';
import React, { FC, useMemo } from 'react';
import Context from './context';
import { TimeParse, IntlContext, TimeFormat } from './types';

export default (({ children, culture }) => {
  const contextValue = useMemo<IntlContext>(
    () => ({
      culture,
      formatNumber: (value, options) => new Intl.NumberFormat(culture, options).format(value),
      formatDate: (value, format) => {
        if (typeof value === 'string') value = moment(value, undefined, culture).startOf('day').toDate();

        return format === TimeFormat.WITHOUT_TIME ? value.toLocaleDateString(culture) : value.toLocaleString(culture);
      },
      parseDate: (value, format) => {
        const date = moment(value, undefined, culture);

        if (format === TimeParse.END_OF_DAY) date.endOf('day');
        else if (format === TimeParse.START_OF_DAY) date.startOf('day');

        return date.toDate();
      },
    }),
    [culture],
  );

  return <Context.Provider value={contextValue}>{children}</Context.Provider>;
}) as FC<{ culture: string }>;
