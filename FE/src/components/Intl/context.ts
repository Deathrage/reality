import emptyContext from '@helpers/emptyContext';
import { createContext } from 'react';
import { IntlContext } from './types';

export default createContext<IntlContext>(emptyContext(nameof<IntlContext>()));
