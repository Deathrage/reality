export interface IntlContext {
  culture: string;
  formatNumber: (value: number, options: Intl.NumberFormatOptions) => string;
  formatDate: (value: Date | string, option: TimeFormat) => string;
  parseDate: (value: string, option: TimeParse) => Date;
}

export enum TimeFormat {
  WITH_TIME,
  WITHOUT_TIME,
}

export enum TimeParse {
  WITH_TIME,
  START_OF_DAY,
  END_OF_DAY,
}
