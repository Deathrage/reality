export interface CurrentDocumentContext {
  currentDocument: Document;
}
export type CurrentDocumentProps = CurrentDocumentContext;
