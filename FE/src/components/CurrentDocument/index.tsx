import React, { useMemo } from 'react';
import { FC } from 'react';
import Context from './context';
import { CurrentDocumentContext, CurrentDocumentProps } from './types';

export default (({ children, currentDocument }) => {
  const value = useMemo<CurrentDocumentContext>(() => ({ currentDocument }), [currentDocument]);

  return <Context.Provider value={value}>{children}</Context.Provider>;
}) as FC<CurrentDocumentProps>;
