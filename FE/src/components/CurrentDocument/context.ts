import emptyContext from '@helpers/emptyContext';
import { createContext } from 'react';
import { CurrentDocumentContext } from './types';

export default createContext<CurrentDocumentContext>(emptyContext(nameof<CurrentDocumentContext>()));
