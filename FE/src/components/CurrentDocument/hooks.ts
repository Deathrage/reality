import { useContext } from 'react';
import context from './context';
import { CurrentDocumentContext } from './types';

export const useCurrentDocumentContext = (): CurrentDocumentContext => useContext(context);

export const useCurrentDocument = (): Document => {
  const { currentDocument } = useCurrentDocumentContext();
  return currentDocument;
};
