import { ActiveWorkspaceItem, WorkspaceItem } from './state/types';
export { WorkspaceComponentProps } from './state/types';

export interface WorkspaceProps {
  initialToolbox: WorkspaceItem[];
  initialActive: ActiveWorkspaceItem[];
}
