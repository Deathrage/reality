import React, { FC, useCallback, useMemo, useRef } from 'react';
import './style.scss';
import RGL, { WidthProvider } from 'react-grid-layout';
import './style.scss';
import WorkspaceGridItem from './WorkspaceGridItem';
import { EventHandler, WorkspaceGridContext, WorkspaceGridProps, Subscribe, Topics } from './types';
import Context from './context';
import SubscriptionHub from '@helpers/SubscriptionHub';

const ReactGridLayout = WidthProvider(RGL);

const WorkspaceGrid: FC<WorkspaceGridProps> & { Item: typeof WorkspaceGridItem } = ({
  children,
  layout,
  onLayoutUpdate,
}) => {
  const subscriptionHub = useRef(new SubscriptionHub<EventHandler, []>());

  const onResizeStart = useCallback(() => subscriptionHub.current.dispatch(Topics.RESIZE_START), []);
  const onResize = useCallback(() => subscriptionHub.current.dispatch(Topics.RESIZE), []);
  const onResizeStop = useCallback(() => subscriptionHub.current.dispatch(Topics.RESIZE_STOP), []);

  const onResizeStartSubscribe = useCallback<Subscribe>(
    handler => subscriptionHub.current.subscribe(Topics.RESIZE_START, handler),
    [],
  );

  const onResizeSubscribe = useCallback<Subscribe>(
    handler => subscriptionHub.current.subscribe(Topics.RESIZE, handler),
    [],
  );

  const onResizeStopSubscribe = useCallback<Subscribe>(
    handler => subscriptionHub.current.subscribe(Topics.RESIZE_STOP, handler),
    [],
  );

  const value = useMemo<WorkspaceGridContext>(
    () => ({
      onResizeStart: onResizeStartSubscribe,
      onResizeStop: onResizeStopSubscribe,
      onResize: onResizeSubscribe,
    }),
    [onResizeStopSubscribe, onResizeStartSubscribe, onResizeSubscribe],
  );

  return (
    <Context.Provider value={value}>
      <ReactGridLayout
        layout={layout}
        cols={12}
        measureBeforeMount
        draggableHandle=".workspace-grid-item .workspace-grid-item__navbar"
        draggableCancel=".workspace-grid-item .workspace-grid-item__controls"
        onResizeStart={onResizeStart}
        onResizeStop={onResizeStop}
        onResize={onResize}
        onLayoutChange={onLayoutUpdate}
        preventCollision
        /*        resizeHandle={axis => {
          console.log(axis);
          return <div>foo</div>;
        }} */
      >
        {children}
      </ReactGridLayout>
    </Context.Provider>
  );
};

WorkspaceGrid.Item = WorkspaceGridItem;

export default WorkspaceGrid;
