import { Button, Intent, NonIdealState } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import React, { FC } from 'react';
import { DetachedMessageProps } from './types';

export default (({ onAttach, onBringForward }) => (
  <div className="workspace-grid-item__detached">
    <NonIdealState
      icon={IconNames.MAXIMIZE}
      title="This window has been detached"
      description="Please reattach the window in order to use this panel"
      action={
        <div className="workspace-grid-item__attach-menu">
          <Button icon={IconNames.EYE_OPEN} intent={Intent.WARNING} onClick={onBringForward}>
            Bring forward
          </Button>
          <Button icon={IconNames.MINIMIZE} intent={Intent.WARNING} onClick={onAttach}>
            Attach
          </Button>
        </div>
      }
    />
  </div>
)) as FC<DetachedMessageProps>;
