import { IconName } from '@blueprintjs/core';
import { Unsubscribe } from '@helpers/SubscriptionHub';
import { ReactNode, ReactText } from 'react';
import { Layout } from 'react-grid-layout';

export type WorkspaceGridComponentRender = (controls: WorkspaceGridComponentProps) => ReactNode;

export interface WorkspaceGridItemProps {
  className?: string;
  title: ReactText;
  icon: IconName;
  children: WorkspaceGridComponentRender;
  onClose?: () => void;
  closeable?: boolean;
  detachable?: boolean;
}

export interface WorkspaceGridProps {
  layout: Layout[];
  onLayoutUpdate: (newLayout: Layout[]) => void;
}

export type EventHandler = () => void;
export type Subscribe = (handler: EventHandler) => Unsubscribe;

export interface WorkspaceGridContext {
  onResizeStart: Subscribe;
  onResizeStop: Subscribe;
  onResize: Subscribe;
}

export interface WorkspaceGridComponentProps extends WorkspaceGridContext {
  detached: boolean;
  /** Window in which is the component being rendered. Initial page window object or window object of the detached child. */
  window: Window;
}

export interface DetachedMessageProps {
  onAttach: () => void;
  onBringForward: () => void;
}

export enum Topics {
  RESIZE_START = 'LAYOUT_GRID/RESIZE_START',
  RESIZE = 'LAYOUT_GRID/RESIZE',
  RESIZE_STOP = 'LAYOUT_GRID/RESIZE_STOP',
}
export interface ContentProps {
  className?: string;
  currentWindow: Window;
  isAttached: boolean;
  children: WorkspaceGridComponentRender;
}

export interface RibbonProps {
  titleIcon: IconName;
  title: ReactText;
  closeable?: boolean;
  detachable?: boolean;
  isAttached: boolean;
  onAttachClick: () => void;
  onDetachClick: () => void;
  onCloseClick?: () => void;
}
