import emptyContext from '@helpers/emptyContext';
import { createContext } from 'react';
import { WorkspaceGridContext } from './types';

export default createContext<WorkspaceGridContext>(emptyContext(nameof<WorkspaceGridContext>()));
