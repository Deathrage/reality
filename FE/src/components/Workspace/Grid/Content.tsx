import CurrentDocument from '@components/CurrentDocument';
import classNames from 'classnames';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react';
import React, { FC } from 'react';
import { useWorkspaceGridContext } from './hooks';
import { ContentProps, WorkspaceGridComponentProps } from './types';

export default (({ currentWindow, isAttached, children, className }) => {
  const context = useWorkspaceGridContext();

  const childProps: WorkspaceGridComponentProps = { ...context, detached: !isAttached, window: currentWindow };
  return (
    <div className={classNames(className, 'workspace-grid-item__container')}>
      <OverlayScrollbarsComponent style={{ height: '100%', width: '100%' }}>
        <CurrentDocument currentDocument={currentWindow.document}>{children && children(childProps)}</CurrentDocument>
      </OverlayScrollbarsComponent>
    </div>
  );
}) as FC<ContentProps>;
