import { useContext } from 'react';
import context from './context';
import { WorkspaceGridContext } from './types';

export const useWorkspaceGridContext = (): WorkspaceGridContext => useContext(context);
