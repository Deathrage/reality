import { Button, ButtonGroup, Icon, Intent, Menu, Tooltip } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import React, { FC } from 'react';
import { Case } from 'react-when-then';
import { RibbonProps } from './types';

export default (({
  titleIcon,
  title,
  detachable,
  closeable,
  isAttached,
  onAttachClick,
  onDetachClick,
  onCloseClick,
}) => {
  return (
    <Menu className="workspace-grid-item__navbar">
      <span>
        <Case when={!!titleIcon}>
          <span className="workspace-grid-item__icon">
            <Icon icon={titleIcon} />
          </span>
        </Case>
        <span className="workspace-grid-item__title">{title}</span>
      </span>
      <ButtonGroup className="workspace-grid-item__controls">
        <Case when={detachable}>
          <Tooltip content={!isAttached ? 'Attach back to the panel' : 'Detach to the window'}>
            <Button
              className="workspace-grid-item__controls-button"
              onClick={() => (isAttached ? onDetachClick() : onAttachClick())}
            >
              <Icon
                icon={!isAttached ? IconNames.MINIMIZE : IconNames.MAXIMIZE}
                iconSize={12}
                className="workspace-grid-item__controls-icon"
              />
            </Button>
          </Tooltip>
        </Case>
        <Case when={closeable}>
          <Tooltip content="Close">
            <Button
              className="workspace-grid-item__controls-button workspace-grid-item__controls-button-close"
              onClick={onCloseClick}
            >
              <Icon icon={IconNames.CROSS} iconSize={12} className="workspace-grid-item__controls-icon" />
            </Button>
          </Tooltip>
        </Case>
      </ButtonGroup>
    </Menu>
  );
}) as FC<RibbonProps>;
