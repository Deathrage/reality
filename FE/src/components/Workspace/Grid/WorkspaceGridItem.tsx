import { useThemeClassName } from '@components/Theme/hooks';
import WindowPortal from '@components/WindowPortal';
import React, { FC, ReactNode, useMemo, useRef, useState } from 'react';
import { Falsy, Ternary, Truthy } from 'react-ternary-operator';
import Content from './Content';
import DetachedMessage from './DetachedMessage';
import Ribbon from './Ribbon';
import { WorkspaceGridItemProps } from './types';

export default (function WorkspaceGridItem({ children, className, title, icon, onClose, closeable, detachable }) {
  const themeClassName = useThemeClassName();

  const windowRef = useRef<WindowPortal>();
  const [remoteWindow, setRemoteWindow] = useState<Window | null>(null);

  const [isAttached, setIsAttached] = useState(true);

  const ribbon = (
    <Ribbon
      title={title}
      titleIcon={icon}
      isAttached={isAttached}
      onDetachClick={() => setIsAttached(false)}
      onAttachClick={() => setIsAttached(true)}
      onCloseClick={onClose}
      closeable={closeable}
      detachable={detachable}
    />
  );

  const content = (
    <Content isAttached={isAttached} className={className} currentWindow={remoteWindow ?? window}>
      {children}
    </Content>
  );

  return (
    <div className="workspace-grid-item">
      {ribbon}
      <Ternary condition={!isAttached}>
        <Truthy>
          <DetachedMessage
            onAttach={() => setIsAttached(true)}
            onBringForward={() => {
              windowRef.current?.focus();
            }}
          />
          <WindowPortal
            onUnload={() => {
              setRemoteWindow(null);
              setIsAttached(true);
            }}
            onLoad={window => {
              window.document.body.classList.add(themeClassName);
              setRemoteWindow(window);
            }}
            title={String(title)}
            width={1000}
            height={1000}
            ref={ref => {
              windowRef.current = ref ?? undefined;
            }}
          >
            <div className="workspace-grid-item">
              {ribbon}
              {content}
            </div>
          </WindowPortal>
        </Truthy>
        <Falsy>{content}</Falsy>
      </Ternary>
    </div>
  );
} as FC<WorkspaceGridItemProps>);
