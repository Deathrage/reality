import React, { FC, useCallback, useMemo, useReducer } from 'react';
import { Layout } from 'react-grid-layout';
import WorkspaceGrid from './Grid';
import Toolbox from '@components/Toolbox';
import { ActiveWorkspaceItem, WorkspaceActions } from './state/types';
import workspaceStateReducer from './state/reducer';
import { WorkspaceProps } from './types';

export default (({ initialActive, initialToolbox }) => {
  const [{ toolbox, active }, dispatch] = useReducer(workspaceStateReducer, {
    toolbox: initialToolbox,
    active: initialActive,
  });

  const onActiveLayoutUpdate = useCallback(
    (layout: Layout[]) => dispatch({ action: WorkspaceActions.UPDATE_ACTIVE_LAYOUT, layout }),
    [],
  );

  const onOpenNewRequested = useCallback((item: ActiveWorkspaceItem) => {
    dispatch({ action: WorkspaceActions.OPEN_NEW, item });
  }, []);
  const onOpenRequested = useCallback((itemKey: string) => dispatch({ action: WorkspaceActions.OPEN, itemKey }), []);
  const onCloseRequested = useCallback((itemKey: string) => dispatch({ action: WorkspaceActions.CLOSE, itemKey }), []);

  return (
    <>
      <Toolbox>
        {toolbox.map(({ i, title, icon }) => (
          <Toolbox.Item key={i} title={title} icon={icon} onClick={() => onOpenRequested(i)} />
        ))}
      </Toolbox>
      <WorkspaceGrid layout={active} onLayoutUpdate={onActiveLayoutUpdate}>
        {active.map(({ i, component: Component, icon, title, detachable, data }) => (
          <div key={i}>
            <WorkspaceGrid.Item
              icon={icon}
              title={title}
              onClose={() => onCloseRequested(i)}
              closeable
              detachable={detachable}
            >
              {controls => (
                <Component
                  {...controls}
                  open={onOpenRequested}
                  openNew={onOpenNewRequested}
                  close={onCloseRequested}
                  data={data ?? null}
                />
              )}
            </WorkspaceGrid.Item>
          </div>
        ))}
      </WorkspaceGrid>
    </>
  );
}) as FC<WorkspaceProps>;
