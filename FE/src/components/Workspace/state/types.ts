import { IconName } from '@blueprintjs/core';
import { ComponentType, ReactText } from 'react';
import { Layout } from 'react-grid-layout';
import { WorkspaceGridComponentProps } from '../Grid/types';

export enum WorkspaceActions {
  CLOSE = 'CLOSE',
  OPEN = 'OPEN',
  OPEN_NEW = 'OPEN_NEW',
  UPDATE_ACTIVE_LAYOUT = 'UPDATE_ACTIVE_LAYOUT',
}

export interface WorkspaceComponentProps<TData = unknown> extends WorkspaceGridComponentProps {
  open: (key: string) => void;
  close: (key: string) => void;
  openNew: (newItem: ActiveWorkspaceItem) => void;
  data: TData | null;
}

export interface WorkspaceItem<TData = unknown> extends Layout {
  component: ComponentType<WorkspaceComponentProps>;
  icon: IconName;
  title: ReactText;
  detachable: boolean;
  data?: TData;
}

export interface ActiveWorkspaceItem extends WorkspaceItem {
  toolboxable: boolean;
}

export interface WorkspaceState {
  toolbox: WorkspaceItem[];
  active: ActiveWorkspaceItem[];
}
