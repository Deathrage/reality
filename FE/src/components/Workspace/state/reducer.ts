import { Reducer } from 'react';
import { WorkspaceAction } from './actions';
import { ActiveWorkspaceItem, WorkspaceActions, WorkspaceState } from './types';

export default (({ toolbox, active }, action) => {
  switch (action.action) {
    // Workspace positions
    case WorkspaceActions.UPDATE_ACTIVE_LAYOUT: {
      const activeItemsMap = new Map(active.map(item => [item.i, item]));

      for (const layout of action.layout) {
        const item = activeItemsMap.get(layout.i);
        if (item) activeItemsMap.set(layout.i, { ...item, ...layout });
      }

      return {
        toolbox,
        active: Array.from(activeItemsMap.values()),
      };
    }
    // Workspace activity
    case WorkspaceActions.OPEN_NEW: {
      const newItem = action.item;
      if (!newItem || active.findIndex(({ i }) => i === newItem.i) > -1) return { toolbox, active };

      return { toolbox, active: [...active, newItem] };
    }
    case WorkspaceActions.OPEN: {
      const item = toolbox.find(item => item.i == action.itemKey);
      if (!item) return { toolbox, active };

      const newActiveItem: ActiveWorkspaceItem = { ...item, toolboxable: true };

      return {
        toolbox: toolbox.filter(toolboxItem => toolboxItem.i !== action.itemKey),
        active: [...active, newActiveItem],
      };
    }
    case WorkspaceActions.CLOSE: {
      const item = active.find(item => item.i == action.itemKey);
      if (!item) return { toolbox, active };

      return {
        toolbox: item.toolboxable ? [...toolbox, item] : toolbox,
        active: active.filter(activeItem => activeItem.i !== action.itemKey),
      };
    }
  }
}) as Reducer<WorkspaceState, WorkspaceAction>;
