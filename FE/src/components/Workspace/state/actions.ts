import { Layout } from 'react-grid-layout';
import { ActiveWorkspaceItem, WorkspaceActions } from './types';

interface WorkspaceOpenCloseAction {
  action: WorkspaceActions.CLOSE | WorkspaceActions.OPEN;
  itemKey: string;
}

interface WorkspaceOpenNewAction {
  action: WorkspaceActions.OPEN_NEW;
  item: ActiveWorkspaceItem;
}

interface WorkspaceLayoutUpdateAction {
  action: WorkspaceActions.UPDATE_ACTIVE_LAYOUT;
  layout: Layout[];
}

export type WorkspaceAction = WorkspaceOpenCloseAction | WorkspaceLayoutUpdateAction | WorkspaceOpenNewAction;
