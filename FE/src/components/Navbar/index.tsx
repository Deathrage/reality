import { Alignment, Button, Intent, Navbar } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { useAuthContext } from '@components/Auth/hooks';
import { useApplicationVersion } from '@components/Environment/hooks';
import React, { FC } from 'react';
import './style.scss';

export default (() => {
  const version = useApplicationVersion();

  const { authenticated, signOut, userName } = useAuthContext();

  return (
    <Navbar className="navbar">
      <Navbar.Group align={Alignment.LEFT}>
        <Navbar.Heading>Reality</Navbar.Heading>
      </Navbar.Group>
      <Navbar.Group align={Alignment.RIGHT}>
        {userName}
        {authenticated && (
          <Button icon={IconNames.LOG_OUT} intent={Intent.WARNING} className="navbar__sign_out" onClick={signOut}>
            Sign out
          </Button>
        )}
        <div className="version-menu">
          <div className="version-menu__item">API: {version.api}</div>
          <div className="version-menu__item">Client: {version.client}</div>
        </div>
      </Navbar.Group>
    </Navbar>
  );
}) as FC;
