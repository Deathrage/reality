import { Classes } from '@blueprintjs/core';
import classNames from 'classnames';
import React, { FC } from 'react';
import { ParagraphProps } from './types';

export default (({ loading, children }) => {
  const loadingClassName = classNames({ [Classes.SKELETON]: loading });
  return <p className={loadingClassName}>{loading ? '&nbps;' : children}</p>;
}) as FC<ParagraphProps>;
