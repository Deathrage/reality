export type Unsubscribe = () => void;

export default class SubscriptionHub<Handler extends Function, Params extends []> {
  private readonly _topicMap: Map<string, Set<Handler>> = new Map();

  private ensureTopic(topic: string): Set<Handler> {
    if (this._topicMap.has(topic)) return this._topicMap.get(topic) as Set<Handler>;

    const set = new Set<Handler>();
    this._topicMap.set(topic, set);
    return set;
  }

  unsubscribe(topic: string, handler: Handler): void {
    this.ensureTopic(topic).delete(handler);
  }

  subscribe(topic: string, handler: Handler): Unsubscribe {
    this.ensureTopic(topic).add(handler);
    return () => this.unsubscribe(topic, handler);
  }

  dispatch(topic: string, ...params: Params): void {
    for (const handler of this.ensureTopic(topic)) handler(...params);
  }
}
