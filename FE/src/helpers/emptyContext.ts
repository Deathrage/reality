export default <TContextType extends object>(contextName = ''): TContextType =>
  new Proxy<TContextType>(
    // This cast is safe as proxy will prevent accessing any member of unitialized context
    ({} as unknown) as TContextType,
    {
      get: () => {
        throw new Error(`Context ${contextName} has not been initialized!`);
      },
    },
  );
