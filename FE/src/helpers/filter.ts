import { filter as filterInternal, VariableMap } from 'graphql-anywhere';
import { DocumentNode } from 'graphql';

export default <FD = any, D extends FD = any>(
  doc: DocumentNode,
  data: D | undefined | null,
  variableValues?: VariableMap,
): FD | undefined => (data ? filterInternal(doc, data, variableValues) : undefined);
