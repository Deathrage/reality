import { EstateType } from 'global-types';

export enum Sprites {
  FLAT = 'flat',
  HOUSE = 'house',
  LAND = 'land',
  BUSINESS = 'business',
  MISC = 'misc',
}

export const getSprite = (estateType: EstateType): Sprites => {
  switch (estateType) {
    case EstateType.BUSINESS:
      return Sprites.BUSINESS;
    case EstateType.FLAT:
      return Sprites.FLAT;
    case EstateType.HOUSE:
      return Sprites.HOUSE;
    case EstateType.LAND:
      return Sprites.LAND;
    case EstateType.MISCELLANEOUS:
      return Sprites.MISC;
    default:
      throw new Error(`Cannot map sprite for ${estateType}`);
  }
};

export const isSprite = (value: unknown): value is Sprites =>
  Array.from(Object.values(Sprites)).includes(value as Sprites);

export function getSpriteUrl(sprite: Sprites): string;
export function getSpriteUrl(estateType: EstateType): string;
export function getSpriteUrl(value: Sprites | EstateType): string {
  const sprite = isSprite(value) ? value : getSprite(value);
  return `/map-markers/${sprite}.png`;
}
