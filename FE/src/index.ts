import ReactDOM from 'react-dom';
import App from './App';
import React from 'react';
import '@localization/i18n';
import './style.scss';

declare global {
  interface Window {
    isMasterWindow: boolean;
  }
}

if (window.isMasterWindow) ReactDOM.render(React.createElement(App), document.getElementById('root'));
