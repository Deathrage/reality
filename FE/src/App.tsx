import React, { FC } from 'react';
import Environment from '@components/Environment';
import clientVersion from './version.txt';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Dev from '@pages/Dev';
import Backdrop from '@components/Backdrop';
import Theme from '@components/Theme';
import ApolloClient from '@components/ApolloClient';
import NotFound from '@pages/NotFound';
import Intl from '@components/Intl';
import Auth from '@components/Auth';
import CurrentDocument from '@components/CurrentDocument';

export default (() => {
  return (
    <CurrentDocument currentDocument={document}>
      <Theme>
        <Backdrop>
          <Intl culture="cs">
            <Environment clientVersion={clientVersion}>
              <Auth>
                <ApolloClient>
                  <BrowserRouter>
                    <Switch>
                      {/* <Route path={Routes.SPLASH_SCREEN} exact component={SplashScreen} />
        <Route component={NotFound} /> */}
                      <Route component={Dev} />
                      <Route component={NotFound} />
                    </Switch>
                  </BrowserRouter>
                </ApolloClient>
              </Auth>
            </Environment>
          </Intl>
        </Backdrop>
      </Theme>
    </CurrentDocument>
  );
}) as FC;
