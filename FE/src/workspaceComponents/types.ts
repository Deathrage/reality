import { ActiveWorkspaceItem, WorkspaceComponentProps } from '@components/Workspace/state/types';
import { FC } from 'react';

export type WorkspaceItemFactory = (arg?: unknown) => ActiveWorkspaceItem;

export type WorkspaceItemFC<
  TData = unknown,
  TProps extends WorkspaceComponentProps<TData> = WorkspaceComponentProps<TData>
> = FC<TProps> & {
  workspaceItem: WorkspaceItemFactory;
};
