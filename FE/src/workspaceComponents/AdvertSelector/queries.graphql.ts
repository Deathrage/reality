import gql from 'graphql-tag';
import { ADVERT_SELECTOR_MAP_FRAGMENT } from './Map/fragments.graphql';

export const ADVERT_SELECTOR_QUERY = gql`
  query AdvertSelectorQuery($filter: ListableAdvertFilterInput!) {
    adverts(filter: $filter) {
      id
      ...AdvertSelectorMapFragment
    }
  }
  ${ADVERT_SELECTOR_MAP_FRAGMENT}
`;
