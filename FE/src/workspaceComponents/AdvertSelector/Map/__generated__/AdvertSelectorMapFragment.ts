/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AdvertType, EstateType } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL fragment: AdvertSelectorMapFragment
// ====================================================

export interface AdvertSelectorMapFragment_estate {
  id: number;
  name: string | null;
  estateType: EstateType | null;
  latitude: any | null;
  longitude: any | null;
}

export interface AdvertSelectorMapFragment {
  id: number;
  name: string | null;
  source: string;
  created: any;
  price: any;
  currency: string;
  advertType: AdvertType;
  estate: AdvertSelectorMapFragment_estate | null;
}
