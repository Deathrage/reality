/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { EstateType } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL fragment: AdvertSelectorMapEstateFragment
// ====================================================

export interface AdvertSelectorMapEstateFragment {
  id: number;
  name: string | null;
  estateType: EstateType | null;
  latitude: any | null;
  longitude: any | null;
}
