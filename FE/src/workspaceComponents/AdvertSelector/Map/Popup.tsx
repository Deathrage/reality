import { Button, H5, Intent, OL } from '@blueprintjs/core';
import { useIntl } from '@components/Intl/hooks';
import { TimeFormat } from '@components/Intl/types';
import { AdvertType } from 'global-types';
import React, { FC } from 'react';
import { renderToString } from 'react-dom/server';
import Intl from '@components/Intl';
import { createElement } from 'react';
import { EstateWithAdverts } from './helpers';
import classNames from 'classnames';

export const Popup: FC<{ data: EstateWithAdverts; showButton: boolean }> = ({
  data: { name, adverts },
  showButton,
}) => {
  const { formatNumber, formatDate } = useIntl();

  return (
    <div className="advert-selector-map-info-box">
      <div className="advert-selector-map-info-box__header">
        <H5 className="advert-selector-map-info-box__heading">{name || 'Estate'}</H5>
      </div>
      <OL className="advert-selector-map-info-box__list">
        {adverts
          .sort(({ created: aCreated }, { created: bCreated }) => bCreated - aCreated)
          .map(({ id, source, name, price, currency, created, advertType }) => (
            <li key={id}>
              <strong>{name}</strong>
              <div className="advert-selector-map-info-box__list-flex">
                <div>
                  <div>Added: {formatDate(created, TimeFormat.WITHOUT_TIME)}</div>
                  <div>
                    Price: {formatNumber(price, { currency, style: 'currency' })}
                    {advertType === AdvertType.RENT ? ' / month' : ''}
                  </div>
                  <div>Source: {source}</div>
                </div>
                <Button
                  data-advert-id={id}
                  intent={Intent.PRIMARY}
                  className={classNames('advert-selector-map-info-box__list-button', {
                    'advert-selector-map-info-box__list-button--hidden': !showButton,
                  })}
                >
                  Detail
                </Button>
              </div>
            </li>
          ))}
      </OL>
    </div>
  );
};

export const getPopUpContent = (data: EstateWithAdverts, culture: string, showButton: boolean): string => {
  const element = createElement(Intl, { culture }, createElement(Popup, { data, showButton }));
  const contentString = renderToString(element);
  return contentString;
};
