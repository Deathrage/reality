import { expect } from 'chai';
import { AdvertType, EstateType } from 'global-types';
import { groupAdvertsByEstates } from './helpers';
import { AdvertSelectorMapFragment } from './__generated__/AdvertSelectorMapFragment';

describe('AdvertMap/helpers.ts', () => {
  describe('groupAdvertsByEstates', () => {
    it('should group', () => {
      const adverts: AdvertSelectorMapFragment[] = [
        {
          id: 1,
          name: 'Foo',
          source: 'Bar',
          created: new Date(),
          price: 500,
          currency: 'CZK',
          advertType: AdvertType.RENT,
          estate: {
            id: 10,
            estateType: EstateType.FLAT,
            latitude: 5,
            longitude: 10,
            name: 'Baz',
          },
        },
        {
          id: 2,
          name: 'Foo',
          source: 'Bar',
          created: new Date(),
          price: 500,
          currency: 'CZK',
          advertType: AdvertType.RENT,
          estate: {
            id: 18,
            estateType: EstateType.FLAT,
            latitude: 5,
            longitude: 10,
            name: 'Baz',
          },
        },
        {
          id: 3,
          name: 'Foo',
          source: 'Bar',
          created: new Date(),
          price: 500,
          currency: 'CZK',
          advertType: AdvertType.RENT,
          estate: {
            id: 10,
            estateType: EstateType.FLAT,
            latitude: 5,
            longitude: 10,
            name: 'Baz',
          },
        },
      ];

      const grouping = groupAdvertsByEstates(adverts);

      expect(grouping.size).to.equal(2);
      expect(grouping.get(10)?.adverts.length).to.equal(2);
      expect(grouping.get(10)?.adverts[0].id).to.equal(1);
      expect(grouping.get(10)?.adverts[1].id).to.equal(3);

      expect(grouping.get(18)?.adverts.length).to.equal(1);
      expect(grouping.get(18)?.adverts[0].id).to.equal(2);
    });
    it('should ignore advert without estate', () => {
      const adverts: AdvertSelectorMapFragment[] = [
        {
          id: 1,
          name: 'Foo',
          source: 'Bar',
          created: new Date(),
          price: 500,
          currency: 'CZK',
          advertType: AdvertType.RENT,
          estate: null,
        },
        {
          id: 2,
          name: 'Foo',
          source: 'Bar',
          created: new Date(),
          price: 500,
          currency: 'CZK',
          advertType: AdvertType.RENT,
          estate: {
            id: 18,
            estateType: EstateType.FLAT,
            latitude: 5,
            longitude: 10,
            name: 'foo',
          },
        },
      ];

      const grouping = groupAdvertsByEstates(adverts);

      const results = Array.from(grouping.values()).flatMap(value => value.adverts);
      expect(results.findIndex(advert => advert.id == 1)).to.equal(-1);
      expect(results.findIndex(advert => advert.id == 2)).to.equal(0);
    });
  });
});
