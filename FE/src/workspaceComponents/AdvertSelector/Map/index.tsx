import React, { useImperativeHandle, forwardRef, ForwardRefRenderFunction, useMemo } from 'react';
import { AdvertSelectorMapControl, AdvertSelectorMapProps } from './types';
import { Case } from 'react-when-then';
import { Intent, Spinner } from '@blueprintjs/core';
import './style.scss';
import { useDataSource, useMap, useNames, useEstateGroupedAdverts, useLayers, useDrawingTools } from './hooks';
import { drawing } from 'azure-maps-drawing-tools';

const AdvertSelectorMap: ForwardRefRenderFunction<AdvertSelectorMapControl, AdvertSelectorMapProps> = (
  {
    name,
    adverts = [],
    defaultPosition,
    defaultZoom,
    loading = false,
    onAdvertClick,
    onDrawing,
    drawingEvents,
    drawingMode,
  },
  ref,
) => {
  const { dataSourceName, containerName, ...restNames } = useNames(name);

  // Data source
  const groupedAdvertsByEstates = useEstateGroupedAdverts(adverts);
  const dataSource = useDataSource(dataSourceName, groupedAdvertsByEstates);

  // Create map element
  const { containerElement, activeMap } = useMap(containerName, defaultPosition, defaultZoom);

  // Map drawing logic
  useLayers(activeMap, dataSource, { ...restNames, dataSourceName }, onAdvertClick);

  // Add drawing tools
  const drawingEventsArray = useMemo(() => {
    if (Array.isArray(drawingEvents)) return drawingEvents;
    if (!drawingEvents) return [];
    return [drawingEvents];
  }, [drawingEvents]);
  const drawingManager = useDrawingTools(
    activeMap,
    drawingMode || drawing.DrawingMode.idle,
    drawingEventsArray,
    onDrawing,
  );

  // Expose the map control to the parent
  useImperativeHandle(
    ref,
    () => ({
      resize: () => activeMap?.resize(),
      removeShape: shape => drawingManager?.getSource().remove(shape),
    }),
    [activeMap, drawingManager],
  );

  return (
    <div className="advert-selector-map">
      <Case when={loading || !activeMap}>
        <div className="advert-selector-map__loader">
          <Spinner intent={Intent.PRIMARY} size={200} />
        </div>
      </Case>
      {containerElement}
    </div>
  );
};

AdvertSelectorMap.displayName = nameof(AdvertSelectorMap);

export default forwardRef<AdvertSelectorMapControl, AdvertSelectorMapProps>(AdvertSelectorMap);
export { AdvertSelectorMapControl, DrawingEvents, OnAdvertClick, OnDrawing } from './types';
