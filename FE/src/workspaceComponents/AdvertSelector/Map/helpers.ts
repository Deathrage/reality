import { AdvertSelectorMapFragment } from './__generated__/AdvertSelectorMapFragment';
import { AdvertSelectorMapEstateFragment } from './__generated__/AdvertSelectorMapEstateFragment';

export interface EstateWithAdverts extends AdvertSelectorMapEstateFragment {
  adverts: AdvertSelectorMapFragment[];
}

type EstateAdvertGroupings = Map<number, EstateWithAdverts>;

export const groupAdvertsByEstates = (adverts: AdvertSelectorMapFragment[]): EstateAdvertGroupings => {
  const result = new Map<number, EstateWithAdverts>();

  for (const advert of adverts.filter(advert => advert.estate?.id)) {
    const estateId = advert.estate!.id;
    const grouping: EstateWithAdverts = result.get(estateId) ?? {
      ...advert.estate!,
      adverts: [],
    };

    grouping.adverts.push(advert);

    result.set(estateId, grouping);
  }

  return result;
};
