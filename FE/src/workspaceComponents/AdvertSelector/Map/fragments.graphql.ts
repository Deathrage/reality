import gql from 'graphql-tag';

export const ADVERT_SELECTOR_MAP_ESTATE_FRAGMENT = gql`
  fragment AdvertSelectorMapEstateFragment on ListableEstate {
    id
    name
    estateType
    latitude
    longitude
  }
`;

export const ADVERT_SELECTOR_MAP_FRAGMENT = gql`
  fragment AdvertSelectorMapFragment on ListableAdvert {
    id
    name
    source
    created
    price
    currency
    advertType
    estate {
      ...AdvertSelectorMapEstateFragment
    }
  }
  ${ADVERT_SELECTOR_MAP_ESTATE_FRAGMENT}
`;
