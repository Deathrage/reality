import { useMemo } from 'react';
import { EstateWithAdverts, groupAdvertsByEstates } from '../helpers';
import { AdvertSelectorMapFragment } from '../__generated__/AdvertSelectorMapFragment';

export const useEstateGroupedAdverts = (adverts: AdvertSelectorMapFragment[]): EstateWithAdverts[] => {
  const groupedAdvertsByEstates = useMemo(() => groupAdvertsByEstates(adverts), [adverts]);
  return Array.from(groupedAdvertsByEstates.values());
};
