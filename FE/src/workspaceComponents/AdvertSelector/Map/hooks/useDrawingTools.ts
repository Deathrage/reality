import { Map, Shape } from 'azure-maps-control';
import { drawing, DrawingManagerOptions } from 'azure-maps-drawing-tools';
import { useEffect, useMemo } from 'react';
import { DrawingEvents, OnDrawing } from '../types';

export const useDrawingTools = (
  activeMap: Map | null,
  drawingMode: drawing.DrawingMode,
  eventsToSubscribe: DrawingEvents[],
  onDrawing: OnDrawing | undefined,
): drawing.DrawingManager | null => {
  const drawingManager = useMemo(
    () => activeMap && new drawing.DrawingManager(activeMap, { interactionType: drawing.DrawingInteractionType.click }),
    [activeMap],
  );

  // set mode
  useEffect(() => {
    if (!drawingManager) return;

    const currentOptions = drawingManager.getOptions();
    const newOptions = { ...currentOptions, mode: drawingMode } as DrawingManagerOptions;
    drawingManager.setOptions(newOptions);
  }, [drawingManager, drawingMode]);

  // Handler binding
  useEffect(() => {
    if (!activeMap || !eventsToSubscribe.length || !onDrawing || !drawingManager) return;

    const boundHandlers: [string, (e: Shape) => void][] = [];

    for (const event of eventsToSubscribe) {
      const handler = (shape: Shape) => onDrawing(event, shape);
      activeMap.events.add(event, drawingManager, handler);
      boundHandlers.push([event, handler]);
    }

    return () => {
      for (const [event, handler] of boundHandlers) activeMap.events.remove(event, drawingManager, handler);
    };
  }, [activeMap, drawingManager, eventsToSubscribe, onDrawing]);

  return drawingManager;
};
