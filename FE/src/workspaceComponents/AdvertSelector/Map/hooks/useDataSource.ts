import { data, source } from 'azure-maps-control';
import { useEffect, useMemo } from 'react';
import { EstateWithAdverts } from '../helpers';

export const useDataSource = (dataSourceName: string, estates: EstateWithAdverts[]): source.DataSource => {
  const dataSource = useMemo(
    () =>
      new source.DataSource(dataSourceName, {
        cluster: true,
        clusterRadius: 40,
        maxZoom: 14,
      }),
    [dataSourceName],
  );

  useEffect(() => {
    const points = estates.map(estate => {
      return new data.Feature<data.Point, EstateWithAdverts>(
        new data.Point(new data.Position(estate!.longitude, estate!.latitude)),
        estate,
      );
    });

    dataSource.add(points);

    return () => dataSource.clear();
  }, [dataSource, estates]);

  return dataSource;
};
