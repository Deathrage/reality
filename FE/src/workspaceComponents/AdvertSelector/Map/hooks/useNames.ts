import { useMemo } from 'react';

export interface MapNames {
  dataSourceName: string;
  clusterLayerName: string;
  markerCountForClusterLayerName: string;
  markerLayerName: string;
  containerName: string;
}

export const useNames = (mapName: string): MapNames => {
  const mapNames = useMemo<MapNames>(
    () => ({
      dataSourceName: `${mapName} dataSource`,
      clusterLayerName: `${mapName} clusterLayer`,
      markerCountForClusterLayerName: `${mapName} markerCountForClusterLayer`,
      markerLayerName: `${mapName} markerLayer`,
      containerName: `${mapName}-map`,
    }),
    [mapName],
  );

  return mapNames;
};
