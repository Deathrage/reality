import { useEffect } from 'react';
import { layer, data, MapMouseEvent, ClusteredProperties, Popup, Shape, source, Map } from 'azure-maps-control';
import { getPopUpContent } from '../Popup';
import { EstateType } from 'global-types';
import { EstateWithAdverts } from '../helpers';
import { getSprite } from '@helpers/sprites';
import { MapNames } from './useNames';
import { useCurrentCulture } from '@components/Intl/hooks';
import { OnAdvertClick } from '../types';

export const useLayers = (
  activeMap: Map | null,
  dataSource: source.DataSource,
  {
    clusterLayerName,
    markerCountForClusterLayerName,
    markerLayerName,
    dataSourceName,
  }: Omit<MapNames, 'containerName'>,
  onAdvertClick: OnAdvertClick | undefined,
): void => {
  const culture = useCurrentCulture();

  useEffect(() => {
    if (!activeMap) return;

    // bind data source
    activeMap.sources.add(dataSource);

    // Cluster Bubbles
    const clusterBubbleLayer = new layer.BubbleLayer(dataSource, clusterLayerName, {
      radius: ['step', ['get', 'point_count'], 20, 100, 30, 750, 40],
      color: [
        'step',
        ['get', 'point_count'],
        'rgba(0,255,0,0.8)',
        100,
        'rgba(255,255,0,0.8)',
        750,
        'rgba(2555,0,0,0.8)',
      ],
      strokeWidth: 0,
      filter: ['has', 'point_count'],
    });

    activeMap.layers.add(clusterBubbleLayer);

    const mouseEnterClusterShowPointer = () => {
      const element = activeMap.getCanvas();
      element.style.cursor = 'pointer';
    };
    const mouseLeaveClusterHidePointer = () => {
      const element = activeMap.getCanvas();
      element.style.cursor = 'inherit';
    };

    activeMap.events.add('mouseenter', clusterBubbleLayer, mouseEnterClusterShowPointer);
    activeMap.events.add('mouseleave', clusterBubbleLayer, mouseLeaveClusterHidePointer);

    const clusterClick = async (e: MapMouseEvent) => {
      if (!e || !e.shapes || !e.shapes.length) return;

      const clickedCluster = e.shapes[0] as data.Feature<data.Geometry, ClusteredProperties>;

      if (!clickedCluster?.properties?.cluster) return;

      const zoom = await dataSource.getClusterExpansionZoom(clickedCluster.properties.cluster_id);

      activeMap.setCamera({
        center: clickedCluster.geometry.coordinates,
        zoom,
        type: 'ease',
        duration: 200,
      });
    };

    activeMap.events.add('click', clusterBubbleLayer, clusterClick);

    // Cluster Bubbles Point Count
    const markerCountForClusterLayer = new layer.SymbolLayer(dataSource, markerCountForClusterLayerName, {
      iconOptions: {
        image: 'none',
        allowOverlap: true,
      },
      textOptions: {
        textField: ['get', 'point_count_abbreviated'],
        offset: [0, 0.4],
      },
    });

    activeMap.layers.add(markerCountForClusterLayer);

    // Markers
    const estateTypeEqualsExpression = (value: EstateType) => ['==', ['get', 'estateType'], value];
    const getCase = (value: EstateType) => {
      const equalExpression = estateTypeEqualsExpression(value);
      const sprite = getSprite(value);
      return [equalExpression, sprite];
    };
    const markerLayer = new layer.SymbolLayer(dataSource, markerLayerName, {
      iconOptions: {
        allowOverlap: true,
        image: [
          'case',
          ...getCase(EstateType.FLAT),
          ...getCase(EstateType.HOUSE),
          ...getCase(EstateType.LAND),
          ...getCase(EstateType.BUSINESS),
          getSprite(EstateType.MISCELLANEOUS),
        ],
      },
      filter: ['!', ['has', 'point_count']],
    });

    activeMap.layers.add(markerLayer);

    const mouseEnterMarkerShowPointer = () => {
      const element = activeMap.getCanvas();
      element.style.cursor = 'pointer';
    };
    const mouseLeaveMarkerHidePointer = () => {
      const element = activeMap.getCanvas();
      element.style.cursor = 'inherit';
    };

    activeMap.events.add('mouseenter', markerLayer, mouseEnterMarkerShowPointer);
    activeMap.events.add('mouseleave', markerLayer, mouseLeaveMarkerHidePointer);

    // Popup
    const popup = new Popup({ pixelOffset: [0, -24], closeButton: true });

    let isForceOpened = false;
    let lastHoveredId: number | null = null;
    const clickPopup = (e: MapMouseEvent) => {
      if (!e || !e.shapes || !e.shapes.length) return;

      const clickMarker = e.shapes[0] as Shape;

      const properties: EstateWithAdverts = clickMarker.getProperties();
      const coordinates = clickMarker.getCoordinates();

      if (!properties) return;

      isForceOpened = true;

      popup._addEventListener(
        'close',
        () => {
          isForceOpened = false;
        },
        true,
      );

      const stringContent = getPopUpContent(properties, culture, true);
      const content = document.createElement('div');
      content.innerHTML = stringContent;
      content
        .querySelector('.advert-selector-map-info-box__list-button')
        ?.addEventListener('click', ({ currentTarget }) => {
          const targetElement = currentTarget as HTMLElement;
          const advertId = targetElement?.dataset?.advertId;
          if (advertId) onAdvertClick?.(Number(advertId));
        });

      popup.setOptions({
        content,
        position: coordinates as data.Position,
      });

      if (!popup.isOpen()) popup.open();
    };

    const mouseEnterShowPopup = (e: MapMouseEvent) => {
      if (!e || !e.shapes || !e.shapes.length) return;

      const hoverMarker = e.shapes[0] as Shape;

      const properties: EstateWithAdverts = hoverMarker.getProperties();
      const coordinates = hoverMarker.getCoordinates();

      if (!properties || !coordinates || lastHoveredId == properties.id) return;

      isForceOpened = false;
      lastHoveredId = properties.id;

      popup.setOptions({
        content: getPopUpContent(properties, culture, false),
        position: coordinates as data.Position,
      });

      popup.open(activeMap);
    };

    const mouseLeaveHidePopup = () => {
      if (!isForceOpened) popup.close();
    };

    activeMap.events.add('mouseenter', markerLayer, mouseEnterShowPopup);
    activeMap.events.add('mouseleave', markerLayer, mouseLeaveHidePopup);
    activeMap.events.add('click', markerLayer, clickPopup);

    return () => {
      activeMap.events.remove('mouseenter', clusterBubbleLayer, mouseEnterClusterShowPointer);
      activeMap.events.remove('mouseleave', clusterBubbleLayer, mouseLeaveClusterHidePointer);

      activeMap.events.remove('mouseenter', markerLayer, mouseEnterMarkerShowPointer);
      activeMap.events.remove('mouseleave', markerLayer, mouseLeaveMarkerHidePointer);

      activeMap.events.remove('mouseenter', markerLayer, mouseEnterShowPopup);
      activeMap.events.remove('mouseleave', markerLayer, mouseLeaveHidePopup);

      activeMap.events.remove('click', clusterBubbleLayer, clusterClick);
      activeMap.events.remove('click', markerLayer, clickPopup);

      if (activeMap.layers.getLayerById(clusterLayerName)) activeMap.layers.remove(clusterLayerName);
      if (activeMap.layers.getLayerById(markerCountForClusterLayerName))
        activeMap.layers.remove(markerCountForClusterLayerName);
      if (activeMap.layers.getLayerById(markerLayerName)) activeMap.layers.remove(markerLayerName);

      if (activeMap.sources.getById(dataSourceName)) activeMap.sources.remove(dataSourceName);
    };
  }, [
    activeMap,
    clusterLayerName,
    culture,
    dataSource,
    markerCountForClusterLayerName,
    markerLayerName,
    dataSourceName,
    onAdvertClick,
  ]);
};
