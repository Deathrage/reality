import { useAzureMapsKey } from '@components/Environment/hooks';
import { getSpriteUrl, Sprites } from '@helpers/sprites';
import {
  AuthenticationOptions,
  AuthenticationType,
  Map,
  data,
  ControlPosition,
  control,
  ControlStyle,
  MapEvent,
} from 'azure-maps-control';
import { useMemo, useLayoutEffect, createElement, useState } from 'react';
import { AdvertSelectorMapInfo, LatLon } from '../types';

const useAuthOptions = (): AuthenticationOptions => {
  const subscriptionKey = useAzureMapsKey();

  const option = useMemo<AuthenticationOptions>(
    () => ({
      authType: AuthenticationType.subscriptionKey,
      subscriptionKey,
    }),
    [subscriptionKey],
  );

  return option;
};

const onReadyFactory = (setActiveMap: (map: Map) => void) => async ({ map }: MapEvent) => {
  const promises: Promise<void>[] = [];
  const addOrIgnore = (sprite: Sprites) => {
    if (!map.imageSprite.hasImage(sprite)) promises.push(map.imageSprite.add(sprite, getSpriteUrl(sprite)));
  };

  addOrIgnore(Sprites.FLAT);
  addOrIgnore(Sprites.HOUSE);
  addOrIgnore(Sprites.LAND);
  addOrIgnore(Sprites.BUSINESS);
  addOrIgnore(Sprites.MISC);

  await Promise.all(promises);

  setActiveMap(map);
};

const addControls = (map: Map) => {
  const style = ControlStyle.auto;
  map.controls.add(
    [
      new control.ZoomControl({ style, zoomDelta: 1 }),
      new control.CompassControl({ style, rotationDegreesDelta: 15 }),
      new control.PitchControl({ style, pitchDegreesDelta: 10 }),
      new control.StyleControl({ style }),
    ],
    {
      position: ControlPosition.BottomLeft,
    },
  );
};

export const useMap = (containerId: string, initialPosition?: LatLon, initialZoom?: number): AdvertSelectorMapInfo => {
  const authOptions = useAuthOptions();

  const [activeMap, setActiveMap] = useState<Map | null>(null);

  useLayoutEffect(() => {
    const container = document.getElementById(containerId);
    if (!container) return;

    const map = new Map(container, {
      authOptions,
      zoom: initialZoom,
      center: initialPosition ? new data.Position(initialPosition.lon, initialPosition.lat) : undefined,
    });

    map.events.add('ready', onReadyFactory(setActiveMap));

    addControls(map);

    return () =>
      setActiveMap(prevMap => {
        prevMap?.dispose();
        return null;
      });
  }, [authOptions, containerId, initialPosition, initialZoom]);

  const containerElement = useMemo(() => createElement('div', { id: containerId, style: { height: '100%' } }), [
    containerId,
  ]);

  return useMemo(
    () => ({
      containerElement,
      activeMap,
    }),
    [containerElement, activeMap],
  );
};
