import { ReactElement } from 'react';
import { Map, Shape } from 'azure-maps-control';
import { AdvertSelectorMapFragment } from './__generated__/AdvertSelectorMapFragment';
import { drawing } from 'azure-maps-drawing-tools';

export interface LatLon {
  lat: number;
  lon: number;
}

export type DrawingEvents = keyof Omit<drawing.DrawingManagerEvents, 'drawingmodechanged'>;

export type OnAdvertClick = (advertId: number) => void;
export type OnDrawing = (action: DrawingEvents, shape: Shape) => void;

export interface AdvertSelectorMapProps {
  name: string;
  adverts?: AdvertSelectorMapFragment[];
  loading?: boolean;
  defaultPosition?: LatLon;
  defaultZoom?: number;
  onAdvertClick?: OnAdvertClick;
  onDrawing?: OnDrawing;
  drawingEvents?: DrawingEvents[] | DrawingEvents;
  drawingMode?: drawing.DrawingMode;
}

export interface AdvertSelectorMapInfo {
  containerElement: ReactElement;
  activeMap: Map | null;
}

export interface AdvertSelectorMapControl {
  resize: () => void;
  removeShape: (shape: Shape) => void;
}
