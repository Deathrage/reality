import { Shape } from 'azure-maps-control';
import { LatLonInput, ListableAdvertFilterInput } from 'global-types';
import { useCallback, useMemo, useState } from 'react';
import { AdvertSelectorBasicFilters } from './BasicFilter';
import { getCoordinatesFromShape } from './ZoneFilter';

type BasicFiltersUpdater = (previous: AdvertSelectorBasicFilters) => AdvertSelectorBasicFilters;
type SetBasicFilters = (update: BasicFiltersUpdater) => void;
type ActiveShapeUpdate = (shape: Shape | null) => Shape | null;
type SetActiveShape = (update: ActiveShapeUpdate) => void;

interface UseFilterDat {
  basicFilters: AdvertSelectorBasicFilters;
  setBasicFilters: SetBasicFilters;
  resetBasicFilters: () => void;
  activeShape: Shape | null;
  setActiveShape: SetActiveShape;
  queryFilters: ListableAdvertFilterInput;
}

export const useFilter = (): UseFilterDat => {
  const initialBasicFilterValues = useMemo(() => {
    const to = new Date();
    const from = new Date();
    from.setMonth(to.getMonth() - 1);

    return {
      from,
      to,
      module: null,
      estateType: null,
      advertType: null,
      ownership: null,
    };
  }, []);
  const [basicFilters, setBasicFilters] = useState<AdvertSelectorBasicFilters>(initialBasicFilterValues);
  const resetBasicFilters = useCallback(() => setBasicFilters(initialBasicFilterValues), [initialBasicFilterValues]);

  const [activeShape, setActiveShape] = useState<Shape | null>(null);
  const shapeCoordinates = useMemo(() => getCoordinatesFromShape(activeShape), [activeShape]);

  const { from, to, estateType, advertType, module, ownership } = basicFilters;
  const queryFilters = useMemo(() => {
    const filter: ListableAdvertFilterInput = {
      from: from.toISOString(),
      to: to.toISOString(),
    };

    if (ownership) filter.ownership = ownership;
    if (estateType) filter.estateType = estateType;
    if (advertType) filter.advertType = advertType;
    if (module) filter.source = module;
    if (shapeCoordinates) {
      const boundary = shapeCoordinates.map<LatLonInput>(([lon, lat]) => ({ lon, lat }));
      filter.boundary = boundary;
    }

    return filter;
  }, [advertType, estateType, from, module, shapeCoordinates, to, ownership]);

  return {
    basicFilters,
    setBasicFilters,
    resetBasicFilters,
    activeShape,
    setActiveShape,
    queryFilters,
  };
};
