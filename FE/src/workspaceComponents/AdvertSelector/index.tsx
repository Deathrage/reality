import { useLazyQuery } from '@apollo/client';
import BasicFilter from './BasicFilter';
import Map, { AdvertSelectorMapControl, DrawingEvents, OnAdvertClick, OnDrawing } from './Map';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { ADVERT_SELECTOR_QUERY } from './queries.graphql';
import { AdvertSelectorQuery, AdvertSelectorQueryVariables } from './__generated__/AdvertSelectorQuery';
import './style.scss';
import AdvertDetail from '@workspaceComponents/AdvertDetail';
import { WorkspaceItemFC } from '@workspaceComponents/types';
import { IconNames } from '@blueprintjs/icons';
import { drawing } from 'azure-maps-drawing-tools';
import { Button, Intent, Navbar } from '@blueprintjs/core';
import ZoneFilter from './ZoneFilter';
import { useFilter } from './useFilter';
import { Tooltip2 } from '@blueprintjs/popover2';

const defaultPosition = { lon: 14.43, lat: 50.05 };

const drawingEvents: DrawingEvents[] = ['drawingcomplete'];

const AdvertSelectorLayout: WorkspaceItemFC = ({ onResize, onResizeStop, openNew }) => {
  const mapControl = useRef<AdvertSelectorMapControl>();

  useEffect(() => {
    const unsubscribe = onResize(() => {
      mapControl.current?.resize();
    });
    const unsubscribeStop = onResizeStop(() => {
      mapControl.current?.resize();
    });

    return () => {
      unsubscribe();
      unsubscribeStop();
    };
  }, [onResize, onResizeStop]);

  const { basicFilters, setBasicFilters, resetBasicFilters, activeShape, setActiveShape, queryFilters } = useFilter();

  const [fetch, { data, loading }] = useLazyQuery<AdvertSelectorQuery, AdvertSelectorQueryVariables>(
    ADVERT_SELECTOR_QUERY,
  );

  const onApplyFilterClick = useCallback(() => {
    fetch({
      variables: {
        filter: queryFilters,
      },
    });
  }, [queryFilters, fetch]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => onApplyFilterClick(), []);

  const onAdvertClick = useCallback<OnAdvertClick>(advertId => openNew(AdvertDetail.workspaceItem(advertId)), [
    openNew,
  ]);

  const onDrawing = useCallback<OnDrawing>(
    (action, shape) => {
      switch (action) {
        case 'drawingcomplete': {
          setActiveShape(prevShape => {
            if (prevShape && prevShape !== shape) mapControl.current?.removeShape(prevShape);
            return shape;
          });
          break;
        }
        default:
          return;
      }
    },
    [setActiveShape],
  );

  const onShapeClear = useCallback(() => {
    setActiveShape(prevShape => {
      if (prevShape) mapControl.current?.removeShape(prevShape);
      return null;
    });
  }, [setActiveShape]);

  const onClear = useCallback(() => {
    resetBasicFilters();
    onShapeClear();
  }, [onShapeClear, resetBasicFilters]);

  const [drawingMode, setDrawingMode] = useState(drawing.DrawingMode.idle);

  return (
    <div className="advert-selector">
      <Navbar className="advert-selector__filter">
        <BasicFilter
          className="advert-selector__filter-item"
          onFilterChange={setBasicFilters}
          filterValues={basicFilters}
          onClear={onClear}
        />
        <Navbar.Divider className="advert-selector__filter-divider" />
        <ZoneFilter
          className="advert-selector__filter-item"
          drawingMode={drawingMode}
          onDrawingModeChange={setDrawingMode}
          activeShape={activeShape}
          onShapeClear={onShapeClear}
        />
        <div className="advert-selector__dataset-buttons">
          <Button intent={Intent.PRIMARY} onClick={onApplyFilterClick} icon={IconNames.FILTER_KEEP}>
            Apply Filter
          </Button>
          <Tooltip2 intent={Intent.DANGER} content="TODO">
            <Button intent={Intent.SUCCESS} disabled icon={IconNames.ADD_TO_ARTIFACT}>
              Save to DataSet
            </Button>
          </Tooltip2>
        </div>
      </Navbar>
      <Map
        name="big"
        loading={loading}
        adverts={data?.adverts}
        defaultPosition={defaultPosition}
        defaultZoom={10.5}
        ref={ref => {
          mapControl.current = ref ?? undefined;
        }}
        onAdvertClick={onAdvertClick}
        onDrawing={onDrawing}
        drawingEvents={drawingEvents}
        drawingMode={drawingMode}
      />
    </div>
  );
};

AdvertSelectorLayout.workspaceItem = () => ({
  i: 'advert-selector',
  x: 0,
  y: 0,
  w: 5,
  h: 5,
  component: AdvertSelectorLayout,
  title: 'Advert Selector',
  icon: IconNames.MAP,
  detachable: false,
  toolboxable: true,
});

export default AdvertSelectorLayout;
