import { data, Shape } from 'azure-maps-control';

export const getCoordinatesFromShape = (shape: Shape | null): data.Position[] | null => {
  const coordinates = shape?.getCoordinates();
  if (!Array.isArray(coordinates)) return null;

  const positions = coordinates[0] as data.Position[];
  if (!positions || !positions.length) return null;

  return positions;
};
