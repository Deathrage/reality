import { Shape } from 'azure-maps-control';
import { drawing } from 'azure-maps-drawing-tools';

export interface AdvertSelectorZoneFilterShapeInfoProps {
  activeShape: Shape | null;
}

export interface AdvertSelectorZoneFilterProps extends AdvertSelectorZoneFilterShapeInfoProps {
  className?: string;
  drawingMode: drawing.DrawingMode;
  onDrawingModeChange: (newMode: drawing.DrawingMode) => void;
  onShapeClear: () => void;
}
