import { Button, ButtonGroup, Intent } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { drawing } from 'azure-maps-drawing-tools';
import classNames from 'classnames';
import React, { FC, useCallback } from 'react';
import ShapeInfo from './ShapeInfo';
import { AdvertSelectorZoneFilterProps } from './types';
import './style.scss';
import { Tooltip2 } from '@blueprintjs/popover2';

export default (({ drawingMode, className, onDrawingModeChange, activeShape, onShapeClear }) => {
  const onIdleModeClick = useCallback(() => onDrawingModeChange(drawing.DrawingMode.idle), [onDrawingModeChange]);
  const onPolygonModeClick = useCallback(() => onDrawingModeChange(drawing.DrawingMode.drawPolygon), [
    onDrawingModeChange,
  ]);
  const onRectangleModeClick = useCallback(() => onDrawingModeChange(drawing.DrawingMode.drawRectangle), [
    onDrawingModeChange,
  ]);

  return (
    <div className={classNames('advert-selector-zone-filter', className)}>
      <ButtonGroup>
        <Tooltip2
          intent={Intent.NONE}
          content="Free movement"
          disabled={drawingMode === drawing.DrawingMode.idle}
          placement="top"
        >
          <Button icon={IconNames.HAND} active={drawingMode === drawing.DrawingMode.idle} onClick={onIdleModeClick} />
        </Tooltip2>
        <Tooltip2
          intent={Intent.NONE}
          content="Draw polygon"
          disabled={drawingMode === drawing.DrawingMode.drawPolygon}
          placement="top"
        >
          <Button
            icon={IconNames.POLYGON_FILTER}
            active={drawingMode === drawing.DrawingMode.drawPolygon}
            onClick={onPolygonModeClick}
          />
        </Tooltip2>
        <Tooltip2
          intent={Intent.NONE}
          content="Draw rectangle"
          disabled={drawingMode === drawing.DrawingMode.drawRectangle}
          placement="top"
        >
          <Button
            icon={IconNames.SQUARE}
            active={drawingMode === drawing.DrawingMode.drawRectangle}
            onClick={onRectangleModeClick}
          />
        </Tooltip2>
      </ButtonGroup>
      <ShapeInfo activeShape={activeShape} />
      <Tooltip2
        className="advert-selector-zone-filter__trash"
        content="Clear shape"
        disabled={!activeShape}
        placement="top"
      >
        <Button icon={IconNames.TRASH} onClick={onShapeClear} disabled={!activeShape} />
      </Tooltip2>
    </div>
  );
}) as FC<AdvertSelectorZoneFilterProps>;

export { getCoordinatesFromShape } from './helpers';
