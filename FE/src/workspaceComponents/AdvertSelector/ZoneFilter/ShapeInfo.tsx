import React, { FC, useMemo } from 'react';
import { AdvertSelectorZoneFilterShapeInfoProps } from './types';
import { Classes } from '@blueprintjs/core';
import { getCoordinatesFromShape } from './helpers';
import { Falsy, Ternary, Truthy } from 'react-ternary-operator';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react';
import classNames from 'classnames';

export default (({ activeShape }) => {
  const positions = useMemo(() => getCoordinatesFromShape(activeShape), [activeShape]);

  return (
    <div className="advert-selector-zone-filter__points">
      <OverlayScrollbarsComponent className={classNames('advert-selector-zone-filter__points-scrolls', Classes.CODE)}>
        <Ternary condition={!!positions}>
          <Truthy>
            {positions?.map(([lon, lat], i, array) => {
              const pointOrder = i + 1;
              const isLast = pointOrder === array.length;
              return (
                <span key={`${lon}|${lat}${isLast ? '|Last' : ''}`} className="advert-selector-zone-filter__point">
                  Point {pointOrder}
                  {i === 0 ? ` (also Point ${array.length})` : ''}
                  {isLast ? ' (also Point 1)' : ''}
                  <br />
                  Lat: {lat}
                  <br />
                  Lon: {lon}
                </span>
              );
            })}
          </Truthy>
          <Falsy>
            <span className="advert-selector-zone-filter__point-disclaimer">
              In order to view the points draw a shape on the map.
            </span>
          </Falsy>
        </Ternary>
      </OverlayScrollbarsComponent>
    </div>
  );
}) as FC<AdvertSelectorZoneFilterShapeInfoProps>;
