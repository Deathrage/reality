import React, { FC, useCallback, useMemo } from 'react';
import { AdvertSelectBasicFilterProps } from './types';
import { Button, FormGroup, HTMLSelect, IOptionProps, Navbar } from '@blueprintjs/core';
import { DateRange, DateRangeInput, TimePrecision } from '@blueprintjs/datetime';
import { isDateValid } from '@blueprintjs/datetime/lib/esm/common/dateUtils';
import { useQuery } from '@apollo/client';
import { ADVERT_SELECTOR_BASIC_FILTER_MODULES_QUERY } from './queries.graphql';
import { AdvertSelectorBasicFilterModulesQuery } from './__generated__/AdvertSelectorBasicFilterModulesQuery';
import { useIntl } from '@components/Intl/hooks';
import { TimeFormat, TimeParse } from '@components/Intl/types';
import './style.scss';
import { AdvertType, EstateType, Ownership } from 'global-types';
import classNames from 'classnames';
import { allOption } from './const';
import { TFunction } from 'i18next';
import { useTranslation } from 'react-i18next';
import { IconNames } from '@blueprintjs/icons';
import { Tooltip2 } from '@blueprintjs/popover2';

const estateTypes = Object.values<string>(EstateType);
const estateTypeOptionsFactory = (t: TFunction): IOptionProps[] => [
  allOption,
  ...estateTypes.map<IOptionProps>(estateType => ({
    value: estateType,
    label: t(estateType),
  })),
];

const advertTypes = Object.values<string>(AdvertType);
const advertTypeOptionsFactory = (t: TFunction): IOptionProps[] => [
  allOption,
  ...advertTypes.map<IOptionProps>(advertType => ({
    value: advertType,
    label: t(advertType),
  })),
];

const ownerships = Object.values<string>(Ownership);
const ownershipOptionsFactory = (t: TFunction): IOptionProps[] => [
  allOption,
  ...ownerships.map<IOptionProps>(ownership => ({
    value: ownership,
    label: t(ownership),
  })),
];

export default (({
  onFilterChange,
  filterValues: { from, to, module, estateType, advertType, ownership },
  className,
  onClear,
}) => {
  const { data: modulesData, loading: modulesLoading } = useQuery<AdvertSelectorBasicFilterModulesQuery>(
    ADVERT_SELECTOR_BASIC_FILTER_MODULES_QUERY,
  );
  const moduleNames = useMemo(() => modulesData?.sourceModuleNames ?? [], [modulesData]);

  const { t: advertTypeT } = useTranslation(nameof<AdvertType>());
  const { t: estateTypeT } = useTranslation(nameof<EstateType>());
  const { t: ownershipT } = useTranslation(nameof<Ownership>());
  const { t: sourceT } = useTranslation('Source');

  const estateTypeOptions = useMemo(() => estateTypeOptionsFactory(estateTypeT), [estateTypeT]);
  const advertTypeOptions = useMemo(() => advertTypeOptionsFactory(advertTypeT), [advertTypeT]);
  const ownershipOptions = useMemo(() => ownershipOptionsFactory(ownershipT), [ownershipT]);

  const moduleOptions = useMemo<IOptionProps[]>(() => {
    return [
      allOption,
      ...moduleNames.map<IOptionProps>(name => ({
        value: name,
        label: sourceT(name),
      })),
    ];
  }, [moduleNames, sourceT]);

  const { formatDate, parseDate } = useIntl();

  const handleDateChange = useCallback(
    ([from, to]: DateRange) => {
      if (isDateValid(from) && isDateValid(to)) onFilterChange(prev => ({ ...prev, from, to }));
    },
    [onFilterChange],
  );

  const handleModuleChange = useCallback<React.ChangeEventHandler<HTMLSelectElement>>(
    ({ currentTarget: { value } }) => {
      if (!value || !moduleNames?.includes(value)) return onFilterChange(prev => ({ ...prev, module: null }));
      return onFilterChange(prev => ({ ...prev, module: value }));
    },
    [onFilterChange, moduleNames],
  );

  const handleEstateTypeChange = useCallback<React.ChangeEventHandler<HTMLSelectElement>>(
    ({ currentTarget: { value } }) => {
      if (!value || !estateTypes.includes(value)) return onFilterChange(prev => ({ ...prev, estateType: null }));
      return onFilterChange(prev => ({ ...prev, estateType: value as EstateType }));
    },
    [onFilterChange],
  );

  const handleAdvertTypeChange = useCallback<React.ChangeEventHandler<HTMLSelectElement>>(
    ({ currentTarget: { value } }) => {
      if (!value || !advertTypes.includes(value)) return onFilterChange(prev => ({ ...prev, advertType: null }));
      return onFilterChange(prev => ({ ...prev, advertType: value as AdvertType }));
    },
    [onFilterChange],
  );

  const handleOwnershipChange = useCallback<React.ChangeEventHandler<HTMLSelectElement>>(
    ({ currentTarget: { value } }) => {
      if (!value || !ownerships.includes(value)) return onFilterChange(prev => ({ ...prev, ownership: null }));
      return onFilterChange(prev => ({ ...prev, ownership: value as Ownership }));
    },
    [onFilterChange],
  );

  const formatDateHandler = useCallback((date: Date) => formatDate(date, TimeFormat.WITH_TIME), [formatDate]);
  const parseDateHandler = useCallback((date: string) => parseDate(date, TimeParse.WITH_TIME), [parseDate]);

  return (
    <div className={classNames('advert-selector-basic-filter', className)}>
      <div>
        <FormGroup className="advert-selector-basic-filter__form-group" label="Date range" labelFor="date-from" inline>
          <DateRangeInput
            startInputProps={{ id: 'date-from' }}
            endInputProps={{ id: 'date-to' }}
            value={[from, to]}
            onChange={handleDateChange}
            formatDate={formatDateHandler}
            parseDate={parseDateHandler}
            timePrecision={TimePrecision.MINUTE}
          />
        </FormGroup>
        <FormGroup
          className="advert-selector-basic-filter__form-group"
          label="Source"
          labelFor="module"
          inline
          disabled={modulesLoading}
        >
          <HTMLSelect
            id="module"
            options={moduleOptions}
            disabled={modulesLoading}
            value={module ?? allOption.value}
            onChange={handleModuleChange}
          />
        </FormGroup>
        <FormGroup
          className="advert-selector-basic-filter__form-group"
          label="Advert type"
          labelFor="advert-type"
          inline
        >
          <HTMLSelect
            id="advert-type"
            options={advertTypeOptions}
            value={advertType ?? allOption.value}
            onChange={handleAdvertTypeChange}
          />
        </FormGroup>
        <FormGroup
          className="advert-selector-basic-filter__form-group"
          label="Estate type"
          labelFor="estate-type"
          inline
        >
          <HTMLSelect
            id="estate-type"
            options={estateTypeOptions}
            value={estateType ?? allOption.value}
            onChange={handleEstateTypeChange}
          />
        </FormGroup>
        <FormGroup
          className="advert-selector-basic-filter__form-group"
          label="Ownership"
          labelFor="ownership-type"
          inline
        >
          <HTMLSelect
            id="ownership-type"
            options={ownershipOptions}
            value={ownership ?? allOption.value}
            onChange={handleOwnershipChange}
          />
        </FormGroup>
      </div>
      <Tooltip2 className="advert-selector-basic-filter__clear-filter" content="Clear filters" placement="top">
        <Button icon={IconNames.FILTER_REMOVE} onClick={onClear} />
      </Tooltip2>
    </div>
  );
}) as FC<AdvertSelectBasicFilterProps>;

export { AdvertSelectorBasicFilters } from './types';
