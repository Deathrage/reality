import gql from 'graphql-tag';

export const ADVERT_SELECTOR_BASIC_FILTER_NAMED_ENTITY_FRAGMENT = gql`
  fragment AdvertSelectorBasicFilterNamedEntityFragment on NamedEntity {
    id
    name
  }
`;
