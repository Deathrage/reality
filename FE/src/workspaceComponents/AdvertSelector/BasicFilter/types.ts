import { AdvertType, EstateType, Ownership } from 'global-types';

export interface AdvertSelectorBasicFilters {
  from: Date;
  to: Date;
  module: string | null;
  estateType: EstateType | null;
  advertType: AdvertType | null;
  ownership: Ownership | null;
}

export interface AdvertSelectBasicFilterProps {
  onFilterChange: (update: (previous: AdvertSelectorBasicFilters) => AdvertSelectorBasicFilters) => void;
  filterValues: AdvertSelectorBasicFilters;
  className?: string;
  onClear: () => void;
}
