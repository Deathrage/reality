/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AdvertSelectorBasicFilterStreetsQuery
// ====================================================

export interface AdvertSelectorBasicFilterStreetsQuery_streets {
  id: number;
  name: string | null;
}

export interface AdvertSelectorBasicFilterStreetsQuery {
  streets: AdvertSelectorBasicFilterStreetsQuery_streets[];
}

export interface AdvertSelectorBasicFilterStreetsQueryVariables {
  municipalityId: number;
}
