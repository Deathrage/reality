/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AdvertSelectorBasicFilterCountriesQuery
// ====================================================

export interface AdvertSelectorBasicFilterCountriesQuery_countries {
  id: number;
  name: string | null;
}

export interface AdvertSelectorBasicFilterCountriesQuery {
  countries: AdvertSelectorBasicFilterCountriesQuery_countries[];
}
