/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AdvertSelectorBasicFilterNamedEntityFragment
// ====================================================

export interface AdvertSelectorBasicFilterNamedEntityFragment {
  id: number;
  name: string | null;
}
