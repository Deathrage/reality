/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AdvertSelectorBasicFilterMunicipalitiesQuery
// ====================================================

export interface AdvertSelectorBasicFilterMunicipalitiesQuery_municipalities {
  id: number;
  name: string | null;
}

export interface AdvertSelectorBasicFilterMunicipalitiesQuery {
  municipalities: AdvertSelectorBasicFilterMunicipalitiesQuery_municipalities[];
}

export interface AdvertSelectorBasicFilterMunicipalitiesQueryVariables {
  countryId: number;
}
