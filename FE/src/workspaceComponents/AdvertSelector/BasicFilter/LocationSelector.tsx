// import { Button, ButtonGroup, FormGroup, HTMLSelect, Tooltip } from '@blueprintjs/core';
// import { IconNames } from '@blueprintjs/icons';
// import React, { FC, useCallback, useState } from 'react';
// import { useCountryFilter, useMunicipalityFilter, useStreetFilter } from './hooks';

// enum LocationSelectorMode {
//   POLYGON = 'POLYGON',
//   ADDRESS = 'ADDRESS',
// }

// export default (() => {
//   const [mode, setMode] = useState(LocationSelectorMode.ADDRESS);

//   const setPolygonMode = useCallback(() => setMode(LocationSelectorMode.POLYGON), []);
//   const setAddressMode = useCallback(() => setMode(LocationSelectorMode.ADDRESS), []);

//   const {
//     options: countryOptions,
//     value: countryId,
//     onChange: onCountryChange,
//     loading: countriesLoading,
//   } = useCountryFilter();

//   const {
//     options: municipalityOptions,
//     value: municipalityId,
//     onChange: onMunicipalityChange,
//     loading: municipalitiesLoading,
//   } = useMunicipalityFilter(countryId);

//   const {
//     options: streetOptions,
//     value: streetId,
//     onChange: onStreetChange,
//     loading: streetsLoading,
//   } = useStreetFilter(municipalityId);

//   return (
//     <div className="advert-selector-basic-filter__location-selector">
//       <ButtonGroup>
//         <Tooltip content="Polygon selector">
//           <Button
//             icon={IconNames.POLYGON_FILTER}
//             active={mode == LocationSelectorMode.POLYGON}
//             onClick={setPolygonMode}
//           />
//         </Tooltip>
//         <Tooltip content="Address selector">
//           <Button icon={IconNames.GEOLOCATION} active={mode == LocationSelectorMode.ADDRESS} onClick={setAddressMode} />
//         </Tooltip>
//       </ButtonGroup>
//       <FormGroup className="advert-selector-basic-filter__form-group" label="Country" labelFor="country" inline>
//         <HTMLSelect
//           id="country"
//           options={countryOptions}
//           value={countryId}
//           disabled={countriesLoading}
//           onChange={onCountryChange}
//         />
//       </FormGroup>
//       <FormGroup
//         className="advert-selector-basic-filter__form-group"
//         label="Municipality"
//         labelFor="municipality"
//         inline
//       >
//         <HTMLSelect
//           id="municipality"
//           options={municipalityOptions}
//           value={municipalityId}
//           disabled={municipalitiesLoading}
//           onChange={onMunicipalityChange}
//         />
//       </FormGroup>
//       <FormGroup className="advert-selector-basic-filter__form-group" label="Street" labelFor="street" inline>
//         <HTMLSelect
//           id="street"
//           options={streetOptions}
//           value={streetId}
//           disabled={streetsLoading}
//           onChange={onStreetChange}
//         />
//       </FormGroup>
//     </div>
//   );
// }) as FC;
