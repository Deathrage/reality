import gql from 'graphql-tag';
import { ADVERT_SELECTOR_BASIC_FILTER_NAMED_ENTITY_FRAGMENT } from './fragments.graphql';

export const ADVERT_SELECTOR_BASIC_FILTER_MODULES_QUERY = gql`
  query AdvertSelectorBasicFilterModulesQuery {
    sourceModuleNames
  }
`;

export const ADVERT_SELECTOR_BASIC_FILTER_COUNTRIES_QUERY = gql`
  query AdvertSelectorBasicFilterCountriesQuery {
    countries {
      id
      ...AdvertSelectorBasicFilterNamedEntityFragment
    }
  }
  ${ADVERT_SELECTOR_BASIC_FILTER_NAMED_ENTITY_FRAGMENT}
`;

export const ADVERT_SELECTOR_BASIC_FILTER_MUNICIPALITIES_QUERY = gql`
  query AdvertSelectorBasicFilterMunicipalitiesQuery($countryId: Int!) {
    municipalities(countryId: $countryId) {
      id
      ...AdvertSelectorBasicFilterNamedEntityFragment
    }
  }
  ${ADVERT_SELECTOR_BASIC_FILTER_NAMED_ENTITY_FRAGMENT}
`;

export const ADVERT_SELECTOR_BASIC_FILTER_STREETS_QUERY = gql`
  query AdvertSelectorBasicFilterStreetsQuery($municipalityId: Int!) {
    streets(municipalityId: $municipalityId) {
      id
      ...AdvertSelectorBasicFilterNamedEntityFragment
    }
  }
  ${ADVERT_SELECTOR_BASIC_FILTER_NAMED_ENTITY_FRAGMENT}
`;
