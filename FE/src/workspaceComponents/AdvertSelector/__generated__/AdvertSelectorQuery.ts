/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ListableAdvertFilterInput, AdvertType, EstateType } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: AdvertSelectorQuery
// ====================================================

export interface AdvertSelectorQuery_adverts_estate {
  id: number;
  name: string | null;
  estateType: EstateType | null;
  latitude: any | null;
  longitude: any | null;
}

export interface AdvertSelectorQuery_adverts {
  id: number;
  name: string | null;
  source: string;
  created: any;
  price: any;
  currency: string;
  advertType: AdvertType;
  estate: AdvertSelectorQuery_adverts_estate | null;
}

export interface AdvertSelectorQuery {
  adverts: AdvertSelectorQuery_adverts[];
}

export interface AdvertSelectorQueryVariables {
  filter: ListableAdvertFilterInput;
}
