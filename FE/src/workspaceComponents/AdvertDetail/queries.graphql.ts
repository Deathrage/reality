import { ADDRESS_FRAGMENT } from '@components/Address/fragments.graphql';
import gql from 'graphql-tag';
import { ADVERT_DETAIL_GEO_COORDINATES_FRAGMENT } from './fragments.graphql';

export const ADVERT_DETAIL_QUERY = gql`
  query AdvertDetailQuery($id: Int!) {
    advert(id: $id) {
      id
      name
      price
      currency
      created
      advertType
      link
      source
      estate {
        id
        estateType
        locality
        hasOwnAddress
        latitude
        longitude
        ...AdvertDetailGeoCoordinatesFragment
        address {
          id
          ...AddressFragment
        }
      }
    }
  }
  ${ADDRESS_FRAGMENT}
  ${ADVERT_DETAIL_GEO_COORDINATES_FRAGMENT}
`;
