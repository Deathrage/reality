/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AdvertDetailGeoCoordinatesFragment
// ====================================================

export interface AdvertDetailGeoCoordinatesFragment {
  longitude: any | null;
  latitude: any | null;
}
