/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AdvertType, EstateType } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: AdvertDetailQuery
// ====================================================

export interface AdvertDetailQuery_advert_estate_address {
  id: number;
  streetNumber: string | null;
  street: string | null;
  municipality: string | null;
  countrySubdivision: string | null;
  countrySecondarySubdivision: string | null;
  country: string | null;
}

export interface AdvertDetailQuery_advert_estate {
  id: number;
  estateType: EstateType;
  locality: string | null;
  hasOwnAddress: boolean;
  latitude: any | null;
  longitude: any | null;
  address: AdvertDetailQuery_advert_estate_address;
}

export interface AdvertDetailQuery_advert {
  id: number;
  name: string | null;
  price: any;
  currency: string;
  created: any;
  advertType: AdvertType;
  link: string | null;
  source: string;
  estate: AdvertDetailQuery_advert_estate;
}

export interface AdvertDetailQuery {
  advert: AdvertDetailQuery_advert;
}

export interface AdvertDetailQueryVariables {
  id: number;
}
