import { useAzureMapsKey } from '@components/Environment/hooks';
import { AuthenticationOptions, Map, AuthenticationType, data, source, layer } from 'azure-maps-control';
import React, { FC, useEffect, useLayoutEffect, useMemo, useState } from 'react';
import { v4 } from 'uuid';

const useAuthOptions = (): AuthenticationOptions => {
  const subscriptionKey = useAzureMapsKey();

  const option = useMemo<AuthenticationOptions>(
    () => ({
      authType: AuthenticationType.subscriptionKey,
      subscriptionKey,
    }),
    [subscriptionKey],
  );

  return option;
};

export default (({ latitude, longitude }) => {
  const authOptions = useAuthOptions();

  const [activeMap, setActiveMap] = useState<Map | null>(null);

  const containerId = useMemo(() => v4(), []);

  useLayoutEffect(() => {
    const container = document.getElementById(containerId);
    if (!container) return;

    const map = new Map(container, {
      authOptions,
      zoom: 10,
      center: latitude && longitude ? new data.Position(longitude, latitude) : undefined,
    });

    map.events.add('ready', () => {
      setActiveMap(map);
    });

    return () =>
      setActiveMap(prevMap => {
        prevMap?.dispose();
        return null;
      });
  }, [authOptions, containerId, latitude, longitude]);

  // Data source
  const dataSource = useMemo(
    () =>
      new source.DataSource(`${containerId}data-source`, {
        cluster: true,
        clusterRadius: 40,
        maxZoom: 14,
      }),
    [containerId],
  );

  // Fill the source with points
  useEffect(() => {
    if (!longitude || !latitude) return;

    const point = new data.Feature<data.Point, unknown>(new data.Point(new data.Position(longitude, latitude)));
    dataSource.add(point);

    return () => dataSource.clear();
  }, [longitude, dataSource, latitude]);

  useEffect(() => {
    if (!activeMap) return;

    activeMap.sources.add(dataSource);
    const markerLayer = new layer.SymbolLayer(dataSource, `${containerId}marker`);
    activeMap.layers.add(markerLayer);
  }, [activeMap, containerId, dataSource]);

  return <div className="advert__location" id={containerId}></div>;
}) as FC<{ latitude: number | undefined; longitude: number | undefined }>;
