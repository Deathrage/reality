import { useQuery } from '@apollo/client';
import { AnchorButton, Classes, H1, HTMLTable, Icon, Intent, Tab, Tabs, Tooltip } from '@blueprintjs/core';
import React, { useMemo, useState } from 'react';
import { ADVERT_DETAIL_QUERY } from './queries.graphql';
import { AdvertDetailQuery, AdvertDetailQueryVariables } from './__generated__/AdvertDetailQuery';
import './style.scss';
import classNames from 'classnames';
import { IconNames } from '@blueprintjs/icons';
import { useIntl } from '@components/Intl/hooks';
import { TimeFormat } from '@components/Intl/types';
import { AdvertType } from 'global-types';
import { getSpriteUrl } from '@helpers/sprites';
import { AdvertDetailTabs } from './types';
import ProxiedFrame from '@components/ProxiedFrame';
import Address from '@components/Address';
import { WorkspaceItemFC } from '@workspaceComponents/types';
import EstateFeatures from '@components/EstateFeatures';
import { useCurrentDocument } from '@components/CurrentDocument/hooks';
import { useTranslation } from 'react-i18next';
import Location from './Location';

const AdvertDetail: WorkspaceItemFC<number> = ({ data: id, detached }) => {
  const [activeTabId, setActiveTabId] = useState(AdvertDetailTabs.ESTATE);

  const document = useCurrentDocument();

  const { t: sourceT } = useTranslation('Source');

  const { data, loading } = useQuery<AdvertDetailQuery, AdvertDetailQueryVariables>(ADVERT_DETAIL_QUERY, {
    variables: {
      id: id!,
    },
    skip: !id,
  });

  const { formatDate, formatNumber } = useIntl();

  const advert = useMemo(() => data?.advert, [data]);

  const commonClassName = classNames({ [Classes.SKELETON]: loading });

  return (
    <div className="advert">
      <div className="advert__header">
        <div className="advert__main">
          <div className="advert__headline">
            <div className="advert__headline-icon">
              {advert && <img src={getSpriteUrl(advert?.estate.estateType)} />}
            </div>
            <div>
              <H1 className={commonClassName}>{advert?.name ?? 'Advert for an estate'}</H1>
              {advert?.estate && (
                <p>
                  {[
                    advert.estate.locality,
                    advert.estate.latitude &&
                      advert.estate.longitude &&
                      `Lat: ${advert.estate.latitude} Lon: ${advert.estate.longitude}`,
                  ]
                    .filter(x => x)
                    .join(', ')}
                </p>
              )}
            </div>
          </div>
          <HTMLTable className="advert__table">
            <thead>
              <tr>
                <th>Created</th>
                <th>Price</th>
                <th>Link</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <div className={classNames(commonClassName, { 'advert__table-skeleton': loading })}>
                    {advert && formatDate(advert.created, TimeFormat.WITHOUT_TIME)}
                  </div>
                </td>
                <td>
                  <div className={classNames(commonClassName, { 'advert__table-skeleton': loading })}>
                    {advert &&
                      formatNumber(advert.price, { currency: advert.currency, style: 'currency' }) +
                        (advert.advertType === AdvertType.RENT ? ' / month' : '')}
                  </div>
                </td>
                <td>
                  <AnchorButton
                    intent={Intent.PRIMARY}
                    className={classNames(commonClassName, 'advert__out')}
                    href={advert && advert.link!}
                    disabled={!advert?.link}
                    target="__blank"
                  >
                    {advert?.source ? sourceT(advert.source) : 'none'}
                    <Icon className="advert__out-icon" iconSize={12} icon={IconNames.SHARE} />
                  </AnchorButton>
                </td>
              </tr>
            </tbody>
          </HTMLTable>
        </div>
        <Address
          loading={loading}
          {...(advert?.estate?.address || {})}
          accurate={advert?.estate.hasOwnAddress ?? true}
        />
      </div>
      <Tabs
        id="advert-detail-tabs"
        selectedTabId={activeTabId}
        onChange={newTab => setActiveTabId(newTab as AdvertDetailTabs)}
        large
        renderActiveTabPanelOnly
      >
        <Tab
          id={AdvertDetailTabs.ESTATE}
          title="Estate Detail"
          panel={loading ? <></> : <EstateFeatures estateId={advert?.estate.id} />}
          disabled={loading}
        />
        <Tab
          id={AdvertDetailTabs.ORIGINAL}
          title="Original Advert"
          panel={loading ? <></> : <ProxiedFrame link={advert?.link} noProxy={advert?.source === 'sreality'} />}
          disabled={loading || !advert?.link}
        />
        <Tab
          id={AdvertDetailTabs.MAP}
          title={
            <Tooltip
              intent={Intent.WARNING}
              content="Map is not available in detached mode"
              usePortal={true}
              portalContainer={document.body}
              disabled={!detached}
            >
              Location
            </Tooltip>
          }
          panel={<Location latitude={advert?.estate.latitude} longitude={advert?.estate.longitude} />}
          disabled={loading || detached}
        />
      </Tabs>
    </div>
  );
};

AdvertDetail.workspaceItem = advertId => ({
  i: `advert-detail-${advertId}`,
  component: AdvertDetail,
  title: `Advert Detail ${advertId}`,
  icon: IconNames.TAG,
  detachable: true,
  toolboxable: false,
  x: 5,
  y: 0,
  h: 5,
  w: 4,
  data: advertId,
});

export default AdvertDetail;
