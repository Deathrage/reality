import gql from 'graphql-tag';

export const ADVERT_DETAIL_GEO_COORDINATES_FRAGMENT = gql`
  fragment AdvertDetailGeoCoordinatesFragment on GeneralEstate {
    longitude
    latitude
  }
`;
