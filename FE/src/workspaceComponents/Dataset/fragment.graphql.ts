import { gql } from '@apollo/client';

export const DATASET_ADVERT_FRAGMENT = gql`
  fragment DatasetAdvertFragment on ListableAdvert {
    id
  }
`;
