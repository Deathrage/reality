import { IconNames } from '@blueprintjs/icons';
import { WorkspaceItemFC } from '@workspaceComponents/types';
import React from 'react';

const Dataset: WorkspaceItemFC = () => {
  return <>Foo</>;
};

Dataset.workspaceItem = () => ({
  i: 'dataset',
  component: Dataset,
  title: 'Dataset',
  icon: IconNames.DATABASE,
  detachable: false,
  toolboxable: true,
  isResizable: false,
  x: 9,
  y: 0,
  h: 1,
  w: 1,
});

export default Dataset;
